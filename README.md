# DevelopmentTools

#### 介绍
C#相关的一些开发工具或示例

#### 当前解决方案包含的项目

### AssemblyTools
MSBuild打包编译生成工具：
支持批量修改C#项目程序集的版本信息，使用MSBuild编译C#项目，指定生成文件的输出目录，以及生成文件的统一抽取。

### CommonLib
公共类库：
C#开发中常用的助手类。

### DeveloperKitsWinForm
C#实验项目:
C#实验性质的代码，写在次项目中

### LanguageTranslate
语言翻译项目：
有道词典API翻译的实验性项目
