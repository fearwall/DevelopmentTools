﻿#pragma checksum "..\..\..\..\Develop\HIS\DbTableContrast.xaml" "{ff1816ec-aa5e-4d10-87f7-6f4963833460}" "B78395F94C969D48A3D0F0090D91CB1DA94B2892"
//------------------------------------------------------------------------------
// <auto-generated>
//     此代码由工具生成。
//     运行时版本:4.0.30319.42000
//
//     对此文件的更改可能会导致不正确的行为，并且如果
//     重新生成代码，这些更改将会丢失。
// </auto-generated>
//------------------------------------------------------------------------------

using DeveloperKits.Develop.HIS;
using DeveloperKits.Develop.HIS.ConvertTools;
using System;
using System.Diagnostics;
using System.Windows;
using System.Windows.Automation;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Markup;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Media.Effects;
using System.Windows.Media.Imaging;
using System.Windows.Media.Media3D;
using System.Windows.Media.TextFormatting;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Shell;


namespace DeveloperKits.Develop.HIS {
    
    
    /// <summary>
    /// DbTableContrast
    /// </summary>
    public partial class DbTableContrast : System.Windows.Window, System.Windows.Markup.IComponentConnector {
        
        
        #line 31 "..\..\..\..\Develop\HIS\DbTableContrast.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.GroupBox groupBoxDbSet1;
        
        #line default
        #line hidden
        
        
        #line 75 "..\..\..\..\Develop\HIS\DbTableContrast.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.GroupBox groupBoxDbSet2;
        
        #line default
        #line hidden
        
        
        #line 147 "..\..\..\..\Develop\HIS\DbTableContrast.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button btnGetTableInfo;
        
        #line default
        #line hidden
        
        
        #line 148 "..\..\..\..\Develop\HIS\DbTableContrast.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.ListView lvDetail;
        
        #line default
        #line hidden
        
        
        #line 189 "..\..\..\..\Develop\HIS\DbTableContrast.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.DataGrid dg3TableInfo;
        
        #line default
        #line hidden
        
        
        #line 213 "..\..\..\..\Develop\HIS\DbTableContrast.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button btn3MakeSQL;
        
        #line default
        #line hidden
        
        
        #line 215 "..\..\..\..\Develop\HIS\DbTableContrast.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.DataGrid dg3SelectInfo;
        
        #line default
        #line hidden
        
        
        #line 238 "..\..\..\..\Develop\HIS\DbTableContrast.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox txt3Log;
        
        #line default
        #line hidden
        
        private bool _contentLoaded;
        
        /// <summary>
        /// InitializeComponent
        /// </summary>
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [System.CodeDom.Compiler.GeneratedCodeAttribute("PresentationBuildTasks", "4.0.0.0")]
        public void InitializeComponent() {
            if (_contentLoaded) {
                return;
            }
            _contentLoaded = true;
            System.Uri resourceLocater = new System.Uri("/DeveloperKits;component/develop/his/dbtablecontrast.xaml", System.UriKind.Relative);
            
            #line 1 "..\..\..\..\Develop\HIS\DbTableContrast.xaml"
            System.Windows.Application.LoadComponent(this, resourceLocater);
            
            #line default
            #line hidden
        }
        
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [System.CodeDom.Compiler.GeneratedCodeAttribute("PresentationBuildTasks", "4.0.0.0")]
        [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Never)]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Design", "CA1033:InterfaceMethodsShouldBeCallableByChildTypes")]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Maintainability", "CA1502:AvoidExcessiveComplexity")]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1800:DoNotCastUnnecessarily")]
        void System.Windows.Markup.IComponentConnector.Connect(int connectionId, object target) {
            switch (connectionId)
            {
            case 1:
            this.groupBoxDbSet1 = ((System.Windows.Controls.GroupBox)(target));
            return;
            case 2:
            
            #line 63 "..\..\..\..\Develop\HIS\DbTableContrast.xaml"
            ((System.Windows.Controls.Button)(target)).Click += new System.Windows.RoutedEventHandler(this.btnDbSetTest_Click);
            
            #line default
            #line hidden
            return;
            case 3:
            
            #line 64 "..\..\..\..\Develop\HIS\DbTableContrast.xaml"
            ((System.Windows.Controls.Button)(target)).Click += new System.Windows.RoutedEventHandler(this.btnDbSetPut_Click);
            
            #line default
            #line hidden
            return;
            case 4:
            this.groupBoxDbSet2 = ((System.Windows.Controls.GroupBox)(target));
            return;
            case 5:
            
            #line 107 "..\..\..\..\Develop\HIS\DbTableContrast.xaml"
            ((System.Windows.Controls.Button)(target)).Click += new System.Windows.RoutedEventHandler(this.btnDbSetTest_Click);
            
            #line default
            #line hidden
            return;
            case 6:
            
            #line 108 "..\..\..\..\Develop\HIS\DbTableContrast.xaml"
            ((System.Windows.Controls.Button)(target)).Click += new System.Windows.RoutedEventHandler(this.btnDbSetPut_Click);
            
            #line default
            #line hidden
            return;
            case 7:
            this.btnGetTableInfo = ((System.Windows.Controls.Button)(target));
            
            #line 147 "..\..\..\..\Develop\HIS\DbTableContrast.xaml"
            this.btnGetTableInfo.Click += new System.Windows.RoutedEventHandler(this.btnGetTableInfo_Click);
            
            #line default
            #line hidden
            return;
            case 8:
            this.lvDetail = ((System.Windows.Controls.ListView)(target));
            return;
            case 9:
            
            #line 188 "..\..\..\..\Develop\HIS\DbTableContrast.xaml"
            ((System.Windows.Controls.Button)(target)).Click += new System.Windows.RoutedEventHandler(this.btn3GetTableInfo_Click);
            
            #line default
            #line hidden
            return;
            case 10:
            this.dg3TableInfo = ((System.Windows.Controls.DataGrid)(target));
            
            #line 192 "..\..\..\..\Develop\HIS\DbTableContrast.xaml"
            this.dg3TableInfo.SelectionChanged += new System.Windows.Controls.SelectionChangedEventHandler(this.dg3TableInfo_SelectionChanged);
            
            #line default
            #line hidden
            return;
            case 11:
            this.btn3MakeSQL = ((System.Windows.Controls.Button)(target));
            
            #line 213 "..\..\..\..\Develop\HIS\DbTableContrast.xaml"
            this.btn3MakeSQL.Click += new System.Windows.RoutedEventHandler(this.btn3MakeSQL_Click);
            
            #line default
            #line hidden
            return;
            case 12:
            this.dg3SelectInfo = ((System.Windows.Controls.DataGrid)(target));
            
            #line 220 "..\..\..\..\Develop\HIS\DbTableContrast.xaml"
            this.dg3SelectInfo.SelectionChanged += new System.Windows.Controls.SelectionChangedEventHandler(this.dg3SelectInfo_SelectionChanged);
            
            #line default
            #line hidden
            return;
            case 13:
            this.txt3Log = ((System.Windows.Controls.TextBox)(target));
            return;
            }
            this._contentLoaded = true;
        }
    }
}

