﻿using DeveloperKits.Develop.HIS.Model;
using DeveloperKits.ProjectCommonClass.DBUtility;
using DeveloperKits.ProjectCommonClass.Helper;
using RDTools.Common;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.Xml;
using System.Xml.Linq;

namespace DeveloperKits.Develop.HIS
{
    /// <summary>
    /// SysMenuManage.xaml 的交互逻辑
    /// </summary>
    public partial class SysMenuManage : Window
    {
        public SysMenuManage()
        {
            InitializeComponent();

            InitializeData();
        }

        List<string> _listReadDllSelectData = new List<string>();//读取DLL后选中的文件ID

        public DataSet SqlHelper { get; private set; }

        private void btnBrowFileName_Click(object sender, RoutedEventArgs e)
        {
            string selectFileName = FilePathHelper.FilePathSelect("应用程序拓展|*.dll");
            if (!string.IsNullOrWhiteSpace(selectFileName))
            {
                _listReadDllSelectData.Clear();
                txtBrowFilePath.Text = selectFileName;
                List<BasesysMenuModel> list = new List<BasesysMenuModel>();
                try
                {
                    Assembly asm = Assembly.LoadFrom(selectFileName.Trim());
                    foreach (System.Type t in asm.GetExportedTypes())
                    {
                        object[] obj = t.GetCustomAttributes((typeof(WindowDescribeAttribute)), false);
                        foreach (WindowDescribeAttribute attribute in obj)
                        {
                            list.Add(new BasesysMenuModel()
                            {
                                NodeId = (list.Count + 1).ToString(),
                                NodeName = attribute.Describe,
                                Pluginid = attribute.ClassName
                            });
                        }
                    }
                    lvReadDllInfo.ItemsSource = list;
                    cbLvReadDllInfo_All.IsChecked = false;
                    ListViewResize(lvReadDllInfo);
                }
                catch (Exception ex)
                {

                }
            }
        }

        private void btnCancel_Click(object sender, RoutedEventArgs e)
        {
            InitializeData();
        }

        /// <summary>
        /// 初始化数据
        /// </summary>
        private void InitializeData()
        {
            txtMenuName.Text = string.Empty;//菜单名称
            txtBrowFilePath.Text = string.Empty;//文件路径
            txtParentMenu.Text = string.Empty;//上级菜单

            DataSet dsMenuInfo = new DataSet();//菜单信息
            string sqlMenuInfo = string.Format(@"SELECT * FROM BaseSys_Menu ORDER BY Disporder");
            dsMenuInfo = SqlServerHelper.ExecuteDataSet(SqlServerHelper.ConnectionString, sqlMenuInfo);
            List<BasesysMenuModel> listBasesysMenu = new List<BasesysMenuModel>();
            listBasesysMenu.Add(new BasesysMenuModel()
            {
                NodeId = "00",
                NodeName = "目录",
                ParentNode = "",
                IsEndNode = "否",
                Pluginid = "",
                Commentary = "",
                Disporder = "00",
                Type = 0,
                IsChoose = 0
            });
            foreach (DataRow dr in dsMenuInfo.Tables[0].Rows)
            {
                listBasesysMenu.Add(new BasesysMenuModel()
                {
                    NodeId = dr["NodeId"] == DBNull.Value ? "" : dr["NodeId"].ToString(),
                    NodeName = dr["NodeName"] == DBNull.Value ? "" : dr["NodeName"].ToString(),
                    ParentNode = dr["ParentNode"] == DBNull.Value ? "" : dr["ParentNode"].ToString(),
                    IsEndNode = dr["IsEndNode"] == DBNull.Value ? "" : dr["IsEndNode"].ToString(),
                    Pluginid = dr["Pluginid"] == DBNull.Value ? "" : dr["Pluginid"].ToString(),
                    Commentary = dr["Commentary"] == DBNull.Value ? "" : dr["Commentary"].ToString(),
                    Disporder = dr["Disporder"] == DBNull.Value ? "" : dr["Disporder"].ToString(),
                    Type = dr["Type"] == DBNull.Value ? 0 : Convert.ToInt32(dr["Type"]),
                    IsChoose = dr["IsChoose"] == DBNull.Value ? 0 : Convert.ToInt32(dr["IsChoose"])
                });
            }

            List<BasesysMenuModel> lstGroup = getTreeData("", listBasesysMenu);//初始化时获取父节点为-1的数据
            this.tvInfo.ItemsSource = lstGroup;//数据绑定

            lvDetail.ItemsSource = null;
            lvReadDllInfo.ItemsSource = null;
            _listReadDllSelectData.Clear();
            cbLvReadDllInfo_All.IsChecked = false;
        }

        /// <summary>
        /// 递归生成树形数据
        /// </summary>
        /// <param name="delst"></param>
        /// <returns></returns>
        public List<BasesysMenuModel> getTreeData(string parentid, List<BasesysMenuModel> nodes)
        {
            List<BasesysMenuModel> mainNodes = nodes.Where(x => x.ParentNode == parentid).ToList<BasesysMenuModel>();
            List<BasesysMenuModel> otherNodes = nodes.Where(x => x.ParentNode != parentid).ToList<BasesysMenuModel>();
            foreach (BasesysMenuModel grp in mainNodes)
            {
                grp.Nodes = getTreeData(grp.NodeId, otherNodes);
            }
            return mainNodes;
        }

        /// <summary>
        /// 树形CheckBox按钮
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void tvInfoCheckBox_Click(object sender, RoutedEventArgs e)
        {
            CheckBox cb = e.OriginalSource as CheckBox;
        }

        private void tvInfo_SelectedItemChanged(object sender, RoutedPropertyChangedEventArgs<object> e)
        {
            BasesysMenuModel bmm = tvInfo.SelectedItem as BasesysMenuModel;
            string sqlMenuInfo = string.Format(@"SELECT * FROM BaseSys_Menu WHERE ParentNode = '{0}' ORDER BY Disporder", bmm.NodeId);
            if(bmm.IsEndNode == "是")
            {
                sqlMenuInfo = string.Format(@"SELECT * FROM BaseSys_Menu WHERE Nodeid='{0}' ",bmm.NodeId);
            }
            DataSet dsMenuInfo = SqlServerHelper.ExecuteDataSet(SqlServerHelper.ConnectionString, sqlMenuInfo);
            lvDetail.ItemsSource = dsMenuInfo.Tables[0].DefaultView;
            txtParentMenu.Text = bmm.NodeName;
            ListViewResize(lvDetail);
        }

        private void ListViewResize(ListView lv)
        {
            try
            {
                GridView gv = lv.View as GridView;
                if (gv != null)
                {
                    foreach (GridViewColumn gvc in gv.Columns)
                    {
                        gvc.Width = gvc.ActualWidth;
                        gvc.Width = Double.NaN;
                    }
                }
            }catch(Exception ex)
            {

            }
        }

        private void txtParentMenu_TextChanged(object sender, TextChangedEventArgs e)
        {
            if (string.IsNullOrWhiteSpace(txtParentMenu.Text.Trim()))
            {
                txtBrowFilePath.IsEnabled = false;
                btnBrowFileName.IsEnabled = false;
                txtBrowFilePath.Text = string.Empty;
            }
            else
            {
                txtBrowFilePath.IsEnabled = true;
                btnBrowFileName.IsEnabled = true;
            }
        }

        private void cbLvReadDllInfo_Checkd(object sender, RoutedEventArgs e)
        {
            try
            {
                CheckBox cb = e.OriginalSource as CheckBox;
                if (!_listReadDllSelectData.Contains(cb.Tag.ToString()))
                {
                    _listReadDllSelectData.Add(cb.Tag.ToString());
                }
                
            }
            catch
            {

            }
        }

        private void cbLvReadDllInfo_Uncheckd(object sender, RoutedEventArgs e)
        {
            try
            {
                CheckBox cb = e.OriginalSource as CheckBox;
                if (_listReadDllSelectData.Contains(cb.Tag.ToString()))
                {
                    _listReadDllSelectData.Remove(cb.Tag.ToString());
                }

            }
            catch
            {

            }
        }

        private void btnSave_Click(object sender, RoutedEventArgs e)
        {
            if (string.IsNullOrWhiteSpace(txtMenuName.Text))
            {
                MessageBox.Show("请填写菜单名称！", "提示", MessageBoxButton.OK, MessageBoxImage.Asterisk);
                return;
            }

            if (_listReadDllSelectData.Count <= 0)
            {
                MessageBox.Show("请至少选择一条菜单信息！", "提示", MessageBoxButton.OK, MessageBoxImage.Asterisk);
                return;
            }

            List<BasesysMenuModel> listReadDllInfo = lvReadDllInfo.ItemsSource as List<BasesysMenuModel>;
            List<BasesysMenuModel> listHandel = new List<BasesysMenuModel>();

            if (MessageBox.Show("选中【" + _listReadDllSelectData.Count + " / " + listReadDllInfo.Count + "】条数据，是否继续进行操作","提示",MessageBoxButton.YesNo,MessageBoxImage.Asterisk) == MessageBoxResult.No)
            {
                return;
            }

            ///根菜单
            listHandel.Add(new BasesysMenuModel()
            {
                NodeName = txtMenuName.Text.Trim(),
                Pluginid = ""
            });
            foreach(string strNodeId in _listReadDllSelectData)
            {
                listHandel.AddRange(listReadDllInfo.Where(x => x.NodeId == strNodeId).ToList<BasesysMenuModel>());                
            }
            PrintLog("将选中的数据写入数据库，写入的文件为：【"+ txtBrowFilePath.Text.Trim()  + "】");
            string strPath = System.IO.Path.GetFileNameWithoutExtension(txtBrowFilePath.Text.Trim());
            //进行数据库操作
            //MessageBox.Show("开始进行数据库操作！", "提示", MessageBoxButton.OK, MessageBoxImage.Asterisk);
            using (SqlConnection connection = new SqlConnection(SqlServerHelper.ConnectionString))
            {
                SqlTransaction transaction;
                connection.Open();
                transaction = connection.BeginTransaction();
                try
                {
                    string sqlStratNo = string.Format("select MAX(CAST(isnull(nodeid,0) as int)) nodeid from BaseSys_Menu");
                    object objStartNo = SqlServerHelper.ExecuteScalar(transaction, sqlStratNo, null);
                    int ?  iStartNo =  Convert.ToInt32(objStartNo == DBNull.Value ? 0 : objStartNo);
                    if(iStartNo == null)
                    {
                        iStartNo = 0;
                    }
                    int iRes = 0;
                    int iSucces = 0;
                    foreach (BasesysMenuModel model in listHandel)
                    {
                        string strPluginid = string.Empty;
                        if (!string.IsNullOrEmpty(model.Pluginid))
                        {
                            strPluginid = model.Pluginid + "," + strPath;
                        }
                        //数据查重
                        string sqlCheck = string.Format("select count(*) from BaseSys_Menu where NodeName='{0}' and Pluginid='{1}' ",model.NodeName, strPluginid);
                        int iCheck = Convert.ToInt32(SqlServerHelper.ExecuteScalar(transaction,sqlCheck,null));
                        if (iCheck <= 0)
                        {
                            iStartNo++;

                            string sql =
                                @"INSERT INTO BaseSys_Menu (
                                  Nodeid,NodeName,Pluginid
                            ) VALUES (
                                  @Nodeid,@NodeName,@Pluginid
                            );";
                            sql = sql + "SELECT @@identity";
                            SqlParameter[] param =
                            {
                                SqlServerHelper.MakeInParam("@Nodeid",SqlDbType.VarChar,50,iStartNo.ToString().PadLeft(4,'0')),
                                SqlServerHelper.MakeInParam("@NodeName",SqlDbType.VarChar,100,model.NodeName),
                                SqlServerHelper.MakeInParam("@Pluginid",SqlDbType.VarChar,100,strPluginid)
                            };
                            iRes = Convert.ToInt32(SqlServerHelper.ExecuteNonQuery(transaction, sql, param));
                        }
                        else
                        {
                            PrintLog("数据库已存在记录：【" + model.ToString() + "】，写入数据库失败" );
                            continue;
                        }

                        if(iRes <= 0)
                        {
                            break;
                        }
                        else
                        {
                            iSucces++;
                            Thread.Sleep(10);
                        }
                    }
                    if(iRes > 0)
                    {
                        transaction.Commit();

                        PrintLog("完成写入数据库！，共完成：【" + iSucces.ToString() + " / "+ listHandel.Count + " - " + listReadDllInfo.Count + "】条记录！");
                        PrintLog("----------------------------------");
                        MessageBox.Show("操作成功！", "提示", MessageBoxButton.OK, MessageBoxImage.Asterisk);
                    }
                    else
                    {
                        transaction.Rollback();
                        MessageBox.Show("保存失败！", "提示", MessageBoxButton.OK, MessageBoxImage.Error);
                    }
                }
                catch (Exception ex)
                {
                    transaction.Rollback();
                    MessageBox.Show("保存失败！\r\n"+ex.ToString(), "提示", MessageBoxButton.OK, MessageBoxImage.Error);
                }
                finally
                {
                    connection.Close();
                }
            }
        }

        private void PrintLog(string strMsg)
        {
            this.txtLog.Dispatcher.Invoke(new Action(delegate
            {
                this.txtLog.Text += "\r\n" + strMsg;
                this.txtLog.SelectionStart = this.txtLog.Text.Length;
                FileHelper.WriteLog(strMsg);
            }));
        }

        private void btnMakeXML_Click(object sender, RoutedEventArgs e)
        {
            string strFilePath = FilePathHelper.SaveFileDialog("test","Xml文件|*.xml");
            if (!string.IsNullOrEmpty(strFilePath))
            {
                try
                {
                    //获取要导出的数据
                    string sqlMenuInfo = string.Format(@"SELECT * FROM BaseSys_Menu ORDER BY NodeId asc");
                    DataTable dtMenuInfo = SqlServerHelper.ExecuteDataSet(SqlServerHelper.ConnectionString, sqlMenuInfo).Tables[0];

                    XmlDocument document = new XmlDocument();
                    XElement xeleFrm = new XElement("Frm");
                    xeleFrm.SetAttributeValue("Name", "FrmTest");
                    xeleFrm.SetAttributeValue("Title", "客户端主界面工作台(动态菜单)");

                    XElement xeleCn = new XElement("Chinese");
                    xeleCn.SetAttributeValue("name", "汉文");
                    xeleCn.SetAttributeValue("Value", "1");

                    XElement xeleTb = new XElement("Tibetan");
                    xeleTb.SetAttributeValue("name", "汉文");
                    xeleTb.SetAttributeValue("Value", "1");

                    foreach(DataRow dr in dtMenuInfo.Rows)
                    {
                        XElement cnItem = new XElement("Item");
                        string name = ConvertToString(dr["pluginid"]);
                        string image = "icons-1.ico";
                        if (string.IsNullOrEmpty(name))
                        {
                            name = ConvertToString(dr["NodeName"]);
                            image = "One_RDBaseSys.ico";
                        }

                        cnItem.SetAttributeValue("Name", name);
                        cnItem.SetAttributeValue("Type", "ToolStripMenuItem");
                        cnItem.SetAttributeValue("Title", ConvertToString(dr["NodeName"]));
                        cnItem.SetAttributeValue("Value", ConvertToString(dr["NodeName"]));
                        cnItem.SetAttributeValue("SelectValue", "");
                        cnItem.SetAttributeValue("ImageName", image);


                        XElement tbItem = new XElement("Item");
                        tbItem.SetAttributeValue("Name", name);
                        tbItem.SetAttributeValue("Type", "ToolStripMenuItem");
                        tbItem.SetAttributeValue("Title", ConvertToString(dr["NodeName"]));
                        tbItem.SetAttributeValue("Value", ConvertToString(dr["NodeName"]));
                        tbItem.SetAttributeValue("SelectValue", "");
                        tbItem.SetAttributeValue("ImageName", image);

                        xeleCn.Add(cnItem);
                        xeleTb.Add(tbItem);
                    }
                    xeleFrm.Add(xeleCn);
                    xeleFrm.Add(xeleTb);

                    if(Directory.Exists(System.IO.Path.GetDirectoryName(strFilePath)) == false)
                    {
                        Directory.CreateDirectory(System.IO.Path.GetDirectoryName(strFilePath));
                    }

                    xeleFrm.Save(strFilePath);
                    MessageBox.Show("操作成功！");
                }
                catch(Exception ex)
                {

                    MessageBox.Show("操作失败！\r\n" + ex.ToString());
                }
            }
        }

        private string ConvertToString(object obj)
        {
            try
            {
                return Convert.ToString(obj);
            }
            catch
            {
                return "";
            }
        }
    }
}
