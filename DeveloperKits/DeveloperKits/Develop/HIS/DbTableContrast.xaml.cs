﻿using DeveloperKits.Develop.HIS.Model.DbTableContrast;
using DeveloperKits.ProjectCommonClass.DBUtility;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace DeveloperKits.Develop.HIS
{
    /// <summary>
    /// DbTableContrast.xaml 的交互逻辑
    /// </summary>
    public partial class DbTableContrast : Window
    {
        /// <summary>
        /// 数据库设置1
        /// </summary>
        public DBSet dbSet1 = new DBSet();
        /// <summary>
        /// 数据库设置2
        /// </summary>
        public DBSet dbSet2 = new DBSet();

        public DbTableContrast()
        {
            InitializeComponent();
            groupBoxDbSet1.DataContext = dbSet1;
            groupBoxDbSet2.DataContext = dbSet2;


        }

        /// <summary>
        /// 数据库连接测试
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnDbSetTest_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                Button btn = e.OriginalSource as Button;
                DBSet dbSet = btn.Tag as DBSet;
                SqlServerHelper.TestConnection(dbSet.ToDbConnectionString(3));
                MessageBox.Show("数据库连接成功！");
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());
            }
        }

        /// <summary>
        /// 数据库应用测试
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnDbSetPut_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                Button btn = e.OriginalSource as Button;
                DBSet dbSet = btn.Tag as DBSet;
                //获取连接中的数据库
                dbSet.DataBaseNameList = SqlServerHelper.GetConnectionAllDataBaseName(dbSet.ConnectionString);
                //读取数据库中的表
                //SqlServerHelper.ExecuteDataSet(dbSet.ConnectionString,)
                //string sql = @"SELECT tbs.name TableName,
                //                      ds.value TableDescribe
                //                FROM sys.extended_properties ds
                //                    LEFT JOIN sysobjects tbs
                //                        ON ds.major_id = tbs.id
                //                WHERE ds.minor_id = 0
                //                        AND ds.name = @dsName ";
                //SqlParameter[] sqlpara = new SqlParameter[]
                //{
                //    SqlServerHelper.MakeInParam("dsName",System.Data.SqlDbType.VarChar,100,"MS_Description")
                //};
                //DataSet ds = SqlServerHelper.ExecuteDataSet("Data Source=127.0.0.1;Initial Catalog=wz_his;User ID=sa;Password=123456;min pool size=1;max pool size=200", sql, sqlpara); 
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());
            }
        }

        private void lvDetail_Selected(object sender, RoutedEventArgs e)
        {

        }

        /// <summary>
        /// 获取相同表的表信息
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnGetTableInfo_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                string sql = @"SELECT tbs.name TalbeName,
                                       ds.value TalbeDescription
                                FROM sys.extended_properties ds
                                    LEFT JOIN sysobjects tbs
                                        ON ds.major_id = tbs.id
                                WHERE ds.minor_id = 0
                                      AND ds.name = 'MS_Description'";
                
                //DataTable dt1 = SqlServerHelper.ExecuteDataSet(dbSet1.ConnectionString, "");
            }
            catch
            {

            }
        }

        #region TableControl3
        //获取表信息
        //数据库2的数据集信息
        DataTable dt3DbSet2TableInfo = new DataTable();
        private void btn3GetTableInfo_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                //string sql = @"SELECT DISTINCT NodeName,Pluginid NodeValue FROM BaseSys_Menu ";
                //DataTable dt = SqlServerHelper.ExecuteDataSet(dbSet1.ConnectionString, sql).Tables[0];
                //dt3DbSet2TableInfo = SqlServerHelper.ExecuteDataSet(dbSet2.ConnectionString, "SELECT NodeName,Pluginid NodeValue FROM BaseSys_Menu").Tables[0];
                //dg3TableInfo.ItemsSource = dt.DefaultView;
                DataView dt = dg3TableInfo.ItemsSource as DataView;
                int i = 0;
                txt3Log.Text += "-------------------------------\r\n";
                foreach (DataRow dr in dt.ToTable().Rows)
                {
                    i++;
                    string strOut = ("[" + i + "]").PadRight(6, ' ');
                    strOut += "-" + (dr["NodeName"].ToString().Trim()).PadRight(50,' ') ;
                    strOut += "|" + dr["NodeValue"].ToString() + "\r\n";
                    txt3Log.Text += strOut;
                }
            }
            catch(Exception ex)
            {
                MessageBox.Show(ex.ToString());
            }
        }

        //生成SQL
        private void btn3MakeSQL_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                DataRowView drSelectInfo = dg3SelectInfo.SelectedItem as DataRowView;
                DataRowView drTableInfo = dg3TableInfo.SelectedItem as DataRowView;
                if (drSelectInfo != null && drTableInfo != null)
                {
                    string sqlFormat = @"UPDATE BaseSys_Menu SET Pluginid = '{0}',UpdateCount = ISNULL(UpDateCount,0) + 1 WHERE NodeName='{1}' AND Pluginid='{2}' ";

                    List<string> listLineCode = new List<string>(txt3Log.Text.Trim().Split(new string[] { "\t\r\n" }, StringSplitOptions.RemoveEmptyEntries));
                    string lineCode = string.Format("[{0}]", listLineCode.Count+1).PadRight(6, ' ');
                    string sql = string.Format(sqlFormat, drSelectInfo["NodeValue"].ToString(), drTableInfo["NodeName"].ToString(), drTableInfo["NodeValue"].ToString());

                    if (txt3Log.Text.Trim().Contains(sql))
                    {
                        if(MessageBox.Show("此项的SQL已存在，是否继续生成？","提示",MessageBoxButton.YesNo,MessageBoxImage.Question,MessageBoxResult.No) == MessageBoxResult.Yes)
                        {
                            sql = lineCode + " " + sql;
                            txt3Log.Text += sql + "\t\r\n";
                        }
                    }
                    else
                    {
                        sql = lineCode + " " + sql;
                        txt3Log.Text += sql + "\t\r\n";
                    }
                }
                int selectIndex = dg3TableInfo.SelectedIndex;
                drTableInfo.Delete();
                if(selectIndex == -1)
                {
                    selectIndex = 0;
                }
                dg3TableInfo.SelectedIndex = selectIndex;
                dg3TableInfo.Focus();
            }
            catch(Exception ex)
            {
                MessageBox.Show("生成SQL失败！" + ex.ToString(),"失败！",MessageBoxButton.OK,MessageBoxImage.Error);
            }

        }

        private void dg3TableInfo_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {

            try
            {
                try
                {
                    DataRowView dr = dg3TableInfo.SelectedItem as DataRowView;
                    DataRow[] drSelects = dt3DbSet2TableInfo.Select("NodeName = '" + dr["NodeName"].ToString() + "'");
                    if (drSelects.Length <= 0)
                    {
                        dg3SelectInfo.ItemsSource = null;
                        dg3SelectInfo.SelectedIndex = -1;
                    }
                    else
                    {
                        dg3SelectInfo.ItemsSource = drSelects.CopyToDataTable<DataRow>().DefaultView;
                        try
                        {
                            dg3SelectInfo.SelectedIndex = 0;
                        }
                        catch
                        {

                        }
                    }
                }
                catch
                {

                }
            }
            catch
            {

            }
        }


        private void dg3SelectInfo_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            btn3MakeSQL.IsEnabled = true;
            try
            {
                DataRowView drSelectInfo = dg3SelectInfo.SelectedItem as DataRowView;
                DataRowView drTableInfo = dg3TableInfo.SelectedItem as DataRowView;
                if (dg3SelectInfo.SelectedIndex <= -1)
                {

                    btn3MakeSQL.IsEnabled = false;
                }
                else if (drSelectInfo["NodeValue"].ToString().Trim().Equals(drTableInfo["NodeValue"].ToString().Trim()) &&
                    drSelectInfo["NodeName"].ToString().Trim().Equals(drTableInfo["NodeName"].ToString().Trim()))
                {
                    btn3MakeSQL.IsEnabled = false;
                }
            }
            catch
            {

            }
        }
        #endregion
    }
}
