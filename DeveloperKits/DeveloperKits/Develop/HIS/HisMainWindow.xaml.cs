﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace DeveloperKits.Develop.HIS
{

    /// <summary>
    /// HisMainWindow.xaml 的交互逻辑
    /// </summary>
    public partial class HisMainWindow : Window
    {
        public HisMainWindow()
        {
            InitializeComponent();
        }


        /// <summary>
        /// 通过反射，打开Button Name的窗体
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Button_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                Button btn = sender as Button;
                string windowName = btn.Name;
                Assembly assembly = Assembly.GetExecutingAssembly();
                Window window = assembly.CreateInstance(GetNamespance() + "." + windowName) as Window;
                window.WindowStartupLocation = WindowStartupLocation.CenterScreen;
                window.Show();
                //Assembly.
            }
            catch (Exception ex)
            {
                MessageBox.Show("打开窗体失败:\r\n\t" + ex.ToString(), "失败！", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        /// <summary>
        /// 获取命名空间
        /// </summary>
        /// <returns></returns>
        public static string GetNamespance()
        {
            return System.Reflection.MethodBase.GetCurrentMethod().DeclaringType.Namespace;
        }

    }
}
