﻿using DeveloperKits.Develop.HIS.Model;
using DeveloperKits.ProjectCommonClass.DBUtility;
using DeveloperKits.ProjectCommonClass.Helper;
using RDTools.Common;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.Xml;
using System.Xml.Linq;

namespace DeveloperKits.Develop.HIS
{
    /// <summary>
    /// ReportCalculate.xaml 的交互逻辑
    /// </summary>
    public partial class ReportCalculate : Window
    {
        public ReportCalculate()
        {
            InitializeComponent();

            InitializeData();
        }

        List<string> _listReadDllSelectData = new List<string>();//读取DLL后选中的文件ID

        public DataSet SqlHelper { get; private set; }

        private void btnCancel_Click(object sender, RoutedEventArgs e)
        {
            InitializeData();
        }

        /// <summary>
        /// 初始化数据
        /// </summary>
        private void InitializeData()
        {

        }

        private void btnCalculate_Click(object sender, RoutedEventArgs e)
        {
            lvDetail.ItemsSource = null;

            #region 门诊操作员交款日报表、药房出库 中成药金额比对

            //Calculate2();
            #endregion

            #region 门诊操作员交款日报表比对
            Calculate1();
            #endregion

            //门诊收费明细和药房发药明细比对
            //Calculaate3();

        }

        /// <summary>
        /// 门诊操作员交款日报表比对
        /// </summary>
        private void Calculate1()
        {
            //日结总账
            string strOperatorId = "0012";//操作员ID
            string strOperatoName = "刘国英";//操作员姓名
            string strAccountStart = "2018/7/22 00:00:00";//账单开始时间
            string strAccountEnd = "2018/10/8 00:00:00";//账单结束时间

            string sqlDaysAccount = string.Format(@"SELECT Clinic_CheckSheetTotal.*,
                                                           BaseSys_Operator.OperatorName,
                                                           BaseSys_Operator.CodeNo
                                                    FROM Clinic_CheckSheetTotal,
                                                         BaseSys_Operator
                                                    WHERE Clinic_CheckSheetTotal.Operatorid in ('{0}')
                                                          AND Clinic_CheckSheetTotal.Operatorid = BaseSys_Operator.Operatorid
                                                          AND EndDate > '{1}'
                                                          AND EndDate <= '{2}'
                                                    ORDER BY StartDate ASC", strOperatorId, strAccountStart, strAccountEnd);
            //收费金额 PaymentMoney
            DataTable dtDaysAccount = SqlServerHelper.ExecuteDataSet(SqlServerHelper.ConnectionString, sqlDaysAccount).Tables[0];

            #region sqlCostDetail  费用清单SQL {0} 操作人，{1}开始时间 {2}结束时间
            string sqlCostDetail = @"SELECT * FROM(
                                        SELECT
                                            (
                                                SELECT d.FeeKind 费用类别 FROM Bfee_Kind d WHERE a.SICKTYPEID = d.FeeKindid
                                            ) AS 费用类别,
                                            (
                                                SELECT e.Office 就诊科室
                                                FROM BaseCom_Office e
                                                WHERE a.REGISTEROFFICEID = e.Officeid
                                            ) AS 就诊科室,
                                            (
                                                SELECT f.Office 执行科室
                                                FROM BaseCom_Office f
                                                WHERE a.EXECOFFICEID = f.Officeid
                                            ) AS 执行科室,
                                            a.Invoiceid 发票号,
                                            a.DetailAccountid 明细号,
                                            a.Diagnoseid 诊疗号,
                                            (
                                                SELECT c.PatientName 病人姓名
                                                FROM BaseCom_Patient c
                                                WHERE a.Diagnoseid = c.Diagnoseid
                                            ) AS 病人姓名,
                                            b.ChargeItem 收费项目名称,
                                            a.Amount 数量,
                                            a.UnitPrice 单价,
                                            CONVERT(DECIMAL(18, 2), a.Money) 金额,
                                            (
                                                SELECT g.OperatorName 操作员
                                                FROM BaseSys_Operator g
                                                WHERE a.Operatorid = g.Operatorid

                                            ) AS 操作员,
                                            CONVERT(VARCHAR, a.OperateDate, 120) 操作时间
                                        FROM Clinic_DetailAccount a,
                                             Bfee_ChargeItem b
                                        WHERE a.OperateDate >= '{1}'
                                              AND a.OperateDate <= '{2}'
                                              AND a.Itemid = b.Itemid
                                              AND a.Itemid NOT IN ( '01', '02', '03', '04' )
                                        UNION ALL
                                        SELECT
                                            (
                                                SELECT e.FeeKind 费用类别 FROM Bfee_Kind e WHERE a.SICKTYPEID = e.FeeKindid
                                            ) AS 费用类别,
                                            (
                                                SELECT f.Office 就诊科室
                                                FROM BaseCom_Office f
                                                WHERE a.REGISTEROFFICEID = f.Officeid
                                            ) AS 就诊科室,
                                            (
                                                SELECT g.Office 执行科室
                                                FROM BaseCom_Office g
                                                WHERE a.EXECOFFICEID = g.Officeid
                                            ) AS 执行科室,
                                            a.Invoiceid 发票号,
                                            a.DetailAccountid 明细号,
                                            a.Diagnoseid 诊疗号,
                                            (
                                                SELECT c.PatientName 病人姓名
                                                FROM BaseCom_Patient c
                                                WHERE a.Diagnoseid = c.Diagnoseid
                                            ) AS 病人姓名,
                                            b.TradeName 收费项目名称,
                                            d.Amount 数量,
                                            d.UnitPrice 单价,
                                            CONVERT(DECIMAL(18, 2), d.TotalMoney) 金额,
                                            (
                                                SELECT h.OperatorName 操作员
                                                FROM BaseSys_Operator h
                                                WHERE a.Operatorid = h.Operatorid
                                            ) AS 操作员,
                                            CONVERT(VARCHAR, a.OperateDate, 120) 操作时间
                                        FROM Clinic_DetailAccount a,
                                             Clinic_MedAccount d,
                                             BaseMed_CodexDetail b
                                        WHERE a.OperateDate >= '{1}'
                                              AND a.OperateDate <= '{2}'
                                              AND a.DetailAccountid = d.DetailAccountid
                                              AND a.CancelMark = d.CancelMark
                                              AND d.LeechdomNo = b.LeechdomDetailNo) AS C WHERE 操作员 = '{0}'";
            #endregion
            DataTable dtCostDetal = new DataTable();


            #region sqlRegister  挂号清单SQL {0} 操作人，{1}开始时间 {2}结束时间
            string sqlRegister = @"SELECT ROW_NUMBER() OVER (ORDER BY a.Diagnoseid DESC) AS 序号,
                                       a.Registerid AS 挂号号,
                                       a.Diagnoseid 诊疗号,
                                       Office AS 就诊科室,
                                       (a.RegisterFee + a.ExamineMoney + a.CaseCost) 金额,
                                       (CASE
                                            WHEN a.CancelMark = '0' THEN
                                                '正常'
                                            ELSE
                                                '退号'
                                        END
                                       ) AS 退号标识,
                                       d.OperatorName 收费员,
                                       b.PatientName AS 病人姓名,
                                       a.OperateDate 操作日期
                                FROM Register_Info a,
                                     BaseCom_Patient b,
                                     BaseCom_Office c,
                                     BaseSys_Operator d
                                WHERE a.Diagnoseid = b.Diagnoseid
                                      AND a.RegisterOfficeid = c.Officeid
                                      AND a.Operatorid = d.Operatorid
                                      AND d.OperatorName = '{0}'
                                      AND OperateDate > '{1}'
                                      AND OperateDate <= '{2}'";
            #endregion
            DataTable dtRegister = new DataTable();

            #region dtResult 结果表
            DataTable dtResult = new DataTable();
            dtResult.Columns.Add("NO", typeof(int));//序号
            dtResult.Columns.Add("Gatheringid", typeof(string));//编号
            dtResult.Columns.Add("StartDate", typeof(DateTime));//开始时间
            dtResult.Columns.Add("EndDate", typeof(DateTime));//结束时间
            dtResult.Columns.Add("PaymentMoney", typeof(decimal));//支付金额
            dtResult.Columns.Add("InfactMoney", typeof(decimal));//实际金额
            dtResult.Columns.Add("HangMoney", typeof(decimal));//挂账金额
            dtResult.Columns.Add("CostMoneyCount", typeof(decimal));//费用金额合计
            dtResult.Columns.Add("RegisterMoneyCount", typeof(decimal));//挂号金额合计
            dtResult.Columns.Add("ResultMoneyCount", typeof(decimal));//结果金额合计 费用+挂号
            dtResult.Columns.Add("DiffMoneyCount", typeof(decimal));//差异金额合计 支付-结果
            #endregion

            int iResultNo = 0;

            foreach (DataRow drDaysAccount in dtDaysAccount.Rows)
            {
                //查询费用SQL
                string rowSqlCostDetail = string.Format(sqlCostDetail, strOperatoName, drDaysAccount["StartDate"].ToString(), drDaysAccount["EndDate"].ToString());
                dtCostDetal = SqlServerHelper.ExecuteDataSet(SqlServerHelper.ConnectionString, rowSqlCostDetail).Tables[0];

                //查询挂号清单SQL
                string rowSqlRegister = string.Format(sqlRegister, strOperatoName, drDaysAccount["StartDate"].ToString(), drDaysAccount["EndDate"].ToString());
                dtRegister = SqlServerHelper.ExecuteDataSet(SqlServerHelper.ConnectionString, rowSqlRegister).Tables[0];

                if (dtCostDetal.Rows.Count > 0 && dtRegister.Rows.Count > 0)
                {
                    iResultNo++;
                    DataRow drResultAdd = dtResult.NewRow();
                    drResultAdd["NO"] = iResultNo;//序号
                    drResultAdd["Gatheringid"] = drDaysAccount["Gatheringid"]; //编号
                    drResultAdd["StartDate"] = Convert.ToDateTime(drDaysAccount["StartDate"]).ToString("yyyy/MM/dd HH:mm:ss"); //开始时间
                    drResultAdd["EndDate"] = Convert.ToDateTime(drDaysAccount["EndDate"]).ToString("yyyy/MM/dd HH:mm:ss"); //结束时间
                    drResultAdd["PaymentMoney"] = drDaysAccount["PaymentMoney"]; //支付金额

                    drResultAdd["InfactMoney"] = drDaysAccount["InfactMoney"]; //实际金额
                    drResultAdd["HangMoney"] = drDaysAccount["HangMoney"]; //挂账金额
                    decimal decCostMoneyCount = Convert.ToDecimal(dtCostDetal.Compute("sum(金额)", ""));
                    drResultAdd["CostMoneyCount"] = decCostMoneyCount; //费用金额合计
                    decimal decRegisterMoneyCount = Convert.ToDecimal(dtRegister.Compute("sum(金额)", ""));
                    drResultAdd["RegisterMoneyCount"] = decRegisterMoneyCount; //挂号金额合计
                    decimal decResultMoneyCount = decCostMoneyCount + decRegisterMoneyCount;
                    drResultAdd["ResultMoneyCount"] = decResultMoneyCount; //结果金额合计 费用+挂号
                    drResultAdd["DiffMoneyCount"] = Convert.ToDecimal(drDaysAccount["PaymentMoney"]) - decResultMoneyCount; //差异金额合计 支付-结果
                    dtResult.Rows.Add(drResultAdd);
                }
            }


            DataRow drResultCount = dtResult.NewRow();
            drResultCount["NO"] = dtResult.Compute("COUNT(NO)", "true");
            drResultCount["ResultMoneyCount"] = dtResult.Compute("SUM(ResultMoneyCount)", "true");
            dtResult.Rows.Add(drResultCount);
            lvDetail.ItemsSource = dtResult.DefaultView;
        }

        /// <summary>
        /// 门诊操作员交款日报表、药房出库 中成药金额比对
        /// </summary>
        private void Calculate2()
        {
            string strAccountStart = "2018/05/01 10:40:05";//结账日期
            string sqlChdekOut = string.Format(@"
                                SELECT Clinic_CheckSheetTotal.*,
                                    BaseSys_Operator.OperatorName,
                                    BaseSys_Operator.CodeNo
                                FROM Clinic_CheckSheetTotal,
                                    BaseSys_Operator
                                WHERE StartDate >= '{0}'
                                    AND Clinic_CheckSheetTotal.Operatorid = BaseSys_Operator.Operatorid ORDER BY StartDate ASC,Clinic_CheckSheetTotal.Operatorid ASC", strAccountStart);//结账日期
            DataTable dTcheckOut = SqlServerHelper.ExecuteDataSet(SqlServerHelper.ConnectionString, sqlChdekOut).Tables[0];

            #region 发药金额
            string sqlDispensingMoney = @"SELECT 1 depNo,
                                                    CASE
                                                        WHEN COUNT(DISTINCT (g.ClinicRecipeid)) IS NULL THEN
                                                            '0'
                                                        ELSE
                                                            COUNT(DISTINCT (g.ClinicRecipeid))
                                                    END AS 门诊处方数,
                                                    CASE
                                                        WHEN SUM(g.Money) IS NULL THEN
                                                            '0'
                                                        ELSE
                                                            SUM(g.Money)
                                                    END 门诊发药金额
                                            FROM
                                            (
                                                SELECT a.ClinicRecipeid,
                                                        a.Money
                                                FROM Clinic_DetailAccount a,
                                                        Pharmacy_ClinicMedicine b
                                                WHERE a.DetailAccountid = b.DetailAccountid
                                                        AND a.CancelMark = b.LeechdomState
                                                  AND a.Operatorid='{0}'
                                                        AND b.OperateDate > '{1}'
                                                        AND b.OperateDate <= '{2}'
                                                GROUP BY a.ClinicRecipeid,
                                                            a.Money
                                            ) g";
            DataTable dTDispensingMoney = new DataTable();
            #endregion

            #region  账目金额
            string sqlAccountMoney = @"
                                    SELECT Item,
                                           SUM(Clinic_CheckSheetDetail.PaymentMoney) AS PAYMENTMONEY,
                                           SUM(Clinic_CheckSheetDetail.InfactMoney) AS INFACTMONEY,
                                           SUM(Clinic_CheckSheetDetail.HangMoney) AS HANGMONEY,
                                           SUM(Clinic_CheckSheetDetail.PaymentReturn) AS PAYMENTRETURN,
                                           SUM(Clinic_CheckSheetDetail.InfactReturn) AS INFACTRETURN
                                    FROM Clinic_CheckSheetDetail,
                                         Clinic_CheckSheetTotal
                                    WHERE Clinic_CheckSheetTotal.Gatheringid = Clinic_CheckSheetDetail.Gatheringid
                                          AND Clinic_CheckSheetTotal.Operatorid = Clinic_CheckSheetDetail.Operatorid
                                          AND Clinic_CheckSheetDetail.Operatorid = '{0}'
                                          AND EndDate > '{1}'
                                          AND EndDate <= '{2}'
                                       AND Item LIKE '%西药费%'
                                    GROUP BY Item;";
            DataTable dTAccountMoney = new DataTable();
            #endregion

            #region 明细账目金额统计
            string sqlDetailAccount = @"SELECT *
                                            FROM
                                            (
                                                SELECT b.*,
                                                       a.ReportItem,
                                                       c.FeeKind,
                                                       c.IsNotMedinsurance
                                                FROM Clinic_DetailAccount b,
                                                     Bfee_ChargeItem a,
                                                     Bfee_Kind c
                                                WHERE a.Itemid = b.Itemid
                                                      AND b.SickTypeid = c.FeeKindid
                                                      AND (
                                                              (
                                                                  b.BalanceDate > '{1}'
                                                                  AND b.BalanceDate <= '{2}'
                                                              )
                                                              OR (
                                                                     b.InvoiceDate > '{1}'
                                                                     AND b.InvoiceDate <= '{2}'
                                                                 )
                                                          )
                                                      AND (
                                                              b.BalanceOperator = '{0}'
                                                              OR b.InvoiceOperator = '{0}'
                                                          )
                                                      AND b.CancelMark <> 2
                                                UNION
                                                SELECT Clinic_DetailAccount.*,
                                                       Bfee_ChargeItem.ReportItem,
                                                       Bfee_Kind.FeeKind,
                                                       Bfee_Kind.IsNotMedinsurance
                                                FROM BaseCom_UseInvoice,
                                                     Clinic_DetailAccount,
                                                     Bfee_ChargeItem,
                                                     Bfee_Kind
                                                WHERE BaseCom_UseInvoice.OldInvoice = Clinic_DetailAccount.Invoiceid
                                                      AND Bfee_ChargeItem.Itemid = Clinic_DetailAccount.Itemid
                                                      AND Clinic_DetailAccount.SickTypeid = Bfee_Kind.FeeKindid
                                                      AND BaseCom_UseInvoice.Operatorid = '{0}'
                                                      AND (
                                                              InvoiceState = 2
                                                              OR InvoiceState = 3
                                                          )
                                                      AND InvoiceType = '门诊发票'
                                                      AND CancelMark <> 0
                                                      AND BaseCom_UseInvoice.OperateDate > '{1}'
                                                      AND BaseCom_UseInvoice.OperateDate <= '{2}'
                                                      AND CancelMark = 2
                                            ) RES
                                            WHERE RES.ReportItem = '西药费'";
            DataTable dTDetailAccount = new DataTable();
            #endregion

            #region 结果运算
            DataTable dtResult = new DataTable();
            dtResult.Columns.Add("NO", typeof(int));//序号
            dtResult.Columns.Add("StartDate", typeof(string));//开始时间
            dtResult.Columns.Add("EndDate", typeof(string));//结束时间
            dtResult.Columns.Add("Operator", typeof(string));//操作人
            dtResult.Columns.Add("ReportMoney", typeof(decimal));//报表金额
            dtResult.Columns.Add("DetailMoney", typeof(decimal));//明细金额
            dtResult.Columns.Add("PharmacyMoney", typeof(decimal));//药房金额
            dtResult.Columns.Add("DiffMoneyCount1", typeof(decimal));//差异金额合计 报表金额-药房金额
            dtResult.Columns.Add("DiffMoneyCount2", typeof(decimal));//差异金额合计 报表金额-药房金额2018/05/01 00:00:00
            int i = 0;
            foreach (DataRow dr in dTcheckOut.Rows)
            {
                i++;
                dTAccountMoney = SqlServerHelper.ExecuteDataSet(SqlServerHelper.ConnectionString, string.Format(sqlAccountMoney, dr["Operatorid"], dr["StartDate"], dr["EndDate"])).Tables[0];
                dTDispensingMoney = SqlServerHelper.ExecuteDataSet(SqlServerHelper.ConnectionString, string.Format(sqlDispensingMoney, dr["Operatorid"], dr["StartDate"], dr["EndDate"])).Tables[0];
                dTDetailAccount = SqlServerHelper.ExecuteDataSet(SqlServerHelper.ConnectionString, string.Format(sqlDetailAccount, dr["Operatorid"], dr["StartDate"], dr["EndDate"])).Tables[0];

                //FileHeleper.WriteLog(string.Format(sqlAccountMoney, dr["Operatorid"], dr["StartDate"], dr["EndDate"]) + "\r\n" + string.Format(sqlDispensingMoney, dr["Operatorid"], dr["StartDate"], dr["EndDate"]));
                DataRow drResult = dtResult.NewRow();
                drResult["NO"] = i;
                drResult["StartDate"] = dr["StartDate"];
                drResult["EndDate"] = dr["EndDate"];
                drResult["Operator"] = dr["OperatorName"];
                decimal dAccountMoney = Convert.ToDecimal(dTAccountMoney.Rows[0]["PAYMENTMONEY"]);
                decimal dDispensingMoney = Convert.ToDecimal(dTDispensingMoney.Rows[0]["门诊发药金额"]);
                decimal dDetailAccount = Convert.ToDecimal(dTDetailAccount.Compute("SUM(Money)", "") == DBNull.Value ? 0 : dTDetailAccount.Compute("SUM(Money)", ""));
                drResult["ReportMoney"] = dAccountMoney;
                drResult["DetailMoney"] = dDetailAccount;
                drResult["DiffMoneyCount1"] = dAccountMoney - dDetailAccount;

                drResult["PharmacyMoney"] = dDispensingMoney;
                drResult["DiffMoneyCount2"] = dAccountMoney - dDispensingMoney;
                dtResult.Rows.Add(drResult);
            }
            DataRow drResultCount = dtResult.NewRow();
            //插入统计数据
            drResultCount["ReportMoney"] = Convert.ToDecimal(dtResult.Compute("SUM(ReportMoney)", "") == DBNull.Value ? 0 : dtResult.Compute("SUM(ReportMoney)", ""));
            drResultCount["DetailMoney"] = Convert.ToDecimal(dtResult.Compute("SUM(DetailMoney)", "") == DBNull.Value ? 0 : dtResult.Compute("SUM(DetailMoney)", ""));
            drResultCount["PharmacyMoney"] = Convert.ToDecimal(dtResult.Compute("SUM(PharmacyMoney)", "") == DBNull.Value ? 0 : dtResult.Compute("SUM(PharmacyMoney)", ""));
            drResultCount["DiffMoneyCount1"] = Convert.ToDecimal(dtResult.Compute("SUM(DiffMoneyCount1)", "") == DBNull.Value ? 0 : dtResult.Compute("SUM(DiffMoneyCount1)", ""));
            drResultCount["DiffMoneyCount2"] = Convert.ToDecimal(dtResult.Compute("SUM(DiffMoneyCount2)", "") == DBNull.Value ? 0 : dtResult.Compute("SUM(DiffMoneyCount2)", ""));
            dtResult.Rows.Add(drResultCount);
            #endregion

            lvDetail.ItemsSource = dtResult.DefaultView;
        }


        /// <summary>
        /// 计算药房与明细账的金额差距
        /// </summary>
        private void Calculaate3()
        {
            //药房发药
            string sqlPharmacy = @"SELECT DetailAccountid, --明细账号
                                           TotalMoney,--金额
                                           OperateDate,--操作时间
                                           LeechdomState --状态
                                    FROM Pharmacy_ClinicMedicine
                                    WHERE DetailAccountid IN (
                                                                 SELECT DISTINCT (DetailAccountid) FROM Pharmacy_ClinicMedicine
                                                             )";
            DataTable dtPharmacy = SqlServerHelper.ExecuteDataSet(SqlServerHelper.ConnectionString, sqlPharmacy).Tables[0];
            
            DataTable dtDetailAccountid = dtPharmacy.DefaultView.ToTable(true, "DetailAccountid");
            List<string> listDetailAccountid = new List<string>();
            foreach(DataRow row in dtDetailAccountid.Rows)
            {
                listDetailAccountid.Add("'"+row["DetailAccountid"].ToString()+"'");
            }

            string sqlAccount = @"SELECT DetailAccountid,CancelMark,Money,OperateDate FROM  Clinic_DetailAccount WHERE Itemid = '01' AND CancelMark='0' AND Operatorid='0033' AND DetailAccountid IN(" + string.Join(",", listDetailAccountid.ToArray() )+ ")";
            DataTable dtAccount = SqlServerHelper.ExecuteDataSet(SqlServerHelper.ConnectionString, sqlAccount).Tables[0];

            #region 结果运算
            DataTable dtResult = new DataTable();
            dtResult.Columns.Add("NO", typeof(int));//序号
            dtResult.Columns.Add("DetailAccountid", typeof(string));//明细号
            dtResult.Columns.Add("State", typeof(string));//状态
            dtResult.Columns.Add("OperateDate", typeof(string));//操作时间
            dtResult.Columns.Add("Money", typeof(decimal));//金额
            dtResult.Columns.Add("TotalMoney", typeof(decimal));//发药金额合计
            dtResult.Columns.Add("DiffMoney", typeof(decimal));//差异金额合计 金额-发药金额合计

            dtResult.Columns["NO"].Caption = "序号";
            dtResult.Columns["DetailAccountid"].Caption = "明细号";
            dtResult.Columns["State"].Caption = "状态";
            dtResult.Columns["OperateDate"].Caption = "操作时间";
            dtResult.Columns["Money"].Caption = "金额";
            dtResult.Columns["TotalMoney"].Caption = "发药金额合计";
            dtResult.Columns["DiffMoney"].Caption = "差异金额合计";

            #endregion

            int i = 0;
            foreach(DataRow rowAccount in dtAccount.Rows)
            {
                i++;
                DataRow rowNew = dtResult.NewRow();
                rowNew["NO"] = i;
                rowNew["DetailAccountid"] = rowAccount["DetailAccountid"];
                rowNew["State"] = rowAccount["CancelMark"];
                rowNew["OperateDate"] = Convert.ToDateTime(rowAccount["OperateDate"]).ToString("yyyy/MM/dd HH:mm:ss");
                decimal dMoney = 0;
                try
                {
                    dMoney = Convert.ToDecimal(rowAccount["Money"]);
                }
                catch
                {

                }
                rowNew["Money"] = dMoney;
                decimal dTotalMoney = 0;
                try
                {
                    dTotalMoney = Convert.ToDecimal(dtPharmacy.Compute("SUM(TotalMoney)", " DetailAccountid='" + rowAccount["DetailAccountid"] + "' AND  LeechdomState='" + rowAccount["CancelMark"] + "' "));
                }
                catch
                {

                }

                decimal dDiffMoney = dMoney - dTotalMoney;
                rowNew["TotalMoney"] = dTotalMoney;
                rowNew["DiffMoney"] = dDiffMoney;

                dtResult.Rows.Add(rowNew);
                //if (dDiffMoney > 0)
                //{
                //    dtResult.Rows.Add(rowNew);
                //}
            }
            //插入统计数据

            DataRow drResultCount = dtResult.NewRow();
            drResultCount["DiffMoney"] = Convert.ToDecimal(dtResult.Compute("SUM(DiffMoney)", "") == DBNull.Value ? 0 : dtResult.Compute("SUM(DiffMoney)", ""));
            drResultCount["Money"] = Convert.ToDecimal(dtResult.Compute("SUM(Money)", "") == DBNull.Value ? 0 : dtResult.Compute("SUM(Money)", ""));
            drResultCount["TotalMoney"] = Convert.ToDecimal(dtResult.Compute("SUM(TotalMoney)", "") == DBNull.Value ? 0 : dtResult.Compute("SUM(TotalMoney)", ""));
            dtResult.Rows.Add(drResultCount);

            lvDetail.ItemsSource = dtResult.DefaultView;
        }

        private void btnExportExce_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                DataView dvExportData =  lvDetail.ItemsSource as DataView;
                NpoiHelper excelHelper = new NpoiHelper();
                excelHelper.DataTableExportExcel("", dvExportData.ToTable(), true);
            }
            catch(Exception ex)
            {
                MessageBox.Show(ex.ToString());
            }
        }
    }
    
}
