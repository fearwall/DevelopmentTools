﻿using DeveloperKits.Develop.HIS.Model;
using DeveloperKits.ProjectCommonClass.Helper;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.Xml;

namespace DeveloperKits.Develop.HIS
{
    /// <summary>
    /// ReflectCodeGenerate.xaml 的交互逻辑
    /// </summary>
    public partial class ReflectCodeGenerate : Window
    {
        //public Dictionary<string,string> DctCtrl = new Dictionary<string,string>();//控件字典信息

        /// <summary>
        /// 控件字典信息
        /// </summary>
        protected ObservableCollection<ReflectCodeGenerateModel> DctCtrl = new ObservableCollection<ReflectCodeGenerateModel>();

        public ReflectCodeGenerate()
        {
            InitializeComponent();

            InitializeDctCtrl();

        }



        /// <summary>
        /// 初始化看控件字典信息
        /// </summary>
        private void InitializeDctCtrl()
        {
            #region 获取代码片段文件列表
            DctCtrl.Clear();
            cbCheckedAll.IsChecked = false;
            
            string strCodeSnippetDirPath = FilePathHelper.GetCurrentAppRootPath() + @"\Develop\HIS\Lib\CodeSnippetFile";
            List<string> listCodeSnippetFilePath = new List<string>();
            FilePathHelper.GetDirectoryFilePaths(strCodeSnippetDirPath, ".code", false, ref listCodeSnippetFilePath);
            #endregion
            foreach (string strCodeSnippetFilePath in listCodeSnippetFilePath)
            {
                string strFileName = string.Empty;//文件名称
                string strControlName = string.Empty;//控件名称
                try
                {
                    strFileName = System.IO.Path.GetFileNameWithoutExtension(strCodeSnippetFilePath);
                    strControlName = strFileName.Substring(strFileName.IndexOf(".") + 1);
                    DctCtrl.Add(new ReflectCodeGenerateModel()
                    {
                        FilePath = strCodeSnippetFilePath,
                        FileName = strFileName,
                        FileContent = GetCodeSnippet(strCodeSnippetFilePath),
                        ControlName = strControlName

                    });
                }
                catch(Exception ex)
                {
                    MessageBox.Show("读取代码文件：\r\n\t【" + strCodeSnippetFilePath + "】 \r\n出错！出错原因为：\r\n\\t" + ex.ToString() );
                    return;
                }
            }
            lbControlList.ItemsSource = null;
            lbControlList.ItemsSource = DctCtrl;
        }

        /// <summary>
        /// 全选/取消全选
        /// </summary>
        private void SelectAllChanged()
        {
            bool isCheckd = cbCheckedAll.IsChecked == true ? true :false ;
            foreach(ReflectCodeGenerateModel control in DctCtrl)
            {
                control.IsChecked = isCheckd;
            }
        }

        /// <summary>
        /// 获取代码片段
        /// </summary>
        /// <param name="strFileName"></param>
        /// <returns></returns>
        private string GetCodeSnippet(string strPath)
        {
            Encoding encoding = Encoding.UTF8;
            return FileHelper.ReadFile(strPath,encoding);
        }

        private void btnRefresh_Click(object sender, RoutedEventArgs e)
        {
            InitializeDctCtrl();
            txtReadXmlName.Text = string.Empty;
            cbCheckedAll.IsChecked = false;
        }

        private void btnGenerateCode_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                List<string> listCheckedCodeSni = new List<string>();
                foreach(ReflectCodeGenerateModel ctrl in DctCtrl)
                {
                    if(ctrl.IsChecked == true)
                    {
                        listCheckedCodeSni.Add(ctrl.FileContent);
                    }
                }
                if (listCheckedCodeSni.Count <= 0)
                {
                    MessageBox.Show("请至少选择一项需要生成的代码片段！", "提示！", MessageBoxButton.OK, MessageBoxImage.Exclamation);
                    return;
                }
                string strRes = string.Join(@"else if", listCheckedCodeSni);
                strRes = @"if" + strRes;
                txtCode.Text = strRes;
                //MessageBox.Show("代码生成成功！", "成功！", MessageBoxButton.OK, MessageBoxImage.Asterisk);
                txtCode.Focus();
                txtCode.SelectAll();
            }
            catch(Exception ex)
            {
                MessageBox.Show("生成代码失败：\r\n\t"+ex.ToString(), "错误！", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        private void btnCopy_Click(object sender, RoutedEventArgs e)
        {
            if (string.IsNullOrWhiteSpace(txtCode.Text.Trim()))
            {
                MessageBox.Show("代码生成结果区内容为空，不可复制！", "提示！", MessageBoxButton.OK, MessageBoxImage.Exclamation);
                return;
            }
            try
            {
                this.txtCode.Dispatcher.Invoke(new Action(delegate
                {
                    Clipboard.SetDataObject(txtCode.Text, true);
                }));
            }
            catch(Exception ex)
            {
                string str = ex.Message;
                MessageBox.Show("复制失败，请手动操作！", "失败！", MessageBoxButton.OK, MessageBoxImage.Error);
            }
            txtCode.Focus();
            txtCode.SelectAll();
        }

        private void btnShear_Click(object sender, RoutedEventArgs e)
        {
            if (string.IsNullOrWhiteSpace(txtCode.Text.Trim()))
            {
                MessageBox.Show("代码生成结果区内容为空，不可剪切！", "提示！", MessageBoxButton.OK, MessageBoxImage.Exclamation);
                return;
            }
            try
            {
                Clipboard.SetDataObject(txtCode.Text, true);
                txtCode.Text = string.Empty;
            }
            catch
            {
                MessageBox.Show("剪切失败，请手动操作！", "失败！", MessageBoxButton.OK, MessageBoxImage.Error);
            }
            txtCode.Focus();
        }

        private void cbCheckedAll_Click(object sender, RoutedEventArgs e)
        {
            SelectAllChanged();
        }

        private void btnEmpty_Click(object sender, RoutedEventArgs e)
        {
            if (MessageBox.Show("确定要清空代码生成结果区的内容吗？", "提示！", MessageBoxButton.OKCancel, MessageBoxImage.Exclamation) == MessageBoxResult.OK)
            {
                txtCode.Text = string.Empty;
                txtCode.Focus();
            }
            else
            {
                txtCode.Focus();
                txtCode.SelectAll();
            }
        }

        private void btnReadXmlChecked_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                //选取xml文件
                string strXmlPath = FilePathHelper.FilePathSelect("XML文件|*.xml");
                if (string.IsNullOrEmpty(strXmlPath))
                {
                    return;
                }
                else
                {
                    txtReadXmlName.Text = System.IO.Path.GetFileNameWithoutExtension(strXmlPath);
                    XmlDocument xmlDoc = new XmlDocument();
                    try
                    {
                        xmlDoc.Load(strXmlPath);
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show("读取XML错误！\r\n\t" + ex.ToString(), "提示！", MessageBoxButton.OK, MessageBoxImage.Error);
                        return;
                    }
                    XmlNodeList xmlNodeCNList = xmlDoc.SelectNodes("/Frm/Chinese");
                    XmlNodeList xmlNodeTBList = xmlDoc.SelectNodes("/Frm/Tibetan");
                    List<string> listCheked = new List<string>();
                    foreach (XmlNode nodesCn in xmlNodeCNList)
                    {
                        XmlNodeList nodelist = nodesCn.SelectNodes("Item");
                        foreach (XmlNode xxm in nodelist)
                        {
                            string value = xxm.Attributes["Type"].Value.ToString();
                            if (!listCheked.Contains(value))
                            {
                                listCheked.Add(value);
                            }
                        }
                    }
                    foreach (XmlNode nodesTb in xmlNodeTBList)
                    {
                        XmlNodeList nodelist = nodesTb.SelectNodes("Item");
                        foreach (XmlNode xxm in nodelist)
                        {
                            string value = xxm.Attributes["Type"].Value.ToString();
                            if (!listCheked.Contains(value))
                            {
                                listCheked.Add(value);
                            }
                        }
                    }
                    if (listCheked.Count > 0)
                    {
                        foreach (ReflectCodeGenerateModel ctrl in DctCtrl)
                        {
                            if (listCheked.Contains(ctrl.ControlName))
                            {
                                ctrl.IsChecked = true;
                            }
                            else
                            {
                                ctrl.IsChecked = false;
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("读取XML选择控件失败！\r\n\t"+ex.ToString(), "错误！", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }
    }
}
