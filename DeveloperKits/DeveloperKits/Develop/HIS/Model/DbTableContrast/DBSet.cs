﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DeveloperKits.Develop.HIS.Model.DbTableContrast
{
    public class DBSet : INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged;

        /// <summary>
        /// 服务器名称/地址
        /// </summary>
        private string serverName = "127.0.0.1";

        /// <summary>
        /// 设置或获取服务器名称/地址
        /// </summary>
        public string ServerName
        {
            get
            {
                return serverName;
            }
            set
            {
                serverName = value;
                if (this.PropertyChanged != null)
                {
                    this.PropertyChanged.Invoke(this, new PropertyChangedEventArgs("ServerName"));
                }
            }
        }

        /// <summary>
        /// 用户名
        /// </summary>
        private string userName="sa";

        /// <summary>
        /// 设置或获取用户名
        /// </summary>
        public string UserName
        {
            get
            {
                return userName;
            }
            set
            {
                userName = value;
                if (this.PropertyChanged != null)
                {
                    this.PropertyChanged.Invoke(this, new PropertyChangedEventArgs("UserNmae"));
                }
            }
        }

        /// <summary>
        /// 密码
        /// </summary>
        private string passWord="123456";

        /// <summary>
        /// 设置或获取密码
        /// </summary>
        public string PassWord
        {
            get
            {
                return passWord;
            }
            set
            {
                passWord = value;
                if (this.PropertyChanged != null)
                {
                    this.PropertyChanged.Invoke(this, new PropertyChangedEventArgs("PassWord"));
                }
            }
        }

        /// <summary>
        /// 选中的数据库
        /// </summary>
        private string selectDataBaseName;

        /// <summary>
        /// 设置或获取选中的数据库
        /// </summary>
        public string SelectDataBaseName
        {
            get
            {
                return selectDataBaseName;
            }
            set
            {
                selectDataBaseName = value;
                if (this.PropertyChanged != null)
                {
                    this.PropertyChanged.Invoke(this, new PropertyChangedEventArgs("SelectDataBaseName"));
                }
            }
        }

        /// <summary>
        /// 数据库列表
        /// </summary>
        private ICollection<string> dataBaseNameList;

        /// <summary>
        /// 设置或获取数据库列表
        /// </summary>
        public ICollection<string> DataBaseNameList
        {
            get
            {
                return dataBaseNameList;
            }
            set
            {
                dataBaseNameList = value;
                if (this.PropertyChanged != null)
                {
                    this.PropertyChanged.Invoke(this, new PropertyChangedEventArgs("DataBaseNameList"));
                }
            }
        }

        /// <summary>
        /// 连接字符串
        /// </summary>
        private string connectionString = string.Empty;

        /// <summary>
        /// 设置或获取连接字符串
        /// </summary>
        public string ConnectionString
        {
            get
            {
                return this.ToDbConnectionString();
            }
        }


        /// <summary>
        /// 将当前对象的值转换为数据库连接字符串
        /// </summary>
        /// <param name="iTimeOut">连接超时时长</param>
        /// <returns></returns>
        public string ToDbConnectionString(int iTimeOut)
        {
            string strRes = string.Empty;
            string strConnectStringFormat = @"Data Source={0};Initial Catalog={1};User ID={2};Password={3};connect Timeout={4};min pool size=1;max pool size=200";
            strRes = string.Format(strConnectStringFormat, serverName, selectDataBaseName, userName, passWord, iTimeOut);
            return strRes;
        }
        /// <summary>
        /// 将当前对象的值转换为数据库连接字符串
        /// </summary>
        /// <returns></returns>
        public string ToDbConnectionString()
        {
            return ToDbConnectionString(20);
        }
    }
}
