﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DeveloperKits.Develop.HIS.Model
{
    public class ReflectCodeGenerateModel:INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged;



        /// <summary>
        /// 是否选中
        /// </summary>
        private bool isChecked = false;

        /// <summary>
        /// 文件路径
        /// </summary>
        private string filePath = string.Empty;

        /// <summary>
        /// 文件名称
        /// </summary>
        private string fileName = string.Empty;

        /// <summary>
        /// 文件内容
        /// </summary>
        private string fileContent = string.Empty;

        /// <summary>
        /// 控件名称
        /// </summary>
        private string controlName = string.Empty;
        
        /// <summary>
        /// 获取或设置是否选中
        /// </summary>
        public bool IsChecked
        {
            get { return isChecked; }
            set
            {
                isChecked = value;
                if(this.PropertyChanged != null)
                {
                    this.PropertyChanged.Invoke(this, new PropertyChangedEventArgs("IsChecked"));
                }
            }
        }        

        /// <summary>
        /// 设置或获取文件路径
        /// </summary>
        public string FilePath
        {
            get { return filePath; }
            set
            {
                filePath = value;
                if (this.PropertyChanged != null)
                {
                    this.PropertyChanged.Invoke(this, new PropertyChangedEventArgs("FilePath"));
                }
            }
        }


        /// <summary>
        /// 设置或获取文件名称
        /// </summary>
        public string FileName
        {
            get { return fileName; }
            set
            {
                fileName = value;
                if (this.PropertyChanged != null)
                {
                    this.PropertyChanged.Invoke(this, new PropertyChangedEventArgs("FileName"));
                }
            }
        }

        /// <summary>
        /// 设置或获取文件内容
        /// </summary>
        public string FileContent
        {
            get { return fileContent; }
            set
            {
                fileContent = value;
                if (this.PropertyChanged != null)
                {
                    this.PropertyChanged.Invoke(this, new PropertyChangedEventArgs("FileContent"));
                }
            }
        }


        /// <summary>
        /// 设置或获取控件名称
        /// </summary>
        public string ControlName
        {
            get { return controlName; }
            set
            {
                controlName = value;
                if (this.PropertyChanged != null)
                {
                    this.PropertyChanged.Invoke(this, new PropertyChangedEventArgs("ControlName"));
                }
            }
        }

        public ReflectCodeGenerateModel()
        {
        }

        public override string ToString()
        {
            string strRes = "";
            strRes += " isChecked: [ " + IsChecked + " ] ";
            strRes += " filePath: [ " + FilePath + " ] ";
            strRes += " fileName: [ " + FileName + " ] ";
            strRes += " fileContent: [ " + FileContent + " ] ";
            strRes += " controlName: [ " + ControlName + " ] ";
            return strRes;
        }

    }
}
