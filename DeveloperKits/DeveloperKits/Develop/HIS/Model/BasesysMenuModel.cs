﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DeveloperKits.Develop.HIS.Model
{
    public class BasesysMenuModel
    {
        public BasesysMenuModel()
        {
            this.Nodes = new List<BasesysMenuModel>();
            this.ParentNode = "00";
        }


        private string _nodeId;
        private string _nodeName;
        private string _parentNode;
        private string _isEndNode;
        private string _pluginid;
        private string _commentary;
        private string _disporder;
        private int _type;
        private int _isChoose;

        public string NodeId { get => _nodeId; set => _nodeId = value; }
        public string NodeName { get => _nodeName; set => _nodeName = value; }
        public string ParentNode { get => _parentNode; set => _parentNode = value; }
        public string IsEndNode { get => _isEndNode; set => _isEndNode = value; }
        public string Pluginid { get => _pluginid; set => _pluginid = value; }
        public string Commentary { get => _commentary; set => _commentary = value; }
        public string Disporder { get => _disporder; set => _disporder = value; }
        public int Type { get => _type; set => _type = value; }
        public int IsChoose { get => _isChoose; set => _isChoose = value; }

        public List<BasesysMenuModel> Nodes { get; set; }

        public override string ToString()
        {
            string strRes = "";
            strRes += " NodeName =  " + _nodeId + " , ";
            strRes += " Pluginid =  " + _pluginid + " , ";
            return strRes;
        }

    }
}
