﻿using DeveloperKits.ProjectCommonClass.Helper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace DeveloperKits.Develop.Test
{

    /// <summary>
    /// TestMainWindow.xaml 的交互逻辑
    /// </summary>
    public partial class TestMainWindow : Window
    {
        public TestMainWindow()
        {
            InitializeComponent();
        }


        /// <summary>
        /// 通过反射，打开Button Name的窗体
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Button_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                Button btn = sender as Button;
                string windowName = btn.Name;
                Assembly assembly = Assembly.GetExecutingAssembly();
                Window window = assembly.CreateInstance(GetNamespance() + "." + windowName) as Window;
                window.WindowStartupLocation = WindowStartupLocation.CenterScreen;
                window.Show();
                //Assembly.
            }
            catch (Exception ex)
            {
                MessageBox.Show("打开窗体失败:\r\n\t" + ex.ToString(), "失败！", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        /// <summary>
        /// 获取命名空间
        /// </summary>
        /// <returns></returns>
        public static string GetNamespance()
        {
            return System.Reflection.MethodBase.GetCurrentMethod().DeclaringType.Namespace;
        }

        private void DateHelperTest_Click(object sender, RoutedEventArgs e)
        {
            DateTime dateTimeNow = DateTime.Now;
            //本月第一天
            txtDateHelperTest.Text = DateTimeHelper.FirstDayOfMonth(dateTimeNow).ToString("yyyy/MM/dd");
            //本月最后一天
            txtDateHelperTest.Text = DateTimeHelper.LastDayOfMonth(dateTimeNow).ToString("yyyy/MM/dd");
            //上月第一天
            txtDateHelperTest.Text = DateTimeHelper.FirstDayOfPrevMonth(dateTimeNow).ToString("yyyy/MM/dd");
            //上月最后一天
            txtDateHelperTest.Text = DateTimeHelper.LastDayOfPrevMonth(dateTimeNow).ToString("yyyy/MM/dd");
            //下月第一天
            txtDateHelperTest.Text = DateTimeHelper.FirstDayOfNextMonth(dateTimeNow).ToString("yyyy/MM/dd");
            //下月最后一天
            txtDateHelperTest.Text = DateTimeHelper.LastDayOfNextMonth(dateTimeNow).ToString("yyyy/MM/dd");
            //前三个月第一天
            txtDateHelperTest.Text = DateTimeHelper.FirstDayOfAssignMonth(dateTimeNow, -3).ToString("yyyy/MM/dd");
            //前三个月最后一天
            txtDateHelperTest.Text = DateTimeHelper.LastDayOfAssignMonth(dateTimeNow, -3).ToString("yyyy/MM/dd");
            //后三个月第一天
            txtDateHelperTest.Text = DateTimeHelper.FirstDayOfAssignMonth(dateTimeNow, 3).ToString("yyyy/MM/dd");
            //后三个月的最后一天
            txtDateHelperTest.Text = DateTimeHelper.LastDayOfAssignMonth(dateTimeNow, 3).ToString("yyyy/MM/dd");

            //去年当月的第一天
            txtDateHelperTest.Text = DateTimeHelper.FirstDayOfAssignMonthAndYear(dateTimeNow, 0, -1).ToString("yyyy/MM/dd");
            //去年当月的最后一天
            txtDateHelperTest.Text = DateTimeHelper.LastDayOfAssignMonthAndYear(dateTimeNow, 0, -1).ToString("yyyy/MM/dd");

            //当年的第一天
            txtDateHelperTest.Text = DateTimeHelper.FirstDayOfYear(dateTimeNow).ToString("yyyy/MM/dd");

            //当年的最后一天
            txtDateHelperTest.Text = DateTimeHelper.LastDayOfYear(dateTimeNow).ToString("yyyy/MM/dd");

            //去年的第一天
            txtDateHelperTest.Text = DateTimeHelper.FirstDayOfPrevYear(dateTimeNow).ToString("yyyy/MM/dd");

            //去年的最后一天
            txtDateHelperTest.Text = DateTimeHelper.LastDayOfPrevYear(dateTimeNow).ToString("yyyy/MM/dd");

            //明年的第一天
            txtDateHelperTest.Text = DateTimeHelper.FirstDayOfNextYear(dateTimeNow).ToString("yyyy/MM/dd");
            
            //明年的最后一天
            txtDateHelperTest.Text = DateTimeHelper.LastDayOfNextYear(dateTimeNow).ToString("yyyy/MM/dd");

            //指定N年前的第一天
            txtDateHelperTest.Text = DateTimeHelper.FirstDayOfAssignYear(dateTimeNow,-2).ToString("yyyy/MM/dd");

            //指定N年前的最后一天
            txtDateHelperTest.Text = DateTimeHelper.LastDayOfAssignYear(dateTimeNow, -2).ToString("yyyy/MM/dd");
        }
    }
}
