﻿using DeveloperKits.ProjectCommonClass.Helper;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Markup;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.Xml;

namespace DeveloperKits.Develop.Test
{
    /// <summary>
    /// RichTextBoxDemo.xaml 的交互逻辑
    /// </summary>
    public partial class RichTextBoxDemo : Window
    {
        public RichTextBoxDemo()
        {
            InitializeComponent();

        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            FlowDocument fd = richTB.Document;
            string s = FileHelper.ReadFile(FilePathHelper.GetCurrentAppDirectory() + "/Logs/rtf.log");
            RTFToString(richTB, s);
            //s = XamlHelper.GetElementXamlCodeToXml(fd, Encoding.UTF8);
            FileHelper.WriteLog(s);
            //ele.Click += Button_Click;
            //sp_Demo.Children.Add(ele);

        }

        public string StringToRTF(RichTextBox richTextBox)
        {
            string rtf = string.Empty;
            TextRange textRange = new TextRange(richTextBox.Document.ContentStart, richTextBox.Document.ContentEnd);
            using (MemoryStream ms = new MemoryStream())
            {
                textRange.Save(ms, System.Windows.DataFormats.Rtf);
                ms.Seek(0, SeekOrigin.Begin);
                StreamReader sr = new StreamReader(ms);
                rtf = sr.ReadToEnd();
            }
            return rtf;
        }

        public string RTFToString(RichTextBox richTextBox, string rtf)
        {
            string strRes = string.Empty;
            if (string.IsNullOrEmpty(rtf))
            {
                throw new ArgumentNullException();
            }

            TextRange textRange = new TextRange(richTextBox.Document.ContentStart, richTextBox.Document.ContentEnd);
            using (MemoryStream ms = new MemoryStream())
            {
                using (StreamWriter sw = new StreamWriter(ms))
                {
                    sw.Write(rtf);
                    sw.Flush();
                    ms.Seek(0, SeekOrigin.Begin);
                    textRange.Load(ms, DataFormats.Rtf);
                    strRes = textRange.Text;
                }
            }
            return strRes;
        }

        /// <summary>
        /// 查找控件
        /// </summary>
        /// <param name="tControl">查找控件的控件类型</param>
        /// <returns></returns>
        private List<Type> SearchControl(Type tControl)
        {
            try
            {
                List<Type> derivedTypes = new List<Type>();
                Assembly assembly = Assembly.GetAssembly(tControl);
                foreach(Type type in assembly.GetTypes())
                {
                    if(type.IsSubclassOf(tControl) && !type.IsAbstract && type.IsPublic)
                    {
                        derivedTypes.Add(type);
                    }
                }
                derivedTypes.Sort();
                return derivedTypes;
            }
            catch(Exception ex)
            {
                throw new Exception("查找控件错误：" + ex.Message);
            }
        }

        /// <summary>
        /// 获取控件的Xaml的代码
        /// </summary>
        /// <param name="tControl">指定的控件类型</param>
        /// <param name="outEncoding">输出的编码类型</param>
        /// <returns></returns>
        private string GetControlXamlCode(Type tControl,Encoding outEncoding)
        {
            try
            {
                ConstructorInfo constructorInfo = tControl.GetConstructor(System.Type.EmptyTypes);
                Control control = (Control)constructorInfo.Invoke(null);
                control.Visibility = Visibility.Collapsed;
                ControlTemplate template = control.Template;
                XmlWriterSettings settings = new XmlWriterSettings();
                settings.Indent = true;
                settings.Encoding = outEncoding;
                StringBuilder sb = new StringBuilder();
                XmlWriter writer = XmlWriter.Create(sb, settings);
                XamlWriter.Save(template, writer);
                return sb.ToString();
            }
            catch(Exception ex)
            {
                throw new Exception("获取控件Xaml出错：" + ex.Message);
            }
        }

        /// <summary>
        /// 获取元素的Xaml代码
        /// </summary>
        /// <param name="oEle">指定元素的类型</param>
        /// <param name="xmlEncoding">输出的编码类型</param>
        /// <returns></returns>
        private string GetElementXamlCodeToXml(object oEle,Encoding xmlEncoding)
        {
            try
            {
                XmlWriterSettings settings = new XmlWriterSettings();
                settings.Indent = true;
                settings.Encoding = xmlEncoding;
                //settings.NewLineOnAttributes = false;
                StringBuilder sb = new StringBuilder();
                XmlWriter writer = XmlWriter.Create(sb, settings);
                XamlWriter.Save(oEle, writer);                
                return sb.ToString();
            }
            catch(Exception ex)
            {
                throw new Exception("获取控件Xaml出错：" + ex.Message);
            }
        }

        /// <summary>
        /// 获取元素的Xaml代码
        /// </summary>
        /// <param name="oEle">指定元素的类型</param>
        /// <param name="xmlEncoding">输出的编码类型</param>
        /// <returns></returns>
        private string GetElementXamlCodeToText(object oEle, Encoding xmlEncoding)
        {
            try
            {
                string strRes = GetElementXamlCodeToXml(oEle, xmlEncoding);
                XmlDocument xmlDocument = new XmlDocument();
                xmlDocument.LoadXml(strRes);
                string strFirstNode = xmlDocument.FirstChild.OuterXml;
                if(strFirstNode.Contains(@"<?xml") && strFirstNode.Contains("@?>"))
                {
                    strRes = strRes.Replace(strFirstNode, "").Trim();
                }
                return strRes;
            }
            catch (Exception ex)
            {
                throw new Exception("去出控件Xml格式声明出错：" + ex.Message);
            }

        }
    }
}
