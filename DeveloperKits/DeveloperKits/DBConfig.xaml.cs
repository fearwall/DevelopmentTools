﻿using DeveloperKits.ProjectCommonClass;
using DeveloperKits.ProjectCommonClass.DBUtility;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace DeveloperKits
{
    /// <summary>
    /// DBConfig.xaml 的交互逻辑
    /// </summary>
    public partial class DBConfig : Window
    {
        Dictionary<string, string> _dictDbConnectionString = new Dictionary<string, string>();
        public DBConfig()
        {
            InitializeComponent();
            InitializeData();
        }

        private void btnTest_Click(object sender, RoutedEventArgs e)
        {
            EditDictionaryDbValue();
            string strConnectionString = DictionaryDbConnectionToString();
            try
            {
                int i = Convert.ToInt32(SqlServerHelper.ExecuteScalar(strConnectionString, "select Count(0) as '用户定义表的个数' from sys.objects Where type='U' And type_desc='USER_TABLE'"));
                MessageBoxResult mbr = MessageBox.Show("数据库连接成功！是否保存配置？", "提示", MessageBoxButton.YesNo, MessageBoxImage.Asterisk, MessageBoxResult.Yes);
                if(mbr == MessageBoxResult.Yes)
                {
                    btnSave_Click(null, null);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("数据库连接错误：\r\n\t"+ex.ToString(), "错误！", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        private void btnSave_Click(object sender, RoutedEventArgs e)
        {
            EditDictionaryDbValue();
            string strConnectionString = DictionaryDbConnectionToString();
            Tools.SetConnectionStringsConfig("MSConnectionString", strConnectionString);
            InitializeData();
        }
        
        private void btnRest_Click(object sender, RoutedEventArgs e)
        {
            InitializeData();
        }

        /// <summary>
        /// 初始化界面数据
        /// </summary>
        private void InitializeData()
        {
            string strDbConnectionString = ConfigurationManager.ConnectionStrings["MSConnectionString"].ConnectionString;
            if (!string.IsNullOrWhiteSpace(strDbConnectionString))
            {
                try
                {
                    List<string> listDbConnectionString = new List<string>(strDbConnectionString.Split(new string[] { ";" }, StringSplitOptions.RemoveEmptyEntries));
                    _dictDbConnectionString.Clear();
                    foreach (string str in listDbConnectionString)
                    {
                        try
                        {
                            List<string> listDict = new List<string>(str.Split(new string[] { "=" }, StringSplitOptions.RemoveEmptyEntries));
                            _dictDbConnectionString.Add(listDict[0], listDict[1]);
                        }
                        catch
                        {

                        }
                    }
                    txtDbName.Text = _dictDbConnectionString["Initial Catalog"];//数据库名称
                    txtServerIp.Text = _dictDbConnectionString["Data Source"];//服务器IP
                    txtUserName.Text = _dictDbConnectionString["User ID"];//用户名
                    txtPassword.Text = _dictDbConnectionString["Password"];//密码
                    txtServerIp.Focus();
                    txtServerIp.SelectionStart = txtServerIp.Text.Length;
                }
                catch(Exception ex)
                {
                    MessageBox.Show("读取数据库配置错误！", "错误！", MessageBoxButton.OK, MessageBoxImage.Error);
                }
            }
        }

        /// <summary>
        /// 将数据库字典配置转换为数据库配置字符串
        /// </summary>
        /// <param name="dictDbConnectionString"></param>
        /// <returns></returns>
        private string DictionaryDbConnectionToString()
        {
            string strRes = string.Empty;
            foreach(KeyValuePair<string,string> kvp in _dictDbConnectionString)
            {
                strRes += kvp.Key + "=" + kvp.Value + ";";
            }
            return strRes;
        }

        /// <summary>
        /// 修改配置的值
        /// </summary>
        /// <returns></returns>
        private void EditDictionaryDbValue()
        {
            _dictDbConnectionString["Initial Catalog"] = txtDbName.Text.Trim();//数据库名称
            _dictDbConnectionString["Data Source"] = txtServerIp.Text.Trim();//服务器IP
            _dictDbConnectionString["User ID"] = txtUserName.Text.Trim();//用户名
            _dictDbConnectionString["Password"] = txtPassword.Text.Trim();//密码
        }
    }
}
