﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace DeveloperKits.Demo.HtmlTextEditor
{
    /// <summary>
    /// Test.xaml 的交互逻辑
    /// </summary>
    public partial class Test : Window
    {
        public Test()
        {
            InitializeComponent();
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            DateTime dtS = new DateTime(2018,5,28,17,0,0);
            DateTime dtE = new DateTime(2018, 5, 28, 18, 0, 0);
            DateTime dtNow = new DateTime(2018, 5, 28, 18, 0, 0);
            if (dtNow >= dtS &&   dtNow <= dtE)
            {
                MessageBox.Show("期间内");
            }
        }
    }
}
