﻿using NPOI.HSSF.UserModel;
using NPOI.SS.UserModel;
using NPOI.XSSF.UserModel;
using System;
using System.Data;
using System.IO;
using System.Windows.Forms;

namespace DeveloperKits.ProjectCommonClass.Helper
{
    /// <summary>
    /// NPOI插件处理Excel
    /// <para>|--DataTable|DataSet导出到Excel</para>   
    /// <para>|--|--若DataTable.TableName不为空，则使用DataTable.TableName 作为Excel的Sheet名</para>
    /// <para>|--|--若DataTable.TableName为空，则使用Sheet+DataTable的索引</para>
    /// <para>|--Excel导入到DataTable|DataSet Excel的Sheet名默认为DataTable.TableName</para>
    /// </summary>
    public class NpoiHelper
    {
        /// <summary>
        /// 传入DataTable导出到Excel
        /// <para>|--DataTable.TableName默认为Sheet名，若无TableName默认使用Sheet1</para>  
        /// </summary>
        /// <param name="fileName">
        /// 导出的文件名，为空则使用yyyy-MM-dd.ext格式
        /// </param>
        /// <param name="dataTable">传入数据</param>
        /// <returns></returns>
        public bool DataTableExportExcel(string fileName, DataTable dataTable)
        {
            return DataTableExportExcel(fileName, dataTable,true,true);
        }

        /// <summary>
        /// 传入DataTable导出到Excel
        /// <para>|--DataTable.TableName默认为Sheet名，若无TableName默认使用Sheet1</para>  
        /// </summary>
        /// <param name="fileName">
        /// 导出的文件名，为空则使用yyyy-MM-dd.ext格式
        /// </param>
        /// <param name="dataTable">传入数据</param>
        /// <param name="columnsCaptionIsTitle">DataTable的column Caption是否作为标题</param>
        /// <returns></returns>
        public bool DataTableExportExcel(string fileName, DataTable dataTable, bool columnsCaptionIsTitle)
        {
            return DataTableExportExcel(fileName, dataTable, columnsCaptionIsTitle, true);
        }

        /// <summary>
        /// 传入DataTable导出到Excel
        /// <para>|--DataTable.TableName默认为Sheet名，若无TableName默认使用Sheet1</para>  
        /// </summary>
        /// <param name="fileName">
        /// 导出的文件名，为空则使用yyyy-MM-dd.ext格式
        /// </param>
        /// <param name="dataTable">传入数据</param>
        /// <param name="columnsCaptionIsTitle">DataTable的column Caption是否作为标题</param>
        /// <param name="isTitleShowDouble">是否同时存在 column Name 和   column Caption</param>
        /// <returns></returns>
        public bool DataTableExportExcel(string fileName, DataTable dataTable,bool columnsCaptionIsTitle,bool isTitleShowDouble)
        {
            #region 
            bool result = false;
            //用于写入文件
            MemoryStream ms = new MemoryStream();
            try
            {
                //选择文件资源路径
                SaveFileDialog fileDialog = new SaveFileDialog();
                //资源管理器设置
                fileDialog.Title = "请选择导出路径";
                fileDialog.Filter = "Excel文件(*.xls)|*.xls|Excel文件(*.xlsx)|*.xlsx";
                //打开时指定默认路径
                fileDialog.InitialDirectory = Environment.GetFolderPath(Environment.SpecialFolder.Desktop);
                if (string.IsNullOrEmpty(fileName))
                {
                    fileDialog.FileName = DateTime.Now.ToString("yyyy-MM-dd");
                }
                else
                {
                    fileDialog.FileName = fileName;
                }
                if (fileDialog.ShowDialog() == System.Windows.Forms.DialogResult.OK)
                {
                    return DataTableExportExcelNoSaveFileDialog(fileDialog.FileName, dataTable, columnsCaptionIsTitle, isTitleShowDouble);
                    #region 
                    //IWorkbook workbook = null;
                    //if (fileDialog.FilterIndex == 1)
                    //{
                    //    workbook = new HSSFWorkbook();
                    //}
                    //else
                    //{
                    //    workbook = new XSSFWorkbook();
                    //}

                    //if (dataTable != null)
                    //{

                    //    //创建sheet
                    //    string sheetName = dataTable.TableName.Trim();
                    //    if (sheetName.Length <= 0)
                    //    {
                    //        sheetName = "Sheet1";
                    //    }
                    //    else
                    //    {
                    //        if (sheetName.Length > 30)
                    //        {
                    //            sheetName = sheetName.Substring(0, 30);
                    //        }
                    //    }
                    //    ISheet sheet = workbook.CreateSheet(sheetName);
                    //    //添加表头
                    //    IRow row = sheet.CreateRow(0);
                    //    int cellIndex = 0;\r\n
                    //    foreach (DataColumn dc in dataTable.Columns)
                    //    {
                    //        //创建列
                    //        ICell cell = row.CreateCell(cellIndex);
                    //        cell.SetCellType(CellType.String);
                    //        if (isTitleShowDouble == false)
                    //        {
                    //            if (columnsCaptionIsTitle)
                    //            {
                    //                if (!string.IsNullOrEmpty(dc.Caption))
                    //                {
                    //                    cell.SetCellValue(dc.Caption);
                    //                }
                    //                else
                    //                {
                    //                    cell.SetCellValue(dc.ColumnName);
                    //                }
                    //            }
                    //            else
                    //            {
                    //                cell.SetCellValue(dc.ColumnName);
                    //            }
                    //        }
                    //        else
                    //        {
                    //            string strTitle = string.Empty;
                    //            if (!string.IsNullOrEmpty(dc.Caption) && !string.IsNullOrEmpty(dc.ColumnName))
                    //            {
                    //                cell.SetCellValue(dc.Caption + "\n" + dc.ColumnName);
                    //            }
                    //            else if (!string.IsNullOrEmpty(dc.Caption) && string.IsNullOrEmpty(dc.ColumnName))
                    //            {
                    //                cell.SetCellValue(dc.Caption);
                    //            }
                    //            else if (string.IsNullOrEmpty(dc.Caption) && !string.IsNullOrEmpty(dc.ColumnName))
                    //            {
                    //                cell.SetCellValue(dc.ColumnName);
                    //            }
                    //            else
                    //            {
                    //                cell.SetCellValue("Cell " + cellIndex + " No Tite");
                    //            }
                    //        }
                    //        cellIndex++;
                    //    }
                    //    //添加数据
                    //    for (int dataRowNum = 0; dataRowNum < dataTable.Rows.Count; dataRowNum++)
                    //    {
                    //        cellIndex = 0;
                    //        row = sheet.CreateRow(dataRowNum + 1);
                    //        foreach (DataColumn dc in dataTable.Columns)
                    //        {
                    //            ICell cell = row.CreateCell(cellIndex);
                    //            cell.SetCellType(CellType.String);
                    //            cell.SetCellValue(Convert.ToString(dataTable.Rows[dataRowNum][cellIndex]));
                    //            cellIndex++;
                    //        }
                    //    }


                    //    //设置列宽度自适应
                    //    for (int i = 0; i < dataTable.Rows.Count; i++)
                    //    {
                    //        sheet.AutoSizeColumn(i);
                    //        //sheet.SetColumnWidth(i, 10 * 256);
                    //    }

                    //    //写入文件
                    //    workbook.Write(ms);
                    //    workbook = null;
                    //    using (FileStream fileStream = new FileStream(fileDialog.FileName, FileMode.Create, FileAccess.Write))
                    //    {
                    //        byte[] data = ms.ToArray();
                    //        fileStream.Write(data, 0, data.Length);
                    //        fileStream.Flush();
                    //    }
                    //    MessageBox.Show("文件已导出到：" + fileDialog.FileName, "提示", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    //    result = true;
                    //}
                    //else
                    //{
                    //    MessageBox.Show("没有要导出的数据", "提示", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    //    result = false;
                    //}
                    #endregion

                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Exception:" + ex.Message, "错误", MessageBoxButtons.OK, MessageBoxIcon.Error);
                result = false;
            }
            finally
            {
                ms.Close();
                ms.Dispose();
            }
            return result;
            #endregion
        }

        /// <summary>
        /// 传入DataSet导出多个sheet的Excel 弹出对话框
        /// <para>|--DataTable.TableName默认为Sheet名，若无TableName默认使用Sheet+DataTable在DataSet中的索引</para>
        /// </summary>
        /// <param name="fileName">导出的文件名，为Null则使用yyyy-MM-dd.ext格式</param>
        /// <param name="dataSet">导出的数据集</param>
        /// <returns></returns>
        public bool DataSetExportExcel(string fileName, DataSet dataSet)
        {
            return DataSetExportExcel(fileName, dataSet, true, true);
        }

        /// <summary>
        /// 传入DataSet导出多个sheet的Excel 弹出对话框
        /// <para>|--DataTable.TableName默认为Sheet名，若无TableName默认使用Sheet+DataTable在DataSet中的索引</para>
        /// </summary>
        /// <param name="fileName">导出的文件名，为Null则使用yyyy-MM-dd.ext格式</param>
        /// <param name="dataSet">导出的数据集</param>
        /// <param name="columnsCaptionIsTitle">DataTable的column Caption是否作为标题</param>
        /// <returns></returns>
        public bool DataSetExportExcel(string fileName, DataSet dataSet, bool columnsCaptionIsTitle)
        {
            return DataSetExportExcel(fileName, dataSet, columnsCaptionIsTitle,true);
        }

        /// <summary>
        /// 传入DataSet导出多个sheet的Excel 弹出对话框
        /// <para>|--DataTable.TableName默认为Sheet名，若无TableName默认使用Sheet+DataTable在DataSet中的索引</para>
        /// </summary>
        /// <param name="fileName">导出的文件名，为Null则使用yyyy-MM-dd.ext格式</param>
        /// <param name="dataSet">导出的数据集</param>
        /// <param name="columnsCaptionIsTitle">DataTable的column Caption是否作为标题</param>
        /// <param name="isTitleShowDouble">是否同时存在 column Name 和   column Caption</param>
        /// <returns></returns>
        public bool DataSetExportExcel(string fileName, DataSet dataSet, bool columnsCaptionIsTitle, bool isTitleShowDouble)
        {

            bool result = false;
            //用于写入文件
            MemoryStream ms = new MemoryStream();
            try
            {
                //选择文件资源路径
                SaveFileDialog fileDialog = new SaveFileDialog();
                //资源管理器设置
                fileDialog.Title = "请选择导出路径";
                fileDialog.Filter = "Excel文件(*.xls)|*.xls|Excel文件(*.xlsx)|*.xlsx";
                //打开时指定默认路径
                fileDialog.InitialDirectory = Environment.GetFolderPath(Environment.SpecialFolder.Desktop);
                if (string.IsNullOrEmpty(fileName))
                {
                    fileDialog.FileName = DateTime.Now.ToString("yyyy-MM-dd");
                }
                else
                {
                    fileDialog.FileName = fileName;
                }
                if (fileDialog.ShowDialog() == System.Windows.Forms.DialogResult.OK)
                {
                    result = DataSetExportExcelNoSaveFileDialog(fileDialog.FileName, dataSet, columnsCaptionIsTitle, isTitleShowDouble);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Exception:" + ex.Message, "错误", MessageBoxButtons.OK, MessageBoxIcon.Error);
                result = false;
            }
            finally
            {
                ms.Close();
                ms.Dispose();
            }
            return result;
        }

        /// <summary>
        /// 传入DataTable导出到Excel 不弹出保存对话框
        /// </summary>
        /// <param name="filePathName">
        /// 导出的路径 .xls使用 03版excel  .xlsx使用高版本excel
        /// </param>
        /// <param name="dataTable">数据</param>
        /// <param name="columnsCaptionIsTitle">DataTable的column Caption是否作为标题</param>
        /// <returns></returns>
        public bool DataTableExportExcelNoSaveFileDialog(string fileName, DataTable dataTable)
        {
            return DataTableExportExcelNoSaveFileDialog(fileName, dataTable, true, true);
        }

        /// <summary>
        /// 传入DataTable导出到Excel 不弹出保存对话框
        /// </summary>
        /// <param name="filePathName">
        /// 导出的路径 .xls使用 03版excel  .xlsx使用高版本excel
        /// </param>
        /// <param name="dataTable">数据</param>
        /// <param name="columnsCaptionIsTitle">DataTable的column Caption是否作为标题</param>
        /// <returns></returns>
        public bool DataTableExportExcelNoSaveFileDialog(string fileName, DataTable dataTable, bool columnsCaptionIsTitle)
        {
            return DataTableExportExcelNoSaveFileDialog(fileName, dataTable, columnsCaptionIsTitle, true);
        }
               
        /// <summary>
        /// 传入DataTable导出到Excel 不弹出保存对话框
        /// <para>|--DataTable.TableName默认为Sheet名，若无TableName默认使用Sheet1</para>  
        /// </summary>
        /// <param name="filePathName">
        /// 导出的路径 .xls使用 03版excel  .xlsx使用高版本excel
        /// </param>
        /// <param name="dataTable">数据</param>
        /// <param name="columnsCaptionIsTitle">DataTable的column Caption是否作为标题</param>
        /// <param name="isTitleShowDouble">是否同时存在 column Name 和   column Caption</param>
        /// <returns></returns>
        public bool DataTableExportExcelNoSaveFileDialog(string filePathName, DataTable dataTable, bool columnsCaptionIsTitle, bool isTitleShowDouble)
        {

            bool result = false;
            //用于写入文件
            MemoryStream ms = new MemoryStream();
            try
            {

                //FileStream fileStream = new FileStream(fileDialog.FileName, FileMode.Open, FileAccess.Read);
                //HSSFWorkbook workbook = new HSSFWorkbook(fileStream);
                IWorkbook workbook = null;
                if (filePathName.Substring(filePathName.LastIndexOf('.'), filePathName.Length - filePathName.LastIndexOf('.')).Equals(".xls"))
                {
                    workbook = new HSSFWorkbook();
                }
                else
                {
                    workbook = new XSSFWorkbook();
                }

                if (dataTable != null)
                {

                    //创建sheet
                    string sheetName = dataTable.TableName.Trim();
                    if (sheetName.Length <= 0)
                    {
                        sheetName = "Sheet1";
                    }
                    else
                    {
                        if (sheetName.Length > 30)
                        {
                            sheetName = sheetName.Substring(0, 30);
                        }
                    }

                    CreateSheet(workbook, sheetName, dataTable, columnsCaptionIsTitle, isTitleShowDouble);


                    //写入文件
                    workbook.Write(ms);
                    workbook = null;
                    using (FileStream fileStream = new FileStream(filePathName, FileMode.Create, FileAccess.Write))
                    {
                        byte[] data = ms.ToArray();
                        fileStream.Write(data, 0, data.Length);
                        fileStream.Flush();
                    }
                    MessageBox.Show("文件已导出到：" + filePathName, "提示", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    result = true;
                }
                else
                {
                    MessageBox.Show("没有要导出的数据", "提示", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    result = false;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Exception:" + ex.Message, "错误", MessageBoxButtons.OK, MessageBoxIcon.Error);
                result = false;
            }
            finally
            {
                ms.Close();
                ms.Dispose();
            }
            return result;
        }

        /// <summary>
        /// 传入DataSet导出多个sheet的Excel 不弹出对话框
        /// <para>|--DataTable.TableName默认为Sheet名，若无TableName默认使用Sheet+DataTable在DataSet中的索引</para>
        /// </summary>
        /// <param name="filePathName">导出的路径文件名， .xls使用 03版excel  .xlsx使用高版本excel</param>
        /// <param name="dataSet">导出的数据集</param>
        /// <returns></returns>
        public bool DataSetExportExcelNoSaveFileDialog(string filePathName, DataSet dataSet)
        {
            return DataSetExportExcelNoSaveFileDialog(filePathName, dataSet, true, true);
        }

        /// <summary>
        /// 传入DataSet导出多个sheet的Excel 不弹出对话框
        /// <para>|--DataTable.TableName默认为Sheet名，若无TableName默认使用Sheet+DataTable在DataSet中的索引</para>
        /// </summary>
        /// <param name="filePathName">导出的路径文件名， .xls使用 03版excel  .xlsx使用高版本excel</param>
        /// <param name="dataSet">导出的数据集</param>
        /// <param name="columnsCaptionIsTitle">DataTable的column Caption是否作为标题</param>
        /// <returns></returns>
        public bool DataSetExportExcelNoSaveFileDialog(string filePathName, DataSet dataSet,bool columnsCaptionIsTitle)
        {
            return DataSetExportExcelNoSaveFileDialog(filePathName, dataSet, columnsCaptionIsTitle, true);
        }

        /// <summary>
        /// 传入DataSet导出多个sheet的Excel 不弹出对话框
        /// <para>|--DataTable.TableName默认为Sheet名，若无TableName默认使用Sheet+DataTable在DataSet中的索引</para>
        /// </summary>
        /// <param name="filePathName">导出的路径文件名， .xls使用 03版excel  .xlsx使用高版本excel</param>
        /// <param name="dataSet">导出的数据集</param>
        /// <param name="columnsCaptionIsTitle">DataTable的column Caption是否作为标题</param>
        /// <param name="isTitleShowDouble">是否同时存在 column Name 和   column Caption</param>
        /// <returns></returns>
        public bool DataSetExportExcelNoSaveFileDialog(string filePathName, DataSet dataSet, bool columnsCaptionIsTitle, bool isTitleShowDouble)
        {

            bool result = false;
            //用于写入文件
            MemoryStream ms = new MemoryStream();
            try
            {
                IWorkbook workbook = null;
                if (filePathName.Substring(filePathName.LastIndexOf('.'), filePathName.Length - filePathName.LastIndexOf('.')).Equals(".xls"))
                {
                    workbook = new HSSFWorkbook();
                }
                else
                {
                    workbook = new XSSFWorkbook();
                }

                if (dataSet != null)
                {
                    for (int dataTableIndex = 0; dataTableIndex < dataSet.Tables.Count; dataTableIndex++)
                    {
                        DataTable dataTable = dataSet.Tables[dataTableIndex];
                        if (dataTable != null)
                        {

                            //创建sheet
                            string sheetName = dataTable.TableName.Trim();
                            if (sheetName.Length <= 0)
                            {
                                sheetName = "Sheet" + dataTableIndex;
                            }
                            else
                            {
                                if (sheetName.Length > 30)
                                {
                                    sheetName = sheetName.Substring(0, 30);
                                }
                            }

                            CreateSheet(workbook, sheetName, dataTable, columnsCaptionIsTitle, isTitleShowDouble);

                        }
                        else
                        {
                            MessageBox.Show("没有要导出的数据!", "提示", MessageBoxButtons.OK, MessageBoxIcon.Information);
                            result = false;
                        }

                    }//from for(dataSet)
                    //写入文件
                    workbook.Write(ms);
                    workbook = null;
                    using (FileStream fileStream = new FileStream(filePathName, FileMode.Create, FileAccess.Write))
                    {
                        byte[] data = ms.ToArray();
                        fileStream.Write(data, 0, data.Length);
                        fileStream.Flush();
                    }
                    MessageBox.Show("文件已导出到：" + filePathName, "提示", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    result = true;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Exception:" + ex.Message, "错误", MessageBoxButtons.OK, MessageBoxIcon.Error);
                result = false;
            }
            finally
            {
                ms.Close();
                ms.Dispose();
            }
            return result;
        }

        /// <summary>
        /// 将excel中的数据导入到DataTable中
        /// </summary>
        /// <param name="sheetName">excel 工作薄sheet的名称,为空则默认读取第一个sheet</param>
        /// <param name="isFirstRowColumn">第一行是否是DataTable的列名</param>
        /// <returns>返回的DataTable</returns>
        public DataTable ExcelImportDataTable(string sheetName, bool isFirstRowColumn)
        {
            //资源文件合法的后缀名
            string[] aryExtList = new string[] { ".xls", ".xlsx" };
            //选择文件资源路径
            OpenFileDialog fileDialog = new OpenFileDialog();
            //资源管理器设置
            fileDialog.Multiselect = false;//选择多个文件
            fileDialog.Title = "请选择文件";
            fileDialog.Filter = "Excel文件(*.xls)|*.xls|Excel文件(*.xlsx)|*.xlsx|所有文件(*.*)|*.*";
            //打开时指定默认路径
            fileDialog.InitialDirectory = Environment.GetFolderPath(Environment.SpecialFolder.Desktop);
            //如果用户点击确定
            string fileName = string.Empty;
            if (fileDialog.ShowDialog() == DialogResult.OK)
            {
                //获取指定路径文件的后缀名
                string ext = Path.GetExtension(fileDialog.FileName);
                //检测选取的文件名是否合法
                if (Array.IndexOf(aryExtList, ext) == -1)
                {
                    MessageBox.Show("请选择正确的Excel文件！");
                    return null;
                }
                else
                {
                    fileName = fileDialog.FileName;
                }

            }
            else
            {
                return null;
            }
            return ExcelImportDataTable(fileName, sheetName, isFirstRowColumn);
        }

        /// <summary>
        /// 将excel中的数据导入到DataTable中
        /// </summary>
        /// <param name="fileName">excel文件路径</param>
        /// <param name="sheetName">excel 工作薄sheet的名称,为空则默认读取第一个sheet</param>
        /// <param name="isFirstRowColumn">第一行是否是DataTable的列名</param>
        /// <returns>返回的DataTable</returns>
        public DataTable ExcelImportDataTable(string fileName, string sheetName, bool isFirstRowColumn)
        {
            FileStream fs = null;
            ISheet sheet = null;
            IWorkbook workbook = null;
            DataTable data = new DataTable();
            int startRow = 0;
            try
            {
                fs = new FileStream(fileName, FileMode.Open, FileAccess.Read);
                if (fileName.IndexOf(".xlsx") > 0)
                {   // 2007版本
                    workbook = new XSSFWorkbook(fs);
                }
                else if (fileName.IndexOf(".xls") > 0)
                {
                    // 2003版本
                    workbook = new HSSFWorkbook(fs);
                }

                if (!string.IsNullOrEmpty(sheetName))
                {
                    sheet = workbook.GetSheet(sheetName);
                    if (sheet == null)
                    {
                        //如果没有找到指定的sheetName对应的sheet，则尝试获取第一个sheet
                        sheet = workbook.GetSheetAt(0);
                    }
                }
                else
                {
                    sheet = workbook.GetSheetAt(0);
                }
                if (sheet != null)
                {
                    IRow firstRow = sheet.GetRow(0);
                    data.TableName = sheet.SheetName.Trim();
                    //第一行最后一个cell的编号 即总的列数
                    int cellCount = firstRow.LastCellNum;



                    //第一行是否是DataTable的列名
                    if (isFirstRowColumn)
                    {
                        //给dt添加列
                        for (int i = firstRow.FirstCellNum; i < cellCount; ++i)
                        {
                            ICell cell = firstRow.GetCell(i);
                            if (cell != null)
                            {
                                string cellValue = cell.StringCellValue;
                                if (cellValue != null)
                                {
                                    DataColumn column = new DataColumn(cellValue);
                                    data.Columns.Add(column);
                                }
                            }
                        }
                        startRow = sheet.FirstRowNum + 1;
                    }
                    else
                    {
                        for (int i = firstRow.FirstCellNum; i < cellCount; ++i)
                        {
                            DataColumn column = new DataColumn("col_" + i);
                            data.Columns.Add(column);
                        }
                        startRow = sheet.FirstRowNum;
                    }

                    //最后一行的标号
                    int rowCount = sheet.LastRowNum;
                    for (int i = startRow; i <= rowCount; ++i)
                    {
                        IRow row = sheet.GetRow(i);
                        if (row == null) continue; //没有数据的行默认是null　　　　　　　

                        DataRow dataRow = data.NewRow();
                        for (int j = row.FirstCellNum; j < cellCount; ++j)
                        {
                            if (row.GetCell(j) != null) //同理，没有数据的单元格都默认是null
                                dataRow[j] = row.GetCell(j).ToString();
                        }
                        data.Rows.Add(dataRow);
                    }
                }
                return data;
            }
            catch (Exception ex)
            {
                MessageBox.Show("Exception:" + ex.Message, "错误", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return null;
            }
        }
        
        /// <summary>
        /// 将excel中的数据导入到DataSet中
        /// </summary>
        /// <param name="sheetName">excel 工作薄sheet的名称,null读取所有sheet</param>
        /// <param name="isFirstRowColumn">第一行是否是DataTable的列名</param>
        /// <returns>返回的DataSet</returns>
        public DataSet ExcelImportDataSet(string sheetName, bool isFirstRowColumn)
        {
            //资源文件合法的后缀名
            string[] aryExtList = new string[] { ".xls", ".xlsx" };
            //选择文件资源路径
            OpenFileDialog fileDialog = new OpenFileDialog();
            //资源管理器设置
            fileDialog.Multiselect = false;//选择多个文件
            fileDialog.Title = "请选择文件";
            fileDialog.Filter = "Excel文件(*.xls)|*.xls|Excel文件(*.xlsx)|*.xlsx|所有文件(*.*)|*.*";
            //打开时指定默认路径
            fileDialog.InitialDirectory = Environment.GetFolderPath(Environment.SpecialFolder.Desktop);
            //如果用户点击确定
            string fileName = string.Empty;
            if (fileDialog.ShowDialog() == DialogResult.OK)
            {
                //获取指定路径文件的后缀名
                string ext = Path.GetExtension(fileDialog.FileName);
                //检测选取的文件名是否合法
                if (Array.IndexOf(aryExtList, ext) == -1)
                {
                    MessageBox.Show("请选择正确的Excel文件！");
                    return null;
                }
                else
                {
                    fileName = fileDialog.FileName;
                }

            }
            else
            {
                return null;
            }
            return ExcelImportDataSet(fileName, sheetName, isFirstRowColumn);
        }

        /// <summary>
        /// 将excel中的数据导入到DataSet中
        /// </summary>
        /// <param name="fileName">excel 文件路径</param>
        /// <param name="sheetName">excel 工作薄sheet的名称,null读取所有sheet</param>
        /// <param name="isFirstRowColumn">第一行是否是DataTable的列名</param>
        /// <returns>返回的DataSet</returns>
        public DataSet ExcelImportDataSet(string fileName, string sheetName, bool isFirstRowColumn)
        {
            DataSet dsRes = new DataSet();//
            FileStream fs = null;
            IWorkbook workbook = null;
            try
            {
                fs = new FileStream(fileName, FileMode.Open, FileAccess.Read);
                if (fileName.IndexOf(".xlsx") > 0)
                {   // 2007版本
                    workbook = new XSSFWorkbook(fs);
                }
                else if (fileName.IndexOf(".xls") > 0)
                {
                    // 2003版本
                    workbook = new HSSFWorkbook(fs);
                }

                //是否只读取第一个sheet
                bool isOnlyReadSheet1 = false;
                for (int iSheetNum = 0; iSheetNum < workbook.NumberOfSheets; iSheetNum++)
                {
                    ISheet sheet = null;
                    int startRow = 0;

                    DataTable dt = new DataTable();
                    if (!string.IsNullOrEmpty(sheetName))
                    {
                        sheet = workbook.GetSheet(sheetName);
                        if (sheet == null)
                        {
                            //如果没有找到指定的sheetName对应的sheet，则尝试获取第一个sheet
                            sheet = workbook.GetSheetAt(0);
                        }
                        isOnlyReadSheet1 = true;
                    }
                    else
                    {
                        sheet = workbook.GetSheetAt(iSheetNum);
                    }
                    if (sheet != null)
                    {
                        IRow firstRow = sheet.GetRow(0);
                        dt.TableName = sheet.SheetName.Trim();
                        //第一行最后一个cell的编号 即总的列数
                        int cellCount = firstRow.LastCellNum;

                        //第一行是否是DataTable的列名
                        if (isFirstRowColumn)
                        {
                            //给dt添加列
                            for (int i = firstRow.FirstCellNum; i < cellCount; ++i)
                            {
                                ICell cell = firstRow.GetCell(i);
                                if (cell != null)
                                {
                                    string cellValue = cell.StringCellValue;
                                    if (cellValue != null)
                                    {
                                        DataColumn column = new DataColumn(cellValue);
                                        dt.Columns.Add(column);
                                    }
                                }
                            }
                            startRow = sheet.FirstRowNum + 1;
                        }
                        else
                        {
                            for (int i = firstRow.FirstCellNum; i < cellCount; ++i)
                            {
                                DataColumn column = new DataColumn("col_" + i);
                                dt.Columns.Add(column);
                            }
                            startRow = sheet.FirstRowNum;
                        }

                        //最后一行的标号
                        int rowCount = sheet.LastRowNum;
                        for (int i = startRow; i <= rowCount; ++i)
                        {
                            IRow row = sheet.GetRow(i);
                            if (row == null) continue; //没有数据的行默认是null　　　　　　　

                            DataRow dataRow = dt.NewRow();
                            for (int j = row.FirstCellNum; j < cellCount; ++j)
                            {
                                if (row.GetCell(j) != null) //同理，没有数据的单元格都默认是null
                                    dataRow[j] = row.GetCell(j).ToString();
                            }
                            dt.Rows.Add(dataRow);
                        }
                        dt.TableName = sheet.SheetName;
                        dsRes.Tables.Add(dt.Copy());
                        if (isOnlyReadSheet1)
                        {
                            break;
                        }
                    }
                }
                return dsRes;
            }
            catch (Exception ex)
            {
                MessageBox.Show("Exception:" + ex.Message, "错误", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return null;
            }
        }

        /// <summary>
        /// 创建Sheet并填充数据
        /// </summary>
        /// <param name="workbook">工作表名称</param>
        /// <param name="sheetName">Sheet名称</param>
        /// <param name="dtData">填充的数据</param>
        /// <param name="columnsCaptionIsTitle">DataTable的column Caption是否作为标题</param>
        /// <param name="isTitleShowDouble">是否同时存在 column Name 和   column Caption,前提是都不为空</param>
        private void CreateSheet(IWorkbook workbook,string sheetName, DataTable dtData,bool columnsCaptionIsTitle,bool isTitleShowDouble)
        {
            ISheet sheet = workbook.CreateSheet(sheetName);

            //添加表头
            IRow row = sheet.CreateRow(0);
            ICellStyle cellStyleTitle = workbook.CreateCellStyle();
            cellStyleTitle.WrapText = true;//设置换行
            int cellIndex = 0;
            foreach (DataColumn dc in dtData.Columns)
            {
                //创建列
                ICell cell = row.CreateCell(cellIndex);
                cell.SetCellType(CellType.String);
                if (isTitleShowDouble == false)
                {
                    if (columnsCaptionIsTitle)
                    {
                        if (!string.IsNullOrEmpty(dc.Caption))
                        {
                            cell.SetCellValue(dc.Caption);
                        }
                        else
                        {
                            cell.SetCellValue(dc.ColumnName);
                        }
                    }
                    else
                    {
                        cell.SetCellValue(dc.ColumnName);
                    }
                }
                else
                {
                    if (columnsCaptionIsTitle)
                    {
                        if (!string.IsNullOrEmpty(dc.Caption) && !string.IsNullOrEmpty(dc.ColumnName))
                        {
                            cell.SetCellValue(dc.Caption + "\n" + dc.ColumnName);
                        }
                        else if (!string.IsNullOrEmpty(dc.Caption) && string.IsNullOrEmpty(dc.ColumnName))
                        {
                            cell.SetCellValue(dc.Caption);
                        }
                        else if (string.IsNullOrEmpty(dc.Caption) && !string.IsNullOrEmpty(dc.ColumnName))
                        {
                            cell.SetCellValue(dc.ColumnName);
                        }
                        else
                        {
                            cell.SetCellValue("Cell " + cellIndex + " No Tite");
                        }
                    }
                    else
                    {
                        cell.SetCellValue(dc.ColumnName);
                    }
                }
                cell.CellStyle = cellStyleTitle;
                cellIndex++;
            }

            //添加数据
            for (int dataRowNum = 0; dataRowNum < dtData.Rows.Count; dataRowNum++)
            {
                cellIndex = 0;
                row = sheet.CreateRow(dataRowNum + 1);
                foreach (DataColumn dc in dtData.Columns)
                {
                    ICell cell = row.CreateCell(cellIndex);
                    cell.SetCellType(CellType.String);
                    cell.SetCellValue(Convert.ToString(dtData.Rows[dataRowNum][cellIndex]));
                    cellIndex++;
                }
            }

            //设置列宽度自适应
            for (int i = 0; i < dtData.Rows.Count; i++)
            {
                sheet.AutoSizeColumn(i);
                //sheet.SetColumnWidth(i, 10 * 256);
            }
        }
    }
}
