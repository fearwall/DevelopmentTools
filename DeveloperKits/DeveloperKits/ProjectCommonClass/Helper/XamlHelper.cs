﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Reflection;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Markup;
using System.Xml;

namespace DeveloperKits.ProjectCommonClass.Helper
{
    /// <summary>
    /// Xaml代码生成助手类
    /// </summary>
    public static class XamlHelper
    {
        /// <summary>
        /// 获取元素的Xaml代码
        /// </summary>
        /// <param name="oEle">指定元素的类型</param>
        /// <param name="xmlEncoding">输出的编码类型</param>
        /// <returns></returns>
        public static string GetElementXamlCodeToXml(object oEle, Encoding xmlEncoding)
        {
            try
            {
                XmlWriterSettings settings = new XmlWriterSettings();
                settings.Indent = true;
                settings.Encoding = xmlEncoding;
                //settings.NewLineOnAttributes = false;
                StringBuilder sb = new StringBuilder();
                XmlWriter writer = XmlWriter.Create(sb, settings);
                XamlWriter.Save(oEle, writer);
                return sb.ToString();
            }
            catch (Exception ex)
            {
                throw new Exception("获取元素Xaml出错：" + ex.Message);
            }
        }

        /// <summary>
        /// 获取元素的Xaml代码
        /// </summary>
        /// <param name="oEle">指定元素的类型</param>
        /// <param name="xmlEncoding">输出的编码类型</param>
        /// <returns></returns>
        public static string GetElementXamlCodeToText(object oEle, Encoding xmlEncoding)
        {
            try
            {
                string strRes = GetElementXamlCodeToXml(oEle, xmlEncoding);
                strRes = XmlHelper.RemoveDeclaration(strRes);
                return strRes;
            }
            catch (Exception ex)
            {
                throw new Exception("获取元素无Xml格式声明出错：" + ex.Message);
            }
        }

        /// <summary>
        /// 将Xaml字符串转换为元素
        /// </summary>
        /// <typeparam name="T">实例化的元素类型 可以是 Windoe,UIElement,DependencyObject 等</typeparam>
        /// <param name="strXaml">元素的Xaml字符串</param>
        /// <returns></returns>
        public static T XamlStringToElement<T>(string strXaml)
        {
            try
            {
                T element;
                using (MemoryStream ms = new MemoryStream(new UnicodeEncoding().GetBytes(strXaml)))
                {
                    element = (T)XamlReader.Load(ms);
                }
                return element;
            }
            catch(Exception ex)
            {
                throw new Exception("Xaml字符串转换为元素出错：" + ex.Message);
            }
        }

        /// <summary>
        /// 查找逻辑节点
        /// </summary>
        /// <param name="dpSource">查找的节点源</param>
        /// <param name="strFindeNodeName">要查找的对象名称</param>
        /// <returns></returns>
        public static DependencyObject FindelLogicalNode(DependencyObject dpSource, string strFindeNodeName)
        {
            try
            {
                return (DependencyObject)System.Windows.LogicalTreeHelper.FindLogicalNode(dpSource, strFindeNodeName);
            }
            catch (Exception ex)
            {
                throw new Exception("查找逻辑节点出错：" + ex.Message);
            }
        }
    }
}
