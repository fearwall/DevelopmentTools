﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Windows.Forms;

namespace DeveloperKits.ProjectCommonClass.Helper
{
    /// <summary>
    /// 文件路径助手类
    /// </summary>
    public class FilePathHelper
    {
        #region 文件夹操作
        /// <summary>
        /// 文件夹路径选择（默认选择桌面路径）
        /// </summary>
        /// <returns></returns>
        public static string FolderPathSelect()
        {
            return FolderPathSelect(false);
        }

        /// <summary>
        /// 文件夹路径选择
        /// </summary>
        /// <param name="isDesktopDirectory">是否默认选择桌面路径</param>
        /// <returns>未选择返回空字符串，选择返回字符串路径</returns>
        public static string FolderPathSelect(bool isDesktopDirectory)
        {
            return FolderPathSelect(null, isDesktopDirectory);
        }

        /// <summary>
        /// 文件夹路径选择
        /// </summary>
        /// <param name="strSelectPath">指定选中的路径</param>
        /// <param name="isDesktopDirectory">指定的路径为空，是否默认选择桌面路径</param>
        /// <returns>未选择返回空字符串，选择返回字符串路径</returns>
        public static string FolderPathSelect(string strSelectPath, bool isDesktopDirectory)
        {
            string strRes = string.Empty;
            //选择文件夹的路径
            System.Windows.Forms.FolderBrowserDialog fbd = new System.Windows.Forms.FolderBrowserDialog();
            //若未指定路径则默认选择桌面路径
            if (!string.IsNullOrEmpty(strSelectPath))
            {
                fbd.SelectedPath = strSelectPath;
            }
            else if (isDesktopDirectory)
            {
                fbd.SelectedPath = Environment.GetFolderPath(Environment.SpecialFolder.DesktopDirectory);
            }

            DialogResult dr = fbd.ShowDialog();
            if (dr == DialogResult.OK || dr == DialogResult.Yes)
            {
                strRes = fbd.SelectedPath;
            }
            return strRes;
        }
        #endregion

        #region 文件路径相关
        /// <summary>
        /// 文件路径选择
        /// </summary>
        /// <returns>未选择返回空字符串，选择返回字符串路径</returns>
        public static string FilePathSelect()
        {
            return FilePathSelect(string.Empty, string.Empty);
        }

        /// <summary>
        /// 文件路径选择 
        /// </summary>
        /// <param name="strFilter">指定文件类型 窗体设计文件|*.designer.cs</param>
        /// <returns>未选择返回空字符串，选择返回字符串路径</returns>
        public static string FilePathSelect(string strFilter)
        {
            return FilePathSelect(string.Empty, strFilter);
        }


        /// <summary>
        /// 文件路径选择
        /// </summary>
        /// <param name="strInitialDirectory">初始目录</param>
        /// <param name="strFilter">指定文件类型 窗体设计文件|*.designer.cs</param>
        /// <returns>未选择返回空字符串，选择返回字符串路径</returns>
        public static string FilePathSelect(string strInitialDirectory, string strFilter)
        {
            string strRes = string.Empty;
            //选择读取文件路径
            string path = string.Empty;
            OpenFileDialog ofd = new OpenFileDialog();    //打开文件窗口  
            if (!string.IsNullOrEmpty(strInitialDirectory))
            {
                ofd.InitialDirectory = strInitialDirectory;
            }
            if (string.IsNullOrEmpty(strFilter))
            {
                ofd.Filter = "所有文件|*.*";     //你要显示/查看的文件格式
            }
            else
            {
                ofd.Filter = strFilter;
            }

            DialogResult dr = ofd.ShowDialog();
            if (dr == DialogResult.OK || dr == DialogResult.Yes)
            {
                strRes = ofd.FileName;
            }

            return strRes;
        }
        #endregion

        #region 文件保存操作相关
        /// <summary>
        /// 保存文件对话框
        /// </summary>
        /// <returns>未选择返回空字符串，选择返回字符串路径</returns>
        public static string SaveFileDialog()
        {
            return SaveFileDialog(null);
        }

        /// <summary>
        /// 保存文件对话框
        /// </summary>
        /// <param name="strDefaultFileName">默认文件名</param>
        /// <returns>未选择返回空字符串，选择返回字符串路径</returns>
        public static string SaveFileDialog(string strDefaultFileName)
        {
            return SaveFileDialog(strDefaultFileName, null);
        }

        /// <summary>
        /// 保存文件对话框
        /// </summary>
        /// <param name="strDefaultFileName">默认文件名</param>
        /// <param name="strFilter">文件筛选器文件类型</param>
        /// <returns>未选择返回空字符串，选择返回字符串路径</returns>
        public static string SaveFileDialog(string strDefaultFileName, string strFilter)
        {
            return SaveFileDialog(strDefaultFileName, strFilter, null);
        }

        /// <summary>
        /// 保存文件对话框
        /// </summary>
        /// <param name="strDefaultFileName">默认文件名</param>
        /// <param name="strFilter">文件筛选器文件类型</param>
        /// <param name="strInitialDirectory">默认打开路径</param>
        /// <returns>未选择返回空字符串，选择返回字符串路径</returns>
        public static string SaveFileDialog(string strDefaultFileName, string strFilter, string strInitialDirectory)
        {
            string strRes = string.Empty;
            SaveFileDialog sfd = new SaveFileDialog();
            if (string.IsNullOrWhiteSpace(strFilter))
            {
                //设置文件类型 
                sfd.Filter = "所有文件|*.*";
            }
            else
            {
                sfd.Filter = strFilter;
            }
            if (!string.IsNullOrEmpty(strInitialDirectory))
            {
                sfd.InitialDirectory = strInitialDirectory;
            }
            if (!string.IsNullOrWhiteSpace(strDefaultFileName))
            {
                sfd.FileName = strDefaultFileName;
            }
            DialogResult dr = sfd.ShowDialog();
            if (dr == DialogResult.OK || dr == DialogResult.Yes)
            {
                strRes = sfd.FileName;
            }
            return strRes;

        }
        #endregion

        #region 默认参数值获取
        /// <summary>
        /// 获取桌面路径
        /// </summary>
        /// <returns></returns>
        /// 
        public static string GetDesktopDirectory()
        {
            return Environment.GetFolderPath(Environment.SpecialFolder.DesktopDirectory);
        }


        /// <summary>
        /// 获取当前应用文件夹路径
        /// </summary>
        /// <returns></returns>
        public static string GetCurrentAppDirectory()
        {
            return System.AppDomain.CurrentDomain.SetupInformation.ApplicationBase;
        }

        /// <summary>
        /// 获取当前引用的根路径（若为开发模式，返回项目的根路径；若为安装模式，返回安装目录的根路径）
        /// </summary>
        /// <returns></returns>
        public static string GetCurrentAppRootPath()
        {
            string strRes = GetCurrentAppDirectory();
            if (strRes.ToLower().Contains(@"bin\debug") || strRes.ToLower().Contains(@"bin\release"))
            {
                //调试模式，返回调试模式下，项目的根路径
                strRes = Path.GetFullPath("../..");
            }
            return strRes;
        }

        /// <summary>
        /// 获取默认文件筛选器文件类型的字符串 
        /// </summary>
        /// <returns>所有文件|*.*</returns>
        public static string GetDefaultFilterStr()
        {
            return "所有文件|*.*";
        }
        #endregion

        #region 获取目录文件路径操作
        /// <summary>
        /// 获取目录的文件路径（不对指定需要获取的目录的文件扩展名过滤）
        /// </summary>
        /// <param name="strDirectoryPath"></param>
        /// <param name="isRecursion"></param>
        /// <param name="listFilePaths"></param>
        public static void GetDirectoryFilePaths(string strDirectoryPath, bool isRecursion, ref List<string> listFilePaths)
        {
            GetDirectoryFilePaths(strDirectoryPath, "", isRecursion, ref listFilePaths);
        }

        /// <summary>
        /// 获取目录的文件路径（不对指定需要获取的目录进行递归和文件扩展名过滤）
        /// </summary>
        /// <param name="strDirectoryPath"></param>
        /// <param name="listFilePaths"></param>
        public static void GetDirectoryFilePaths(string strDirectoryPath,ref List<string> listFilePaths)
        {
            GetDirectoryFilePaths(strDirectoryPath, "", false, ref listFilePaths);
        }

        /// <summary>
        /// 获取目录的文件路径（不对指定需要获取的目录进行递归）
        /// </summary>
        /// <param name="strDirectoryPath">指定需要获取的目录</param>
        /// <param name="strExtFilter">文件扩展名过滤</param>
        /// <param name="listFilePaths">引用值返回结果</param>
        public static void GetDirectoryFilePaths(string strDirectoryPath, string strExtFilter, ref List<string> listFilePaths)
        {
            GetDirectoryFilePaths(strDirectoryPath, strExtFilter, false, ref  listFilePaths);
        }

        /// <summary>
        /// 获取目录的文件路径
        /// </summary>
        /// <param name="strDirectoryPath">指定需要获取的目录</param>
        /// <param name="strExtFilter">文件扩展名过滤</param>
        /// <param name="isRecursion">是否需要递归查找执行目录下的文件</param>
        /// <param name="listFilePaths">引用值返回结果</param>
        public static void GetDirectoryFilePaths(string strDirectoryPath,string strExtFilter, bool isRecursion, ref List<string> listFilePaths)
        {
            try
            {
                //Thread.Sleep(50);
                if (isRecursion)
                {
                    string[] subPaths = System.IO.Directory.GetDirectories(strDirectoryPath);//得到所有子目录
                    foreach (string path in subPaths)
                    {
                        GetDirectoryFilePaths(path, strExtFilter, isRecursion, ref listFilePaths);//对每一个字目录做与根目录相同的操作：即找到子目录并将当前目录的文件名存入List  
                    }
                }


                string[] files = System.IO.Directory.GetFiles(strDirectoryPath);
                foreach (string file in files)
                {
                    //判断是否过滤文件扩展名,为空则不过滤
                    if (!string.IsNullOrWhiteSpace(strExtFilter))
                    {
                        //文件扩展名
                        string fileExtension = System.IO.Path.GetExtension(file);
                        if (fileExtension.ToLower().Equals(strExtFilter.ToLower()))
                        {
                            listFilePaths.Add(file);
                        }
                    }
                    else
                    {

                        listFilePaths.Add(file);
                    }
                }
            }catch(Exception ex)
            {
                throw new Exception("获取指定目录的文件路径出错！" + ex.ToString()); 
            }
        }
        #endregion
    }
}
