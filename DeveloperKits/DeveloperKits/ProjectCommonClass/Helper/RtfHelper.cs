﻿using System;
using System.IO;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;

namespace DeveloperKits.ProjectCommonClass.Helper
{
    /// <summary>
    /// RichTextBox的RTF格式化处理
    /// </summary>
    public static class RtfHelper
    {
        /// <summary>
        /// 将字符串转换为RTF格式
        /// </summary>
        /// <param name="richTextBox"></param>
        /// <returns></returns>
        public static string StringToRTF(this RichTextBox richTextBox)
        {
            string rtf = string.Empty;
            TextRange textRange = new TextRange(richTextBox.Document.ContentStart, richTextBox.Document.ContentEnd);
            using (MemoryStream ms = new MemoryStream())
            {
                textRange.Save(ms, DataFormats.Rtf);
                ms.Seek(0, SeekOrigin.Begin);
                StreamReader sr = new StreamReader(ms);
                rtf = sr.ReadToEnd();
                rtf = RtfUndelineMatch(rtf);
            }
            return rtf;
        }

        /// <summary>
        /// 加载RTF到RichTextBox中，并返回RTF转换后的字符串
        /// </summary>
        /// <param name="richTextBox"></param>
        /// <param name="rtf"></param>
        public static string LoadFromRTF(this RichTextBox richTextBox, string rtf)
        {
            string strRes = string.Empty;
            if (string.IsNullOrEmpty(rtf))
            {
                throw new ArgumentNullException();
            }
            TextRange textRange = new TextRange(richTextBox.Document.ContentStart, richTextBox.Document.ContentEnd);
            using (MemoryStream ms = new MemoryStream())
            {
                using (StreamWriter sw = new StreamWriter(ms))
                {
                    sw.Write(rtf);
                    sw.Flush();
                    ms.Seek(0, SeekOrigin.Begin);
                    textRange.Load(ms, DataFormats.Rtf);
                    strRes = textRange.Text;
                }
            }
            return strRes;
        }

        /// <summary>
        /// 匹配Rtf中下划线 如：将其中{\ul \'8e\'a9\}，替换为{\ul \'8e\'a9\ul0} 
        /// (因为FlowDocument转换 带有下划线的文字时丢失下划线结束符ul0)
        /// </summary>
        /// <param name="rtf"></param>
        public static string RtfUndelineMatch(string rtf)
        {
            if (string.IsNullOrEmpty(rtf)) return string.Empty; ;
            string pattern = @"\\ul\s+\S{0,8}\}";//  \ul\s+\S{0,8}\}
            return System.Text.RegularExpressions.Regex.Replace(rtf, pattern, (m) =>
            {
                return m.Value.Insert(m.Value.LastIndexOf('}'), @"\ul0");
            });
        }
    }
}
