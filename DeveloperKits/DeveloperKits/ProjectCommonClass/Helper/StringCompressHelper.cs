﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DeveloperKits.ProjectCommonClass.Helper
{
    /// <summary>
    /// 字符串解压缩
    /// </summary>
    public class StringCompressHelper
    {
        /// <summary>
        /// 将byte[] 解压为字符串
        /// </summary>
        /// <param name="bys"></param>
        /// <returns></returns>
        public static string Decompress(byte[] bys)
        {
            return Decompress(Convert.ToBase64String(bys));
        }

        /// <summary>
        /// 解压字符串 字符串不会超过3M
        /// </summary>
        /// <param name="strSource">解压的字符串源</param>
        /// <returns></returns>
        public static string Decompress(string strSource)
        {
            return Decompress(strSource, (3 * 1024 * 1024 + 256));//字符串不会超过3M
        }

        /// <summary>
        /// 解压字符串
        /// </summary>
        /// <param name="strSource">解压的字符串源</param>
        /// <param name="length">最多要读取的字节数</param>
        /// <returns></returns>
        public static string Decompress(string strSource, int length)
        {
            byte[] buffer = Convert.FromBase64String(strSource);

            System.IO.MemoryStream ms = new System.IO.MemoryStream();
            ms.Write(buffer, 0, buffer.Length);
            ms.Position = 0;
            System.IO.Compression.DeflateStream stream = new System.IO.Compression.DeflateStream(ms, System.IO.Compression.CompressionMode.Decompress);
            stream.Flush();

            int nSize = length;
            byte[] decompressBuffer = new byte[nSize];
            int nSizeIncept = stream.Read(decompressBuffer, 0, nSize);
            stream.Close();

            return System.Text.Encoding.Unicode.GetString(decompressBuffer, 0, nSizeIncept);//转换为普通的字符串
        }

        /// <summary>
        /// 压缩字符串为string 
        /// </summary>
        /// <param name="strSource"></param>
        /// <returns></returns>
        public static string CompressToBase64String(string strSource)
        {
            //将压缩后的byte[]转换为Base64String
            return Convert.ToBase64String(Compress(strSource));
        }

        /// <summary>
        /// 压缩字符串为Byte
        /// </summary>
        /// <param name="strSource">需要被压缩的字符串</param>
        /// <returns></returns>
        public static byte[] Compress(string strSource)
        {
            if (strSource == null)
                throw new System.ArgumentException("字符串为空！");

            System.Text.Encoding encoding = System.Text.Encoding.Unicode;
            byte[] buffer = encoding.GetBytes(strSource);

            System.IO.MemoryStream ms = new System.IO.MemoryStream();
            System.IO.Compression.DeflateStream stream = new System.IO.Compression.DeflateStream(ms, System.IO.Compression.CompressionMode.Compress, true);
            stream.Write(buffer, 0, buffer.Length);
            stream.Close();

            buffer = ms.ToArray();
            ms.Close();

            return buffer;
            // return Convert.ToBase64String(buffer); //将压缩后的byte[]转换为Base64String
        }

    }
}
