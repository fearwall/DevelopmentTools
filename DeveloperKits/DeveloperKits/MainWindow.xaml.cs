﻿using DeveloperKits.ProjectCommonClass.Helper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace DeveloperKits
{
    /// <summary>
    /// MainWindow.xaml 的交互逻辑
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }

        /// <summary>
        /// 通过反射，打开Button Name的窗体
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Button_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                Button btn = sender as Button;
                string windowName = btn.Name;
                Assembly assembly = Assembly.GetExecutingAssembly();
                string strFullName = "DeveloperKits.";
                if (!string.IsNullOrWhiteSpace(btn.Tag.ToString()))
                {
                    strFullName += btn.Tag.ToString() + ".";
                }
                Window window = assembly.CreateInstance(strFullName + windowName) as Window;
                window.WindowStartupLocation = WindowStartupLocation.CenterScreen;
                window.Show();
                //Assembly.
            }
            catch(Exception ex)
            {
                MessageBox.Show("打开窗体失败:\r\n\t" + ex.ToString(), "失败！", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        private void Button_Click_1(object sender, RoutedEventArgs e)
        {
            //for (int i = 0; i < 20; i++)
            //{
            //    //if (i % 2 == 0)
            //    //{
            //    //    Thread t1 = new Thread(new ThreadStart(httpGet));
            //    //    t1.IsBackground = true;
            //    //    t1.Start();
            //    //}
            //    //else
            //    //{
            //    //}

            //    Thread t1 = new Thread(new ThreadStart(httpPost));
            //    t1.IsBackground = true;
            //    t1.Start();
            //}
        }

        private void httpGet()
        {
            int i = 0;
            while (i < 200)
            {
                try
                {
                    HttpRequestsHelper http = new HttpRequestsHelper();
                    Dictionary<string, string> paramDic = new Dictionary<string, string>();
                    paramDic.Add("province", "32");
                    paramDic.Add("sfzh", "230523199502114426");

                    string srcString = http.Get("http://kzp.mof.gov.cn/cjcx/2_a4036a022596182.jsp", http.GetHttpParam(paramDic), Encoding.GetEncoding("gbk"));
                    //string srcString2 = http.Post("http://kzp.mof.gov.cn/cjcx/2_a4036a022596182.jsp", http.GetHttpParam(paramDic), Encoding.GetEncoding("gbk"));

                    if (!string.IsNullOrEmpty(srcString) )
                    {
                        try
                        {
                            FileHelper.WriteLog(FilePathHelper.GetCurrentAppDirectory() + @"\chengji.log",srcString);
                        }
                        catch(Exception ex)
                        {
                            if (ex.ToString().Contains("因此该进程无法访问此文件"))
                            {
                                string s = srcString;
                            }
                        }
                        MessageBox.Show("已获取成绩！");
                    }
                }
                catch (Exception ex)
                {
                    if (ex.ToString().Contains("连接尝试失败") || ex.ToString().Contains("操作超时"))
                    {
                        try
                        {
                            FileHelper.WriteLog(FilePathHelper.GetCurrentAppDirectory() + @"\http.log", "Get超时");
                        }
                        catch (Exception ex2)
                        {
                            if (ex.ToString().Contains("因此该进程无法访问此文件"))
                            {
                            }
                        }

                    }
                    else
                    {
                        try
                        {
                            FileHelper.WriteLog(FilePathHelper.GetCurrentAppDirectory() + @"\http.log", ex.ToString());
                        }
                        catch (Exception ex2)
                        {
                            if (ex.ToString().Contains("因此该进程无法访问此文件"))
                            {
                                string s = ex.ToString();
                            }
                        }
                    }
                    //throw new Exception("语言转换工具转换失败！" + ex.ToString());
                }
                i++;
                //Thread.Sleep(1000);
                if(i== 199)
                {
                    MessageBox.Show("已请求200次！");
                }
            }
        }

        private void httpPost()
        {
            int i = 0;
            while (i < 200)
            {
                try
                {
                    HttpRequestsHelper http = new HttpRequestsHelper();
                    Dictionary<string, string> paramDic = new Dictionary<string, string>();
                    paramDic.Add("province", "32");
                    paramDic.Add("sfzh", "230523199502114426");

                    //string srcString = http.Get("http://kzp.mof.gov.cn/cjcx/2_a4036a022596182.jsp", http.GetHttpParam(paramDic), Encoding.GetEncoding("gbk"));
                    string srcString2 = http.Post("http://kzp.mof.gov.cn/cjcx/2_a4036a022596182.jsp", http.GetHttpParam(paramDic), Encoding.GetEncoding("gbk"));

                    if ( !string.IsNullOrEmpty(srcString2))
                    {
                        try
                        {
                            FileHelper.WriteLog(FilePathHelper.GetCurrentAppDirectory() + @"\chengji.log", srcString2);
                        }
                        catch (Exception ex)
                        {
                            if (ex.ToString().Contains("因此该进程无法访问此文件"))
                            {
                                string s = srcString2;
                            }
                        }
                        MessageBox.Show("已获取成绩！");
                    }
                }
                catch (Exception ex)
                {
                    if (ex.ToString().Contains("连接尝试失败") || ex.ToString().Contains("操作超时"))
                    {
                        try
                        {
                            FileHelper.WriteLog(FilePathHelper.GetCurrentAppDirectory() + @"\http.log", "Post超时");
                        }
                        catch (Exception ex2)
                        {
                            if (ex.ToString().Contains("因此该进程无法访问此文件"))
                            {
                            }
                        }

                    }
                    else
                    {
                        try
                        {
                            FileHelper.WriteLog(FilePathHelper.GetCurrentAppDirectory() + @"\http.log", ex.ToString());
                        }
                        catch (Exception ex2)
                        {
                            if (ex.ToString().Contains("因此该进程无法访问此文件"))
                            {
                                string s = ex.ToString();
                            }
                        }
                    }
                    //throw new Exception("语言转换工具转换失败！" + ex.ToString());
                }
                i++;
                //Thread.Sleep(1000);
                if (i == 199)
                {
                    MessageBox.Show("已请求200次！");
                }
            }
        }
    }
}
