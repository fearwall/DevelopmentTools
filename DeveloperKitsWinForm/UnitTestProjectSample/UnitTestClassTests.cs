﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using DeveloperKitsWinForm;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DeveloperKitsWinForm.Tests
{
    [TestClass()]
    public class UnitTestClassTests
    {
        [TestMethod()]
        public void AdditionTest()
        {
            int iRes = 3;
            int iTest = new UnitTestClass().Addition(1, 2);
            Assert.AreEqual(iRes, iTest);
        }
    }
}