﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;

namespace LanguageTranslate.YouDaoAPI
{
    /// <summary>
    /// 有道API的SDK
    /// </summary>
    public class YouDaoApiSDK
    {

        /// <summary>
        /// 接口请求
        /// </summary>
        /// <param name="q">要翻译的文本</param>
        /// <param name="from">源语言 语言列表 (可设置为auto) 为空默认使用zh-CHS</param>
        /// <param name="to">目标语言 语言列表 (可设置为auto) 为空默认使用 en</param>
        /// <param name="requestParams">请求的参数信息</param>
        /// <returns>返回请求结果的JSON</returns>
        public static string RequestApi(string q, string from, string to, ref string requestParams)
        {
            try
            {
                //您的应用ID
                string appKey = "6f255e893ec410f1";
                if (string.IsNullOrEmpty(from))
                {
                    from = "zh-CHS";
                }
                if (string.IsNullOrEmpty(to))
                {
                    to = "en";
                }
                string salt = DateTime.Now.Millisecond.ToString();
                //您的应用密钥
                string appSecret = "6gFbwk1fr1kVpP9C1bTxveXTfwZRVKqP";
                requestParams += "appSecret:" + appSecret + "\r\n";
                MD5 md5 = new MD5CryptoServiceProvider();
                string md5Str = appKey + q + salt + appSecret;
                requestParams += "md5Str:" + md5Str + "\r\n";
                byte[] output = md5.ComputeHash(System.Text.Encoding.UTF8.GetBytes(md5Str));
                string sign = BitConverter.ToString(output).Replace("-", "");
                requestParams += "sign:" + sign + "\r\n";

                string url = string.Format("http://openapi.youdao.com/api?appKey={0}&q={1}&from={2}&to={3}&sign={4}&salt={5}",
                    appKey,
                    System.Web.HttpUtility.UrlDecode(q, System.Text.Encoding.GetEncoding("UTF-8")), from, to, sign, salt);
                requestParams += "url:" + url + "\r\n";
                WebRequest translationWebRequest = WebRequest.Create(url);

                WebResponse response = null;

                response = translationWebRequest.GetResponse();
                Stream stream = response.GetResponseStream();

                Encoding encode = Encoding.GetEncoding("utf-8");

                StreamReader reader = new StreamReader(stream, encode);
                string result = reader.ReadToEnd();
                return result;
            }
            catch
            {
                return "";
            }
        }

        /// <summary>
        /// 接口请求 中 -> 英
        /// </summary>
        /// <param name="q">要翻译的文本</param>
        /// <param name="requestParams">请求的参数信息</param>
        /// <returns>返回请求结果的JSON</returns>
        public static string RequestApiChinese2En(string q, ref string requestParams)
        {
            return RequestApi(q, "zh-CHS", "en", ref requestParams);
        }

        /// <summary>
        /// 接口请求 英 ->中
        /// </summary>
        /// <param name="q">要翻译的文本</param>
        /// <param name="requestParams">请求的参数信息</param>
        /// <returns>返回请求结果的JSON</returns>
        public static string RequestApiEn2Chinese(string q, ref string requestParams)
        {
            return RequestApi(q, "en", "zh-CHS", ref requestParams);
        }

        /// <summary>
        /// 接口请求 自动检测
        /// </summary>
        /// <param name="q">要翻译的文本</param>
        /// <param name="requestParams">请求的参数信息</param>
        /// <returns>返回请求结果的JSON</returns>
        public static string RequestApiAuto(string q, ref string requestParams)
        {
            return RequestApi(q, "auto", "auto", ref requestParams);
        }
    }
}
