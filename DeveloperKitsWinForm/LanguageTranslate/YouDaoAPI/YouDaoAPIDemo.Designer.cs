﻿namespace LanguageTranslate.YouDaoAPI
{
    partial class YouDaoAPIDemo
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnRequest = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.teSource = new System.Windows.Forms.TextBox();
            this.teRequestPara = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.teResult = new System.Windows.Forms.TextBox();
            this.teReturnParam = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.btnTranslationRules = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // btnRequest
            // 
            this.btnRequest.Location = new System.Drawing.Point(713, 51);
            this.btnRequest.Name = "btnRequest";
            this.btnRequest.Size = new System.Drawing.Size(75, 23);
            this.btnRequest.TabIndex = 0;
            this.btnRequest.Text = "请求接口";
            this.btnRequest.UseVisualStyleBackColor = true;
            this.btnRequest.Click += new System.EventHandler(this.btnRequest_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(43, 18);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(53, 12);
            this.label1.TabIndex = 1;
            this.label1.Text = "翻译文本";
            // 
            // teSource
            // 
            this.teSource.Location = new System.Drawing.Point(102, 14);
            this.teSource.Multiline = true;
            this.teSource.Name = "teSource";
            this.teSource.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.teSource.Size = new System.Drawing.Size(605, 76);
            this.teSource.TabIndex = 2;
            // 
            // teRequestPara
            // 
            this.teRequestPara.Location = new System.Drawing.Point(102, 178);
            this.teRequestPara.Multiline = true;
            this.teRequestPara.Name = "teRequestPara";
            this.teRequestPara.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.teRequestPara.Size = new System.Drawing.Size(605, 110);
            this.teRequestPara.TabIndex = 3;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(43, 181);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(53, 12);
            this.label2.TabIndex = 4;
            this.label2.Text = "请求参数";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(43, 99);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(53, 12);
            this.label3.TabIndex = 5;
            this.label3.Text = "翻译结果";
            // 
            // teResult
            // 
            this.teResult.Location = new System.Drawing.Point(102, 96);
            this.teResult.Multiline = true;
            this.teResult.Name = "teResult";
            this.teResult.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.teResult.Size = new System.Drawing.Size(605, 76);
            this.teResult.TabIndex = 6;
            // 
            // teReturnParam
            // 
            this.teReturnParam.Location = new System.Drawing.Point(102, 294);
            this.teReturnParam.Multiline = true;
            this.teReturnParam.Name = "teReturnParam";
            this.teReturnParam.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.teReturnParam.Size = new System.Drawing.Size(605, 110);
            this.teReturnParam.TabIndex = 7;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(43, 297);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(53, 12);
            this.label4.TabIndex = 8;
            this.label4.Text = "返回参数";
            // 
            // btnTranslationRules
            // 
            this.btnTranslationRules.Location = new System.Drawing.Point(713, 18);
            this.btnTranslationRules.Name = "btnTranslationRules";
            this.btnTranslationRules.Size = new System.Drawing.Size(75, 23);
            this.btnTranslationRules.TabIndex = 9;
            this.btnTranslationRules.Tag = "0";
            this.btnTranslationRules.Text = "中 -> 英";
            this.btnTranslationRules.UseVisualStyleBackColor = true;
            this.btnTranslationRules.Click += new System.EventHandler(this.btnTranslationRules_Click);
            // 
            // YouDaoAPIDemo
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 418);
            this.Controls.Add(this.btnTranslationRules);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.teReturnParam);
            this.Controls.Add(this.teResult);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.teRequestPara);
            this.Controls.Add(this.teSource);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.btnRequest);
            this.Name = "YouDaoAPIDemo";
            this.Text = "YouDaoAPI";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btnRequest;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox teSource;
        private System.Windows.Forms.TextBox teRequestPara;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox teResult;
        private System.Windows.Forms.TextBox teReturnParam;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Button btnTranslationRules;
    }
}