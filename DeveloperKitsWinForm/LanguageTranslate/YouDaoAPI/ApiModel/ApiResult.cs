﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LanguageTranslate.YouDaoAPI.ApiModel
{
    /// <summary>
    /// API输出结果
    /// </summary>
    public class ApiResult
    {

        /// <summary>
        /// 错误返回码 一定存在 成功返回0
        /// </summary>
        public string ErrorCode
        {   get;
            set;
        }

        /// <summary>/// 源语言 查询正确时，一定存在
        public string Query
        /// </summary>
        {
            get;
            set;
        }

        /// <summary>
        /// 翻译结果 查询正确时，一定存在
        /// </summary>
        public string[] Translation
        {
            get;
            set;
        }

        /// <summary>
        /// 词义 基本词典,查词时才有
        /// </summary>
        public ApiBasicModel Basic
        {
            get;
            set;
        }

        /// <summary>
        /// 词义 网络释义，该结果不一定存在
        /// </summary>
        public ApiWebDictModel[] Web
        {
            get;
            set;
        }

        /// <summary>
        /// text	源语言和目标语言	一定存在
        /// </summary>
        public string L
        {
            get;
            set;
        }

        /// <summary>
        /// 词典deeplink	查询语种为支持语言时，存在
        /// </summary>
        public ApiDictModel Dict
        {
            get;
            set;
        }

        /// <summary>
        /// webdeeplink 查询语种为支持语言时，存在
        /// </summary>
        public ApiWebDictModel Webdict
        {
            get;
            set;
        }

        /// <summary>
        /// 翻译结果发音地址	翻译成功一定存在
        /// </summary>
        public string TSpeakUrl
        {
            get;
            set;
        }

        /// <summary>
        /// 源语言发音地址	翻译成功一定存在
        /// </summary>
        public string SpeakUrl
        {
            get;
            set;
        }
    }
}
