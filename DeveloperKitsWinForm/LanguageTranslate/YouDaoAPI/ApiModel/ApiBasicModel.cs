﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LanguageTranslate.YouDaoAPI.ApiModel
{
    public class ApiBasicModel
    {
        /// <summary>
        /// 基本释义
        /// </summary>
        public string[] Explains
        {
            get;
            set;
        }



        public string[] Exam_Type
        {
            get;
            set;
        }

        /// <summary>
        /// 美式音标，英文查词成功，一定存在
        /// </summary>
        public string Us_phonetic
        {
            get;
            set;
        }

        /// <summary>
        /// 默认音标，默认是英式音标，英文查词成功，一定存在
        /// </summary>
        public string Phonetic
        {
            get;
            set;
        }

        /// <summary>
        /// 英式音标，英文查词成功，一定存在
        /// </summary>
        public string Uk_phonetic
        {
            get;
            set;
        }

        /// <summary>
        /// 英式发音，英文查词成功，一定存在
        /// </summary>
        public string Uk_speech
        {
            get;
            set;
        }

        /// <summary>
        /// 美式发音，英文查词成功，一定存在
        /// </summary>
        public string Us_speech
        {
            get;
            set;
        }

    }
}
