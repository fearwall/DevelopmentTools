﻿using CommonLib.Helper.Common;
using LanguageTranslate.YouDaoAPI.ApiModel;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Net;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace LanguageTranslate.YouDaoAPI
{
    public partial class YouDaoAPIDemo : Form
    {
        public YouDaoAPIDemo()
        {
            InitializeComponent();
        }

        /// <summary>
        /// 请求按钮
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnRequest_Click(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(teSource.Text.Trim()))
            {
                string requestParams = string.Empty;
                //string strRequestApiRes = YouDaoApiSDK.RequestApi(teSource.Text.Trim(),null,null,ref requestParams);
                string strFrom = "zh-CHS";
                string strTo = "en";
                int iTransRules = 0;
                int.TryParse(btnTranslationRules.Tag.ToString(), out iTransRules);
                if (iTransRules % 3 == 1)
                {
                    strFrom = "en";
                    strTo = "zh-CHS";
                }
                else if (iTransRules % 3 == 2)
                {
                    strFrom = "auto";
                    strTo = "auto";
                }

                string strRequestApiRes = YouDaoApiSDK.RequestApi(teSource.Text.Trim(), strFrom, strTo, ref requestParams);
                teReturnParam.Text = strRequestApiRes;
                teRequestPara.Text = requestParams;

                //解析JSON数据
                ApiResult apiResultModel = new ApiResult();
                apiResultModel = JsonHelper.DeserializeJsonToObject<ApiResult>(strRequestApiRes);

                if (apiResultModel.ErrorCode.Equals("0"))
                {
                    teResult.Text = string.Join("\r\n", apiResultModel.Translation);
                }
            }
        }

        private void btnTranslationRules_Click(object sender, EventArgs e)
        {
            int i = -1;
            try
            {
                i = Convert.ToInt32(btnTranslationRules.Tag);
            }
            catch
            {

            }
            i++;
            string strText = "中 -> 英";
            if(i % 3 == 0)
            {
                strText = "中 -> 英";
            }
            else if(i % 3 == 1)
            {
                strText = "英 -> 中";
            }
            else if(i % 3 == 2)
            {
                strText = "自动检测";
            }
            btnTranslationRules.Tag = i;
            btnTranslationRules.Text = strText;
        }
    }
}
