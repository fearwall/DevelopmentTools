﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LanguageTranslate.Model
{
    /// <summary>
    /// 学生信息实体-测试用
    /// </summary>
    public class Student
    {
        public int ID { get; set; }
        public string Name { get; set; }
        public string NickName { get; set; }
        public Class Class { get; set; }
    }

    /// <summary>
    /// 学生班级实体-测试用
    /// </summary>
    public class Class
    {
        public int ID { get; set; }
        public string Name { get; set; }
    }
}
