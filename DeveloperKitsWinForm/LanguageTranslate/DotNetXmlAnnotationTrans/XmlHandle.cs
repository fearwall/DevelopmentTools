﻿using CommonLib.Helper.Common;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;

namespace LanguageTranslate.DotNetXmlAnnotationTrans
{
    /// <summary>
    /// XML节点处理
    /// </summary>
    public class XmlHandle
    {
        public XmlHandle()
        {
        }

        /// <summary>
        /// XML节点翻译
        /// </summary>
        /// <param name="doc"></param>
        /// <returns></returns>
        public XmlDocument XmlTranslate(XmlDocument doc)
        {
            XmlNodeList memberNodes = doc.SelectNodes("doc/members/member");
            foreach (XmlNode memberNode in memberNodes)
            {
                ChildNode("summary", memberNode.SelectSingleNode("summary"));
                ChildNode("remarks", memberNode.SelectSingleNode("remarks"));
                XmlNodeList paramNodeList = memberNode.SelectNodes("param");
                if (paramNodeList != null && paramNodeList.Count > 0)
                {
                    foreach (XmlNode paramNode in paramNodeList)
                    {
                        ChildNode("", paramNode);
                    }
                }
                ChildNode("returns", memberNode.SelectSingleNode("returns"));
            }
            return doc;
        }

        /// <summary>
        /// 子节点处理
        /// </summary>
        /// <param name="nodeName"></param>
        /// <param name="xmlParent"></param>
        private void ChildNode(string nodeName, XmlNode xmlParent)
        {
            try
            {
                if (xmlParent == null)
                {
                    return;
                }

                string xpath = string.Empty;
                if (!string.IsNullOrEmpty(nodeName))
                {
                    xpath = xpath + "/para";
                }
                else
                {
                    xpath = "para";
                }
                XmlNodeList paraNodes = xmlParent.SelectNodes(xpath);
                if (paraNodes != null && paraNodes.Count > 0)
                {
                    //提取参数进行处理
                    foreach (XmlNode paraNode in paraNodes)
                    {
                        string strEnglish = paraNode.InnerXml;
                        string strChinese = Translate.EnToCn(strEnglish);
                        paraNode.InnerXml = strChinese;
                    }
                }
                else
                {
                    string strEnglish = xmlParent.InnerXml;
                    string strChinese = Translate.EnToCn(strEnglish);
                    xmlParent.InnerXml = strChinese;
                }
            }
            catch
            {

            }
        }

        /// <summary>
        /// XML转换及格式化
        /// </summary>
        /// <returns></returns>
        public XmlWriter XmlTranslateAndFormate(XmlDocument doc)
        {
            //XmlReaderSettings readerSettings = new XmlReaderSettings();
            //readerSettings.IgnoreComments = true;
            //readerSettings.IgnoreWhitespace = true;
            //XmlReader xmlReader = XmlReader.Create(strFile, readerSettings);


            XmlWriterSettings writeSettings = new XmlWriterSettings();
            writeSettings.OmitXmlDeclaration = true;
            writeSettings.ConformanceLevel = ConformanceLevel.Fragment;
            writeSettings.CloseOutput = false;
            StringBuilder sb = new StringBuilder();
            XmlWriter xmlWriter = XmlWriter.Create(sb, writeSettings);
            //xmlWriter.WriteStartDocument();

            XmlNodeList root = doc.ChildNodes;
            ChildNodeRecursion(xmlWriter, root);

            //xmlWriter.WriteEndDocument();
            xmlWriter.Flush();
            xmlWriter.Close();
            string s = sb.ToString();

            return xmlWriter;
        }

        private void ChildNodeRecursion(XmlWriter xmlWriter,XmlNodeList xmlNodeList)
        {
            foreach(XmlNode node in xmlNodeList)
            {
                switch (node.NodeType)
                {
                    case XmlNodeType.Attribute:                        
                        break;
                    case XmlNodeType.Element:
                        xmlWriter.WriteStartElement(node.Name);
                        if (node.ChildNodes.Count > 0)
                        {
                            ChildNodeRecursion(xmlWriter, node.ChildNodes);
                        }

                        if (node.Attributes.Count > 0)
                        {
                            string s = node.Attributes.GetType().ToString();
                            XmlAttributeCollection xac = node.Attributes;
                            
                            foreach(XmlAttribute xab in xac)
                            {
                                
                            }
                        }
                        xmlWriter.WriteEndElement();
                        break;
                    case XmlNodeType.EndElement:
                        break;
                    case XmlNodeType.Text:
                        xmlWriter.WriteValue(node.InnerXml);
                        break;
                    default:break;
                }

            }
        }
    }
}
