﻿using CommonLib.Helper.Common;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Xml;

namespace LanguageTranslate.DotNetXmlAnnotationTrans
{
    public partial class FrmMain : Form
    {
        public FrmMain()
        {
            InitializeComponent();
        }


        private void FrmMain_Load(object sender, EventArgs e)
        {
            try
            {
                cbPattern.SelectedIndex = 0;
            }
            catch
            {

            }
        }

        private void btnTranslate_Click(object sender, EventArgs e)
        {
            string strPattern = cbPattern.SelectedItem.ToString();
            if (strPattern.Equals("单文件"))
            {
                FileTranslate();
            }
            else if (strPattern.Equals("文件夹"))
            {

            }
        }

        /// <summary>
        /// 单文件操作
        /// </summary>
        private void FileTranslate()
        {
            try
            {
                string strFilePath = FilePathHelper.FilePathSelect();
                if (!string.IsNullOrEmpty(strFilePath))
                {
                    string strFileName = Path.GetFileName(strFilePath);
                    if (MessageBox.Show("确定要操作文件：" + strFileName + "吗？", "提示", MessageBoxButtons.OKCancel, MessageBoxIcon.Question) == DialogResult.OK)
                    {
                        tePath.Text = strFilePath;
                        XmlDocument docSource = XmlHelper.XmlDeserializeFromFile<XmlDocument>(strFilePath, Encoding.Default);
                        XmlDocument doc = XmlHelper.XmlDeserializeFromFile<XmlDocument>(strFilePath, Encoding.Default);
                        XmlHandle xmlHandle = new XmlHandle();
                        xmlHandle.XmlTranslateAndFormate(doc);

                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("操作出错！\r\n" + ex.Message);
            }
        }

        ///// <summary>
        ///// 单文件操作
        ///// </summary>
        //private void FileTranslate()
        //{
        //    try
        //    {
        //        string strFilePath = FilePathHelper.FilePathSelect();
        //        if (!string.IsNullOrEmpty(strFilePath))
        //        {
        //            string strFileName = Path.GetFileName(strFilePath);
        //            if (MessageBox.Show("确定要操作文件：" + strFileName + "吗？", "提示", MessageBoxButtons.OKCancel, MessageBoxIcon.Question) == DialogResult.OK)
        //            {
        //                tePath.Text = strFilePath;
        //                XmlDocument docSource = XmlHelper.XmlDeserializeFromFile<XmlDocument>(strFilePath, Encoding.Default);
        //                XmlDocument doc = XmlHelper.XmlDeserializeFromFile<XmlDocument>(strFilePath, Encoding.Default);
        //                XmlHandle xmlHandle = new XmlHandle();


        //                doc = xmlHandle.XmlTranslate(doc);

        //                string strOutPath = FilePathHelper.SaveFileDialog(strFileName);
        //                //FilePathHelper.CreateDirectory(strOutPath);


        //                XmlWriterSettings settings = new XmlWriterSettings();
        //                settings.OmitXmlDeclaration = true;
        //                settings.ConformanceLevel = ConformanceLevel.Fragment;
        //                settings.CloseOutput = false;                        

        //                XmlWriter xmlWriter = XmlWriter.Create(strOutPath,settings);
        //                //xmlWriter.WriteStartDocument();
        //                xmlWriter.WriteString(doc.InnerXml);
        //               // xmlWriter.WriteEndDocument();
        //                xmlWriter.Flush();
        //                xmlWriter.Close();

        //                if (strOutPath.Equals(strFilePath))
        //                {
        //                    string strFileNameNoExtend = Path.GetFileNameWithoutExtension(strFilePath);
        //                    string strOutPathDir = Path.GetDirectoryName(strFilePath);
        //                    docSource.Save(strOutPathDir + @"\" + strFileNameNoExtend + "_backup.xml");
        //                }
        //                teOutPath.Text = strOutPath;
        //            }
        //        }

        //    }
        //    catch(Exception ex)
        //    {
        //        MessageBox.Show("操作出错！\r\n" + ex.Message);
        //    }
        //}


    }
}
