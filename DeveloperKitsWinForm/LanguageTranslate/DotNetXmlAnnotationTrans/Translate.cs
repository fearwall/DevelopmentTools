﻿using CommonLib.Helper.Common;
using LanguageTranslate.YouDaoAPI;
using LanguageTranslate.YouDaoAPI.ApiModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LanguageTranslate.DotNetXmlAnnotationTrans
{
    /// <summary>
    /// 翻译类
    /// </summary>
    public class Translate
    {
        /// <summary>
        /// 英语翻译至汉语
        /// </summary>
        /// <param name="strEnSource"></param>
        /// <returns></returns>
        public static string EnToCn(string strEnSource)
        {
            string strRes = strEnSource;
            try
            {
                if (string.IsNullOrEmpty(strEnSource))
                {
                    return strRes;
                }
                //int iEnSourceLen = strEnSource.Length;

                //左侧空白字符
                //string strEnSourceLeftTrim = strEnSource.TrimStart();
                //int iEnSourceLeftTrimLen = iEnSourceLen - strEnSourceLeftTrim.Length;
                //string strLeftChar = strEnSource.Substring(0, iEnSourceLeftTrimLen);
                ////右侧空白字符
                //string strEnSourceRightTrim = strEnSource.TrimEnd();
                //int iEnSourceRightTrimLen = iEnSourceLen - strEnSourceRightTrim.Length;
                //string strRightChar = strEnSource.Substring(iEnSourceLen - iEnSourceRightTrimLen);
                string strEnsourceXml = string.Empty;
                if(strEnSource.Contains("<") && strEnSource.Contains("/>."))
                {
                    strEnsourceXml = " " + strEnSource.Substring(strEnSource.IndexOf("<"), strEnSource.IndexOf("/>.") - strEnSource.IndexOf("<") + 3);
                }
                if (!string.IsNullOrEmpty(strEnsourceXml))
                {
                    strEnSource = strEnSource.Replace(strEnsourceXml, "");
                }

                string requestParams = string.Empty;
                string strRequestApiRes = YouDaoApiSDK.RequestApiEn2Chinese(strEnSource.Trim(), ref requestParams);
                //解析JSON数据
                ApiResult apiResultModel = new ApiResult();
                try
                {
                    apiResultModel = JsonHelper.DeserializeJsonToObject<ApiResult>(strRequestApiRes);
                }
                catch
                {
                    apiResultModel = null;
                }
                if(apiResultModel == null)
                {
                    return strRes;
                }
                else if(Convert.ToInt32(apiResultModel.ErrorCode) != 0)
                {
                    return strRes;
                }
                string strChinese = string.Empty;
                strChinese =  "\n" + apiResultModel.Translation[0] + strEnsourceXml  + "\n原文: " + strEnSource.Trim() + strEnsourceXml + "\n"; 
                return strChinese;
            }
            catch
            {
                return strRes;
            }
        }
    }
}
