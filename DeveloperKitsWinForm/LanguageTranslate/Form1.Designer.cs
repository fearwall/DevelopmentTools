﻿namespace LanguageTranslate
{
    partial class Form1
    {
        /// <summary>
        /// 必需的设计器变量。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// 清理所有正在使用的资源。
        /// </summary>
        /// <param name="disposing">如果应释放托管资源，为 true；否则为 false。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows 窗体设计器生成的代码

        /// <summary>
        /// 设计器支持所需的方法 - 不要修改
        /// 使用代码编辑器修改此方法的内容。
        /// </summary>
        private void InitializeComponent()
        {
            this.btnYoudaoApi = new System.Windows.Forms.Button();
            this.btnJsonTest = new System.Windows.Forms.Button();
            this.btnXml = new System.Windows.Forms.Button();
            this.btnDotNetXmlAnnotationTrans = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // btnYoudaoApi
            // 
            this.btnYoudaoApi.Location = new System.Drawing.Point(12, 12);
            this.btnYoudaoApi.Name = "btnYoudaoApi";
            this.btnYoudaoApi.Size = new System.Drawing.Size(157, 23);
            this.btnYoudaoApi.TabIndex = 2;
            this.btnYoudaoApi.Text = "有道API翻译";
            this.btnYoudaoApi.UseVisualStyleBackColor = true;
            this.btnYoudaoApi.Click += new System.EventHandler(this.btnYoudaoApi_Click);
            // 
            // btnJsonTest
            // 
            this.btnJsonTest.Location = new System.Drawing.Point(175, 12);
            this.btnJsonTest.Name = "btnJsonTest";
            this.btnJsonTest.Size = new System.Drawing.Size(157, 23);
            this.btnJsonTest.TabIndex = 3;
            this.btnJsonTest.Text = "JSON序列化反序列化";
            this.btnJsonTest.UseVisualStyleBackColor = true;
            this.btnJsonTest.Click += new System.EventHandler(this.btnJsonTest_Click);
            // 
            // btnXml
            // 
            this.btnXml.Location = new System.Drawing.Point(338, 12);
            this.btnXml.Name = "btnXml";
            this.btnXml.Size = new System.Drawing.Size(157, 23);
            this.btnXml.TabIndex = 4;
            this.btnXml.Text = "注释XML解析";
            this.btnXml.UseVisualStyleBackColor = true;
            this.btnXml.Click += new System.EventHandler(this.btnXml_Click);
            // 
            // btnDotNetXmlAnnotationTrans
            // 
            this.btnDotNetXmlAnnotationTrans.Location = new System.Drawing.Point(12, 41);
            this.btnDotNetXmlAnnotationTrans.Name = "btnDotNetXmlAnnotationTrans";
            this.btnDotNetXmlAnnotationTrans.Size = new System.Drawing.Size(157, 23);
            this.btnDotNetXmlAnnotationTrans.TabIndex = 5;
            this.btnDotNetXmlAnnotationTrans.Text = ".NET XML注释翻译";
            this.btnDotNetXmlAnnotationTrans.UseVisualStyleBackColor = true;
            this.btnDotNetXmlAnnotationTrans.Click += new System.EventHandler(this.btnDotNetXmlAnnotationTrans_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.btnDotNetXmlAnnotationTrans);
            this.Controls.Add(this.btnXml);
            this.Controls.Add(this.btnJsonTest);
            this.Controls.Add(this.btnYoudaoApi);
            this.Name = "Form1";
            this.Text = "Form1";
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button btnYoudaoApi;
        private System.Windows.Forms.Button btnJsonTest;
        private System.Windows.Forms.Button btnXml;
        private System.Windows.Forms.Button btnDotNetXmlAnnotationTrans;
    }
}

