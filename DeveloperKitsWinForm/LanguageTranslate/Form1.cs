﻿using CommonLib.Helper.Common;
using LanguageTranslate.Model;
using LanguageTranslate.YouDaoAPI;
using LanguageTranslate.YouDaoAPI.ApiModel;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Xml;

namespace LanguageTranslate
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void btnYoudaoApi_Click(object sender, EventArgs e)
        {
            new YouDaoAPIDemo().ShowDialog();
            //new YouDaoAPI().ShowDialog();
        }

        private void btnJsonTest_Click(object sender, EventArgs e)
        {
            Student sdudent = new Student();
            sdudent.ID = 1;
            sdudent.Name = "陈晨";
            sdudent.NickName = "石子儿";
            sdudent.Class = new Class() { Name = "CS0216", ID = 0216 };

            //实体序列化和反序列化
            string json1 = JsonHelper.SerializeObject(sdudent);
            //json1 : {"ID":1,"Name":"陈晨","NickName":"石子儿","Class":{"ID":216,"Name":"CS0216"}}
            Student sdudent1 = JsonHelper.DeserializeJsonToObject<Student>(json1);

            ////实体集合序列化和反序列化
            //List<Student> sdudentList = new List<Student>() { sdudent, sdudent1 };
            //string json2 = JsonHelper.SerializeObject(sdudentList);
            ////json: [{"ID":1,"Name":"陈晨","NickName":"石子儿","Class":{"ID":216,"Name":"CS0216"}},{"ID":1,"Name":"陈晨","NickName":"石子儿","Class":{"ID":216,"Name":"CS0216"}}]
            //List<Student> sdudentList2 = JsonHelper.DeserializeJsonToList<Student>(json2);

            ////DataTable序列化和反序列化
            //DataTable dt = new DataTable();
            //dt.TableName = "Student";
            //dt.Columns.Add("ID", typeof(int));
            //dt.Columns.Add("Name");
            //dt.Columns.Add("NickName");
            //DataRow dr = dt.NewRow();
            //dr["ID"] = 112;
            //dr["Name"] = "战三";
            //dr["NickName"] = "小三";
            //dt.Rows.Add(dr);
            //string json3 = JsonHelper.SerializeObject(dt);
            ////json3 : [{"ID":112,"Name":"战三","NickName":"小三"}]
            //DataTable sdudentDt3 = JsonHelper.DeserializeJsonToObject<DataTable>(json3);
            //List<Student> sdudentList3 = JsonHelper.DeserializeJsonToList<Student>(json3);

            ////验证对象和数组
            //Student sdudent4 = JsonHelper.DeserializeJsonToObject<Student>("{\"ID\":\"112\",\"Name\":\"石子儿\"}");
            //List<Student> sdudentList4 = JsonHelper.DeserializeJsonToList<Student>("[{\"ID\":\"112\",\"Name\":\"石子儿\"}]");

            //匿名对象解析
            //var tempEntity = new { ID = 0, Name = string.Empty };
            //string json5 = JsonHelper.SerializeObject(tempEntity);
            string json5 = @"{
    'tSpeakUrl': 'http://openapi.youdao.com/ttsapi?q=%E6%B5%8B%E8%AF%95&langType=zh-CHS&sign=FA1FE623C6B0AB797EA49E85CAD6ED70&salt=1542962727423&voice=4&format=mp3&appKey=6f255e893ec410f1',
    'web': [
        {
            'value': [
                '测试',
                '测验',
                '试验',
                '考试'
            ],
            'key': 'Test'
        },
        {
            'value': [
                '测试工程师',
                '测试员',
                '软件测试工程师',
                '产品试验工程师'
            ],
            'key': 'Test engineer'
        },
        {
            'value': [
                '压力测试',
                '应力测试',
                '负荷试验',
                '压力试验'
            ],
            'key': 'stress test'
        }
    ],
    'query': 'test',
    'translation': [
        '测试'
    ],
    'errorCode': '0',
    'dict': {
        'url': 'yddict://m.youdao.com/dict?le=eng&q=test'
    },
    'webdict': {
        'url': 'http://m.youdao.com/dict?le=eng&q=test'
    },
    'basic': {
        'exam_type': [
            '高中',
            '初中'
        ],
        'us-phonetic': 'tɛst',
        'phonetic': 'test',
        'uk-phonetic': 'test',
        'uk-speech': 'http://openapi.youdao.com/ttsapi?q=test&langType=en&sign=8CA7C8B9069F5CD0C6FF2689FA2407FE&salt=1542962727423&voice=5&format=mp3&appKey=6f255e893ec410f1',
        'explains': [
            'n. 试验；检验',
            'vt. 试验；测试',
            'vi. 试验；测试',
            'n. (Test)人名；(英)特斯特'
        ],
        'us-speech': 'http://openapi.youdao.com/ttsapi?q=test&langType=en&sign=8CA7C8B9069F5CD0C6FF2689FA2407FE&salt=1542962727423&voice=6&format=mp3&appKey=6f255e893ec410f1'
    },
    'l': 'en2zh-CHS',
    'speakUrl': 'http://openapi.youdao.com/ttsapi?q=test&langType=en&sign=8CA7C8B9069F5CD0C6FF2689FA2407FE&salt=1542962727423&voice=4&format=mp3&appKey=6f255e893ec410f1'
}";
            
            ApiResult apiResultModel = new ApiResult();
            apiResultModel = JsonHelper.DeserializeJsonToObject<ApiResult>(json5);
            //json5 : {"ID":0,"Name":""}
            //var tempEntity = new { };
            //tempEntity = JsonHelper.DeserializeAnonymousType(json5, tempEntity);
            //var tempStudent = new Student();
            //tempStudent = JsonHelper.DeserializeAnonymousType("{\"ID\":\"112\",\"Name\":\"石子儿\"}", tempStudent);

        }

        /// <summary>
        /// <para>
        /// <c></c>
        /// </para>
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnXml_Click(object sender, EventArgs e)
        {
            string strFilePath = FilePathHelper.FilePathSelect();
            XmlDocument doc = XmlHelper.XmlDeserializeFromFile<XmlDocument>(strFilePath, Encoding.Default);
            XmlDocument docSource = XmlHelper.XmlDeserializeFromFile<XmlDocument>(strFilePath, Encoding.Default);
            XmlNodeList memberNodes = doc.SelectNodes("doc/members/member");
            foreach (XmlNode memberNode in memberNodes)
            {
                ChildNode("summary", memberNode.SelectSingleNode("summary"));
                ChildNode("remarks", memberNode.SelectSingleNode("remarks"));
                XmlNodeList paramNodeList = memberNode.SelectNodes("param");
                if(paramNodeList !=null && paramNodeList.Count > 0)
                {
                    foreach (XmlNode paramNode in paramNodeList)
                    {
                        ChildNode("", paramNode);
                    }
                }
                ChildNode("returns", memberNode.SelectSingleNode("returns"));
            }
            XmlDocument docReturn = doc;
        }

        private void ChildNode(string nodeName, XmlNode xmlParent)
        {
            try
            {
                if(xmlParent == null)
                {
                    return;
                }

                string xpath = string.Empty;
                if (!string.IsNullOrEmpty(nodeName))
                {
                    xpath = xpath + "/para";
                }
                else
                {
                    xpath = "para";
                }
                XmlNodeList paraNodes = xmlParent.SelectNodes(xpath);
                if (paraNodes != null && paraNodes.Count > 0)
                {
                    //提取参数进行处理
                    foreach(XmlNode paraNode in paraNodes)
                    {
                        string strEnglish = paraNode.InnerXml;
                        string strChinese = string.Empty;
                        paraNode.InnerXml = strChinese + " 原文: " + strEnglish;
                    }
                }
                else
                {
                    string strEnglish = xmlParent.InnerXml;
                    string strChinese = string.Empty;
                    xmlParent.InnerXml = strChinese + " 原文: " + strEnglish;
                }
            }
            catch
            {

            }
        }

        private void btnDotNetXmlAnnotationTrans_Click(object sender, EventArgs e)
        {
            new LanguageTranslate.DotNetXmlAnnotationTrans.FrmMain().ShowDialog();
        }
    }
}
