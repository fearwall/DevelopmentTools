﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;

namespace DeveloperKitsWinForm
{
    /// <summary>
    /// 窗口捕获助手类
    /// </summary>
    public class WindowCatchHelper
    {
        /// <summary>
        /// 查找窗口
        /// </summary>
        /// <param name="lpClassName"></param>
        /// <param name="lpWindowName"></param>
        /// <returns></returns>
        [DllImport("User32.dll")]
        public static extern IntPtr FindWindow(string lpClassName, string lpWindowName);

        /// <summary>
        /// 向窗口发送消息
        /// </summary>
        /// <param name="hWnd">窗口句柄</param>
        /// <param name="Msg">消息</param>
        /// <param name="wParam">控件类型</param>
        /// <param name="lParam"></param>
        /// <returns></returns>
        [DllImport("User32.dll", EntryPoint = "SendMessage")]
        private static extern int SendMessage(IntPtr hWnd, int Msg, IntPtr wParam, string lParam);

        [DllImport("user32.dll", EntryPoint = "SendMessageA")]
        public static extern int SendMessage(IntPtr hwnd, int wMsg, int wParam, int lParam);

        /// <summary>
        /// 查找字窗口
        /// </summary>
        /// <param name="parent"></param>
        /// <param name="childe"></param>
        /// <param name="strclass"></param>
        /// <param name="FrmText"></param>
        /// <returns></returns>
        [DllImport("User32.dll")]
        public static extern IntPtr FindWindowEx(IntPtr parent, IntPtr childe, string strclass, string FrmText);

        /// <summary>
        /// 控制窗口显示
        /// </summary>
        /// <param name="hwnd"></param>
        /// <param name="nCmdShow">0|隐藏窗口并激活其他窗口 1|显示 2|最小化 3|最大化 4|显示最近状态的窗口 </param>
        /// <returns></returns>
        [DllImport("user32.dll", CharSet = CharSet.Auto, ExactSpelling = true)]
        public static extern int ShowWindow(IntPtr hwnd, int nCmdShow);

        /// <summary>
        /// 获取当前窗口句柄
        /// </summary>
        /// <returns></returns>
        [DllImport("user32.dll", CharSet = CharSet.Auto, ExactSpelling = true)]
        public static extern IntPtr GetForegroundWindow();

        /// <summary>
        /// 关闭窗口的代码
        /// </summary>
        public const int WM_CLOSE = 0x10;

        /// <summary>
        /// 关闭窗口
        /// </summary>
        public static int CloseWindow(IntPtr hWnd)
        {
            return SendMessage(hWnd, WM_CLOSE, 0, 0);
        }
    }
}
