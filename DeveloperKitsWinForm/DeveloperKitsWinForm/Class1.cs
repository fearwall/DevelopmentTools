﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;

namespace DeveloperKitsWinForm
{
    public class Class1
    {
        /// <summary>
        /// 初始化函数
        /// <para>
        /// 功能描述：
        /// 检查整个运行环境：包括网络环境、运行所需文件、参数等的检查
        /// 本地临时文件的清除，比如获取照片信息交易所生成的本地jpg文件。
        /// 返回值： 成功：返回0 ；  失败：返回 -1
        /// </para>
        /// </summary>
        /// <param name="pErrMsg">动态库初始化错误信息</param>
        /// <returns></returns>
        [DllImport("SiInterface.dll", EntryPoint = "INIT", CharSet = CharSet.Ansi)]
        public extern static int INIT(StringBuilder pErrMsg);

        /// <summary>
        /// 交易函数
        /// <para>
        /// 功能描述：
        /// HIS系统开发商需要向医保中心发送业务请求的通用函数
        /// 输入参数：inputData
        /// 输出参数：outputData char*
        /// 返回值：成功：返回0 ；  失败：返回小于 0
        /// 输入输出参数是以“^、$、|”分割的字符串
        /// </para>
        /// </summary>
        /// <param name="inputData">入参格式：业务编号^医疗机构编号^操作员编号^业务周期号^医院交易流水号^中心编码^入参^联机标志</param>
        /// <param name="outputData">
        /// 出参格式：中心交易流水号^联脱机标志^输出参数^
        /// <para>
        /// 该参数为输出参数，客户程序必须在调用本函数之前分配足够长 的空间，其最小值为1024字节，如果未给本参数分配空间或分配的空间长度小于实际返回的长度，客户程序将会出现内存保护错误
        /// 输出参数的规则同入参格式中的输入参数说明
        /// </para>
        /// </param>
        /// <returns></returns>
        [DllImport(@"SiInterface.dll", EntryPoint = "BUSINESS_HANDLE", CharSet = CharSet.Ansi)]
        public extern static int BUSINESS_HANDLE(StringBuilder inputData,StringBuilder outputData);

        /// <summary>
        /// 测试函数
        /// </summary>
        /// <returns></returns>
        [DllImport(@"SiInterface.dll", EntryPoint = "Test", CharSet = CharSet.Ansi)]
        public extern static int Test();
    }
}
