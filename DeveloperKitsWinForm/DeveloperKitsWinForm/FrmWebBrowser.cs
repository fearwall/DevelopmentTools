﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace DeveloperKitsWinForm
{
    public partial class FrmWebBrowser : Form
    {
        public FrmWebBrowser()
        {
            InitializeComponent();
        }

        private void btnJump_Click(object sender, EventArgs e)
        {
            JumpUrl();
        }

        private void tbAddress_KeyPress(object sender, KeyPressEventArgs e)
        {
            if(e.KeyChar == 13)
            {
                JumpUrl();
            }
        }

        private void JumpUrl()
        {
            try
            {
                string url = tbAddress.Text.Trim();
                if(url.IndexOf("http://") < 0 && url.IndexOf("https://") < 0)
                {
                    url = "http://" + url;
                }
                wbView.Url = new Uri(url);
            }
            catch (Exception ex)
            {
                MessageBox.Show("跳转失败！\r\n" + ex.Message);
            }
        }
    }
}
