﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace DeveloperKitsWinForm
{
    public partial class FrmBuild : Form
    {
        /// <summary>
        /// 批量文件路径
        /// </summary>
        List<string> listBatchFilePath = new List<string>();

        /// <summary>
        /// 日志StringBuilder
        /// </summary>
        StringBuilder sbLogs = new StringBuilder();

        public FrmBuild()
        {
            InitializeComponent();
        }

        private void cbModel_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                sbLogs.AppendLine("当前选中的路径为:" + listBatchFilePath[cbModel.SelectedIndex]);
                ShowLogs();
            }
            catch
            {

            }
        }

        private void btnBatchPath_Click(object sender, EventArgs e)
        {
            sbLogs.Clear();
            cbModel.Items.Clear();
            string strFolderPath = CommonLib.Helper.Common.FilePathHelper.FolderPathSelect(tbBatchPath.Text.Trim(),true);
            if (string.IsNullOrEmpty(strFolderPath))
            {
                MessageBox.Show("请选择批量生成的路径！", "错误", MessageBoxButtons.OK, MessageBoxIcon.Error);
                listBatchFilePath.Clear();
                return;
            }
            else
            {
                // @"^[^\.]+\.sln|^[^\.].+\.csproj$"
                listBatchFilePath = CommonLib.Helper.Common.FileSearchHelper.GetFileSystemEntries(strFolderPath, @"^[^\.].+\.csproj$", true, false).ToList();
                if(listBatchFilePath.Count <= 0)
                {
                    MessageBox.Show("选中的路径无可操作的指定文件【AssemblyInfo.cs】！", "错误", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    listBatchFilePath.Clear();
                    return;
                }
                
                foreach(string str in listBatchFilePath)
                {
                    cbModel.Items.Add(Path.GetFileNameWithoutExtension(str));
                    //cbModel.Items.Add()
                }
                tbBatchPath.Text = strFolderPath;
                sbLogs.AppendLine("读取文件路径【" + strFolderPath + "】成功！共获取到" + listBatchFilePath.Count +"个文件！");
            }
            ShowLogs();
        }

        private void btnSingleEdit_Click(object sender, EventArgs e)
        {
            sbLogs.Clear();
            string strFolderPath = CommonLib.Helper.Common.FilePathHelper.FolderPathSelect(tbBatchPath.Text.Trim(), true);
            if (string.IsNullOrEmpty(strFolderPath))
            {
                MessageBox.Show("请选择批量生成的路径！", "错误", MessageBoxButtons.OK, MessageBoxIcon.Error);
                listBatchFilePath.Clear();
                return;
            }
            else
            {                
                sbLogs.AppendLine("设置输出路径为【" + strFolderPath + "】！");
            }
            ShowLogs();
        }

        private void ShowLogs()
        {
            tbInfo.Text = sbLogs.ToString();
            tbInfo.SelectionStart = tbInfo.Text.Length;
        }

        /// <summary>
        /// 校验参数
        /// </summary>
        /// <returns></returns>
        private bool ValidateParams()
        {            
            return true;
        }

        /// <summary>
        /// 字符串转换为版本号的正整数，转换失败返回-1
        /// </summary>
        /// <param name="str"></param>
        /// <returns></returns>
        private int StringToVersion(string str)
        {
            try
            {
                int i = Convert.ToInt32(str);
                if(i >= 0 && i <= 65534)
                {
                    return i;
                }
                else
                {
                    return -1;
                }
            }
            catch
            {
                return -1;
            }
        }
        
        private void btnMake_Click(object sender, EventArgs e)
        {            
            sbLogs.Append("操作完毕！");
            MessageBox.Show("操作完毕！", "提示", MessageBoxButtons.OK, MessageBoxIcon.Information);
            ShowLogs();
        }

        private void btnMake_Click_1(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(tbSinglePath.Text.Trim()))
            {
                MessageBox.Show("请选择输出路径！", "错误", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
            if (cbModel.Items.Count <= 0)
            {
                MessageBox.Show("请选择生成列表！", "错误", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
            try
            {
                string strFloderName = cbModel.Text;
                cbModel.SelectedIndex = cbModel.Items.IndexOf(cbModel.Text);
                if (cbModel.SelectedIndex < 0)
                {
                    MessageBox.Show("请重新选择要生成的项目！", "错误", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return;
                }
                string strOutPath = tbSinglePath.Text.Trim() + @"";
                sbLogs.AppendLine("");
                string strBuild = string.Format(
                    @"msbuild " + "\"" +
                    @"{0}" + "\"" +
                    @" /t:Clean /t:Build /p:Configuration=Release /p:OutDir=" + "\"" +
                    @"{1}" + "\"", listBatchFilePath[cbModel.SelectedIndex], strOutPath + @"\" + strFloderName + "");
                sbLogs.Append(strBuild);
                sbLogs.AppendLine("");
                ShowLogs();
            }
            catch(Exception ex)
            {
                MessageBox.Show("生成失败!\r\n" + ex.ToString());
            }
        }

        private void FrmBuild_Load(object sender, EventArgs e)
        {
            tbBatchPath.Text = @"G:\DevelopDir\KEMUCH\HIS\050.Implement\HIS ClinicManage";
            tbSinglePath.Text = @"C:\Users\lfc_l\Desktop\MSBuildOut";
        }
    }
}
