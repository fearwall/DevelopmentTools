﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace DeveloperKitsWinForm
{
    public partial class FrmWindowCatch : Form
    {
        public int ReturnParam = -1;

        public FrmWindowCatch()
        {
            InitializeComponent();
        }

        private void FrmWindowCatch_FormClosed(object sender, FormClosedEventArgs e)
        {
            ReturnParam = 1;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
