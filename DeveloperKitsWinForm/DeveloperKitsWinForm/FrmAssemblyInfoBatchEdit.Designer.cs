﻿namespace DeveloperKitsWinForm
{
    partial class FrmAssemblyInfoBatchEdit
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.tbBatchPath = new System.Windows.Forms.TextBox();
            this.btnBatchPath = new System.Windows.Forms.Button();
            this.label2 = new System.Windows.Forms.Label();
            this.tbSinglePath = new System.Windows.Forms.TextBox();
            this.btnSingleEdit = new System.Windows.Forms.Button();
            this.label3 = new System.Windows.Forms.Label();
            this.cbModel = new System.Windows.Forms.ComboBox();
            this.tbInfo = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.gbParams = new System.Windows.Forms.GroupBox();
            this.label5 = new System.Windows.Forms.Label();
            this.tbAssemblyTitle = new System.Windows.Forms.TextBox();
            this.tbAssemblyDescription = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.tbAssemblyCompany = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.tbAssemblyProduct = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.tbAssemblyCopyright = new System.Windows.Forms.TextBox();
            this.label9 = new System.Windows.Forms.Label();
            this.tbAssemblyTrademark = new System.Windows.Forms.TextBox();
            this.label10 = new System.Windows.Forms.Label();
            this.tbAssemblyVersion1 = new System.Windows.Forms.TextBox();
            this.label11 = new System.Windows.Forms.Label();
            this.tbAssemblyFileVersion1 = new System.Windows.Forms.TextBox();
            this.label12 = new System.Windows.Forms.Label();
            this.btnMake = new System.Windows.Forms.Button();
            this.tbAssemblyVersion2 = new System.Windows.Forms.TextBox();
            this.tbAssemblyVersion3 = new System.Windows.Forms.TextBox();
            this.tbAssemblyVersion4 = new System.Windows.Forms.TextBox();
            this.tbAssemblyFileVersion4 = new System.Windows.Forms.TextBox();
            this.tbAssemblyFileVersion3 = new System.Windows.Forms.TextBox();
            this.tbAssemblyFileVersion2 = new System.Windows.Forms.TextBox();
            this.label13 = new System.Windows.Forms.Label();
            this.gbParams.SuspendLayout();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(12, 42);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(89, 12);
            this.label1.TabIndex = 2;
            this.label1.Text = "文件批量修改：";
            // 
            // tbBatchPath
            // 
            this.tbBatchPath.Location = new System.Drawing.Point(107, 39);
            this.tbBatchPath.Name = "tbBatchPath";
            this.tbBatchPath.Size = new System.Drawing.Size(674, 21);
            this.tbBatchPath.TabIndex = 3;
            // 
            // btnBatchPath
            // 
            this.btnBatchPath.Enabled = false;
            this.btnBatchPath.Location = new System.Drawing.Point(778, 38);
            this.btnBatchPath.Name = "btnBatchPath";
            this.btnBatchPath.Size = new System.Drawing.Size(75, 23);
            this.btnBatchPath.TabIndex = 4;
            this.btnBatchPath.Text = "选择";
            this.btnBatchPath.UseVisualStyleBackColor = true;
            this.btnBatchPath.Click += new System.EventHandler(this.btnBatchPath_Click);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(12, 72);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(89, 12);
            this.label2.TabIndex = 5;
            this.label2.Text = "单个文件修改：";
            // 
            // tbSinglePath
            // 
            this.tbSinglePath.Location = new System.Drawing.Point(107, 69);
            this.tbSinglePath.Name = "tbSinglePath";
            this.tbSinglePath.Size = new System.Drawing.Size(674, 21);
            this.tbSinglePath.TabIndex = 6;
            // 
            // btnSingleEdit
            // 
            this.btnSingleEdit.Enabled = false;
            this.btnSingleEdit.Location = new System.Drawing.Point(778, 68);
            this.btnSingleEdit.Name = "btnSingleEdit";
            this.btnSingleEdit.Size = new System.Drawing.Size(75, 23);
            this.btnSingleEdit.TabIndex = 7;
            this.btnSingleEdit.Text = "选择";
            this.btnSingleEdit.UseVisualStyleBackColor = true;
            this.btnSingleEdit.Click += new System.EventHandler(this.btnSingleEdit_Click);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(36, 17);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(65, 12);
            this.label3.TabIndex = 0;
            this.label3.Text = "修改模式：";
            // 
            // cbModel
            // 
            this.cbModel.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbModel.FormattingEnabled = true;
            this.cbModel.Items.AddRange(new object[] {
            "批量模式",
            "单文件模式"});
            this.cbModel.Location = new System.Drawing.Point(107, 14);
            this.cbModel.Name = "cbModel";
            this.cbModel.Size = new System.Drawing.Size(746, 20);
            this.cbModel.TabIndex = 1;
            this.cbModel.SelectedIndexChanged += new System.EventHandler(this.cbModel_SelectedIndexChanged);
            // 
            // tbInfo
            // 
            this.tbInfo.Location = new System.Drawing.Point(14, 249);
            this.tbInfo.Multiline = true;
            this.tbInfo.Name = "tbInfo";
            this.tbInfo.ReadOnly = true;
            this.tbInfo.Size = new System.Drawing.Size(839, 189);
            this.tbInfo.TabIndex = 10;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(12, 234);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(65, 12);
            this.label4.TabIndex = 9;
            this.label4.Text = "日志信息：";
            // 
            // gbParams
            // 
            this.gbParams.Controls.Add(this.label13);
            this.gbParams.Controls.Add(this.tbAssemblyFileVersion4);
            this.gbParams.Controls.Add(this.tbAssemblyFileVersion3);
            this.gbParams.Controls.Add(this.tbAssemblyFileVersion2);
            this.gbParams.Controls.Add(this.tbAssemblyVersion4);
            this.gbParams.Controls.Add(this.tbAssemblyVersion3);
            this.gbParams.Controls.Add(this.tbAssemblyVersion2);
            this.gbParams.Controls.Add(this.btnMake);
            this.gbParams.Controls.Add(this.tbAssemblyFileVersion1);
            this.gbParams.Controls.Add(this.label12);
            this.gbParams.Controls.Add(this.tbAssemblyVersion1);
            this.gbParams.Controls.Add(this.label11);
            this.gbParams.Controls.Add(this.tbAssemblyTrademark);
            this.gbParams.Controls.Add(this.label10);
            this.gbParams.Controls.Add(this.tbAssemblyCopyright);
            this.gbParams.Controls.Add(this.label9);
            this.gbParams.Controls.Add(this.tbAssemblyProduct);
            this.gbParams.Controls.Add(this.label8);
            this.gbParams.Controls.Add(this.tbAssemblyCompany);
            this.gbParams.Controls.Add(this.label7);
            this.gbParams.Controls.Add(this.tbAssemblyDescription);
            this.gbParams.Controls.Add(this.label6);
            this.gbParams.Controls.Add(this.tbAssemblyTitle);
            this.gbParams.Controls.Add(this.label5);
            this.gbParams.Enabled = false;
            this.gbParams.Location = new System.Drawing.Point(14, 97);
            this.gbParams.Name = "gbParams";
            this.gbParams.Size = new System.Drawing.Size(839, 134);
            this.gbParams.TabIndex = 8;
            this.gbParams.TabStop = false;
            this.gbParams.Text = "参数值";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(46, 27);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(41, 12);
            this.label5.TabIndex = 0;
            this.label5.Text = "标题：";
            // 
            // tbAssemblyTitle
            // 
            this.tbAssemblyTitle.Location = new System.Drawing.Point(93, 22);
            this.tbAssemblyTitle.Name = "tbAssemblyTitle";
            this.tbAssemblyTitle.Size = new System.Drawing.Size(180, 21);
            this.tbAssemblyTitle.TabIndex = 1;
            // 
            // tbAssemblyDescription
            // 
            this.tbAssemblyDescription.Location = new System.Drawing.Point(402, 22);
            this.tbAssemblyDescription.Name = "tbAssemblyDescription";
            this.tbAssemblyDescription.Size = new System.Drawing.Size(180, 21);
            this.tbAssemblyDescription.TabIndex = 3;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(355, 27);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(41, 12);
            this.label6.TabIndex = 2;
            this.label6.Text = "说明：";
            // 
            // tbAssemblyCompany
            // 
            this.tbAssemblyCompany.Location = new System.Drawing.Point(673, 24);
            this.tbAssemblyCompany.Name = "tbAssemblyCompany";
            this.tbAssemblyCompany.Size = new System.Drawing.Size(150, 21);
            this.tbAssemblyCompany.TabIndex = 5;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(626, 29);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(41, 12);
            this.label7.TabIndex = 4;
            this.label7.Text = "公司：";
            // 
            // tbAssemblyProduct
            // 
            this.tbAssemblyProduct.Location = new System.Drawing.Point(93, 49);
            this.tbAssemblyProduct.Name = "tbAssemblyProduct";
            this.tbAssemblyProduct.Size = new System.Drawing.Size(180, 21);
            this.tbAssemblyProduct.TabIndex = 7;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(46, 54);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(41, 12);
            this.label8.TabIndex = 6;
            this.label8.Text = "产品：";
            // 
            // tbAssemblyCopyright
            // 
            this.tbAssemblyCopyright.Location = new System.Drawing.Point(402, 49);
            this.tbAssemblyCopyright.Name = "tbAssemblyCopyright";
            this.tbAssemblyCopyright.Size = new System.Drawing.Size(180, 21);
            this.tbAssemblyCopyright.TabIndex = 9;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(355, 54);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(41, 12);
            this.label9.TabIndex = 8;
            this.label9.Text = "版权：";
            // 
            // tbAssemblyTrademark
            // 
            this.tbAssemblyTrademark.Location = new System.Drawing.Point(673, 51);
            this.tbAssemblyTrademark.Name = "tbAssemblyTrademark";
            this.tbAssemblyTrademark.Size = new System.Drawing.Size(150, 21);
            this.tbAssemblyTrademark.TabIndex = 11;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(626, 56);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(41, 12);
            this.label10.TabIndex = 10;
            this.label10.Text = "商标：";
            // 
            // tbAssemblyVersion1
            // 
            this.tbAssemblyVersion1.Location = new System.Drawing.Point(93, 76);
            this.tbAssemblyVersion1.Name = "tbAssemblyVersion1";
            this.tbAssemblyVersion1.Size = new System.Drawing.Size(40, 21);
            this.tbAssemblyVersion1.TabIndex = 13;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(10, 79);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(77, 12);
            this.label11.TabIndex = 12;
            this.label11.Text = "程序集版本：";
            // 
            // tbAssemblyFileVersion1
            // 
            this.tbAssemblyFileVersion1.Location = new System.Drawing.Point(402, 76);
            this.tbAssemblyFileVersion1.Name = "tbAssemblyFileVersion1";
            this.tbAssemblyFileVersion1.Size = new System.Drawing.Size(40, 21);
            this.tbAssemblyFileVersion1.TabIndex = 18;
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(331, 79);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(65, 12);
            this.label12.TabIndex = 17;
            this.label12.Text = "文件版本：";
            // 
            // btnMake
            // 
            this.btnMake.Location = new System.Drawing.Point(673, 79);
            this.btnMake.Name = "btnMake";
            this.btnMake.Size = new System.Drawing.Size(150, 23);
            this.btnMake.TabIndex = 24;
            this.btnMake.Text = "生成";
            this.btnMake.UseVisualStyleBackColor = true;
            this.btnMake.Click += new System.EventHandler(this.btnMake_Click);
            // 
            // tbAssemblyVersion2
            // 
            this.tbAssemblyVersion2.Location = new System.Drawing.Point(139, 76);
            this.tbAssemblyVersion2.Name = "tbAssemblyVersion2";
            this.tbAssemblyVersion2.Size = new System.Drawing.Size(40, 21);
            this.tbAssemblyVersion2.TabIndex = 14;
            // 
            // tbAssemblyVersion3
            // 
            this.tbAssemblyVersion3.Location = new System.Drawing.Point(187, 76);
            this.tbAssemblyVersion3.Name = "tbAssemblyVersion3";
            this.tbAssemblyVersion3.Size = new System.Drawing.Size(40, 21);
            this.tbAssemblyVersion3.TabIndex = 15;
            // 
            // tbAssemblyVersion4
            // 
            this.tbAssemblyVersion4.Location = new System.Drawing.Point(233, 76);
            this.tbAssemblyVersion4.Name = "tbAssemblyVersion4";
            this.tbAssemblyVersion4.Size = new System.Drawing.Size(40, 21);
            this.tbAssemblyVersion4.TabIndex = 16;
            // 
            // tbAssemblyFileVersion4
            // 
            this.tbAssemblyFileVersion4.Location = new System.Drawing.Point(542, 76);
            this.tbAssemblyFileVersion4.Name = "tbAssemblyFileVersion4";
            this.tbAssemblyFileVersion4.Size = new System.Drawing.Size(40, 21);
            this.tbAssemblyFileVersion4.TabIndex = 21;
            // 
            // tbAssemblyFileVersion3
            // 
            this.tbAssemblyFileVersion3.Location = new System.Drawing.Point(496, 76);
            this.tbAssemblyFileVersion3.Name = "tbAssemblyFileVersion3";
            this.tbAssemblyFileVersion3.Size = new System.Drawing.Size(40, 21);
            this.tbAssemblyFileVersion3.TabIndex = 20;
            // 
            // tbAssemblyFileVersion2
            // 
            this.tbAssemblyFileVersion2.Location = new System.Drawing.Point(448, 76);
            this.tbAssemblyFileVersion2.Name = "tbAssemblyFileVersion2";
            this.tbAssemblyFileVersion2.Size = new System.Drawing.Size(40, 21);
            this.tbAssemblyFileVersion2.TabIndex = 19;
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Font = new System.Drawing.Font("宋体", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label13.ForeColor = System.Drawing.Color.Red;
            this.label13.Location = new System.Drawing.Point(9, 105);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(632, 16);
            this.label13.TabIndex = 25;
            this.label13.Text = "*数值为空则不做改变；版本号必须填写！版本号必须是介于0和65534之间的整数；";
            // 
            // FrmAssemblyInfoBatchEdit
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(881, 450);
            this.Controls.Add(this.gbParams);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.tbInfo);
            this.Controls.Add(this.cbModel);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.tbBatchPath);
            this.Controls.Add(this.tbSinglePath);
            this.Controls.Add(this.btnSingleEdit);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.btnBatchPath);
            this.Controls.Add(this.label1);
            this.MaximizeBox = false;
            this.Name = "FrmAssemblyInfoBatchEdit";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "程序集信息批量修改";
            this.gbParams.ResumeLayout(false);
            this.gbParams.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox tbBatchPath;
        private System.Windows.Forms.Button btnBatchPath;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox tbSinglePath;
        private System.Windows.Forms.Button btnSingleEdit;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.ComboBox cbModel;
        private System.Windows.Forms.TextBox tbInfo;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.GroupBox gbParams;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox tbAssemblyTitle;
        private System.Windows.Forms.TextBox tbAssemblyDescription;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox tbAssemblyCompany;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox tbAssemblyProduct;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.TextBox tbAssemblyCopyright;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.TextBox tbAssemblyTrademark;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.TextBox tbAssemblyVersion1;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.TextBox tbAssemblyFileVersion1;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Button btnMake;
        private System.Windows.Forms.TextBox tbAssemblyVersion3;
        private System.Windows.Forms.TextBox tbAssemblyVersion2;
        private System.Windows.Forms.TextBox tbAssemblyVersion4;
        private System.Windows.Forms.TextBox tbAssemblyFileVersion4;
        private System.Windows.Forms.TextBox tbAssemblyFileVersion3;
        private System.Windows.Forms.TextBox tbAssemblyFileVersion2;
        private System.Windows.Forms.Label label13;
    }
}