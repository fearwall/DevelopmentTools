﻿using CommonLib.Helper.Common;
using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Resources;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace DeveloperKitsWinForm
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            Byte[] bPara = new Byte[102400];
            StringBuilder sb = new StringBuilder(null, 1024);
            StringBuilder outData = new StringBuilder(null, 1024);
            //INIT(ref s[0]);

            try
            {
                //int i = Test();
                //MessageBox.Show(i.ToString() + ";" + s.ToString());

                int i = Class1.INIT(sb);
                MessageBox.Show(i.ToString() + ";" + sb.ToString());
                //int j = Class1.Test();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());
            }
            //MessageBox.Show(j.ToString());
        }

        [DllImport(@"SiInterface.dll")]
        public static extern int Test();


        [DllImport(@"SiInterface.dll")]
        public static extern int INIT(StringBuilder sb);

        private void button2_Click(object sender, EventArgs e)
        {
            DataTable dt = new DataTable();
            dt.TableName = "Test";
            dt.Columns.Add("1", typeof(string));
            dt.Columns.Add("2", typeof(string));
            dt.Rows.Add(new object[] { "A1", "A2" });
            dt.Rows.Add(new object[] { "B1", "B2" });
            //Path path = new Path();
            string outPath = System.AppDomain.CurrentDomain.SetupInformation.ApplicationBase + @" / TestLog/1.xml";
            if (Directory.Exists(Path.GetDirectoryName(outPath)) == false)//如果不存在就创建文件夹
            {
                Directory.CreateDirectory(Path.GetDirectoryName(outPath));
            }
            if (!File.Exists(outPath))
            {
                File.Create(outPath).Close();//创建该文件 
            }
            dt.WriteXml(outPath);
        }

        /// <summary>
        /// 获取exe或动态库中的图像
        /// </summary>
        private static void GetProcedureIcons()
        {
            string appPath = "";
            OpenFileDialog ofd = new OpenFileDialog();    //打开文件窗口  


            DialogResult dr = ofd.ShowDialog();
            if (dr == DialogResult.OK || dr == DialogResult.Yes)
            {
                appPath = ofd.FileName;
            }
            else
            {
                return;
            }

            try
            {
                string strOutPath;
                System.Windows.Forms.FolderBrowserDialog fbd = new System.Windows.Forms.FolderBrowserDialog();
                DialogResult dialogResult = fbd.ShowDialog();
                if (dialogResult == DialogResult.OK || dialogResult == DialogResult.Yes)
                {
                    strOutPath = fbd.SelectedPath;
                }
                else
                {
                    return;
                }
                //判断路径文件夹是否存在
                if (!Directory.Exists(Path.GetDirectoryName(strOutPath)))
                {
                    Directory.CreateDirectory(Path.GetDirectoryName(strOutPath));
                }
                strOutPath += @"/";

                //从Exe里面提取图标和图片
                Assembly assm = Assembly.LoadFrom(appPath);
                HashSet<string> l_strHashType = new HashSet<string>();
                try
                {
                    foreach (string resName in assm.GetManifestResourceNames())
                    {
                        Stream stream = assm.GetManifestResourceStream(resName);
                        ResourceReader rr = new ResourceReader(stream);
                        IDictionaryEnumerator enumerator = rr.GetEnumerator();
                        while (enumerator.MoveNext())
                        {
                            DictionaryEntry de = (DictionaryEntry)enumerator.Current;
                            l_strHashType.Add(de.Value.GetType().ToString());
                            try
                            {
                                switch (de.Value.GetType().ToString())
                                {
                                    case "System.Drawing.Icon":
                                        System.Drawing.Icon ic = (Icon)de.Value;
                                        //路径根据自己需要更改
                                        using (System.IO.Stream sm = new FileStream(strOutPath + de.Key.ToString() + ".ico", FileMode.Create))
                                        {
                                            ic.Save(sm);
                                        }

                                        break;
                                    case "System.Drawing.Bitmap":
                                        System.Drawing.Bitmap icbmp = (Bitmap)de.Value;
                                        icbmp.Save(strOutPath + de.Key.ToString() + ".bmp");
                                        break;
                                    case "System.Windows.Forms.ImageListStreamer":
                                        System.Windows.Forms.ImageListStreamer ils = (System.Windows.Forms.ImageListStreamer)de.Value;
                                        //Image img = Image.FromStream()
                                        ImageList imageList = new ImageList();
                                        imageList.ImageStream = ils;
                                        DateTime dateTime = DateTime.Now;
                                        foreach (Image image in imageList.Images)
                                        {
                                            //ImageFormat imageFormat = new ImageFormat(image.RawFormat.Guid);
                                            string strFormat = string.Empty;
                                            GetImageFormat(image, out strFormat);                                            
                                            string name = strOutPath + dateTime.ToString("MMddHHmmss") + DateTime.Now.Millisecond.ToString() + strFormat;
                                            image.Save(name, image.RawFormat);
                                        }
                                        foreach(string strKey in imageList.Images.Keys)
                                        {
                                            string strKeys = strKey;
                                        }

                                        break;
                                    default:
                                        break;
                                }
                            }
                            catch (Exception ex)
                            {

                            }
                            //de.Key是资源名
                            //de.Value是资源内容
                        }
                    }
                }
                catch (Exception ex)
                {

                }
                MessageBox.Show("图标抽取完成！");
            }
            catch (Exception ex)
            {
                MessageBox.Show("抽取图标出错！" + ex.Message);
            }
        }

        /// <summary>
        /// 提取exe或dll中的图像
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnGetIcon_Click(object sender, EventArgs e)
        {
            GetProcedureIcons();
            ////第一步：获取程序中的图标数

            //int IconCount = ExtractIconEx(appPath, -1, null, null, 0);

            ////第二步：创建存放大/小图标的空间

            //largeIcons = new IntPtr[IconCount];

            //smallIcons = new IntPtr[IconCount];

            ////第三步：抽取所有的大小图标保存到largeIcons和smallIcons中

            //ExtractIconEx(appPath, 0, largeIcons, smallIcons, IconCount);

            ////第四步：显示抽取的图标(推荐使用imageList和listview搭配显示）

            //for (int i = 0; i < IconCount; i++)
            //{
            //    this.imageList1.Images.Add(Icon.FromHandle(largeIcons[i])); //图标添加进imageList中

            //    ListViewItem lvi = new ListViewItem();

            //    lvi.ImageIndex = i;  //listview子项图标索引项

            //    this.listview1.Items.Add(lvi);
            //}

            ////第五步：保存图标

            //string strrOutPath = "";

            ////System.Windows.Forms.FolderBrowserDialog fbd = new System.Windows.Forms.FolderBrowserDialog();
            ////DialogResult dialogResult = fbd.ShowDialog();
            ////if (dialogResult == DialogResult.OK || dialogResult == DialogResult.Yes)
            ////{
            ////    strrOutPath = fbd.SelectedPath;
            ////}
            ////else
            ////{
            ////    return;
            ////}

            //for (int i = 0; i < this.listview1.Items.Count; i++)
            //{
            //    System.IO.FileStream fs = new System.IO.FileStream(Application.StartupPath + "\\newIcon.png", System.IO.FileMode.Create);


            //    this.imageList1.Images[this.listview1.Items[i].ImageIndex].Save(fs, System.Drawing.Imaging.ImageFormat.Png);

            //    fs.Close();
            //}
        }

        [System.Runtime.InteropServices.DllImport("shell32.dll")]
        private static extern int ExtractIconEx(string lpszFile, int niconIndex, IntPtr[] phiconLarge, IntPtr[] phiconSmall, int nIcons);

        private IntPtr[] largeIcons, smallIcons;  //存放大/小图标的指针数组

        //private string appPath = @"D:\Program Files\Tencent\QQ\Bin\QQ.exe";
        /// <summary>
        /// 输出日志
        /// </summary>
        /// <param name="strFileFullPath">输出日志的完整路径（包含文件夹及文件名）</param>
        /// <param name="msg">日志输出的信息</param>
        public static void WriteLog(string strFileFullPath, string msg)
        {
            try
            {
                //强制文件名后缀为.log
                if (!strFileFullPath.ToLower().Contains(".log"))
                {
                    strFileFullPath += ".log";
                }

                //判断路径文件夹是否存在
                if (!Directory.Exists(Path.GetDirectoryName(strFileFullPath)))
                {
                    Directory.CreateDirectory(Path.GetDirectoryName(strFileFullPath));
                }

                //判断文件名称是否存在
                if (!File.Exists(strFileFullPath))
                {
                    //不存在文件
                    File.Create(strFileFullPath).Close();//创建该文件
                }
                StreamWriter writer = File.AppendText(strFileFullPath);//文件中添加文件流  
                try
                {
                    writer.WriteLine(DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss") + " " + msg);

                }
                catch (Exception e)
                {

                }
                finally
                {
                    writer.Flush();
                    writer.Close();
                }
            }
            catch (Exception ex)
            {
                throw new Exception("日志输出失败！" + ex.ToString());
            }
        }

        private void button3_Click(object sender, EventArgs e)
        {
            Dictionary<string, string> pairs = new Dictionary<string, string>()
            {
                { "1","v1"},
                { "3","v2"},
                { "3","v3"},
                { "2","v4"},
                { "5","v5"},
            };
            List<string> list = pairs.Keys.ToList();

            string strOut = string.Empty;
            MessageBox.Show(string.Join("\r\n",list.ToArray()));
        }

        private Icon normal = Properties.Resources.IcoNormal;
        private Icon blank = Properties.Resources.IcoLogo;
        private bool _isBlink = false;
        private bool _status = true;
        private void btnNotifyIcon_Click(object sender, EventArgs e)
        {
            if (timer1.Enabled == false)
            {
                _isBlink = true;
                timer1.Enabled = true;
                //timer1.Start();
                notifyIcon1.ShowBalloonTip(10000, "提示", "这是一条来自托盘的提示消息！", ToolTipIcon.Info);
            }
            else
            {
                _isBlink = false;
                timer1.Enabled = false;
                //timer1.Stop();
                notifyIcon1.ShowBalloonTip(5000, "提示", "关闭闪烁效果！", ToolTipIcon.Info);
                notifyIcon1.Icon = blank;
            }
        }
        private void timer1_Tick(object sender, EventArgs e)
        {
            if (notifyIcon1.Icon == blank)
                notifyIcon1.Icon = normal;
            else
                notifyIcon1.Icon = blank;            
        }

        
        public string text = "csdn baihe_591";
        private void Form1_Load(object sender, EventArgs e)
        {
            //this.label.Location = new Point(149, 13);
            //this.label.Size = new Size(134, 16);
            //this.Controls.Add(label);
            this.label1.Text = text;
            this.timer2.Enabled = true;
            this.timer2.Interval = 500;
            p = new PointF(this.label1.Size.Width, 0);
        }
        PointF p;
        Font f = new Font("宋体", 10);
        Color c = Color.FromArgb(224, 238, 255);
        string temp;

        private void timer2_Tick(object sender, EventArgs e)
        {
            Graphics g = this.label1.CreateGraphics();
            SizeF s = new SizeF();
            s = g.MeasureString(text, f);//测量文字长度  
            Brush brush = Brushes.Black;
            g.Clear(c);//清除背景  

            if (temp != text)//文字改变时,重新显示  
            {
                p = new PointF(this.label1.Size.Width, 0);
                temp = text;
            }
            else
                p = new PointF(p.X - 20, 0);//每次偏移10  
            if (p.X <= -s.Width)
                p = new PointF(this.label1.Size.Width, 0);
            g.DrawString(text, f, brush, p);
        }


        private void MySqlTest()
        {
            //string connetStr = "data source=127.0.0.1;database=Test; uid=root;pwd=root;Allow User Variables=True;";
            //MySqlConnection conn = new MySqlConnection(connetStr);
            //try
            //{
            //    conn.Open();
            //    string sql = "set @systemDatetime = SYSDATE() ";//在sql语句中定义parameter，然后再给parameter赋值
            //    MySqlCommand cmd = new MySqlCommand(sql, conn);
            //    //MySqlParameter mpName = new MySqlParameter("Name", MySqlDbType.VarChar, 50);
            //    //mpName.Value = "Tests";
            //    //mpName.Direction = ParameterDirection.Input;
            //    MySqlParameter mpId = new MySqlParameter("systemDatetime", MySqlDbType.DateTime);
            //    mpId.Direction = ParameterDirection.InputOutput;
            //    //mpId.Value = 0;
            //    //cmd.Parameters.Add(mpName);
            //    cmd.Parameters.Add(mpId);
            //    object obj = cmd.ExecuteNonQuery();
            //    Console.WriteLine("Values: " + mpId.Value);
                
            //    cmd.Parameters.Clear();
            //    //conn.Close();
            //}
            //catch (Exception ex)
            //{
            //    Console.WriteLine(ex.ToString());
            //    //throw new Exception("Mysql执行出错:\r\n" + ex.Message, ex);
            //}
            //finally
            //{
            //    conn.Close();
            //}
        }

        private void button4_Click(object sender, EventArgs e)
        {
            MySqlTest();
        }

        private void btnAssemblyInfoBatchEdit_Click(object sender, EventArgs e)
        {
            new FrmAssemblyInfoBatchEdit().ShowDialog();
        }

        private void button5_Click(object sender, EventArgs e)
        {
            new FrmBuild().ShowDialog();
        }

        private void button6_Click(object sender, EventArgs e)
        {
            //string s = FileSearchHelper.GetAssignExtensionNameRegexRules(new List<string>() { "exe", "txt" });
            Assembly myAssembly = Assembly.Load(@"C:\Users\lfc_l\Desktop\MSBuild\Extract\RDBaseSys.dll");
            System.Resources.ResourceManager myManager = new System.Resources.ResourceManager(@"C:\Users\lfc_l\Desktop\MSBuild\Extract\RDBaseSys.dll.Properties.Resources", myAssembly);
            System.Drawing.Image myImage = (System.Drawing.Image)myManager.GetObject("HW");
        }

        private void button7_Click(object sender, EventArgs e)
        {
            new FrmWebBrowser().ShowDialog();
        }

        int iButtonClickCount = 0;
        /// <summary>
        /// 窗口句柄捕获
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void button8_Click(object sender, EventArgs e)
        {
            iButtonClickCount++;
            FileHelper.WriteLog("点击按钮第：" + iButtonClickCount.ToString().PadLeft(3, '0') + "次");
            thread = new Thread(myStaticThreadMethod);
            thread.Start();

            int i = WindowCatchTest.ShowTestWindow();

            if(i >= 1)
            {
                MessageBox.Show("窗体捕获关闭成功！");
            }
            else
            {
                MessageBox.Show("窗体捕获关闭失败！");
            }
            thread.Abort();
        }

        System.Threading.Timer timerWindowCatch = null;

        public static void myStaticThreadMethod(object obj)
        {
            string strWindowTitle = "读卡窗口";
            IntPtr ParenthWnd = new IntPtr(0);
            int i = ParenthWnd.ToInt32();
            while (ParenthWnd.ToInt32() <= 0)
            {
                ParenthWnd = WindowCatchHelper.FindWindow(null, strWindowTitle);
            }

            if(ParenthWnd.ToInt32() > 0)
            {
                string s = ParenthWnd.ToInt32().ToString();
            }
            WindowCatchHelper.CloseWindow(ParenthWnd).ToString();
            //MessageBox.Show(WindowCatchHelper.CloseWindow(ParenthWnd).ToString());
        }

        Thread thread = new Thread(myStaticThreadMethod);

        private void button9_Click(object sender, EventArgs e)
        {
            MessageBox.Show(new UnitTestClass().Addition(1, 2).ToString());
        }

        private void UnitTest(string str)
        {

        }

        private void button10_Click(object sender, EventArgs e)
        {
            try
            {
                decimal d1 = Convert.ToDecimal(textBox1.Text);
                decimal d2 = Convert.ToDecimal(textBox2.Text);
                decimal d3 = Convert.ToDecimal(textBox3.Text);
                decimal res = d1 + (d2 + d3 / 60) / 60;
                textBox4.Text = res.ToString();
            }
            catch(Exception ex)
            {
                MessageBox.Show($"计算失败!{ex.Message}");
            }
        }

        /// <summary>
        /// 获取Image图片格式
        /// </summary>
        /// <param name="_img"></param>
        /// <param name="format"></param>
        /// <returns></returns>
        private static System.Drawing.Imaging.ImageFormat GetImageFormat(Image _img, out string format)
        {
            if (_img.RawFormat.Guid.Equals(System.Drawing.Imaging.ImageFormat.Bmp.Guid))
            {
                format = ".bmp";
                return System.Drawing.Imaging.ImageFormat.Bmp;
            }
            if (_img.RawFormat.Guid.Equals(System.Drawing.Imaging.ImageFormat.Emf.Guid))
            {
                format = ".emf";
                return System.Drawing.Imaging.ImageFormat.Emf;
            }
            if (_img.RawFormat.Guid.Equals(System.Drawing.Imaging.ImageFormat.Exif.Guid))
            {
                format = ".exif";
                return System.Drawing.Imaging.ImageFormat.Exif;
            }
            if (_img.RawFormat.Guid.Equals(System.Drawing.Imaging.ImageFormat.Gif.Guid))
            {
                format = ".gif";
                return System.Drawing.Imaging.ImageFormat.Gif;
            }
            if (_img.RawFormat.Guid.Equals(System.Drawing.Imaging.ImageFormat.Icon.Guid))
            {
                format = ".icon";
                return System.Drawing.Imaging.ImageFormat.Icon;
            }
            if (_img.RawFormat.Guid.Equals(System.Drawing.Imaging.ImageFormat.Jpeg.Guid))
            {
                format = ".jpg";
                return System.Drawing.Imaging.ImageFormat.Jpeg;
            }
            if (_img.RawFormat.Guid.Equals(System.Drawing.Imaging.ImageFormat.MemoryBmp.Guid))
            {
                format = ".bmp";
                return System.Drawing.Imaging.ImageFormat.MemoryBmp;
            }
            if (_img.RawFormat.Guid.Equals(System.Drawing.Imaging.ImageFormat.Png.Guid))
            {
                format = ".png";
                return System.Drawing.Imaging.ImageFormat.Png;
            }
            format = ".png";
            return null;
        }
    }
}
