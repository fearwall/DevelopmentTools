﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DeveloperKitsWinForm
{
    /// <summary>
    /// 单元测试类
    /// </summary>
    public class UnitTestClass
    {
        public int Addition(int i1,int i2)
        {
            return i1 + i2;
        }
    }
}
