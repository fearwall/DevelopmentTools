﻿namespace DeveloperKitsWinForm
{
    partial class FrmBuild
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.tbBatchPath = new System.Windows.Forms.TextBox();
            this.btnBatchPath = new System.Windows.Forms.Button();
            this.label2 = new System.Windows.Forms.Label();
            this.tbSinglePath = new System.Windows.Forms.TextBox();
            this.btnSingleEdit = new System.Windows.Forms.Button();
            this.label3 = new System.Windows.Forms.Label();
            this.cbModel = new System.Windows.Forms.ComboBox();
            this.tbInfo = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.btnMake = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(24, 45);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(77, 12);
            this.label1.TabIndex = 2;
            this.label1.Text = "项目文件夹：";
            // 
            // tbBatchPath
            // 
            this.tbBatchPath.Location = new System.Drawing.Point(107, 42);
            this.tbBatchPath.Name = "tbBatchPath";
            this.tbBatchPath.Size = new System.Drawing.Size(674, 21);
            this.tbBatchPath.TabIndex = 3;
            // 
            // btnBatchPath
            // 
            this.btnBatchPath.Location = new System.Drawing.Point(778, 41);
            this.btnBatchPath.Name = "btnBatchPath";
            this.btnBatchPath.Size = new System.Drawing.Size(75, 23);
            this.btnBatchPath.TabIndex = 4;
            this.btnBatchPath.Text = "选择";
            this.btnBatchPath.UseVisualStyleBackColor = true;
            this.btnBatchPath.Click += new System.EventHandler(this.btnBatchPath_Click);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(36, 76);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(65, 12);
            this.label2.TabIndex = 5;
            this.label2.Text = "输出路径：";
            // 
            // tbSinglePath
            // 
            this.tbSinglePath.Location = new System.Drawing.Point(107, 72);
            this.tbSinglePath.Name = "tbSinglePath";
            this.tbSinglePath.Size = new System.Drawing.Size(674, 21);
            this.tbSinglePath.TabIndex = 6;
            // 
            // btnSingleEdit
            // 
            this.btnSingleEdit.Location = new System.Drawing.Point(778, 71);
            this.btnSingleEdit.Name = "btnSingleEdit";
            this.btnSingleEdit.Size = new System.Drawing.Size(75, 23);
            this.btnSingleEdit.TabIndex = 7;
            this.btnSingleEdit.Text = "选择";
            this.btnSingleEdit.UseVisualStyleBackColor = true;
            this.btnSingleEdit.Click += new System.EventHandler(this.btnSingleEdit_Click);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(36, 17);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(65, 12);
            this.label3.TabIndex = 0;
            this.label3.Text = "生成列表：";
            // 
            // cbModel
            // 
            this.cbModel.FormattingEnabled = true;
            this.cbModel.Location = new System.Drawing.Point(107, 14);
            this.cbModel.Name = "cbModel";
            this.cbModel.Size = new System.Drawing.Size(674, 20);
            this.cbModel.TabIndex = 1;
            this.cbModel.SelectedIndexChanged += new System.EventHandler(this.cbModel_SelectedIndexChanged);
            // 
            // tbInfo
            // 
            this.tbInfo.Location = new System.Drawing.Point(14, 130);
            this.tbInfo.Multiline = true;
            this.tbInfo.Name = "tbInfo";
            this.tbInfo.ReadOnly = true;
            this.tbInfo.Size = new System.Drawing.Size(839, 311);
            this.tbInfo.TabIndex = 10;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(36, 102);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(65, 12);
            this.label4.TabIndex = 9;
            this.label4.Text = "日志信息：";
            // 
            // btnMake
            // 
            this.btnMake.Location = new System.Drawing.Point(778, 13);
            this.btnMake.Name = "btnMake";
            this.btnMake.Size = new System.Drawing.Size(75, 23);
            this.btnMake.TabIndex = 11;
            this.btnMake.Text = "生成";
            this.btnMake.UseVisualStyleBackColor = true;
            this.btnMake.Click += new System.EventHandler(this.btnMake_Click_1);
            // 
            // FrmBuild
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(881, 450);
            this.Controls.Add(this.cbModel);
            this.Controls.Add(this.btnMake);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.tbInfo);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.tbBatchPath);
            this.Controls.Add(this.tbSinglePath);
            this.Controls.Add(this.btnSingleEdit);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.btnBatchPath);
            this.Controls.Add(this.label1);
            this.MaximizeBox = false;
            this.Name = "FrmBuild";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "编译文件批量生成";
            this.Load += new System.EventHandler(this.FrmBuild_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox tbBatchPath;
        private System.Windows.Forms.Button btnBatchPath;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox tbSinglePath;
        private System.Windows.Forms.Button btnSingleEdit;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.ComboBox cbModel;
        private System.Windows.Forms.TextBox tbInfo;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Button btnMake;
    }
}