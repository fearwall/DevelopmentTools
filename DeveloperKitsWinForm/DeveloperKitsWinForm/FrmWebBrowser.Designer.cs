﻿namespace DeveloperKitsWinForm
{
    partial class FrmWebBrowser
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmWebBrowser));
            this.tbAddress = new System.Windows.Forms.TextBox();
            this.plTop = new System.Windows.Forms.Panel();
            this.btnJump = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.wbView = new System.Windows.Forms.WebBrowser();
            this.plTop.SuspendLayout();
            this.SuspendLayout();
            // 
            // tbAddress
            // 
            this.tbAddress.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tbAddress.Location = new System.Drawing.Point(41, 0);
            this.tbAddress.Name = "tbAddress";
            this.tbAddress.Size = new System.Drawing.Size(684, 21);
            this.tbAddress.TabIndex = 7;
            this.tbAddress.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.tbAddress_KeyPress);
            // 
            // plTop
            // 
            this.plTop.Controls.Add(this.tbAddress);
            this.plTop.Controls.Add(this.btnJump);
            this.plTop.Controls.Add(this.label1);
            this.plTop.Dock = System.Windows.Forms.DockStyle.Top;
            this.plTop.Location = new System.Drawing.Point(0, 0);
            this.plTop.Name = "plTop";
            this.plTop.Size = new System.Drawing.Size(800, 21);
            this.plTop.TabIndex = 8;
            // 
            // btnJump
            // 
            this.btnJump.Dock = System.Windows.Forms.DockStyle.Right;
            this.btnJump.Location = new System.Drawing.Point(725, 0);
            this.btnJump.Name = "btnJump";
            this.btnJump.Size = new System.Drawing.Size(75, 21);
            this.btnJump.TabIndex = 9;
            this.btnJump.Text = "转到 →";
            this.btnJump.UseVisualStyleBackColor = true;
            this.btnJump.Click += new System.EventHandler(this.btnJump_Click);
            // 
            // label1
            // 
            this.label1.Dock = System.Windows.Forms.DockStyle.Left;
            this.label1.Location = new System.Drawing.Point(0, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(41, 21);
            this.label1.TabIndex = 8;
            this.label1.Text = "URL：";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // wbView
            // 
            this.wbView.Dock = System.Windows.Forms.DockStyle.Fill;
            this.wbView.Location = new System.Drawing.Point(0, 21);
            this.wbView.MinimumSize = new System.Drawing.Size(20, 20);
            this.wbView.Name = "wbView";
            this.wbView.Size = new System.Drawing.Size(800, 429);
            this.wbView.TabIndex = 9;
            // 
            // FrmWebBrowser
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.wbView);
            this.Controls.Add(this.plTop);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "FrmWebBrowser";
            this.Text = "FrmWebBrowser";
            this.plTop.ResumeLayout(false);
            this.plTop.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TextBox tbAddress;
        private System.Windows.Forms.Panel plTop;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button btnJump;
        private System.Windows.Forms.WebBrowser wbView;
    }
}