﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace DeveloperKitsWinForm
{
    public partial class FrmAssemblyInfoBatchEdit : Form
    {
        /// <summary>
        /// 批量文件路径
        /// </summary>
        List<string> listBatchFilePath = new List<string>();

        /// <summary>
        /// 日志StringBuilder
        /// </summary>
        StringBuilder sbLogs = new StringBuilder();

        public FrmAssemblyInfoBatchEdit()
        {
            InitializeComponent();
        }

        private void cbModel_SelectedIndexChanged(object sender, EventArgs e)
        {
            if(cbModel.SelectedIndex == 0)
            {
                btnBatchPath.Enabled = true;
                btnSingleEdit.Enabled = false;
            }
            else if(cbModel.SelectedIndex == 1)
            {
                btnBatchPath.Enabled = false;
                btnSingleEdit.Enabled = true;
            }
        }

        private void btnBatchPath_Click(object sender, EventArgs e)
        {
            sbLogs.Clear();
            string strFolderPath = CommonLib.Helper.Common.FilePathHelper.FolderPathSelect(tbBatchPath.Text.Trim(),true);
            if (string.IsNullOrEmpty(strFolderPath))
            {
                MessageBox.Show("请选择批量生成的路径！", "错误", MessageBoxButtons.OK, MessageBoxIcon.Error);
                listBatchFilePath.Clear();
                gbParams.Enabled = false;
                return;
            }
            else
            {
                listBatchFilePath = CommonLib.Helper.Common.FileSearchHelper.GetFileSystemEntries(strFolderPath, "AssemblyInfo.cs", true, false).ToList();
                if(listBatchFilePath.Count <= 0)
                {
                    MessageBox.Show("选中的路径无可操作的指定文件【AssemblyInfo.cs】！", "错误", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    listBatchFilePath.Clear();
                    gbParams.Enabled = false;
                    return;
                }
                gbParams.Enabled = true;
                tbBatchPath.Text = strFolderPath;
                sbLogs.AppendLine("读取文件路径【" + strFolderPath + "】成功！共获取到" + listBatchFilePath.Count +"个文件！");
            }
            ShowLogs();
        }

        private void btnSingleEdit_Click(object sender, EventArgs e)
        {
            sbLogs.Clear();
            string strFilePath = "";
            if (string.IsNullOrEmpty(tbSinglePath.Text.Trim()))
            {
                strFilePath = CommonLib.Helper.Common.FilePathHelper.FilePathSelect("程序集信息文件|AssemblyInfo.cs");
                if (string.IsNullOrEmpty(strFilePath) || !strFilePath.Contains("AssemblyInfo.cs"))
                {
                    MessageBox.Show("请选择指定的文件【AssemblyInfo.cs】！", "错误", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    gbParams.Enabled = false;
                    return;
                }
                tbSinglePath.Text = strFilePath;
            }
            else
            {
                strFilePath = tbSinglePath.Text.Trim();
            }

            if (!File.Exists(strFilePath) && !strFilePath.Contains("AssemblyInfo.cs"))
            {
                MessageBox.Show("请选择指定的文件【AssemblyInfo.cs】！", "错误", MessageBoxButtons.OK, MessageBoxIcon.Error);
                gbParams.Enabled = false;
                return;
            }
            gbParams.Enabled = true;
            //读取文件参数到表格中
            ClearControlValue();
            AssemblyInfoLoadToControl(ReadAssemblyInfo(strFilePath));
            sbLogs.AppendLine("读取文件【" + strFilePath + "】成功！");
            ShowLogs();
        }

        private void ShowLogs()
        {
            tbInfo.Text = sbLogs.ToString();
            tbInfo.SelectionStart = tbInfo.Text.Length;
        }

        /// <summary>
        /// 校验参数
        /// </summary>
        /// <returns></returns>
        private bool ValidateParams()
        {
            if (StringToVersion(tbAssemblyVersion1.Text.Trim()) < 0 
                || StringToVersion(tbAssemblyVersion1.Text.Trim()) < 0
                || StringToVersion(tbAssemblyVersion2.Text.Trim()) < 0
                || StringToVersion(tbAssemblyVersion3.Text.Trim()) < 0
                || StringToVersion(tbAssemblyVersion4.Text.Trim()) < 0
                || StringToVersion(tbAssemblyFileVersion1.Text.Trim()) < 0
                || StringToVersion(tbAssemblyFileVersion2.Text.Trim()) < 0
                || StringToVersion(tbAssemblyFileVersion3.Text.Trim()) < 0
                || StringToVersion(tbAssemblyFileVersion4.Text.Trim()) < 0)
            {
                MessageBox.Show("请输入正确的版本号,版本号各个参数必须是介于0和65534之间的整数！", "错误", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return false;
            }
            return true;
        }

        /// <summary>
        /// 字符串转换为版本号的正整数，转换失败返回-1
        /// </summary>
        /// <param name="str"></param>
        /// <returns></returns>
        private int StringToVersion(string str)
        {
            try
            {
                int i = Convert.ToInt32(str);
                if(i >= 0 && i <= 65534)
                {
                    return i;
                }
                else
                {
                    return -1;
                }
            }
            catch
            {
                return -1;
            }
        }

        /// <summary>
        /// 读取程序及文件信息
        /// </summary>
        /// <param name="strFilePath"></param>
        /// <returns></returns>
        private DataTable ReadAssemblyInfo(string strFilePath)
        {
            DataTable dataTable = new DataTable("AssemblyInfo.cs");
            dataTable.Columns.Add("LineCode", typeof(int));
            dataTable.Columns.Add("Name", typeof(string));
            dataTable.Columns.Add("Key", typeof(string));
            dataTable.Columns.Add("Value", typeof(string));
            dataTable.Columns.Add("Content", typeof(string));
            try
            {
                FileStream fs = new FileStream(strFilePath, FileMode.Open, FileAccess.Read);
                StreamReader m_streamReader = new StreamReader(fs, System.Text.Encoding.Default);
                //使用StreamReader类来读取文件
                m_streamReader.BaseStream.Seek(0, SeekOrigin.Begin);
                //从数据流中读取每一行，直到文件的最后一行，并在textBox中显示出内容，其中textBox为文本框，如果不用可以改为别的
                string strLine = m_streamReader.ReadLine();
                int LineCode = 0;
                string strName = "";
                string strKey = "";
                string strValue = "";
                while (strLine != null)
                {
                    DataRow row = dataTable.NewRow();
                    LineCode++;
                    ///this.txtFileContent.Text += strLine + "\n";
                    if (!strLine.Trim().Contains(@"//"))
                    {
                        if (strLine.Contains("AssemblyTitle"))
                        {
                            strName = "标题";
                            strKey = "AssemblyTitle";
                        }
                        else if (strLine.Contains("AssemblyDescription"))
                        {
                            strName = "说明";
                            strKey = "AssemblyDescription";
                        }
                        else if (strLine.Contains("AssemblyCompany"))
                        {
                            strName = "公司";
                            strKey = "AssemblyCompany";
                        }
                        else if (strLine.Contains("AssemblyProduct"))
                        {
                            strName = "产品";
                            strKey = "AssemblyProduct";
                        }
                        else if (strLine.Contains("AssemblyCopyright"))
                        {
                            strName = "版权";
                            strKey = "AssemblyCopyright";
                        }
                        else if (strLine.Contains("AssemblyTrademark"))
                        {
                            strName = "商标";
                            strKey = "AssemblyTrademark";
                        }
                        //else if (strLine.Contains("Guid"))
                        //{
                        //    strName = "Guid";
                        //    strKey = "Guid";
                        //}
                        else if (strLine.Contains("AssemblyVersion"))
                        {
                            strName = "程序集版本";
                            strKey = "AssemblyVersion";
                        }
                        else if (strLine.Contains("AssemblyFileVersion"))
                        {
                            strName = "文件版本";
                            strKey = "AssemblyFileVersion";
                        }
                        else
                        {
                            strName = "";
                            strKey = "";
                            strValue = "";
                        }
                        if (!string.IsNullOrEmpty(strKey))
                        {
                            try
                            {
                                int keyLength = strKey.Length;
                                int startIndex = strLine.IndexOf(strKey + "(\"") + keyLength + 2;
                                strValue = strLine.Substring(startIndex, strLine.IndexOf("\")") - startIndex);
                            }
                            catch
                            {

                            }
                        }
                    }
                    row.ItemArray = new object[] { LineCode,strName,strKey,strValue, strLine };
                    dataTable.Rows.Add(row);
                    strLine = m_streamReader.ReadLine();
                }
                //关闭此StreamReader对象
                m_streamReader.Close();
                fs.Close();
            }
            catch(Exception ex)
            {
                sbLogs.AppendLine("【ERROR】 读取文件【" + strFilePath + "】程序集信息出错！");
                sbLogs.AppendLine("\t" + ex.Message);
                //this.Close();
            }
            finally
            {

            }

            return dataTable;
        }

        /// <summary>
        /// 写入程序文件信息
        /// </summary>
        /// <param name="dataTable"></param>
        /// <param name="strFilePath"></param>
        private void WriteAssemblyInfo(DataTable dataTable, string strFilePath)
        {
            try
            {
                StringBuilder sbContentAll = new StringBuilder();
                foreach (DataRow row in dataTable.Rows)
                {
                    string strNewValue = string.Empty;
                    if (row["Key"].ToString().Equals("AssemblyTitle") && !string.IsNullOrEmpty(tbAssemblyTitle.Text))
                    {
                        strNewValue = tbAssemblyTitle.Text.Trim();
                    }
                    else if (row["Key"].ToString().Equals("AssemblyDescription") && !string.IsNullOrEmpty(tbAssemblyDescription.Text))
                    {
                        strNewValue = tbAssemblyDescription.Text.Trim();
                    }
                    else if (row["Key"].ToString().Equals("AssemblyCompany") && !string.IsNullOrEmpty(tbAssemblyCompany.Text))
                    {
                        strNewValue = tbAssemblyCompany.Text.Trim();
                    }
                    else if (row["Key"].ToString().Equals("AssemblyProduct") && !string.IsNullOrEmpty(tbAssemblyProduct.Text))
                    {
                        strNewValue = tbAssemblyProduct.Text.Trim();
                    }
                    else if (row["Key"].ToString().Equals("AssemblyCopyright") && !string.IsNullOrEmpty(tbAssemblyCopyright.Text))
                    {
                        strNewValue = tbAssemblyCopyright.Text.Trim();
                    }
                    else if (row["Key"].ToString().Equals("AssemblyTrademark") && !string.IsNullOrEmpty(tbAssemblyTrademark.Text))
                    {
                        strNewValue = tbAssemblyTrademark.Text.Trim();
                    }
                    else if (row["Key"].ToString().Equals("AssemblyVersion"))
                    {
                        //版本号
                        strNewValue = tbAssemblyVersion1.Text.Trim()
                                        + "." + tbAssemblyVersion2.Text.Trim()
                                        + "." + tbAssemblyVersion3.Text.Trim()
                                        + "." + tbAssemblyVersion4.Text.Trim();
                    }
                    else if (row["Key"].ToString().Equals("AssemblyFileVersion"))
                    {
                        //文件版本号
                        strNewValue = tbAssemblyFileVersion1.Text.Trim()
                                        + "." + tbAssemblyFileVersion2.Text.Trim()
                                        + "." + tbAssemblyFileVersion3.Text.Trim()
                                        + "." + tbAssemblyFileVersion4.Text.Trim();
                    }
                    else
                    {
                        strNewValue = "";
                    }

                    if (!string.IsNullOrEmpty(strNewValue))
                    {
                        if (string.IsNullOrEmpty(row["Value"].ToString()))
                        {
                            string strKey = row["Key"].ToString();
                            int keyLength = strKey.Length;
                            int startIndex = row["Content"].ToString().IndexOf(strKey + "(\"") + keyLength + 2;
                            string strNewContent = row["Content"].ToString().Insert(startIndex, strNewValue);
                            row["Content"] = strNewContent;
                        }
                        else
                        {
                            row["Content"] = row["Content"].ToString().Replace(row["Value"].ToString(), strNewValue);
                        }
                    }

                    //writer.WriteLine(row["Content"].ToString());
                    sbContentAll.AppendLine(row["Content"].ToString());
                }
                //writer.WriteLine(DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss") + " " + msg);
                try
                {
                    //覆盖文件内容
                    File.WriteAllText(strFilePath, sbContentAll.ToString(), System.Text.Encoding.Default);
                }
                catch (Exception e)

                {
                    sbLogs.Append("【ERROR】 写入文件【" + strFilePath + "】出错！");
                    sbLogs.Append("\t" + e.Message);
                }
                

            }
            catch (Exception ex)
            {
                sbLogs.Append("【ERROR】 写入文件【" + strFilePath + "】出错！");
                sbLogs.Append("\t" + ex.Message);
                //this.Close();
            }
            finally
            {

            }
        }

        /// <summary>
        /// 程序集信息加载到控件
        /// </summary>
        /// <param name="table"></param>
        private void AssemblyInfoLoadToControl(DataTable table)
        {
            foreach(DataRow row in table.Rows)
            {
                string strKey = string.Empty;
                strKey = row["Key"].ToString();
                if (strKey.Equals("AssemblyTitle"))
                {
                    //标题
                    tbAssemblyTitle.Text = row["Value"].ToString();
                }
                else if (strKey.Equals("AssemblyDescription"))
                {
                    //说明   
                    tbAssemblyDescription.Text = row["Value"].ToString();
                }
                else if (strKey.Equals("AssemblyCompany"))
                {
                    //公司   
                    tbAssemblyCompany.Text = row["Value"].ToString();
                }
                else if (strKey.Equals("AssemblyProduct"))
                {
                    //产品   
                    tbAssemblyProduct.Text = row["Value"].ToString();
                }
                else if (strKey.Equals("AssemblyCopyright"))
                {
                    //版权   
                    tbAssemblyCopyright.Text = row["Value"].ToString();
                }
                else if (strKey.Equals("AssemblyTrademark"))
                {
                    //商标   
                    tbAssemblyTrademark.Text = row["Value"].ToString();
                }
                //else if (strKey.Equals("Guid"))
                //{
                //    //Guid   
                //    tbGuid.Text = row["Value"].ToString();
                //}
                else if (strKey.Equals("AssemblyVersion"))
                {
                    //程序集版本   
                    List<string> listVersion = row["Value"].ToString().Split('.').ToList() ;
                    tbAssemblyVersion1.Text = listVersion[0];
                    tbAssemblyVersion2.Text = listVersion[1];
                    tbAssemblyVersion3.Text = listVersion[2];
                    tbAssemblyVersion4.Text = listVersion[3];
                }
                else if (strKey.Equals("AssemblyFileVersion"))
                {
                    //文件版本                       
                    List<string> listVersion = row["Value"].ToString().Split('.').ToList() ;
                    tbAssemblyFileVersion1.Text = listVersion[0];
                    tbAssemblyFileVersion2.Text = listVersion[1];
                    tbAssemblyFileVersion3.Text = listVersion[2];
                    tbAssemblyFileVersion4.Text = listVersion[3];
                }
            }
        }

        /// <summary>
        /// 清空控件的值
        /// </summary>
        private void ClearControlValue()
        {
            tbAssemblyTitle.Text = string.Empty;
            tbAssemblyDescription.Text = string.Empty;
            tbAssemblyCompany.Text = string.Empty;
            tbAssemblyProduct.Text = string.Empty;
            tbAssemblyCopyright.Text = string.Empty;
            tbAssemblyTrademark.Text = string.Empty;
            //tbGuid.Text = string.Empty;
            tbAssemblyVersion1.Text = string.Empty;
            tbAssemblyVersion2.Text = string.Empty;
            tbAssemblyVersion3.Text = string.Empty;
            tbAssemblyVersion4.Text = string.Empty;
            tbAssemblyFileVersion1.Text = string.Empty;
            tbAssemblyFileVersion2.Text = string.Empty;
            tbAssemblyFileVersion3.Text = string.Empty;
            tbAssemblyFileVersion4.Text = string.Empty;
        }

        private void btnMake_Click(object sender, EventArgs e)
        {
            if (!Validate())
            {
                return;
            }
            if(cbModel.SelectedIndex == 0)
            {
                //多文件模式
                foreach (string filePath in listBatchFilePath)
                {
                    WriteAssemblyInfo(ReadAssemblyInfo(filePath), filePath);
                }
            }
            else if(cbModel.SelectedIndex == 1)
            {
                //单文件模式
                WriteAssemblyInfo(ReadAssemblyInfo(tbSinglePath.Text), tbSinglePath.Text);
            }
            sbLogs.Append("操作完毕！");
            MessageBox.Show("操作完毕！", "提示", MessageBoxButtons.OK, MessageBoxIcon.Information);
            ShowLogs();
        }
    }
}
