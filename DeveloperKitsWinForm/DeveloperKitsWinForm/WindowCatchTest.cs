﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace DeveloperKitsWinForm
{
    public class WindowCatchTest
    {
        public static int ShowTestWindow()
        {
            FrmWindowCatch frm = new FrmWindowCatch();
            Random rd = new Random();
            Thread.Sleep(rd.Next(500, 2500));
            frm.ShowDialog();
            return frm.ReturnParam;
        }
    }
}
