﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sample.DesignMode.SingletonMode
{
    public class Singleton
    {
        private static Singleton instance = null;

        /// <summary>
        /// 私有构造方法 防止被实例化
        /// </summary>
        private Singleton()
        {

        }

        /// <summary>
        /// 静态工程方法，创建实例
        /// </summary>
        /// <returns></returns>
        public static Singleton GetSingleton()
        {
            //if(instance == null)
            //{
            //    instance = new Singleton();
            //}
            //return instance;


            if (instance == null)
            {
                lock(instance) {
                    if (instance == null)
                    {
                        instance = new Singleton();
                    }
                }
            }
            return instance;
        }


    }
}
