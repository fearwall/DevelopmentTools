﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sample.DesignMode.Decorator
{
    /// <summary>
    /// 该抽象类就是装饰接口的定义，该类型就相当于是Decorator类型，如果需要具体的功能，可以子类化该类型
    /// 持有一个构件（Component）对象的实例，并实现一个与抽象构件接口一致的接口。
    /// </summary>
    public abstract class DecorationStrategy : House
    {
        //通过组合方式引用Decorator类型，该类型实施具体功能的增加
        //这是关键点之一，包含关系，体现为Has-a
        protected House _house;

        //通过构造器注入，初始化平台实现
        protected DecorationStrategy(House house)
        {
            this._house = house;
        }

        public override void Renovation()
        {
            //该方法就相当于Decorator类型的Operation方法
            if (this._house != null)
            {
                this._house.Renovation();
            }
        }
    }
}
