﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sample.DesignMode.Bridge
{
    /// <summary>
    /// 1.该抽象类就是实现接口的定义，该类型就相当于是Implementor类型
    /// </summary>
    public abstract class PlatformImplementor
    {
        //该方法就相当于Implementor类型的OperationImpl方法
        public abstract void Process();
    }
}
