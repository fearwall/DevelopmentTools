﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sample.DesignMode.Bridge
{
    /// <summary>
    /// 2.该抽象类就是抽象接口的定义，该类型就相当于是抽象角色(Abstraction)类型 
    /// </summary>
    public abstract class DataBase
    {
        //通过组合方式引用平台接口，此处就是桥梁，该类型相当于Implementor类型
        protected PlatformImplementor _implementor;

        //通过构造器注入，初始化平台实现
        protected DataBase(PlatformImplementor implementor)
        {
            this._implementor = implementor;
        }


        //创建数据库--该操作相当于Abstraction类型的Operation方法
        public abstract void Create();
    }
}
