﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sample.DesignMode.Bridge
{
    /// <summary>
    /// 3.SqlServer2000版本的数据库，相当于RefinedAbstraction类型
    /// </summary>
    public class SqlServer2000 : DataBase
    {
        public SqlServer2000(PlatformImplementor implementor) : base(implementor)
        {
        }

        public override void Create()
        {
            this._implementor.Process();
        }
    }
}
