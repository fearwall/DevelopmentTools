﻿该文件夹下为桥接模式示例
            /*
             * 输出结果
                SqlServer2000针对Unix的具体实现
                SqlServer2005针对Unix的具体实现
                按下任意键退出对话框...
             */

            DataBase db = null;

            //数据库连接 SqlServer2000的创建实现
            PlatformImplementor SqlServer2000UnixImp = new SqlServer2000UnixImplementor();
            db = new SqlServer2000(SqlServer2000UnixImp);
            db.Create();

            //数据库连接 SqlSever2005的创建实现
            PlatformImplementor SqlServer2005UnixImp = new SqlServer2005UnixImplementor();
            db = new SqlServer2005(SqlServer2005UnixImp);
            db.Create();
            
            Console.WriteLine("按下任意键退出对话框...");
            Console.ReadLine();