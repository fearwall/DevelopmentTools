﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sample.DesignMode.Bridge
{
    /// <summary>
    /// 4.SqlServer2005版本的数据库针对Unix操作系统的具体实现，相当于ConcreteImplementorB类型
    /// </summary>
    public class SqlServer2005UnixImplementor : PlatformImplementor
    {
        public override void Process()
        {
            Console.WriteLine("SqlServer2005针对Unix的具体实现");
        }
    }
}
