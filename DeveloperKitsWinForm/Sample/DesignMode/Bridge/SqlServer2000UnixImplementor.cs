﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sample.DesignMode.Bridge
{
    /// <summary>
    /// 3.SqlServer2000版本的数据库针对Unix操作系统具体的实现，相当于ConcreteImplementorA类型
    /// </summary>
    public class SqlServer2000UnixImplementor : PlatformImplementor
    {
        public override void Process()
        {
            Console.WriteLine("SqlServer2000针对Unix的具体实现");
        }
    }
}
