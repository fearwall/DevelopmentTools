﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Sample.DesignMode.SimpleFactoryMode
{
    /// <summary>
    /// 短信-发送接口实现类
    /// </summary>
    public class SmsSender : ISender
    {
        public void Sender()
        {
            Console.WriteLine(" this is SmsSender!");
            //throw new NotImplementedException();
        }
    }
}
