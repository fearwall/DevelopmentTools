﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Sample.DesignMode.SimpleFactoryMode
{
    /// <summary>
    /// 发送工厂类
    /// </summary>
    public class SenderFactory
    {
        #region 普通简单工厂模式
        public ISender Produce(string strType)
        {
            if ("Mail".Equals(strType))
            {
                return new MailSender();
            }
            else if ("Sms".Equals(strType))
            {
                return new SmsSender();
            }
            else
            {
                Console.WriteLine("请输入正确的类型！");
                return null;
            }
        }
        #endregion

        #region 多个方法简单工厂模式
        /// <summary>
        /// 构造短信发送实例
        /// </summary>
        /// <returns></returns>
        public ISender ProduceSms()
        {
            return new SmsSender();
        }

        /// <summary>
        /// 构造邮件发送实例
        /// </summary>
        /// <returns></returns>
        public ISender ProduceMail()
        {
            return new MailSender();
        }
        #endregion

        #region 多个静态方法简单工厂模式
        ///// <summary>
        ///// 构造短信发送实例
        ///// </summary>
        ///// <returns></returns>
        //public static ISender ProduceSms()
        //{
        //    return new SmsSender();
        //}

        ///// <summary>
        ///// 构造邮件发送实例
        ///// </summary>
        ///// <returns></returns>
        //public static ISender ProduceMail()
        //{
        //    return new MailSender();
        //}
        #endregion

    }
}
