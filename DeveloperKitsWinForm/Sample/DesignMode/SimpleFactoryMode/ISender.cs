﻿using System;
using System.Collections.Generic;
using System.Text;

/// <summary>
/// 简单工厂模式
/// </summary>
namespace Sample.DesignMode.SimpleFactoryMode
{
    /// <summary>
    /// Sender 接口
    /// </summary>
    public interface ISender
    {
        void Sender();
    }
}
