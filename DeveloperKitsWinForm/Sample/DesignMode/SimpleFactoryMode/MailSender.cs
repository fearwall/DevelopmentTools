﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Sample.DesignMode.SimpleFactoryMode
{
    /// <summary>
    /// 邮件-发送接口实现类
    /// </summary>
    public class MailSender : ISender
    {
        public void Sender()
        {
            Console.WriteLine(" this is MailSender!");
            //throw new NotImplementedException();
        }
    }
}
