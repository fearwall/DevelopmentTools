﻿using Sample.DesignMode.AbstractFactoryMode.AbstractClass;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sample.DesignMode.AbstractFactoryMode
{
    /// <summary>
    /// 欧式房门的实现类
    /// </summary>
    public class EuropeanDoor : IDoor
    {
        public override void Create()
        {
            Console.WriteLine("创建欧式的房门");
        }
    }
}
