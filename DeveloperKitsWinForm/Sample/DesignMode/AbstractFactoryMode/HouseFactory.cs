﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Sample.DesignMode.AbstractFactoryMode.AbstractClass;

namespace Sample.DesignMode.AbstractFactoryMode
{
    /// <summary>
    /// 房屋抽象工厂类，提供创建不同类型房屋的接口
    /// </summary>
    public abstract class HouseFactory
    {
        // 抽象工厂提供创建一系列产品的接口，这里作为例子，只给出了房顶、地板、窗户和房门创建接口
        public abstract IRoof CreateRoof();
        public abstract IFloor CreateFloor();
        public abstract IWindow CreateWindow();
        public abstract IDoor CreateDoor();
    }
}
