﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Sample.DesignMode.AbstractFactoryMode.AbstractClass;

namespace Sample.DesignMode.AbstractFactoryMode
{
    /// <summary>
    /// 现代风格房子的工厂类，负责创建现代风格的房子
    /// </summary>
    public class ModernizationFactory : HouseFactory
    {
        /// <summary>
        /// 创建房门
        /// </summary>
        /// <returns></returns>
        public override IDoor CreateDoor()
        {
            return new ModernizationDoor();
        }

        /// <summary>
        /// 创建地板
        /// </summary>
        /// <returns></returns>
        public override IFloor CreateFloor()
        {
            return new ModernizationFloor();
        }

        /// <summary>
        /// 创建房顶
        /// </summary>
        /// <returns></returns>
        public override IRoof CreateRoof()
        {
            return new ModernizationRoof();
        }

        /// <summary>
        /// 创建窗户
        /// </summary>
        /// <returns></returns>
        public override IWindow CreateWindow()
        {
            return new ModernizationWindow();
        }

        public ModernizationFactory Clone()
        {
            return (ModernizationFactory)this.MemberwiseClone();
        }
    }
}
