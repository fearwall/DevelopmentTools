﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sample.DesignMode.AbstractFactoryMode.AbstractClass
{
    /// <summary>
    /// 房顶抽象类，子类的房顶必须继承该类
    /// </summary>
    public abstract class IRoof
    {
        /// <summary>
        /// 创建房顶
        /// </summary>
        public abstract void Create();
    }
}
