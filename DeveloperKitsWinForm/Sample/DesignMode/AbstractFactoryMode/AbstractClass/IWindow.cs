﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sample.DesignMode.AbstractFactoryMode.AbstractClass
{
    /// <summary>
    /// 窗户抽象类，子类的窗户必须继承该类
    /// </summary>
    public abstract class IWindow
    {
        /// <summary>
        /// 创建窗户
        /// </summary>
        public abstract void Create();
    }
}
