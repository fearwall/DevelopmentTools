﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sample.DesignMode.AbstractFactoryMode.AbstractClass
{
    /// <summary>
    /// 地板抽象类，子类的地板必须继承该类
    /// </summary>
    public abstract class IFloor
    {
        /// <summary>
        /// 创建地板
        /// </summary>
        public abstract void Create();
    }
}
