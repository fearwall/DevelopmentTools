﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sample.DesignMode.AbstractFactoryMode.AbstractClass
{
    /// <summary>
    /// 房门抽象类，子类的房门必须继承该类
    /// </summary>
    public abstract class IDoor
    {
        /// <summary>
        /// 创建房门
        /// </summary>
        public abstract void Create();
    }
}
