﻿using Sample.DesignMode.AbstractFactoryMode.AbstractClass;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sample.DesignMode.AbstractFactoryMode
{
    /// <summary>
    /// 现代房门的实现类
    /// </summary>
    public class ModernizationDoor : IDoor
    {
        public override void Create()
        {
            Console.WriteLine("创建现代的房门");
        }
    }
}
