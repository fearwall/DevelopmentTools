﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Sample.DesignMode.AbstractFactoryMode.AbstractClass;

namespace Sample.DesignMode.AbstractFactoryMode
{
    /// <summary>
    /// 欧式风格房子的工厂类，负责创建欧式风格的房子
    /// </summary>
    public class EuropeanFactory : HouseFactory
    {
        /// <summary>
        /// 创建房门
        /// </summary>
        /// <returns></returns>
        public override IDoor CreateDoor()
        {
            return new EuropeanDoor();
        }

        /// <summary>
        /// 创建地板
        /// </summary>
        /// <returns></returns>
        public override IFloor CreateFloor()
        {
            return new EuropeanFloor();
        }

        /// <summary>
        /// 创建房顶
        /// </summary>
        /// <returns></returns>
        public override IRoof CreateRoof()
        {
            return new EuropeanRoof();
        }

        /// <summary>
        /// 创建窗户
        /// </summary>
        /// <returns></returns>
        public override IWindow CreateWindow()
        {
            return new EuropeanWindow();
        }
    }
}
