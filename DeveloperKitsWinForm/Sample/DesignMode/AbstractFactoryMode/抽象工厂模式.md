﻿该文件夹下所有的类为抽象工厂模式的示例：
测试方法：
/* 输出结果
    创建欧式的房顶
    创建欧式的地板
    创建欧式的窗户
    创建欧式的房门

    创建现代的房顶
    创建现代的地板
    创建现代的窗户
    创建现代的房门
    按下任意键退出对话框...
    */

// 哥哥的欧式风格的房子
HouseFactory europeanFactory = new EuropeanFactory();
europeanFactory.CreateRoof().Create();
europeanFactory.CreateFloor().Create();
europeanFactory.CreateWindow().Create();
europeanFactory.CreateDoor().Create();

Console.WriteLine("");

//弟弟的现代风格的房子
HouseFactory modernizationFactory = new ModernizationFactory();
modernizationFactory.CreateRoof().Create();
modernizationFactory.CreateFloor().Create();
modernizationFactory.CreateWindow().Create();
modernizationFactory.CreateDoor().Create();
Console.WriteLine("按下任意键退出对话框...");
Console.ReadLine();