﻿using Sample.DesignMode.AbstractFactoryMode.AbstractClass;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sample.DesignMode.AbstractFactoryMode
{
    /// <summary>
    /// 欧式窗户的实现类
    /// </summary>
    public class EuropeanWindow : IWindow
    {
        public override void Create()
        {
            Console.WriteLine("创建欧式的窗户"); ;
        }
    }
}
