﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sample.DesignMode.FactoryMode
{
    /// <summary>
    /// 实现类的接口
    /// </summary>
    public interface IProvider
    {
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        ISender Produce();
    }
}
