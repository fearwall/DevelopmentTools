﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sample.DesignMode.FactoryMode
{
    public class SmsSender : ISender
    {
        public void Send()
        {
            Console.WriteLine("This is SmsSender");
        }
    }
}
