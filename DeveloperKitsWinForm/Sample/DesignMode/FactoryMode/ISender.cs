﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sample.DesignMode.FactoryMode
{
    /// <summary>
    /// 发送接口
    /// </summary>
    public interface ISender
    {
        void Send();
    }
}
