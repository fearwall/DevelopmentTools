﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sample.DesignMode.FactoryMode
{
    /// <summary>
    /// 邮件发送工厂类
    /// </summary>
    public class MailSenderFactory : IProvider
    {
        public ISender Produce()
        {
            return new MailSender();
        }
    }
}
