﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sample.DesignMode.AdapterPatternMode
{
    /// <summary>
    /// 适配器类，接口要放在类的后面，在此无法适配更多的对象，这是类适配器的不足
    /// </summary>
    public class ThreeToTwoAdapter2 : ThreeHoleAdaptee, ITwoHoleTarget
    {
        public void Request()
        {
            this.SpecificRequest();
            Console.WriteLine("这是类适配器模式输出的！");
        }


    }
}
