﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sample.DesignMode.AdapterPatternMode
{
    /// <summary>
    /// 1.定义Client使用的与特定领域相关的接口 Taget
    /// </summary>
    public class TwoHoleTarget
    {
        public virtual void Request()
        {
            Console.WriteLine("两孔的充电器可以使用了！");
        }
    }
}
