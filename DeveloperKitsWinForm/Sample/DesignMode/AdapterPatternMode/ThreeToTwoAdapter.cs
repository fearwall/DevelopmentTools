﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sample.DesignMode.AdapterPatternMode
{
    /// <summary>
    /// 3.适配器类
    /// </summary>
    public class ThreeToTwoAdapter:TwoHoleTarget
    {
        /// <summary>
        /// 引用三孔插头的实例,从而将客户端与TwoHole联系起来
        /// </summary>
        private ThreeHoleAdaptee ThreeHoleAdaptee = new ThreeHoleAdaptee();

        public override void Request()
        {
            base.Request();
            //可以做具体的转换工作
            this.ThreeHoleAdaptee.SpecificRequest();
            //可以做具体的转换工作
        }
    }
}
