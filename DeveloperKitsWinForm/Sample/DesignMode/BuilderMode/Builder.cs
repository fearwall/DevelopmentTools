﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sample.DesignMode.BuilderMode
{
    /// <summary>
    /// 2.抽象建造者 它定义了要创建什么部件和最后创建的结果，但是不是组装的的类型，切记
    /// </summary>
    public abstract class Builder
    {
        /// <summary>
        /// 创建车门
        /// </summary>
        public abstract void BuildCarDoor();

        /// <summary>
        /// 创建车轮
        /// </summary>
        public abstract void BuildCarWheel();

        /// <summary>
        /// 创建引擎
        /// </summary>
        public abstract void BuildCarEngine();

        /// <summary>
        /// 获得组装的结果
        /// </summary>
        /// <returns></returns>
        public abstract Car GetCar();
    }
}
