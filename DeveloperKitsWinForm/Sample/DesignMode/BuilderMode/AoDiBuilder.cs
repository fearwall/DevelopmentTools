﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sample.DesignMode.BuilderMode
{
    /// <summary>
    /// 3.具体创建者，具体车型的创建者 例如：奥迪
    /// </summary>
    public sealed class AoDiBuilder : Builder
    {
        Car buildCar = new Car();
        public override void BuildCarDoor()
        {
            buildCar.Add("AoDiBuilder's Door");
        }

        public override void BuildCarEngine()
        {
            buildCar.Add("AoDiBuilder's Engine");
        }

        public override void BuildCarWheel()
        {
            buildCar.Add("AoDiBuilder's Wheel");
        }

        public override Car GetCar()
        {
            return buildCar;
        }
    }
}
