﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sample.DesignMode.BuilderMode
{
    /// <summary>
    /// 3.具体创建者，具体车型的创建者 例如：别克
    /// </summary>
    public sealed class BuickBuilder : Builder
    {
        Car buildCar = new Car();
        public override void BuildCarDoor()
        {
            buildCar.Add("Buick's Door");
        }

        public override void BuildCarEngine()
        {
            buildCar.Add("Buick's Engine");
        }

        public override void BuildCarWheel()
        {
            buildCar.Add("Buick's Wheel");
        }

        public override Car GetCar()
        {
            return buildCar;
        }
    }
}
