﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sample.DesignMode.BuilderMode
{
    /// <summary>
    /// 1.创建 产品类（汽车）
    /// </summary>
    public sealed class Car
    {
        /// <summary>
        /// 汽车部件的集合
        /// </summary>
        private IList<string> parats = new List<string>();

        /// <summary>
        /// 添加数据到集合中
        /// </summary>
        /// <param name="parat"></param>
        public void Add(string parat)
        {
            parats.Add(parat);
        }

        public void Show()
        {
            Console.WriteLine("汽车开始组装...");
            foreach(string parat in parats)
            {
                Console.WriteLine("组件【 " + parat +" 】 已完成." );
            }
            Console.WriteLine("汽车所有组件已完成！");
        }
    }
}
