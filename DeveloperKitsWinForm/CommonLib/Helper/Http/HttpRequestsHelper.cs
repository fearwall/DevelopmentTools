﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Web;

namespace CommonLib.Helper.Http
{
    public class HttpRequestsHelper
    {
        /// <summary>
        /// 发送Get请求
        /// </summary>
        /// <param name="url">发起请求的URL地址</param>
        /// <returns></returns>
        public string Get(string url)
        {
            return Get(url, "");
        }

        /// <summary>
        /// 发送Get请求
        /// </summary>
        /// <param name="url">发起请求的URL地址</param>
        /// <param name="postDictionary">参数字典</param>
        /// <returns></returns>
        public string Get(string url, Dictionary<string, string> postDictionary)
        {
            return Get(url, GetHttpParam(postDictionary));
        }

        /// <summary>
        /// 发送Get请求
        /// </summary>
        /// <param name="url">发起请求的</param>
        /// <param name="urlParam">URL参数字符串</param>
        /// <returns></returns>
        public string Get(string url, string urlParam)
        {
            return Get(url, urlParam, Encoding.Default);
        }

        /// <summary>
        /// 发送Get请求
        /// </summary>
        /// <param name="url">发起请求的</param>
        /// <param name="urlParam">URL参数字符串</param>
        /// <param name="ecd">字符集编码 Encoding.GetEncoding("gb2312")</param>
        /// <returns></returns>
        public string Get(string url, string urlParam, Encoding ecd)
        {
            try
            {
                if (!string.IsNullOrEmpty(urlParam))
                {
                    if (url.Contains("?"))
                    {
                        url = url + "&" + urlParam;
                    }
                    else
                    {
                        url = url + "?" + urlParam;
                    }
                }
                HttpWebRequest request = (HttpWebRequest)WebRequest.Create(url);
                request.Timeout = 15000;
                request.ReadWriteTimeout = 15000;
                HttpWebResponse response = (HttpWebResponse)request.GetResponse();
                Stream stream = response.GetResponseStream();
                string responseString = string.Empty;
                //设置编码的获取方式
                //中文编码获取
                //using (StreamReader reader = new StreamReader(stream, Encoding.GetEncoding("gb2312")))  
                using (StreamReader reader = new StreamReader(stream, ecd))
                {
                    responseString = UnicodeToString(reader.ReadToEnd());
                }
                //string responseString = new StreamReader(response.GetResponseStream()).ReadToEnd();
                //responseString = UnicodeToString(responseString);
                return responseString;
            }
            catch (Exception ex)
            {
                throw new Exception("远程HTTP获取数据错误，错误信息为：" + ex.ToString());
            }
        }

        /// <summary>
        /// 发起POST请求
        /// </summary>
        /// <param name="url">请求的url地址</param>
        /// <returns></returns>
        public string Post(string url)
        {
            return Post(url, "");
        }

        /// <summary>
        /// 发起POST请求
        /// </summary>
        /// <param name="url">请求的url地址</param>
        /// <param name="postDictionary">参数字典</param>
        /// <returns></returns>
        public string Post(string url, Dictionary<string, string> postDictionary)
        {
            return Post(url, GetHttpParam(postDictionary));
        }

        /// <summary>
        /// 发起POST请求
        /// </summary>
        /// <param name="url">请求的url地址</param>
        /// <param name="postData">请求携带的数据</param>
        /// <returns></returns>
        public string Post(string url, string postData)
        {
            return Post(url, postData, Encoding.Default);
        }

        /// <summary>
        /// 发起POST请求
        /// </summary>
        /// <param name="url">请求的url地址</param>
        /// <param name="postData">请求携带的数据</param>
        /// <param name="ecd">字符集编码 Encoding.GetEncoding("gb2312")</param>
        /// <returns></returns>
        public string Post(string url, string postData, Encoding ecd)
        {
            try
            {
                HttpWebRequest request = (HttpWebRequest)WebRequest.Create(url);
                request.Timeout = 15000;
                request.ReadWriteTimeout = 15000;
                //string postData = "thing1=hello";
                //postData += "&thing2=中文呢";
                byte[] data = Encoding.UTF8.GetBytes(postData);
                request.Method = "POST";
                request.ContentType = "application/x-www-form-urlencoded";
                request.UserAgent = "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/66.0.3359.181 Safari/537.36";
                request.ContentLength = data.Length;
                string responseString = string.Empty;
                using (Stream stream = request.GetRequestStream())
                {
                    stream.Write(data, 0, data.Length);
                    HttpWebResponse response = (HttpWebResponse)request.GetResponse();//中文编码获取
                    //using (StreamReader reader = new StreamReader(response.GetResponseStream(), Encoding.GetEncoding("gb2312")))  
                    using (StreamReader reader = new StreamReader(response.GetResponseStream(), ecd))
                    {
                        responseString = UnicodeToString(reader.ReadToEnd());
                    }
                }
                return responseString;
            }
            catch (Exception ex)
            {
                throw new Exception("远程HTTP获取数据错误，错误信息为：" + ex.ToString());
            }
        }

        /// <summary>  
        /// <summary>  
        /// 字符串转Unicode  
        /// </summary>  
        /// <param name="source">源字符串</param>  
        /// <returns>Unicode编码后的字符串</returns>  
        public static string StringToUnicode(string source)
        {
            byte[] bytes = Encoding.Unicode.GetBytes(source);
            StringBuilder stringBuilder = new StringBuilder();
            for (int i = 0; i < bytes.Length; i += 2)
            {
                stringBuilder.AppendFormat("\\u{0}{1}", bytes[i + 1].ToString("x").PadLeft(2, '0'), bytes[i].ToString("x").PadLeft(2, '0'));
            }
            return stringBuilder.ToString();
        }

        /// <summary>  
        /// Unicode转字符串  
        /// </summary>  
        /// <param name="source">经过Unicode编码的字符串</param>  
        /// <returns>正常字符串</returns>  
        public static string UnicodeToString(string source)
        {
            return new Regex(@"\\u([0-9A-F]{4})", RegexOptions.IgnoreCase | RegexOptions.Compiled).Replace(
                         source, x => string.Empty + Convert.ToChar(Convert.ToUInt16(x.Result("$1"), 16)));
        }

        /// <summary>
        /// 获取Url参数序列化后的字符串
        /// </summary>
        /// <param name="paramDic">需要序列化的字符串</param>
        /// <returns></returns>
        public string GetHttpParam(Dictionary<string, string> paramDic)
        {
            string strResult = string.Empty;
            StringBuilder builder = new StringBuilder();
            int i = 0;
            foreach (var item in paramDic)
            {
                if (i > 0)
                {
                    builder.Append("&");
                }
                builder.AppendFormat("{0}={1}", item.Key, HttpUtility.UrlEncode(item.Value));
                i++;
            }
            strResult = builder.ToString();
            return strResult;
        }
    }
}
