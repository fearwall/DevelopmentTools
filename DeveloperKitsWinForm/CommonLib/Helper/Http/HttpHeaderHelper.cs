﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CommonLib.Helper.Http
{
    /// <summary>
    /// Http Header头的相关参数设置
    /// </summary>
    public class HttpHeaderHelper
    {
        public HttpHeaderHelper()
        {
            Accept = string.Empty;
            UserAgent = string.Empty;
            ContentType = string.Empty;
            Method = string.Empty;
            MaxTry = 300;
            EncodingCode = Encoding.Default;
            AllowAutoRedirect = false;
        }

        #region 请求头相关参数
        /// <summary>
        /// 获取默认Post方法的Header头
        /// </summary>
        /// <returns></returns>
        public static HttpHeaderHelper GetDefaultPostHeader()
        {
            HttpHeaderHelper httpHeader = new HttpHeaderHelper();
            httpHeader.Accept = "text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8";
            httpHeader.UserAgent = "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/63.0.3239.84 Safari/537.36";
            httpHeader.ContentType = "application/x-www-form-urlencoded";
            httpHeader.Method = "POST";
            httpHeader.MaxTry = 300;
            httpHeader.EncodingCode = Encoding.Default;
            httpHeader.AllowAutoRedirect = false;
            return httpHeader;
        }

        /// <summary>
        /// 获取默认Get方法的Header头
        /// </summary>
        /// <returns></returns>
        public static HttpHeaderHelper GetDefaultGetHeader()
        {
            HttpHeaderHelper httpHeader = new HttpHeaderHelper();
            httpHeader.Accept = "*/*";
            httpHeader.UserAgent = "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/63.0.3239.84 Safari/537.36";
            httpHeader.Method = "GET";
            httpHeader.MaxTry = 300;
            httpHeader.EncodingCode = Encoding.Default;
            httpHeader.AllowAutoRedirect = false;
            return httpHeader;
        }

        /// <summary>
        /// 获取或设置 Content-type HTTP 标头的值
        /// </summary>
        public string ContentType { get; set; }

        /// <summary>
        /// 获取或设置 Accept HTTP 标头的值
        /// </summary>
        public string Accept { get; set; }

        /// <summary>
        /// 获取或设置 User-agent HTTP 标头的值
        /// </summary>
        public string UserAgent { get; set; }

        /// <summary>
        /// 获取或设置请求的方法 POST/GET
        /// </summary>
        public string Method { get; set; }

        /// <summary>
        /// 获取或设置允许的最大连接数
        /// </summary>
        public int MaxTry { get; set; }

        /// <summary>
        /// 设置请求的编码
        /// </summary>
        public Encoding EncodingCode { get; set; }

        /// <summary>
        /// 获取或设置一个值，该值指示请求是否应跟随重定向响应
        /// </summary>
        public bool AllowAutoRedirect { get; set; }

        #endregion       
    }
}
