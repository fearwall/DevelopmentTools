﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CommonLib.Helper.Common
{
    /// <summary>
    /// 年龄转出生日期
    /// </summary>
    public class AgeToBirthdayHelper
    {
        /// <summary>
        /// 出生日期
        /// </summary>
        public DateTime BirthDay
        {
            get;
            private set;
        }

        /// <summary>
        /// 年龄转出生日期
        /// </summary>
        /// <param name="Age">年龄 单位:岁</param>
        public AgeToBirthdayHelper(int Age)
        {
            AgeToBirthday(DateTime.Now, Age, "岁");
        }

        /// <summary>
        /// 年龄转出生日期
        /// </summary>
        /// <param name="Age">年龄</param>
        /// <param name="strAgeUnit">年龄单位 岁|月|天</param>
        public AgeToBirthdayHelper(int Age,string strAgeUnit)
        {
            AgeToBirthday(DateTime.Now, Age, strAgeUnit);
        }

        /// <summary>
        /// 年龄计算
        /// </summary>
        /// <param name="dateTime">当前计算日期</param>
        /// <param name="Age">当前年龄</param>
        /// <param name="strAgeUnit">年龄单位</param>
        private void AgeToBirthday(DateTime dateTime,int Age,string strAgeUnit)
        {
            if (strAgeUnit == "天")
            {
                BirthDay = dateTime.AddDays(-1 * Age);
            }
            else if (strAgeUnit == "月")
            {
                BirthDay = dateTime.AddMonths(-1 * Age);
            }
            else
            {
                BirthDay = dateTime.AddYears(-1 * Age);
            }
        }
    }
}
