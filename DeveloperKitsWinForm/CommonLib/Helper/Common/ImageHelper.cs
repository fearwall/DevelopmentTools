﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CommonLib.Helper.Common
{
    public class ImageHelper
    {
        /// <summary>
        /// 获取Image图片格式
        /// </summary>
        /// <param name="_img"></param>
        /// <param name="format"></param>
        /// <returns></returns>
        public static System.Drawing.Imaging.ImageFormat GetImageFormat(Image _img, out string format)
        {
            if (_img.RawFormat.Guid.Equals(System.Drawing.Imaging.ImageFormat.Bmp.Guid))
            {
                format = ".bmp";
                return System.Drawing.Imaging.ImageFormat.Bmp;
            }
            if (_img.RawFormat.Guid.Equals(System.Drawing.Imaging.ImageFormat.Emf.Guid))
            {
                format = ".emf";
                return System.Drawing.Imaging.ImageFormat.Emf;
            }
            if (_img.RawFormat.Guid.Equals(System.Drawing.Imaging.ImageFormat.Exif.Guid))
            {
                format = ".exif";
                return System.Drawing.Imaging.ImageFormat.Exif;
            }
            if (_img.RawFormat.Guid.Equals(System.Drawing.Imaging.ImageFormat.Gif.Guid))
            {
                format = ".gif";
                return System.Drawing.Imaging.ImageFormat.Gif;
            }
            if (_img.RawFormat.Guid.Equals(System.Drawing.Imaging.ImageFormat.Icon.Guid))
            {
                format = ".icon";
                return System.Drawing.Imaging.ImageFormat.Icon;
            }
            if (_img.RawFormat.Guid.Equals(System.Drawing.Imaging.ImageFormat.Jpeg.Guid))
            {
                format = ".jpg";
                return System.Drawing.Imaging.ImageFormat.Jpeg;
            }
            if (_img.RawFormat.Guid.Equals(System.Drawing.Imaging.ImageFormat.MemoryBmp.Guid))
            {
                format = ".bmp";
                return System.Drawing.Imaging.ImageFormat.MemoryBmp;
            }
            if (_img.RawFormat.Guid.Equals(System.Drawing.Imaging.ImageFormat.Png.Guid))
            {
                format = ".png";
                return System.Drawing.Imaging.ImageFormat.Png;
            }
            format = ".png";
            return null;
        }
    }
}
