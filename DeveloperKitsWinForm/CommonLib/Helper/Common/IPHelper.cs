﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Management;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace CommonLib.Helper.Common
{
    public class IPHelper
    {
        /// <summary>
        /// 获取本机IP
        /// </summary>
        /// <returns></returns>
        public static string GetLocalMachineIP()
        {
            ManagementObjectCollection instances = new ManagementClass("Win32_NetworkAdapterConfiguration").GetInstances();
            string str = "";
            foreach (ManagementObject managementObject in instances)
            {
                if ((bool)managementObject["IPEnabled"] && !(managementObject["IPAddress"] as string[])[0].StartsWith("0") && !str.Contains((managementObject["IPAddress"] as string[])[0]))
                    str = str + "|" + (managementObject["IPAddress"] as string[])[0];
            }
            if (str.Length > 1)
                return str.Substring(1, str.Length - 1);
            return "Unknown IP";
        }


        /// <summary>
        /// 获取本机IP数组
        /// </summary>
        /// <returns></returns>
        public static string[] GetLocalMachineIPs()
        {
            string str = GetLocalMachineIP();
            if (str.Length > 0)
            {
                return str.Split('|');
            }
            return null;
        }

        /// <summary>
        /// 获取计算机名称
        /// </summary>
        /// <returns></returns>
        public static string GetMachineName()
        {
            return Dns.GetHostName();
        }

        /// <summary>
        /// 获取本机的MAC地址
        /// </summary>
        /// <returns></returns>
        public static string[] GetLocalMachineMac()
        {
            ManagementObjectCollection instances = new ManagementClass("Win32_NetworkAdapterConfiguration").GetInstances();
            string str = "";
            foreach (ManagementObject managementObject in instances)
            {
                if ((bool)managementObject["IPEnabled"])
                    str = str + "|" + managementObject["MacAddress"].ToString();
            }
            if (str.Length > 1)
            {
                str = str.Substring(1, str.Length - 1);
            }
            return str.Split('|');
        }
    }
}
