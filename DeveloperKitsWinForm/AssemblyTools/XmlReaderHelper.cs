﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;

namespace AssemblyTools
{
    /// <summary>
    /// XML读取助手类
    /// </summary>
    public class XmlReaderHelper
    {

        /// <summary>
        /// 加载Xml获取程序集名称
        /// </summary>
        /// <param name="strXmlPath">Xml文件路径，文件不存在将抛出异常</param>
        /// <param name="strSerachRootNode">搜索的根路径</param>
        /// <param name="strNodeName">搜索根路径的节点Name 若无则填 string.Empty</param>
        /// <param name="strNodeSource">搜索的子项</param>
        /// <returns></returns>
        public static string LoadXmlFileGetCsprojAssemblyName(string strXmlPath)
        {
            string strSerachRootNode = "PropertyGroup";
            string strNodeName = "";
            string strNodeSource = "AssemblyName";
            if (!File.Exists(strXmlPath))
            {
                throw new Exception("指定的XML文件：\r\n\t" + strXmlPath + "\r\n不存在，请检查！");
            }
            else
            {
                using (XmlReader source = XmlReader.Create(strXmlPath))
                {
                    using (XmlReader sourceSerach = GetSpecifiedXmlNode(source, strSerachRootNode, strNodeName))
                    {
                        using (XmlReader itemSource = sourceSerach)
                        {
                            while (itemSource.Read())
                            {
                                if (itemSource.NodeType == XmlNodeType.Element &&
                                    itemSource.Name == strNodeSource)
                                {
                                    return itemSource.ReadInnerXml();                                    
                                }
                            }
                        }
                    }
                }
                return "";
            }
        }


        /// <summary>
        /// 加载Xml获取项目类型
        /// </summary>
        /// <param name="strXmlPath">Xml文件路径，文件不存在将抛出异常</param>
        /// <param name="strSerachRootNode">搜索的根路径</param>
        /// <param name="strNodeName">搜索根路径的节点Name 若无则填 string.Empty</param>
        /// <param name="strNodeSource">搜索的子项</param>
        /// <returns></returns>
        public static string LoadXmlFileGetCsProjectType(string strXmlPath)
        {
            string strSerachRootNode = "PropertyGroup";
            string strNodeName = "";
            string strNodeSource = "OutputType";
            if (!File.Exists(strXmlPath))
            {
                throw new Exception("指定的XML文件：\r\n\t" + strXmlPath + "\r\n不存在，请检查！");
            }
            else
            {
                using (XmlReader source = XmlReader.Create(strXmlPath))
                {
                    using (XmlReader sourceSerach = GetSpecifiedXmlNode(source, strSerachRootNode, strNodeName))
                    {
                        using (XmlReader itemSource = sourceSerach)
                        {
                            while (itemSource.Read())
                            {
                                if (itemSource.NodeType == XmlNodeType.Element &&
                                    itemSource.Name == strNodeSource)
                                {
                                    return itemSource.ReadInnerXml();
                                }
                            }
                        }
                    }
                }
                return "";
            }
        }

        /// <summary>
        /// 获取 指定的Xml节点
        /// </summary>
        /// <param name="souce"></param>
        /// <param name="strSerachRootNode"></param>
        /// <returns></returns>
        public static XmlReader GetSpecifiedXmlNode(XmlReader source, string strSerachRootNode, string strNodeName)
        {
            List<string> list = strSerachRootNode.Split('/').ToList();
            if (list.Count > 0)
            {
                for (int i = 0; i < list.Count; i++)
                {
                    if (i < list.Count - 1)
                    {
                        source = XmlReaderNodeSerach(source, list[i], "");
                    }
                    else
                    {
                        source = XmlReaderNodeSerach(source, list[i], strNodeName);
                    }
                }
            }
            return source;
        }

        /// <summary>
        /// Xml指定节点搜索
        /// </summary>
        /// <param name="source"></param>
        /// <param name="strSearchNodeName"></param>
        /// <param name="strNodeName">搜索根路径的节点Name 若无则填 string.Empty</param>
        /// <returns></returns>
        public static XmlReader XmlReaderNodeSerach(XmlReader source, string strSearchNodeName, string strNodeName)
        {
            while (source.Read())
            {
                if (source.NodeType == XmlNodeType.Element &&
                    source.Name == strSearchNodeName)
                {
                    if (string.IsNullOrEmpty(strNodeName))
                    {
                        return source;
                    }
                    else
                    {
                        if (source.GetAttribute("Name").Equals(strNodeName))
                        {
                            return source;
                        }
                    }
                }
            }
            return source;
        }
    }
}
