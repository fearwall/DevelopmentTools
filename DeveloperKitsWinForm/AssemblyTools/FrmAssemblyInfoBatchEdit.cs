﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace AssemblyTools
{
    public partial class FrmAssemblyInfoBatchEdit : Form
    {
        /// <summary>
        /// Ini配置文件名称
        /// </summary>
        private const string ConfigIniName = "AssemblyInfoBatchEdit";

        /// <summary>
        /// 日志StringBuilder
        /// </summary>
        StringBuilder sbLogs = new StringBuilder();

        /// <summary>
        /// 生成时间
        /// </summary>
        private DateTime MakeDateTime = DateTime.Now;

        public FrmAssemblyInfoBatchEdit()
        {
            InitializeComponent();
        }

        private void cbModel_SelectedIndexChanged(object sender, EventArgs e)
        {
            if(cbModel.SelectedIndex == 0)
            {
                //多文件模式
                btnBatchPath.Enabled = true;
                btnSingleEdit.Enabled = false;

                tbBatchPath.ReadOnly = false;
                tbSinglePath.ReadOnly = true;

                tcButtom.SelectedTab = tpMultiFile;

                dgvBatch.DataSource = null;
            }
            else if(cbModel.SelectedIndex == 1)
            {
                btnBatchPath.Enabled = false;
                btnSingleEdit.Enabled = true;

                tbBatchPath.ReadOnly = true;
                tbSinglePath.ReadOnly = false;

                tcButtom.SelectedTab = tpLogInfo;
            }
            gbParams.Enabled = false;
            sbLogs.Clear();
            ShowLogs();
            ClearControlValue();
        }

        private void btnBatchPath_Click(object sender, EventArgs e)
        {
            ClearControlValue();
            sbLogs.Clear();
            string strFolderPath = CommonLib.Helper.Common.FilePathHelper.FolderPathSelect(tbBatchPath.Text.Trim(),true);
            if (string.IsNullOrEmpty(strFolderPath))
            {
                MessageBox.Show("请选择批量生成的路径！", "错误", MessageBoxButtons.OK, MessageBoxIcon.Error);
                gbParams.Enabled = false;
                return;
            }
            else
            {
                List<string> listBatchFilePath = new List<string>();
                LoadingLib.Win10Style.LoadingHelper.ShowBlueLoading("正在加载,请稍等...", this, o => 
                {
                    listBatchFilePath = CommonLib.Helper.Common.FileSearchHelper.GetFileSystemEntries(strFolderPath, "^AssemblyInfo.cs$", true, false).ToList();                                       
                });

                if (listBatchFilePath.Count <= 0)
                {
                    MessageBox.Show("选中的路径无可操作的指定文件【AssemblyInfo.cs】！", "错误", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    gbParams.Enabled = false;
                    return;
                }

                gbParams.Enabled = true;
                tbBatchPath.Text = strFolderPath;
                BindDataGridView(listBatchFilePath);
                sbLogs.AppendLine("读取文件路径【" + strFolderPath + "】成功！共获取到" + listBatchFilePath.Count + "个文件！");
            }
            //设置选中的路径到缓存中
            ConfigIniFileHelper.SetIniData(ConfigIniName, "FormData", "BatchPath", strFolderPath);

            ShowLogs();
        }

        private void btnSingleEdit_Click(object sender, EventArgs e)
        {
            ClearControlValue();
            sbLogs.Clear();
            string strFilePath = "";
            if (string.IsNullOrEmpty(tbSinglePath.Text.Trim()))
            {
                strFilePath = CommonLib.Helper.Common.FilePathHelper.FilePathSelect("程序集信息文件|AssemblyInfo.cs");
                if (string.IsNullOrEmpty(strFilePath) || !strFilePath.Contains("AssemblyInfo.cs"))
                {
                    MessageBox.Show("请选择指定的文件【AssemblyInfo.cs】！", "错误", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    gbParams.Enabled = false;
                    return;
                }
                tbSinglePath.Text = strFilePath;
            }
            else
            {
                strFilePath = tbSinglePath.Text.Trim();
            }

            if (!File.Exists(strFilePath) && !strFilePath.Contains("AssemblyInfo.cs"))
            {
                MessageBox.Show("请选择指定的文件【AssemblyInfo.cs】！", "错误", MessageBoxButtons.OK, MessageBoxIcon.Error);
                gbParams.Enabled = false;
                return;
            }
            gbParams.Enabled = true;
            //读取文件参数到表格中
            ClearControlValue();

            //设置选中的路径到缓存中
            ConfigIniFileHelper.SetIniData(ConfigIniName, "FormData", "SinglePath", strFilePath);

            AssemblyInfoLoadToControl(ReadAssemblyInfo(strFilePath));
            ShowLogs();
        }        

        private void btnMake_Click(object sender, EventArgs e)
        {
            try
            {
                if (!ValidateParams())
                {
                    return;
                }

                MakeDateTime = DateTime.Now;
                if (cbModel.SelectedIndex == 0)
                {
                    if (dgvBatch.DataSource == null)
                    {
                        MessageBox.Show("请在文件选择中至少勾选一个文件！", "错误", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        return;
                    }

                    LoadingLib.Win10Style.LoadingHelper.ShowBlueLoading("正在生成,请稍等...", this, o =>
                    {
                        int iSelectCount = 0;
                        //多文件模式
                        foreach (DataRow row in (dgvBatch.DataSource as DataTable).Rows)
                        {
                            bool isChecked = row["IsChecked"] == DBNull.Value ? false : Convert.ToBoolean(row["IsChecked"]);
                            if (isChecked)
                            {
                                string strFilePath = row["FilePath"].ToString();
                                WriteAssemblyInfo(ReadAssemblyInfo(strFilePath), strFilePath);
                                iSelectCount++;
                                row["IsChecked"] = false;
                            }
                        }
                        if (iSelectCount <= 0)
                        {
                            MessageBox.Show("请在文件选择中至少勾选一个文件！", "错误", MessageBoxButtons.OK, MessageBoxIcon.Error);
                            return;
                        }
                    });
                }
                else if (cbModel.SelectedIndex == 1)
                {
                    LoadingLib.Win10Style.LoadingHelper.ShowBlueLoading("正在生成,请稍等...", this, o =>
                    {
                        //单文件模式
                        WriteAssemblyInfo(ReadAssemblyInfo(tbSinglePath.Text), tbSinglePath.Text);
                    });
                }
                tcButtom.SelectedTab = tpLogInfo;
                sbLogs.AppendLine("操作完毕！");
                MessageBox.Show("操作完毕！", "提示", MessageBoxButtons.OK, MessageBoxIcon.Information);
                ShowLogs();
            }
            catch(Exception ex)
            {
                MessageBox.Show("生成失败！\r\n" + ex.Message, "失败", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
        }

        /// <summary>
        /// 备份路径选择
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnBackFilePath_Click(object sender, EventArgs e)
        {
            string strFolderPath = CommonLib.Helper.Common.FilePathHelper.FolderPathSelect(tbBackFilePath.Text.Trim(), true);
            if (string.IsNullOrEmpty(strFolderPath))
            {
                MessageBox.Show("请选择备份文件的路径！", "错误", MessageBoxButtons.OK, MessageBoxIcon.Error);
                gbParams.Enabled = false;
                return;
            }

            if (string.IsNullOrEmpty(Path.GetDirectoryName(strFolderPath + @"\")))
            {
                MessageBox.Show(
                    "请选择【生成前备份文件】合法的路径！当前路径：\r\n" + tbBackFilePath.Text.Trim() + "不合法！",
                    "错误",
                    MessageBoxButtons.OK,
                    MessageBoxIcon.Error);
                return;
            }

            tbBackFilePath.Text = strFolderPath;

            //设置选中的路径到缓存中
            ConfigIniFileHelper.SetIniData(ConfigIniName, "FormData", "BackFilePath", strFolderPath);
        }

        private void cbBackFile_CheckedChanged(object sender, EventArgs e)
        {
            tbBackFilePath.Enabled = cbBackFile.Checked;
            btnBackFilePath.Enabled = cbBackFile.Checked;
        }

        private void FrmAssemblyInfoBatchEdit_Load(object sender, EventArgs e)
        {
            LoadIniDataToForm();
            cbBackFile.Checked = true;
        }

        /// <summary>
        /// 绘制表格行号
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void dgvBatch_RowPostPaint(object sender, DataGridViewRowPostPaintEventArgs e)
        {
            try
            {
                DataGridView dgv = sender as DataGridView;
                Rectangle rectangle = new Rectangle(e.RowBounds.Location.X,
                                                    e.RowBounds.Location.Y,
                                                    dgv.RowHeadersWidth - 4,
                                                    e.RowBounds.Height);


                TextRenderer.DrawText(e.Graphics, (e.RowIndex + 1).ToString(),
                                        dgv.RowHeadersDefaultCellStyle.Font,
                                        rectangle,
                                        dgv.RowHeadersDefaultCellStyle.ForeColor,
                                        TextFormatFlags.VerticalCenter | TextFormatFlags.Right);
            }
            catch
            {

            }
        }

        /// <summary>
        /// 全选按钮
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnSelectAll_Click(object sender, EventArgs e)
        {
            try
            {
                bool bResult = false;
                if (btnSelectAll.Text.Contains("全不选"))
                {
                    bResult = false;
                    btnSelectAll.Text = "全选";
                }
                else
                {
                    bResult = true;
                    btnSelectAll.Text = "全不选";
                }
                foreach (DataRow row in (dgvBatch.DataSource as DataTable).Rows)
                {
                    row["IsChecked"] = bResult;
                }
            }
            catch
            {
                
            }
        }

        /// <summary>
        /// 反选
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnInvertSelect_Click(object sender, EventArgs e)
        {
            try
            {
                int iCheckedCount = 0;
                int iRowCount = 0;
                foreach (DataRow row in (dgvBatch.DataSource as DataTable).Rows)
                {
                    iRowCount++;
                    bool checkedValue = !Convert.ToBoolean(row["IsChecked"]);
                    row["IsChecked"] = checkedValue;
                    if (checkedValue)
                    {
                        iCheckedCount++;
                    }
                }
                if(iCheckedCount == iRowCount)
                {
                    btnSelectAll.Text = "全不选";
                }
                else
                {
                    btnSelectAll.Text = "全选";
                }
            }
            catch
            {

            }
        }

        /// <summary>
        /// 双击选中
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void dgvBatch_MouseDoubleClick(object sender, MouseEventArgs e)
        {
            try
            {
                DataGridViewRow currentRow = dgvBatch.CurrentRow as DataGridViewRow;
                bool bRes = false;
                try
                {
                    bRes = !Convert.ToBoolean(currentRow.Cells["IsChecked"].Value);
                }
                catch
                {
                    bRes = true;
                }
                currentRow.Cells["IsChecked"].Value = bRes;
            }
            catch
            {

            }
        }

        /// <summary>
        /// 监听值改变
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void dgvBatch_CellValueChanged(object sender, DataGridViewCellEventArgs e)
        {
            try
            {
                DataTable dtCopys = (dgvBatch.DataSource as DataTable).Copy();
                dtCopys.Rows[e.RowIndex][e.ColumnIndex] = dgvBatch.Rows[e.RowIndex].Cells[e.ColumnIndex].Value.ToString();
                dtCopys.AcceptChanges();
                if (dtCopys.Select("IsChecked = true").Count() == dtCopys.Rows.Count)
                {
                    btnSelectAll.Text = "全不选";
                }
                else
                {
                    btnSelectAll.Text = "全选";
                }
                dgvBatch.EndEdit();
            }
            catch
            {

            }

        }

        private void dgvBatch_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            try
            {
                //点击IsChecked列的内容时，提交列更改
                if (dgvBatch.Columns[e.ColumnIndex].DataPropertyName.ToUpper().Equals("IsChecked".ToUpper()))
                {
                    dgvBatch.EndEdit();
                }
            }
            catch
            {

            }
        }
        #region 私有方法

        /// <summary>
        /// 绑定DataGrid的数据
        /// </summary>
        /// <param name="listFiles">文件列</param>
        private void BindDataGridView(List<string> listFiles)
        {
            DataTable dataTable = new DataTable();
            DataColumn dcIsChecked = new DataColumn();
            dcIsChecked.ColumnName = "IsChecked";
            dcIsChecked.Caption = "选择";
            dcIsChecked.DataType = typeof(bool);
            dataTable.Columns.Add(dcIsChecked);

            DataColumn dcId = new DataColumn();
            dcId.ColumnName = "Id";
            dcId.Caption = "Id";
            dcId.DataType = typeof(int);
            dataTable.Columns.Add(dcId);

            DataColumn dcFileDirectoryName = new DataColumn();
            dcFileDirectoryName.ColumnName = "FileDirectoryName";
            dcFileDirectoryName.Caption = "文件目录名称";
            dcFileDirectoryName.DataType = typeof(string);
            dataTable.Columns.Add(dcFileDirectoryName);

            DataColumn dcFilePath = new DataColumn();
            dcFilePath.ColumnName = "FilePath";
            dcFilePath.Caption = "文件路径";
            dcFilePath.DataType = typeof(string);
            dataTable.Columns.Add(dcFilePath);
            LoadingLib.Win10Style.LoadingHelper.ShowBlueLoading("正在加载数据,请稍等...", this, o =>
            {
                int iId = 0;
                foreach (string strFile in listFiles)
                {
                    DataRow dr = dataTable.NewRow();
                    iId++;
                    dr["Id"] = iId;
                    try
                    {
                        List<string> listDirectoryNames = strFile.Split(new string[] { @"\" }, StringSplitOptions.None).ToList();
                        string strFileDirectoryName = string.Empty;
                        for (int iList = 0; iList < listDirectoryNames.Count; iList++)
                        {
                            if (iList + 2 < listDirectoryNames.Count)
                            {

                                if (listDirectoryNames[iList + 1].ToUpper().Equals("Properties".ToUpper())
                                && listDirectoryNames[iList + 2].ToUpper().Equals("AssemblyInfo.cs".ToUpper()))
                                {
                                    strFileDirectoryName = listDirectoryNames[iList];
                                    break;
                                }
                            }

                            if (iList + 1 < listDirectoryNames.Count)
                            {
                                if (listDirectoryNames[iList + 1].ToUpper().Equals("AssemblyInfo.cs".ToUpper()))
                                {
                                    strFileDirectoryName = listDirectoryNames[iList];
                                }
                            }
                        }
                        dr["FileDirectoryName"] = strFileDirectoryName;
                    }
                    catch
                    {
                        dr["FileDirectoryName"] = DBNull.Value;
                    }
                    dr["FilePath"] = strFile;
                    dataTable.Rows.Add(dr);
                }
            });

            dgvBatch.DataSource = dataTable;
            
            foreach (DataGridViewColumn dgvc in dgvBatch.Columns)
            {
                dgvc.ReadOnly = false;
                dgvc.HeaderText = dataTable.Columns[dgvc.DataPropertyName].Caption;
                //dgvc.ReadOnly = true;
                if (!dgvc.DataPropertyName.ToUpper().Equals("IsChecked".ToUpper()))
                {
                    dgvc.ReadOnly = true;
                }
            }
            dgvBatch.Columns["Id"].ReadOnly = true;
            dgvBatch.Columns["Id"].Visible = false;
            dgvBatch.AutoResizeColumns();
            dgvBatch.SelectionMode = DataGridViewSelectionMode.FullRowSelect;
            dgvBatch.MultiSelect = false;

        }

        /// <summary>
        /// 加载Ini数据到窗体中
        /// </summary>
        private void LoadIniDataToForm()
        {
            //设置文件批量修改的路径
            tbBatchPath.Text = ConfigIniFileHelper.GetIniData(ConfigIniName, "FormData", "BatchPath");
            tbSinglePath.Text = ConfigIniFileHelper.GetIniData(ConfigIniName, "FormData", "SinglePath");
            tbBackFilePath.Text = ConfigIniFileHelper.GetIniData(ConfigIniName, "FormData", "BackFilePath");
        }

        /// <summary>
        /// 显示日志
        /// </summary>
        private void ShowLogs()
        {
            tbInfo.Text = sbLogs.ToString();
            tbInfo.SelectionStart = tbInfo.Text.Length;
        }

        /// <summary>
        /// 校验参数
        /// </summary>
        /// <returns></returns>
        private bool ValidateParams()
        {
            if (StringToVersion(tbAssemblyVersion1.Text.Trim()) < 0
                || StringToVersion(tbAssemblyVersion1.Text.Trim()) < 0
                || StringToVersion(tbAssemblyVersion2.Text.Trim()) < 0
                || StringToVersion(tbAssemblyVersion3.Text.Trim()) < 0
                || StringToVersion(tbAssemblyVersion4.Text.Trim()) < 0
                || StringToVersion(tbAssemblyFileVersion1.Text.Trim()) < 0
                || StringToVersion(tbAssemblyFileVersion2.Text.Trim()) < 0
                || StringToVersion(tbAssemblyFileVersion3.Text.Trim()) < 0
                || StringToVersion(tbAssemblyFileVersion4.Text.Trim()) < 0)
            {
                MessageBox.Show("请输入正确的版本号,版本号各个参数必须是介于0和65534之间的整数！", "错误", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return false;
            }

            if (cbBackFile.Checked)
            {
                if (string.IsNullOrEmpty(tbBackFilePath.Text))
                {
                    MessageBox.Show("请选择【生成前备份文件】路径！", "错误", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return false;
                }
                else
                {
                    if (string.IsNullOrEmpty(Path.GetDirectoryName(tbBackFilePath.Text.Trim() + @"\")))
                    {
                        MessageBox.Show(
                            "请选择【生成前备份文件】合法的路径！当前路径：\r\n" + tbBackFilePath.Text.Trim() + "不合法！",
                            "错误",
                            MessageBoxButtons.OK,
                            MessageBoxIcon.Error);
                        return false;
                    }
                }
                //设置选中的路径到缓存中
                ConfigIniFileHelper.SetIniData(ConfigIniName, "FormData", "BackFilePath", tbBackFilePath.Text.Trim());
            }
            return true;
        }

        /// <summary>
        /// 字符串转换为版本号的正整数，转换失败返回-1
        /// </summary>
        /// <param name="str"></param>
        /// <returns></returns>
        private int StringToVersion(string str)
        {
            try
            {
                int i = Convert.ToInt32(str);
                if (i >= 0 && i <= 65534)
                {
                    return i;
                }
                else
                {
                    return -1;
                }
            }
            catch
            {
                return -1;
            }
        }

        /// <summary>
        /// 读取程序及文件信息
        /// </summary>
        /// <param name="strFilePath"></param>
        /// <returns></returns>
        private DataTable ReadAssemblyInfo(string strFilePath)
        {
            DataTable dataTable = new DataTable("AssemblyInfo.cs");
            dataTable.Columns.Add("LineCode", typeof(int));
            dataTable.Columns.Add("Name", typeof(string));
            dataTable.Columns.Add("Key", typeof(string));
            dataTable.Columns.Add("Value", typeof(string));
            dataTable.Columns.Add("Content", typeof(string));
            try
            {
                FileStream fs = new FileStream(strFilePath, FileMode.Open, FileAccess.Read);
                StreamReader m_streamReader = new StreamReader(fs, System.Text.Encoding.Default);
                //使用StreamReader类来读取文件
                m_streamReader.BaseStream.Seek(0, SeekOrigin.Begin);
                //从数据流中读取每一行，直到文件的最后一行，并在textBox中显示出内容，其中textBox为文本框，如果不用可以改为别的
                string strLine = m_streamReader.ReadLine();
                int LineCode = 0;
                string strName = "";
                string strKey = "";
                string strValue = "";
                while (strLine != null)
                {
                    DataRow row = dataTable.NewRow();
                    LineCode++;
                    ///this.txtFileContent.Text += strLine + "\n";
                    if (!strLine.Trim().Contains(@"//"))
                    {
                        if (strLine.Contains("AssemblyTitle"))
                        {
                            strName = "标题";
                            strKey = "AssemblyTitle";
                        }
                        else if (strLine.Contains("AssemblyDescription"))
                        {
                            strName = "说明";
                            strKey = "AssemblyDescription";
                        }
                        else if (strLine.Contains("AssemblyCompany"))
                        {
                            strName = "公司";
                            strKey = "AssemblyCompany";
                        }
                        else if (strLine.Contains("AssemblyProduct"))
                        {
                            strName = "产品";
                            strKey = "AssemblyProduct";
                        }
                        else if (strLine.Contains("AssemblyCopyright"))
                        {
                            strName = "版权";
                            strKey = "AssemblyCopyright";
                        }
                        else if (strLine.Contains("AssemblyTrademark"))
                        {
                            strName = "商标";
                            strKey = "AssemblyTrademark";
                        }
                        //else if (strLine.Contains("Guid"))
                        //{
                        //    strName = "Guid";
                        //    strKey = "Guid";
                        //}
                        else if (strLine.Contains("AssemblyVersion"))
                        {
                            strName = "程序集版本";
                            strKey = "AssemblyVersion";
                        }
                        else if (strLine.Contains("AssemblyFileVersion"))
                        {
                            strName = "文件版本";
                            strKey = "AssemblyFileVersion";
                        }
                        else
                        {
                            strName = "";
                            strKey = "";
                            strValue = "";
                        }
                        if (!string.IsNullOrEmpty(strKey))
                        {
                            try
                            {
                                int keyLength = strKey.Length;
                                int startIndex = strLine.IndexOf(strKey + "(\"") + keyLength + 2;
                                strValue = strLine.Substring(startIndex, strLine.IndexOf("\")") - startIndex);
                            }
                            catch
                            {

                            }
                        }
                    }
                    row.ItemArray = new object[] { LineCode, strName, strKey, strValue, strLine };
                    dataTable.Rows.Add(row);
                    strLine = m_streamReader.ReadLine();
                }
                //关闭此StreamReader对象
                m_streamReader.Close();
                fs.Close();
            }
            catch (Exception ex)
            {
                sbLogs.AppendLine("【ERROR】 读取文件【" + strFilePath + "】程序集信息出错！");
                sbLogs.AppendLine("\t" + ex.Message);
                //this.Close();
            }
            finally
            {

            }

            sbLogs.AppendLine("【SUCCESS】 读取文件【" + strFilePath + "】成功！");
            return dataTable;
        }

        /// <summary>
        /// 写入程序文件信息
        /// </summary>
        /// <param name="dataTable"></param>
        /// <param name="strFilePath"></param>
        private void WriteAssemblyInfo(DataTable dataTable, string strFilePath)
        {
            try
            {
                StringBuilder sbContentAll = new StringBuilder();
                foreach (DataRow row in dataTable.Rows)
                {
                    #region 修改数据
                    string strNewValue = string.Empty;
                    if (row["Key"].ToString().Equals("AssemblyTitle") && !string.IsNullOrEmpty(tbAssemblyTitle.Text))
                    {
                        strNewValue = tbAssemblyTitle.Text.Trim();
                    }
                    else if (row["Key"].ToString().Equals("AssemblyDescription") && !string.IsNullOrEmpty(tbAssemblyDescription.Text))
                    {
                        strNewValue = tbAssemblyDescription.Text.Trim();
                    }
                    else if (row["Key"].ToString().Equals("AssemblyCompany") && !string.IsNullOrEmpty(tbAssemblyCompany.Text))
                    {
                        strNewValue = tbAssemblyCompany.Text.Trim();
                    }
                    else if (row["Key"].ToString().Equals("AssemblyProduct") && !string.IsNullOrEmpty(tbAssemblyProduct.Text))
                    {
                        strNewValue = tbAssemblyProduct.Text.Trim();
                    }
                    else if (row["Key"].ToString().Equals("AssemblyCopyright") && !string.IsNullOrEmpty(tbAssemblyCopyright.Text))
                    {
                        strNewValue = tbAssemblyCopyright.Text.Trim();
                    }
                    else if (row["Key"].ToString().Equals("AssemblyTrademark") && !string.IsNullOrEmpty(tbAssemblyTrademark.Text))
                    {
                        strNewValue = tbAssemblyTrademark.Text.Trim();
                    }
                    else if (row["Key"].ToString().Equals("AssemblyVersion"))
                    {
                        //版本号
                        strNewValue = tbAssemblyVersion1.Text.Trim()
                                        + "." + tbAssemblyVersion2.Text.Trim()
                                        + "." + tbAssemblyVersion3.Text.Trim()
                                        + "." + tbAssemblyVersion4.Text.Trim();
                    }
                    else if (row["Key"].ToString().Equals("AssemblyFileVersion"))
                    {
                        //文件版本号
                        strNewValue = tbAssemblyFileVersion1.Text.Trim()
                                        + "." + tbAssemblyFileVersion2.Text.Trim()
                                        + "." + tbAssemblyFileVersion3.Text.Trim()
                                        + "." + tbAssemblyFileVersion4.Text.Trim();
                    }
                    else
                    {
                        strNewValue = "";
                    }
                    #endregion

                    if (!string.IsNullOrEmpty(strNewValue))
                    {
                        if (string.IsNullOrEmpty(row["Value"].ToString()))
                        {
                            string strKey = row["Key"].ToString();
                            int keyLength = strKey.Length;
                            int startIndex = row["Content"].ToString().IndexOf(strKey + "(\"") + keyLength + 2;
                            string strNewContent = row["Content"].ToString().Insert(startIndex, strNewValue);
                            row["Content"] = strNewContent;
                        }
                        else
                        {
                            row["Content"] = row["Content"].ToString().Replace(row["Value"].ToString(), strNewValue);
                        }
                    }

                    //writer.WriteLine(row["Content"].ToString());
                    sbContentAll.AppendLine(row["Content"].ToString());
                }
                //writer.WriteLine(DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss") + " " + msg);
                try
                {
                    if(cbBackFile.Checked)
                    {
                        string strBackRootPath = Path.GetDirectoryName(tbBackFilePath.Text.Trim() + @"\");
                        strBackRootPath += @"\" + MakeDateTime.ToString("yyyyMMddHHmm") + @"\";
                        if(BackFile(strBackRootPath, strFilePath))
                        {
                            //覆盖文件内容
                            File.WriteAllText(strFilePath, sbContentAll.ToString(), System.Text.Encoding.Default);
                            sbLogs.Append("【SUCCESS】 写入文件【" + strFilePath + "】成功！");
                        }
                        else
                        {
                            sbLogs.Append("【ERROR】 写入文件【" + strFilePath + "】失败，可能是备份源文件失败！");
                        }
                    }
                    else
                    {

                        //覆盖文件内容
                        File.WriteAllText(strFilePath, sbContentAll.ToString(), System.Text.Encoding.Default);
                        sbLogs.Append("【SUCCESS】 写入文件【" + strFilePath + "】成功！");
                    }
                }
                catch (Exception e)

                {
                    sbLogs.Append("【ERROR】 写入文件【" + strFilePath + "】出错！");
                    sbLogs.Append("\t" + e.Message);
                }


            }
            catch (Exception ex)
            {
                sbLogs.Append("【ERROR】 写入文件【" + strFilePath + "】出错！");
                sbLogs.Append("\t" + ex.Message);
                //this.Close();
            }
            finally
            {

            }
        }

        /// <summary>
        /// 程序集信息加载到控件
        /// </summary>
        /// <param name="table"></param>
        private void AssemblyInfoLoadToControl(DataTable table)
        {
            foreach (DataRow row in table.Rows)
            {
                string strKey = string.Empty;
                strKey = row["Key"].ToString();
                if (strKey.Equals("AssemblyTitle"))
                {
                    //标题
                    tbAssemblyTitle.Text = row["Value"].ToString();
                }
                else if (strKey.Equals("AssemblyDescription"))
                {
                    //说明   
                    tbAssemblyDescription.Text = row["Value"].ToString();
                }
                else if (strKey.Equals("AssemblyCompany"))
                {
                    //公司   
                    tbAssemblyCompany.Text = row["Value"].ToString();
                }
                else if (strKey.Equals("AssemblyProduct"))
                {
                    //产品   
                    tbAssemblyProduct.Text = row["Value"].ToString();
                }
                else if (strKey.Equals("AssemblyCopyright"))
                {
                    //版权   
                    tbAssemblyCopyright.Text = row["Value"].ToString();
                }
                else if (strKey.Equals("AssemblyTrademark"))
                {
                    //商标   
                    tbAssemblyTrademark.Text = row["Value"].ToString();
                }
                //else if (strKey.Equals("Guid"))
                //{
                //    //Guid   
                //    tbGuid.Text = row["Value"].ToString();
                //}
                else if (strKey.Equals("AssemblyVersion"))
                {
                    //程序集版本   
                    List<string> listVersion = row["Value"].ToString().Split('.').ToList();
                    tbAssemblyVersion1.Text = listVersion[0];
                    tbAssemblyVersion2.Text = listVersion[1];
                    tbAssemblyVersion3.Text = listVersion[2];
                    tbAssemblyVersion4.Text = listVersion[3];
                }
                else if (strKey.Equals("AssemblyFileVersion"))
                {
                    //文件版本                       
                    List<string> listVersion = row["Value"].ToString().Split('.').ToList();
                    tbAssemblyFileVersion1.Text = listVersion[0];
                    tbAssemblyFileVersion2.Text = listVersion[1];
                    tbAssemblyFileVersion3.Text = listVersion[2];
                    tbAssemblyFileVersion4.Text = listVersion[3];
                }
            }
        }

        /// <summary>
        /// 清空控件的值
        /// </summary>
        private void ClearControlValue()
        {
            tbAssemblyTitle.Text = string.Empty;
            tbAssemblyDescription.Text = string.Empty;
            tbAssemblyCompany.Text = string.Empty;
            tbAssemblyProduct.Text = string.Empty;
            tbAssemblyCopyright.Text = string.Empty;
            tbAssemblyTrademark.Text = string.Empty;
            //tbGuid.Text = string.Empty;
            tbAssemblyVersion1.Text = string.Empty;
            tbAssemblyVersion2.Text = string.Empty;
            tbAssemblyVersion3.Text = string.Empty;
            tbAssemblyVersion4.Text = string.Empty;
            tbAssemblyFileVersion1.Text = string.Empty;
            tbAssemblyFileVersion2.Text = string.Empty;
            tbAssemblyFileVersion3.Text = string.Empty;
            tbAssemblyFileVersion4.Text = string.Empty;
        }

        /// <summary>
        /// 备份文件
        /// </summary>
        /// <param name="strBackRootPath">备份根路径</param>
        /// <param name="strFilePath">备份文件路径</param>
        /// <returns></returns>
        private bool BackFile(string strBackRootPath,string strFilePath)
        {
            try
            {
                //备份文件
                //检查文件备份路径
                if (!Directory.Exists(strBackRootPath))
                {
                    Directory.CreateDirectory(strBackRootPath);
                }

                //获取当前操作文件的上级目录
                List<string> listDirectoryNames = strFilePath.Split(new string[] { @"\" }, StringSplitOptions.None).ToList();
                string strFileDirectoryName = string.Empty;
                for (int iList = 0; iList < listDirectoryNames.Count; iList++)
                {
                    if (strFilePath.Contains(@"\Properties"))
                    {
                        if (iList + 1 < listDirectoryNames.Count)
                        {
                            if (listDirectoryNames[iList + 1].ToUpper().Equals(@"Properties".ToUpper()))
                            {
                                strFileDirectoryName = listDirectoryNames[iList];
                            }
                        }
                    }
                    else
                    {
                        if (iList + 1 < listDirectoryNames.Count)
                        {
                            if (listDirectoryNames[iList + 1].ToUpper().Equals("AssemblyInfo.cs".ToUpper()))
                            {
                                strFileDirectoryName = listDirectoryNames[iList];
                            }
                        }
                    }
                }

                if (string.IsNullOrEmpty(strFileDirectoryName))
                {
                    //上级目录为空，返回false
                    sbLogs.AppendLine("【ERROR】无法获取路径【" + strFilePath + "】的上级目录！");
                    return false;
                }

                if (!Directory.Exists(Path.GetDirectoryName(strBackRootPath + @"\" + strFileDirectoryName + @"\")))
                {
                    Directory.CreateDirectory(Path.GetDirectoryName(strBackRootPath + @"\" + strFileDirectoryName + @"\"));
                }
                string strFileName = Path.GetFileName(strFilePath);
                string strCopyFilePath = Path.GetDirectoryName(strBackRootPath + @"\" + strFileDirectoryName + @"\") + @"\" + strFileName;
                File.Copy(strFilePath, strCopyFilePath, true);
                sbLogs.AppendLine("【SUCCESS】已将文件\r\t\n【" + strFilePath + "】\r\n备份至\r\n\t【"+ strCopyFilePath+"】");
                return true;

            }
            catch(Exception ex)
            {
                //上级目录为空，返回false
                sbLogs.AppendLine("【ERROR】备份文件【" + strFilePath + "】失败！\r\n\t" + ex.Message);
                return false;
            }
        }
        #endregion
    }
}
