﻿namespace AssemblyTools
{
    partial class FrmMsBuildManage
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmMsBuildManage));
            this.label1 = new System.Windows.Forms.Label();
            this.tbProjcetRootPath = new System.Windows.Forms.TextBox();
            this.btnProjcetRootPath = new System.Windows.Forms.Button();
            this.tbInfo = new System.Windows.Forms.TextBox();
            this.gbParams = new System.Windows.Forms.GroupBox();
            this.cbBuildExecMethod = new System.Windows.Forms.ComboBox();
            this.label4 = new System.Windows.Forms.Label();
            this.cbExtractRootPath = new System.Windows.Forms.CheckBox();
            this.tbExtractRootPath = new System.Windows.Forms.TextBox();
            this.btnExtractRootPath = new System.Windows.Forms.Button();
            this.cbClearProjet = new System.Windows.Forms.CheckBox();
            this.cbBuildMethod = new System.Windows.Forms.ComboBox();
            this.label2 = new System.Windows.Forms.Label();
            this.cbBuildOutputRootPath = new System.Windows.Forms.CheckBox();
            this.tbBuildOutputRootPath = new System.Windows.Forms.TextBox();
            this.cbEditVersionParam = new System.Windows.Forms.CheckBox();
            this.btnBuildOutputRootPath = new System.Windows.Forms.Button();
            this.label11 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.tbAssemblyVersion3 = new System.Windows.Forms.TextBox();
            this.tbAssemblyVersion1 = new System.Windows.Forms.TextBox();
            this.tbAssemblyVersion4 = new System.Windows.Forms.TextBox();
            this.tbAssemblyFileVersion4 = new System.Windows.Forms.TextBox();
            this.tbAssemblyVersion2 = new System.Windows.Forms.TextBox();
            this.label12 = new System.Windows.Forms.Label();
            this.tbAssemblyFileVersion2 = new System.Windows.Forms.TextBox();
            this.tbAssemblyFileVersion3 = new System.Windows.Forms.TextBox();
            this.tbAssemblyFileVersion1 = new System.Windows.Forms.TextBox();
            this.tcButtom = new System.Windows.Forms.TabControl();
            this.tpLogInfo = new System.Windows.Forms.TabPage();
            this.tpProjcet = new System.Windows.Forms.TabPage();
            this.plFileSelectFull = new System.Windows.Forms.Panel();
            this.dgvProjcet = new System.Windows.Forms.DataGridView();
            this.plFileSelectTop = new System.Windows.Forms.Panel();
            this.tbSearch = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.plTop = new System.Windows.Forms.Panel();
            this.tbMsBuildToolPath = new System.Windows.Forms.TextBox();
            this.btnMsBuildToolPath = new System.Windows.Forms.Button();
            this.label3 = new System.Windows.Forms.Label();
            this.pcParams = new System.Windows.Forms.Panel();
            this.plFull = new System.Windows.Forms.Panel();
            this.gbParams.SuspendLayout();
            this.tcButtom.SuspendLayout();
            this.tpLogInfo.SuspendLayout();
            this.tpProjcet.SuspendLayout();
            this.plFileSelectFull.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvProjcet)).BeginInit();
            this.plFileSelectTop.SuspendLayout();
            this.plTop.SuspendLayout();
            this.pcParams.SuspendLayout();
            this.plFull.SuspendLayout();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(99, 16);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(77, 12);
            this.label1.TabIndex = 2;
            this.label1.Text = "项目根路径：";
            // 
            // tbProjcetRootPath
            // 
            this.tbProjcetRootPath.Location = new System.Drawing.Point(182, 13);
            this.tbProjcetRootPath.Name = "tbProjcetRootPath";
            this.tbProjcetRootPath.Size = new System.Drawing.Size(673, 21);
            this.tbProjcetRootPath.TabIndex = 3;
            // 
            // btnProjcetRootPath
            // 
            this.btnProjcetRootPath.Location = new System.Drawing.Point(853, 12);
            this.btnProjcetRootPath.Name = "btnProjcetRootPath";
            this.btnProjcetRootPath.Size = new System.Drawing.Size(75, 23);
            this.btnProjcetRootPath.TabIndex = 4;
            this.btnProjcetRootPath.Text = "选择";
            this.btnProjcetRootPath.UseVisualStyleBackColor = true;
            this.btnProjcetRootPath.Click += new System.EventHandler(this.btnProjcetRootPath_Click);
            // 
            // tbInfo
            // 
            this.tbInfo.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tbInfo.Location = new System.Drawing.Point(3, 3);
            this.tbInfo.Multiline = true;
            this.tbInfo.Name = "tbInfo";
            this.tbInfo.ReadOnly = true;
            this.tbInfo.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.tbInfo.Size = new System.Drawing.Size(994, 484);
            this.tbInfo.TabIndex = 0;
            // 
            // gbParams
            // 
            this.gbParams.Controls.Add(this.cbBuildExecMethod);
            this.gbParams.Controls.Add(this.label4);
            this.gbParams.Controls.Add(this.cbExtractRootPath);
            this.gbParams.Controls.Add(this.tbExtractRootPath);
            this.gbParams.Controls.Add(this.btnExtractRootPath);
            this.gbParams.Controls.Add(this.cbClearProjet);
            this.gbParams.Controls.Add(this.cbBuildMethod);
            this.gbParams.Controls.Add(this.label2);
            this.gbParams.Controls.Add(this.cbBuildOutputRootPath);
            this.gbParams.Controls.Add(this.tbBuildOutputRootPath);
            this.gbParams.Controls.Add(this.cbEditVersionParam);
            this.gbParams.Controls.Add(this.btnBuildOutputRootPath);
            this.gbParams.Controls.Add(this.label11);
            this.gbParams.Controls.Add(this.label13);
            this.gbParams.Controls.Add(this.tbAssemblyVersion3);
            this.gbParams.Controls.Add(this.tbAssemblyVersion1);
            this.gbParams.Controls.Add(this.tbAssemblyVersion4);
            this.gbParams.Controls.Add(this.tbAssemblyFileVersion4);
            this.gbParams.Controls.Add(this.tbAssemblyVersion2);
            this.gbParams.Controls.Add(this.label12);
            this.gbParams.Controls.Add(this.tbAssemblyFileVersion2);
            this.gbParams.Controls.Add(this.tbAssemblyFileVersion3);
            this.gbParams.Controls.Add(this.tbAssemblyFileVersion1);
            this.gbParams.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gbParams.Location = new System.Drawing.Point(0, 0);
            this.gbParams.Name = "gbParams";
            this.gbParams.Size = new System.Drawing.Size(1008, 137);
            this.gbParams.TabIndex = 0;
            this.gbParams.TabStop = false;
            this.gbParams.Text = "参数值";
            // 
            // cbBuildExecMethod
            // 
            this.cbBuildExecMethod.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbBuildExecMethod.FormattingEnabled = true;
            this.cbBuildExecMethod.Items.AddRange(new object[] {
            "Cmd",
            "Bat"});
            this.cbBuildExecMethod.Location = new System.Drawing.Point(507, 47);
            this.cbBuildExecMethod.Name = "cbBuildExecMethod";
            this.cbBuildExecMethod.Size = new System.Drawing.Size(180, 20);
            this.cbBuildExecMethod.TabIndex = 36;
            this.cbBuildExecMethod.SelectedIndexChanged += new System.EventHandler(this.cbBuildExecMethod_SelectedIndexChanged);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(413, 50);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(89, 12);
            this.label4.TabIndex = 35;
            this.label4.Text = "生成执行方式：";
            // 
            // cbExtractRootPath
            // 
            this.cbExtractRootPath.AutoSize = true;
            this.cbExtractRootPath.Location = new System.Drawing.Point(20, 104);
            this.cbExtractRootPath.Name = "cbExtractRootPath";
            this.cbExtractRootPath.Size = new System.Drawing.Size(156, 16);
            this.cbExtractRootPath.TabIndex = 34;
            this.cbExtractRootPath.Text = "提取编译后的程序集到：";
            this.cbExtractRootPath.UseVisualStyleBackColor = true;
            this.cbExtractRootPath.CheckedChanged += new System.EventHandler(this.cbExtractRootPath_CheckedChanged);
            // 
            // tbExtractRootPath
            // 
            this.tbExtractRootPath.Location = new System.Drawing.Point(182, 101);
            this.tbExtractRootPath.Name = "tbExtractRootPath";
            this.tbExtractRootPath.ReadOnly = true;
            this.tbExtractRootPath.Size = new System.Drawing.Size(673, 21);
            this.tbExtractRootPath.TabIndex = 32;
            // 
            // btnExtractRootPath
            // 
            this.btnExtractRootPath.Enabled = false;
            this.btnExtractRootPath.Location = new System.Drawing.Point(853, 100);
            this.btnExtractRootPath.Name = "btnExtractRootPath";
            this.btnExtractRootPath.Size = new System.Drawing.Size(75, 23);
            this.btnExtractRootPath.TabIndex = 33;
            this.btnExtractRootPath.Text = "选择";
            this.btnExtractRootPath.UseVisualStyleBackColor = true;
            this.btnExtractRootPath.Click += new System.EventHandler(this.btnExtractRootPath_Click);
            // 
            // cbClearProjet
            // 
            this.cbClearProjet.AutoSize = true;
            this.cbClearProjet.Location = new System.Drawing.Point(697, 49);
            this.cbClearProjet.Name = "cbClearProjet";
            this.cbClearProjet.Size = new System.Drawing.Size(108, 16);
            this.cbClearProjet.TabIndex = 31;
            this.cbClearProjet.Text = "编译前清理项目";
            this.cbClearProjet.UseVisualStyleBackColor = true;
            this.cbClearProjet.CheckedChanged += new System.EventHandler(this.cbClearProjet_CheckedChanged);
            // 
            // cbBuildMethod
            // 
            this.cbBuildMethod.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbBuildMethod.FormattingEnabled = true;
            this.cbBuildMethod.Items.AddRange(new object[] {
            "Release",
            "Debug"});
            this.cbBuildMethod.Location = new System.Drawing.Point(216, 47);
            this.cbBuildMethod.Name = "cbBuildMethod";
            this.cbBuildMethod.Size = new System.Drawing.Size(180, 20);
            this.cbBuildMethod.TabIndex = 30;
            this.cbBuildMethod.SelectedIndexChanged += new System.EventHandler(this.cbBuildMethod_SelectedIndexChanged);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(145, 50);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(65, 12);
            this.label2.TabIndex = 29;
            this.label2.Text = "编译方式：";
            // 
            // cbBuildOutputRootPath
            // 
            this.cbBuildOutputRootPath.AutoSize = true;
            this.cbBuildOutputRootPath.Location = new System.Drawing.Point(20, 75);
            this.cbBuildOutputRootPath.Name = "cbBuildOutputRootPath";
            this.cbBuildOutputRootPath.Size = new System.Drawing.Size(156, 16);
            this.cbBuildOutputRootPath.TabIndex = 28;
            this.cbBuildOutputRootPath.Text = "指定编译后输出根路径：";
            this.cbBuildOutputRootPath.UseVisualStyleBackColor = true;
            this.cbBuildOutputRootPath.CheckedChanged += new System.EventHandler(this.cbBuildOutputRootPath_CheckedChanged);
            // 
            // tbBuildOutputRootPath
            // 
            this.tbBuildOutputRootPath.Location = new System.Drawing.Point(182, 73);
            this.tbBuildOutputRootPath.Name = "tbBuildOutputRootPath";
            this.tbBuildOutputRootPath.ReadOnly = true;
            this.tbBuildOutputRootPath.Size = new System.Drawing.Size(673, 21);
            this.tbBuildOutputRootPath.TabIndex = 6;
            // 
            // cbEditVersionParam
            // 
            this.cbEditVersionParam.AutoSize = true;
            this.cbEditVersionParam.Location = new System.Drawing.Point(20, 23);
            this.cbEditVersionParam.Name = "cbEditVersionParam";
            this.cbEditVersionParam.Size = new System.Drawing.Size(96, 16);
            this.cbEditVersionParam.TabIndex = 27;
            this.cbEditVersionParam.Text = "修改版本参数";
            this.cbEditVersionParam.UseVisualStyleBackColor = true;
            this.cbEditVersionParam.CheckedChanged += new System.EventHandler(this.cbEditVersionParam_CheckedChanged);
            // 
            // btnBuildOutputRootPath
            // 
            this.btnBuildOutputRootPath.Enabled = false;
            this.btnBuildOutputRootPath.Location = new System.Drawing.Point(853, 72);
            this.btnBuildOutputRootPath.Name = "btnBuildOutputRootPath";
            this.btnBuildOutputRootPath.Size = new System.Drawing.Size(75, 23);
            this.btnBuildOutputRootPath.TabIndex = 7;
            this.btnBuildOutputRootPath.Text = "选择";
            this.btnBuildOutputRootPath.UseVisualStyleBackColor = true;
            this.btnBuildOutputRootPath.Click += new System.EventHandler(this.btnBuildOutputRootPath_Click);
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(133, 24);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(77, 12);
            this.label11.TabIndex = 12;
            this.label11.Text = "程序集版本：";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Font = new System.Drawing.Font("宋体", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label13.ForeColor = System.Drawing.Color.Red;
            this.label13.Location = new System.Drawing.Point(694, 23);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(288, 14);
            this.label13.TabIndex = 25;
            this.label13.Text = "*版本号必须是介于0和65534之间的整数；";
            // 
            // tbAssemblyVersion3
            // 
            this.tbAssemblyVersion3.Location = new System.Drawing.Point(310, 20);
            this.tbAssemblyVersion3.Name = "tbAssemblyVersion3";
            this.tbAssemblyVersion3.Size = new System.Drawing.Size(40, 21);
            this.tbAssemblyVersion3.TabIndex = 15;
            // 
            // tbAssemblyVersion1
            // 
            this.tbAssemblyVersion1.Location = new System.Drawing.Point(216, 20);
            this.tbAssemblyVersion1.Name = "tbAssemblyVersion1";
            this.tbAssemblyVersion1.Size = new System.Drawing.Size(40, 21);
            this.tbAssemblyVersion1.TabIndex = 13;
            // 
            // tbAssemblyVersion4
            // 
            this.tbAssemblyVersion4.Location = new System.Drawing.Point(356, 20);
            this.tbAssemblyVersion4.Name = "tbAssemblyVersion4";
            this.tbAssemblyVersion4.Size = new System.Drawing.Size(40, 21);
            this.tbAssemblyVersion4.TabIndex = 16;
            // 
            // tbAssemblyFileVersion4
            // 
            this.tbAssemblyFileVersion4.Location = new System.Drawing.Point(648, 20);
            this.tbAssemblyFileVersion4.Name = "tbAssemblyFileVersion4";
            this.tbAssemblyFileVersion4.Size = new System.Drawing.Size(40, 21);
            this.tbAssemblyFileVersion4.TabIndex = 21;
            // 
            // tbAssemblyVersion2
            // 
            this.tbAssemblyVersion2.Location = new System.Drawing.Point(262, 20);
            this.tbAssemblyVersion2.Name = "tbAssemblyVersion2";
            this.tbAssemblyVersion2.Size = new System.Drawing.Size(40, 21);
            this.tbAssemblyVersion2.TabIndex = 14;
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(437, 24);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(65, 12);
            this.label12.TabIndex = 17;
            this.label12.Text = "文件版本：";
            // 
            // tbAssemblyFileVersion2
            // 
            this.tbAssemblyFileVersion2.Location = new System.Drawing.Point(554, 20);
            this.tbAssemblyFileVersion2.Name = "tbAssemblyFileVersion2";
            this.tbAssemblyFileVersion2.Size = new System.Drawing.Size(40, 21);
            this.tbAssemblyFileVersion2.TabIndex = 19;
            // 
            // tbAssemblyFileVersion3
            // 
            this.tbAssemblyFileVersion3.Location = new System.Drawing.Point(602, 20);
            this.tbAssemblyFileVersion3.Name = "tbAssemblyFileVersion3";
            this.tbAssemblyFileVersion3.Size = new System.Drawing.Size(40, 21);
            this.tbAssemblyFileVersion3.TabIndex = 20;
            // 
            // tbAssemblyFileVersion1
            // 
            this.tbAssemblyFileVersion1.Location = new System.Drawing.Point(508, 20);
            this.tbAssemblyFileVersion1.Name = "tbAssemblyFileVersion1";
            this.tbAssemblyFileVersion1.Size = new System.Drawing.Size(40, 21);
            this.tbAssemblyFileVersion1.TabIndex = 18;
            // 
            // tcButtom
            // 
            this.tcButtom.Controls.Add(this.tpLogInfo);
            this.tcButtom.Controls.Add(this.tpProjcet);
            this.tcButtom.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tcButtom.Location = new System.Drawing.Point(0, 0);
            this.tcButtom.Name = "tcButtom";
            this.tcButtom.SelectedIndex = 0;
            this.tcButtom.Size = new System.Drawing.Size(1008, 516);
            this.tcButtom.TabIndex = 0;
            // 
            // tpLogInfo
            // 
            this.tpLogInfo.Controls.Add(this.tbInfo);
            this.tpLogInfo.Location = new System.Drawing.Point(4, 22);
            this.tpLogInfo.Name = "tpLogInfo";
            this.tpLogInfo.Padding = new System.Windows.Forms.Padding(3);
            this.tpLogInfo.Size = new System.Drawing.Size(1000, 490);
            this.tpLogInfo.TabIndex = 0;
            this.tpLogInfo.Text = "日志信息";
            this.tpLogInfo.UseVisualStyleBackColor = true;
            // 
            // tpProjcet
            // 
            this.tpProjcet.Controls.Add(this.plFileSelectFull);
            this.tpProjcet.Controls.Add(this.plFileSelectTop);
            this.tpProjcet.Location = new System.Drawing.Point(4, 22);
            this.tpProjcet.Name = "tpProjcet";
            this.tpProjcet.Padding = new System.Windows.Forms.Padding(3);
            this.tpProjcet.Size = new System.Drawing.Size(1000, 490);
            this.tpProjcet.TabIndex = 1;
            this.tpProjcet.Text = "项目选择";
            this.tpProjcet.UseVisualStyleBackColor = true;
            // 
            // plFileSelectFull
            // 
            this.plFileSelectFull.Controls.Add(this.dgvProjcet);
            this.plFileSelectFull.Dock = System.Windows.Forms.DockStyle.Fill;
            this.plFileSelectFull.Location = new System.Drawing.Point(3, 26);
            this.plFileSelectFull.Name = "plFileSelectFull";
            this.plFileSelectFull.Size = new System.Drawing.Size(994, 461);
            this.plFileSelectFull.TabIndex = 2;
            // 
            // dgvProjcet
            // 
            this.dgvProjcet.AllowUserToAddRows = false;
            this.dgvProjcet.AllowUserToDeleteRows = false;
            this.dgvProjcet.AllowUserToOrderColumns = true;
            this.dgvProjcet.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvProjcet.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgvProjcet.Location = new System.Drawing.Point(0, 0);
            this.dgvProjcet.Name = "dgvProjcet";
            this.dgvProjcet.RowTemplate.Height = 23;
            this.dgvProjcet.Size = new System.Drawing.Size(994, 461);
            this.dgvProjcet.TabIndex = 0;
            this.dgvProjcet.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvProjcet_CellContentClick);
            this.dgvProjcet.CellMouseDoubleClick += new System.Windows.Forms.DataGridViewCellMouseEventHandler(this.dgvProjcet_CellMouseDoubleClick);
            this.dgvProjcet.RowPostPaint += new System.Windows.Forms.DataGridViewRowPostPaintEventHandler(this.dgvProjcet_RowPostPaint);
            // 
            // plFileSelectTop
            // 
            this.plFileSelectTop.Controls.Add(this.tbSearch);
            this.plFileSelectTop.Controls.Add(this.label5);
            this.plFileSelectTop.Dock = System.Windows.Forms.DockStyle.Top;
            this.plFileSelectTop.Location = new System.Drawing.Point(3, 3);
            this.plFileSelectTop.Name = "plFileSelectTop";
            this.plFileSelectTop.Size = new System.Drawing.Size(994, 23);
            this.plFileSelectTop.TabIndex = 1;
            // 
            // tbSearch
            // 
            this.tbSearch.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tbSearch.Location = new System.Drawing.Point(67, 0);
            this.tbSearch.Name = "tbSearch";
            this.tbSearch.Size = new System.Drawing.Size(927, 21);
            this.tbSearch.TabIndex = 0;
            this.tbSearch.TextChanged += new System.EventHandler(this.tbSearch_TextChanged);
            // 
            // label5
            // 
            this.label5.Dock = System.Windows.Forms.DockStyle.Left;
            this.label5.Font = new System.Drawing.Font("宋体", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label5.ImageAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.label5.Location = new System.Drawing.Point(0, 0);
            this.label5.Margin = new System.Windows.Forms.Padding(0, 0, 3, 0);
            this.label5.Name = "label5";
            this.label5.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.label5.Size = new System.Drawing.Size(67, 23);
            this.label5.TabIndex = 18;
            this.label5.Text = "筛选：";
            this.label5.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // plTop
            // 
            this.plTop.Controls.Add(this.tbMsBuildToolPath);
            this.plTop.Controls.Add(this.btnMsBuildToolPath);
            this.plTop.Controls.Add(this.label3);
            this.plTop.Controls.Add(this.tbProjcetRootPath);
            this.plTop.Controls.Add(this.label1);
            this.plTop.Controls.Add(this.btnProjcetRootPath);
            this.plTop.Dock = System.Windows.Forms.DockStyle.Top;
            this.plTop.Location = new System.Drawing.Point(0, 0);
            this.plTop.Name = "plTop";
            this.plTop.Size = new System.Drawing.Size(1008, 76);
            this.plTop.TabIndex = 0;
            // 
            // tbMsBuildToolPath
            // 
            this.tbMsBuildToolPath.Location = new System.Drawing.Point(182, 44);
            this.tbMsBuildToolPath.Name = "tbMsBuildToolPath";
            this.tbMsBuildToolPath.Size = new System.Drawing.Size(673, 21);
            this.tbMsBuildToolPath.TabIndex = 6;
            // 
            // btnMsBuildToolPath
            // 
            this.btnMsBuildToolPath.Location = new System.Drawing.Point(853, 43);
            this.btnMsBuildToolPath.Name = "btnMsBuildToolPath";
            this.btnMsBuildToolPath.Size = new System.Drawing.Size(75, 23);
            this.btnMsBuildToolPath.TabIndex = 7;
            this.btnMsBuildToolPath.Text = "选择";
            this.btnMsBuildToolPath.UseVisualStyleBackColor = true;
            this.btnMsBuildToolPath.Click += new System.EventHandler(this.btnMsBuildToolPath_Click);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(69, 47);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(107, 12);
            this.label3.TabIndex = 5;
            this.label3.Text = "MSBuild工具路径：";
            // 
            // pcParams
            // 
            this.pcParams.Controls.Add(this.gbParams);
            this.pcParams.Dock = System.Windows.Forms.DockStyle.Top;
            this.pcParams.Location = new System.Drawing.Point(0, 76);
            this.pcParams.Name = "pcParams";
            this.pcParams.Size = new System.Drawing.Size(1008, 137);
            this.pcParams.TabIndex = 3;
            // 
            // plFull
            // 
            this.plFull.Controls.Add(this.tcButtom);
            this.plFull.Dock = System.Windows.Forms.DockStyle.Fill;
            this.plFull.Location = new System.Drawing.Point(0, 213);
            this.plFull.Name = "plFull";
            this.plFull.Size = new System.Drawing.Size(1008, 516);
            this.plFull.TabIndex = 4;
            // 
            // FrmMsBuildManage
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1008, 729);
            this.Controls.Add(this.plFull);
            this.Controls.Add(this.pcParams);
            this.Controls.Add(this.plTop);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.Name = "FrmMsBuildManage";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "MSBuild管理工具";
            this.Load += new System.EventHandler(this.FrmMsBuildManage_Load);
            this.gbParams.ResumeLayout(false);
            this.gbParams.PerformLayout();
            this.tcButtom.ResumeLayout(false);
            this.tpLogInfo.ResumeLayout(false);
            this.tpLogInfo.PerformLayout();
            this.tpProjcet.ResumeLayout(false);
            this.plFileSelectFull.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgvProjcet)).EndInit();
            this.plFileSelectTop.ResumeLayout(false);
            this.plFileSelectTop.PerformLayout();
            this.plTop.ResumeLayout(false);
            this.plTop.PerformLayout();
            this.pcParams.ResumeLayout(false);
            this.plFull.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox tbProjcetRootPath;
        private System.Windows.Forms.Button btnProjcetRootPath;
        private System.Windows.Forms.TextBox tbInfo;
        private System.Windows.Forms.GroupBox gbParams;
        private System.Windows.Forms.TextBox tbAssemblyVersion1;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.TextBox tbAssemblyFileVersion1;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.TextBox tbAssemblyVersion3;
        private System.Windows.Forms.TextBox tbAssemblyVersion2;
        private System.Windows.Forms.TextBox tbAssemblyVersion4;
        private System.Windows.Forms.TextBox tbAssemblyFileVersion4;
        private System.Windows.Forms.TextBox tbAssemblyFileVersion3;
        private System.Windows.Forms.TextBox tbAssemblyFileVersion2;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.TabControl tcButtom;
        private System.Windows.Forms.TabPage tpLogInfo;
        private System.Windows.Forms.TabPage tpProjcet;
        private System.Windows.Forms.Panel plTop;
        private System.Windows.Forms.Panel pcParams;
        private System.Windows.Forms.Panel plFull;
        private System.Windows.Forms.DataGridView dgvProjcet;
        private System.Windows.Forms.Panel plFileSelectTop;
        private System.Windows.Forms.Panel plFileSelectFull;
        private System.Windows.Forms.CheckBox cbEditVersionParam;
        private System.Windows.Forms.TextBox tbBuildOutputRootPath;
        private System.Windows.Forms.Button btnBuildOutputRootPath;
        private System.Windows.Forms.CheckBox cbBuildOutputRootPath;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.ComboBox cbBuildMethod;
        private System.Windows.Forms.CheckBox cbClearProjet;
        private System.Windows.Forms.CheckBox cbExtractRootPath;
        private System.Windows.Forms.TextBox tbExtractRootPath;
        private System.Windows.Forms.Button btnExtractRootPath;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox tbMsBuildToolPath;
        private System.Windows.Forms.Button btnMsBuildToolPath;
        private System.Windows.Forms.ComboBox cbBuildExecMethod;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox tbSearch;
        private System.Windows.Forms.Label label5;
    }
}