﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace AssemblyTools
{
    /// <summary>
    /// 打印输出
    /// </summary>
    public partial class FrmOutputPrint : Form
    {
        /// <summary>
        /// 命令
        /// </summary>
        public string Command { get; set; } = string.Empty;

        /// <summary>
        /// 信息
        /// </summary>
        public string Info { get; set; } = string.Empty;

        public FrmOutputPrint()
        {
            InitializeComponent();
        }

        private void btnCopyInfo_Click(object sender, EventArgs e)
        {
            Clipboard.SetText(rtbInfo.Text);
        }

        private void btnCopyCommand_Click(object sender, EventArgs e)
        {
            Clipboard.SetText(Command);
        }

        /// <summary>
        /// 显示命令
        /// </summary>
        public void ShowCommand()
        {
            rtbInfo.Text = Command;
        }

        /// <summary>
        /// 显示信息
        /// </summary>
        public void ShowInfo()
        {
            rtbInfo.Text = Info;
        }

        /// <summary>
        /// 进程执行
        /// </summary>
        /// <param name="strCommand"></param>
        public bool ProcessExecuteByCmd(string strCommand)
        {
            try
            {
                StringBuilder sb = new StringBuilder();
                Command = strCommand;                
                strCommand = strCommand.Trim().TrimEnd('&') + "&exit";//说明：不管命令是否成功均执行exit命令，否则当调用ReadToEnd()方法时，会处于假死状态
                using (Process p = new Process())
                {
                    p.StartInfo.FileName = "cmd.exe";
                    p.StartInfo.UseShellExecute = false;        //是否使用操作系统shell启动
                    p.StartInfo.RedirectStandardInput = true;   //接受来自调用程序的输入信息
                    p.StartInfo.RedirectStandardOutput = true;  //由调用程序获取输出信息
                    p.StartInfo.RedirectStandardError = true;   //重定向标准错误输出
                    p.StartInfo.CreateNoWindow = true;          //不显示程序窗口
                    p.Start();//启动程序
                              //向cmd窗口写入命令
                    p.StandardInput.WriteLine(strCommand);
                    p.StandardInput.AutoFlush = true;
                    //获取cmd窗口的输出信息
                    StreamReader reader = p.StandardOutput;//截取输出流
                    string line = reader.ReadLine();//每次读取一行
                    sb.AppendLine(line);
                    rtbInfo.AppendText(line + "\n");
                    while (!reader.EndOfStream)
                    {
                        line = reader.ReadLine();
                        sb.AppendLine(line);
                        rtbInfo.AppendText(line + "\n");
                    }
                    p.WaitForExit();//等待程序执行完退出进程
                    p.Close();
                    Info = sb.ToString();
                    DialogResult = DialogResult.OK;
                    return true;
                }
            }
            catch
            {
                DialogResult = DialogResult.Cancel;
                return false;
            }
        }


        /// <summary>
        /// 进程执行
        /// </summary>
        /// <param name="strCommand"></param>
        public bool ProcessExecuteByBat(string strBatPath, string strCommand)
        {
            try
            {
                StringBuilder sb = new StringBuilder();
                Command = strCommand;
                //strCommand = strCommand.Trim().TrimEnd('&') + "&exit";//说明：不管命令是否成功均执行exit命令，否则当调用ReadToEnd()方法时，会处于假死状态
                using (Process p = new Process())
                {
                    p.StartInfo.FileName = strBatPath;
                    p.StartInfo.UseShellExecute = false;        //是否使用操作系统shell启动
                    p.StartInfo.RedirectStandardInput = true;   //接受来自调用程序的输入信息
                    p.StartInfo.RedirectStandardOutput = true;  //由调用程序获取输出信息
                    p.StartInfo.RedirectStandardError = true;   //重定向标准错误输出
                    p.StartInfo.CreateNoWindow = true;          //不显示程序窗口
                    p.Start();//启动程序
                              //向cmd窗口写入命令
                    //p.StandardInput.WriteLine(strCommand);
                    p.StandardInput.AutoFlush = true;
                    //获取cmd窗口的输出信息
                    StreamReader reader = p.StandardOutput;//截取输出流
                    string line = reader.ReadLine();//每次读取一行
                    sb.AppendLine(line);
                    rtbInfo.AppendText(line + "\n");
                    while (!reader.EndOfStream)
                    {
                        line = reader.ReadLine();
                        sb.AppendLine(line);
                        rtbInfo.AppendText(line + "\n");
                    }
                    p.WaitForExit();//等待程序执行完退出进程
                    p.Close();
                    Info = sb.ToString();
                    DialogResult = DialogResult.OK;
                    return true;
                }
            }
            catch
            {
                DialogResult = DialogResult.Cancel;
                return false;
            }
        }

        private void FrmOutputPrint_Load(object sender, EventArgs e)
        {
            rtbInfo.SelectionStart = rtbInfo.Text.Length;
            rtbInfo.Focus();
        }

        private void btnOK_Click(object sender, EventArgs e)
        {
            DialogResult = DialogResult.OK;
            this.Close();
        }
    }
}
