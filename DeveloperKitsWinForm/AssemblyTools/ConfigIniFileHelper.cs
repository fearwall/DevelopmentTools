﻿using CommonLib.Helper.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AssemblyTools
{
    /// <summary>
    /// ini配置文件助手类
    /// </summary>
    public class ConfigIniFileHelper
    {
        /// <summary>
        /// ConfigIni路径
        /// </summary>
        public static string Path = FileHelper.GetCurrentAppPath() + @"Config\";

        /// <summary>
        /// 获取Ini数据，无数据则返回 string.Empty
        /// </summary>
        /// <param name="ConfigFileName">ini配置文件名</param>
        /// <param name="Section">配置节</param>
        /// <param name="Key">指定配置节的Key的名称</param>
        /// <returns></returns>
        public static string GetIniData(string ConfigFileName,string Section,string Key)
        {
            return FileHelper.GetIniData(Path + ConfigFileName, Section, Key);
        }


        /// <summary>
        /// 设置Ini数据，成功返回True，失败返回False
        /// </summary>
        /// <param name="ConfigFileName">ini配置文件名</param>
        /// <param name="Section">配置节</param>
        /// <param name="Key">指定配置节的Key名称</param>
        /// <param name="Key">指定配置节的Key的值</param>
        /// <returns></returns>
        public static bool SetIniData(string ConfigFileName, string Section, string Key,string Value)
        {
            return FileHelper.WriteIniData(Path + ConfigFileName, Section, Key,Value);
        }
    }
}
