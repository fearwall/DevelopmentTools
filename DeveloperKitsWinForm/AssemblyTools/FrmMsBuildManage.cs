﻿using CommonLib.Helper.Common;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace AssemblyTools
{
    public partial class FrmMsBuildManage : Form
    {
        /// <summary>
        /// Ini配置文件名称
        /// </summary>
        private const string ConfigIniName = "MsBuildManage";

        /// <summary>
        /// 日志StringBuilder
        /// </summary>
        StringBuilder sbLogs = new StringBuilder();

        /// <summary>
        /// 生成时间
        /// </summary>
        private DateTime MakeDateTime = DateTime.Now;

        public FrmMsBuildManage()
        {
            InitializeComponent();
        }       

        /// <summary>
        /// 项目根路径
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnProjcetRootPath_Click(object sender, EventArgs e)
        {
            //ClearAssembluyParamValue();
            sbLogs.Clear();
            string strFolderPath = CommonLib.Helper.Common.FilePathHelper.FolderPathSelect(tbProjcetRootPath.Text.Trim(),true);
            if (string.IsNullOrEmpty(strFolderPath))
            {
                MessageBox.Show("请选择项目根路径！", "错误", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
            else
            {
                List<string> listBatchFilePath = new List<string>();
                LoadingLib.Win10Style.LoadingHelper.ShowBlueLoading("正在读取文件,请稍等...", this, o =>
                {
                    listBatchFilePath = CommonLib.Helper.Common.FileSearchHelper.GetFileSystemEntries(
                    strFolderPath,
                    CommonLib.Helper.Common.FileSearchHelper.GetAssignExtensionNameRegexRules(new List<string>() { "csproj" }),
                    true, false).ToList();
                    if (listBatchFilePath.Count <= 0)
                    {
                        MessageBox.Show("选中的路径无可操作的文件类型【*.csproj】！", "错误", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        return;
                    }
                });
                tbProjcetRootPath.Text = strFolderPath;
                BindDataGridView(listBatchFilePath);
                sbLogs.AppendLine("读取文件路径【" + strFolderPath + "】成功！共获取到" + listBatchFilePath.Count + "个文件！");
            }
            //设置选中的路径到缓存中
            ConfigIniFileHelper.SetIniData(ConfigIniName, "FormData", "ProjcetRootPath", strFolderPath);
            tcButtom.SelectedTab = tpProjcet;
            ShowLogs();
            dgvProjcet.AutoResizeColumns();
        }

        /// <summary>
        /// 修改版本参数
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void cbEditVersionParam_CheckedChanged(object sender, EventArgs e)
        {
            SetAssembluyParamStatus(cbEditVersionParam.Checked);
            if (cbEditVersionParam.Checked)
            {
                MakeDateTime = DateTime.Now;
                SetAssembluyParamDefaultValue();
            }
            else
            {
                ClearAssembluyParamValue();
            }

            ConfigIniFileHelper.SetIniData(ConfigIniName, "FormControlStatus", "cbEditVersionParamChecked", cbEditVersionParam.Checked.ToString());
        }

        /// <summary>
        /// 清理按钮
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void cbClearProjet_CheckedChanged(object sender, EventArgs e)
        {
            ConfigIniFileHelper.SetIniData(ConfigIniName, "FormControlStatus", "cbClearProjetChecked", cbClearProjet.Checked.ToString());
        }

        /// <summary>
        /// MSBuild工具路径
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnMsBuildToolPath_Click(object sender, EventArgs e)
        {
            sbLogs.Clear();
            string strFilePath = "";
            if (string.IsNullOrEmpty(tbMsBuildToolPath.Text.Trim()))
            {
                strFilePath = CommonLib.Helper.Common.FilePathHelper.FilePathSelect("MSBuild工具|MSBuild.exe");
                if (string.IsNullOrEmpty(strFilePath) || !strFilePath.Contains("MSBuild.exe"))
                {
                    MessageBox.Show("请选择指定的文件【MSBuild.exe】！", "错误", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return;
                }
                tbMsBuildToolPath.Text = strFilePath;
            }
            else
            {
                strFilePath = tbMsBuildToolPath.Text.Trim();
            }

            if (!File.Exists(strFilePath) && !strFilePath.Contains("MSBuild.exe"))
            {
                MessageBox.Show("请选择指定的文件【MSBuild.exe】！", "错误", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }

            //设置选中的路径到缓存中
            ConfigIniFileHelper.SetIniData(ConfigIniName, "FormData", "MsBuildToolPath", strFilePath);
            ShowLogs();
        }

        /// <summary>
        /// 输出根路径
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnBuildOutputRootPath_Click(object sender, EventArgs e)
        {
            string strFolderPath = CommonLib.Helper.Common.FilePathHelper.FolderPathSelect(tbBuildOutputRootPath.Text.Trim(), true);
            if (string.IsNullOrEmpty(strFolderPath))
            {
                MessageBox.Show("请选择输出根路径！", "错误", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
            //设置选中的路径到缓存中
            ConfigIniFileHelper.SetIniData(ConfigIniName, "FormData", "BuildOutputRootPath", strFolderPath);
            tbBuildOutputRootPath.Text = strFolderPath;
        }

        /// <summary>
        /// 提取根路径
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnExtractRootPath_Click(object sender, EventArgs e)
        {
            string strFolderPath = CommonLib.Helper.Common.FilePathHelper.FolderPathSelect(tbExtractRootPath.Text.Trim(), true);
            if (string.IsNullOrEmpty(strFolderPath))
            {
                MessageBox.Show("请选择提取根路径！", "错误", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
            //设置选中的路径到缓存中
            ConfigIniFileHelper.SetIniData(ConfigIniName, "FormData", "ExtractRootPath", strFolderPath);
            tbExtractRootPath.Text = strFolderPath;
        }

        /// <summary>
        /// 输出根路径选择
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void cbBuildOutputRootPath_CheckedChanged(object sender, EventArgs e)
        {
            tbBuildOutputRootPath.ReadOnly = !cbBuildOutputRootPath.Checked;
            btnBuildOutputRootPath.Enabled = cbBuildOutputRootPath.Checked;
            ConfigIniFileHelper.SetIniData(ConfigIniName, "FormControlStatus", "cbBuildOutputRootPathChecked", cbBuildOutputRootPath.Checked.ToString());
            if (cbExtractRootPath.Checked && cbBuildOutputRootPath.Checked == false)
            {
                cbBuildOutputRootPath.Checked = true;
                MessageBox.Show("若要提取程序集，必须选择输出根路径！", "提示", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }

        /// <summary>
        /// 提取跟路径选择
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void cbExtractRootPath_CheckedChanged(object sender, EventArgs e)
        {
            tbExtractRootPath.ReadOnly = !cbExtractRootPath.Checked;
            btnExtractRootPath.Enabled = cbExtractRootPath.Checked;
            ConfigIniFileHelper.SetIniData(ConfigIniName, "FormControlStatus", "cbExtractRootPathChecked", cbExtractRootPath.Checked.ToString());
            if (cbExtractRootPath.Checked && cbBuildOutputRootPath.Checked == false)
            {
                //必须选中输出根路径，否则无法抽取程序集
                cbBuildOutputRootPath.Checked = true;                
                MessageBox.Show("若要提取程序集，必须选择输出根路径！", "提示", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }
               
        /// <summary>
        /// 编译方式
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void cbBuildMethod_SelectedIndexChanged(object sender, EventArgs e)
        {
            ConfigIniFileHelper.SetIniData(ConfigIniName, "FormControlValue", "cbBuildMethod", cbBuildMethod.Text.ToString());
        }

        /// <summary>
        /// 生成执行方式
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void cbBuildExecMethod_SelectedIndexChanged(object sender, EventArgs e)
        {
            ConfigIniFileHelper.SetIniData(ConfigIniName, "FormControlValue", "cbBuildExecMethod", cbBuildExecMethod.Text.ToString());
        }

        private void FrmMsBuildManage_Load(object sender, EventArgs e)
        {
            LoadIniDataToForm();
        }

        /// <summary>
        /// 绘制表格行号
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void dgvProjcet_RowPostPaint(object sender, DataGridViewRowPostPaintEventArgs e)
        {
            try
            {
                DataGridView dgv = sender as DataGridView;
                Rectangle rectangle = new Rectangle(e.RowBounds.Location.X,
                                                    e.RowBounds.Location.Y,
                                                    dgv.RowHeadersWidth - 4,
                                                    e.RowBounds.Height);


                TextRenderer.DrawText(e.Graphics, (e.RowIndex + 1).ToString(),
                                        dgv.RowHeadersDefaultCellStyle.Font,
                                        rectangle,
                                        dgv.RowHeadersDefaultCellStyle.ForeColor,
                                        TextFormatFlags.VerticalCenter | TextFormatFlags.Right);
            }
            catch
            {

            }
        }        

        private void dgvProjcet_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            try
            {
                //点击IsChecked列的内容时，提交列更改
                if (dgvProjcet.Columns[e.ColumnIndex].DataPropertyName.ToUpper().Equals("Make".ToUpper()))
                {
                    Make();
                }
            }
            catch(Exception ex)
            {
                MessageBox.Show("生成失败！\r\n" + ex.Message, "错误", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
        }


        private void dgvProjcet_CellMouseDoubleClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            try
            {
                //点击IsChecked列的内容时，提交列更改
                Make();
            }
            catch (Exception ex)
            {
                MessageBox.Show("生成失败！\r\n" + ex.Message, "错误", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
        }

        /// <summary>
        /// 筛选
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void tbSearch_TextChanged(object sender, EventArgs e)
        {
            try
            {
                if (string.IsNullOrEmpty(tbSearch.Text))
                {
                    dgvProjcet.DataSource = dgvProjcet.Tag;
                }
                else
                {
                    dgvProjcet.DataSource = (dgvProjcet.Tag as DataTable).Select("  AssemblyName LIKE '%" + tbSearch.Text + "%' OR FilePath LIKE '%" + tbSearch.Text + "%'").CopyToDataTable();
                }
            }
            catch
            {

            }
        }
        #region 私有方法

        /// <summary>
        /// 生成
        /// </summary>
        private void Make()
        {
            try
            {
                if (ValidateParams())
                {
                    if (MessageBox.Show("确定生成选中的项目吗？", "确认", MessageBoxButtons.OKCancel, MessageBoxIcon.Question, MessageBoxDefaultButton.Button1) == DialogResult.OK)
                    {
                        DataGridViewRow currentRow = dgvProjcet.CurrentRow as DataGridViewRow;
                        //最后一个目录
                        string strFolderLastName = currentRow.Cells["AssemblyName"].Value.ToString();

                        IMSBuildCommand msBuildCommand = null;
                        if (cbBuildExecMethod.Text == "Bat")
                        {
                            msBuildCommand = new BatMsBuildCommand();
                        }
                        else if (cbBuildExecMethod.Text == "Cmd")
                        {
                            msBuildCommand = new CmdMsBuildCommand();
                        }
                        else
                        {
                            MessageBox.Show("生成失败！没有合适的生成指定方式！", "错误", MessageBoxButtons.OK, MessageBoxIcon.Error);
                            return;
                        }

                        if (cbEditVersionParam.Checked)
                        {
                            LoadingLib.Win10Style.LoadingHelper.ShowBlueLoading("正在修改版本号,请稍等...", this, o =>
                            {
                                //修改版本号
                                string strAssemblyInfoPath = Path.GetDirectoryName(currentRow.Cells["FilePath"].Value.ToString()) + @"\Properties\";
                                List<string> listAssemblyInfoPath = CommonLib.Helper.Common.FileSearchHelper.GetFileSystemEntries(strAssemblyInfoPath, "^AssemblyInfo.cs$", true, false).ToList();
                                foreach (string strAssemblyInfoFile in listAssemblyInfoPath)
                                {
                                    WriteAssemblyInfo(ReadAssemblyInfo(strAssemblyInfoFile), strAssemblyInfoFile);
                                }
                            });
                        }

                        if (cbBuildMethod.Text == "Release")
                        {
                            msBuildCommand.BuildMethod = EnumMSBuildBuidMethod.Release;
                        }
                        else
                        {
                            msBuildCommand.BuildMethod = EnumMSBuildBuidMethod.Debug;
                        }
                        msBuildCommand.IsClear = cbClearProjet.Checked;
                        if (cbBuildOutputRootPath.Checked)
                        {
                            msBuildCommand.OutPutPath = Path.GetDirectoryName(tbBuildOutputRootPath.Text + @"\") + @"\" + strFolderLastName + @"\";
                        }
                        string strCommand = string.Empty;
                        if (cbExtractRootPath.Checked)
                        {
                            //抽取程序集
                            string strExtractPath = msBuildCommand.OutPutPath;
                            if (string.IsNullOrEmpty(strExtractPath))
                            {
                                strExtractPath = Path.GetDirectoryName(currentRow.Cells["FilePath"].Value.ToString());
                            }
                            
                            string strExtension = Extract(strExtractPath, currentRow.Cells["ProjectType"].Value.ToString());
                            if (string.IsNullOrEmpty(strExtension))
                            {
                                strCommand = msBuildCommand.GetCommand(Path.GetDirectoryName(tbMsBuildToolPath.Text), currentRow.Cells["FilePath"].Value.ToString());
                            }
                            else
                            {
                                strCommand = msBuildCommand.GetBuildAndCopyCommand(Path.GetDirectoryName(
                                    tbMsBuildToolPath.Text), 
                                    currentRow.Cells["FilePath"].Value.ToString(),
                                    Path.GetDirectoryName(tbExtractRootPath.Text.Trim() + @"\"),
                                    currentRow.Cells["AssemblyName"].Value.ToString() + strExtension);
                            }
                        }
                        else
                        {
                            strCommand = msBuildCommand.GetCommand(Path.GetDirectoryName(tbMsBuildToolPath.Text), currentRow.Cells["FilePath"].Value.ToString());
                        }

                        if (cbBuildExecMethod.Text == "Bat")
                        {
                            //生成BAT文件
                            string strBatPath = Path.GetDirectoryName(tbBuildOutputRootPath.Text + @"\") + @"\MSBuildBat\" + currentRow.Cells["AssemblyName"].Value.ToString() + ".bat";
                            FileHelper.WriteFile(strBatPath, strCommand);

                            using (Process p = new Process())
                            {
                                LoadingLib.Win10Style.LoadingHelper.ShowBlueLoading("正在执行Bat命令,请稍等...", this, o =>
                                {
                                    p.StartInfo.FileName = strBatPath;
                                    p.Start();
                                    p.WaitForExit();//等待程序执行完退出进程
                                    p.Close();
                                });
                                MessageBox.Show("生成操作成功！", "成功", MessageBoxButtons.OK, MessageBoxIcon.Information);
                            }
                        }
                        else if (cbBuildExecMethod.Text == "Cmd")
                        {
                            LoadingLib.Win10Style.LoadingHelper.ShowBlueLoading("正在执行Cmd命令,请稍等...", this, o =>
                            {
                                FrmOutputPrint frmOutputPrint = new FrmOutputPrint();
                                bool bRes = frmOutputPrint.ProcessExecuteByCmd(strCommand);
                                frmOutputPrint.ShowDialog();
                                if (bRes)
                                {
                                    //使用cmd命令无法在命令中添加copy，添加copy程序会被无限期等待，故使用C#复制文件
                                    if (cbExtractRootPath.Checked)
                                    {
                                        string strExtractPath = msBuildCommand.OutPutPath;
                                        if (string.IsNullOrEmpty(strExtractPath))
                                        {
                                            strExtractPath = Path.GetDirectoryName(currentRow.Cells["FilePath"].Value.ToString());
                                        }
                                        string strExtension = Extract(strExtractPath, currentRow.Cells["ProjectType"].Value.ToString());
                                        string strAssemblyName = currentRow.Cells["AssemblyName"].Value.ToString() + strExtension;
                                        if (!string.IsNullOrEmpty(strExtension))
                                        {
                                            try
                                            {
                                                File.Copy(
                                                    Path.GetDirectoryName(msBuildCommand.OutPutPath + @"\") + @"\" + strAssemblyName,
                                                    Path.GetDirectoryName(tbExtractRootPath.Text.Trim() + @"\") + @"\" + strAssemblyName, true);
                                            }
                                            catch (Exception ex)
                                            {
                                                MessageBox.Show("提取程序集失败！\r\n" + ex.Message, "失败", MessageBoxButtons.OK, MessageBoxIcon.Error);
                                                return;
                                            }
                                        }
                                    }
                                    MessageBox.Show("生成操作成功！", "成功", MessageBoxButtons.OK, MessageBoxIcon.Information);
                                }
                            });
                        }

                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("生成失败！\r\n" + ex.Message, "错误", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
        }

        /// <summary>
        /// 抽取程序集 创建抽取的目录并返回项目扩展名
        /// </summary>
        /// <param name="strSourcePath"></param>
        /// <param name="strProjectType">项目类型</param>
        /// <returns></returns>
        private string Extract(string strSourcePath,string strProjectType)
        {
            try
            {
                if(!Directory.Exists(tbExtractRootPath.Text.Trim()))
                {
                    Directory.CreateDirectory(tbExtractRootPath.Text.Trim());
                }
                if (strProjectType.ToUpper().Equals("Library".ToUpper()))
                {
                    return ".dll";
                }
                else if (strProjectType.ToUpper().Equals("Exe".ToUpper()))
                {
                    return ".exe";
                }
                else if(strProjectType.ToUpper().Equals("WinExe".ToUpper()))
                {
                    return ".exe";
                }
                else
                {
                    return "";
                }
            }
            catch
            {

            }
            return "";
        }

        /// <summary>
        /// 绑定DataGrid的数据
        /// </summary>
        /// <param name="listFiles">文件列</param>
        private void BindDataGridView(List<string> listFiles)
        {

            DataTable dataTable = new DataTable();
            dgvProjcet.Columns.Clear();

            DataGridViewButtonColumn btn = new DataGridViewButtonColumn();
            btn.Name = "btnMake";
            btn.HeaderText = "操作";
            btn.DefaultCellStyle.NullValue = "生成";
            btn.DataPropertyName = "Make";

            DataColumn dcId = new DataColumn();
            dcId.ColumnName = "Id";
            dcId.Caption = "Id";
            dcId.DataType = typeof(int);
            dataTable.Columns.Add(dcId);

            DataColumn dcAssemblyName = new DataColumn();
            dcAssemblyName.ColumnName = "AssemblyName";
            dcAssemblyName.Caption = "程序集名称";
            dcAssemblyName.DataType = typeof(string);
            dataTable.Columns.Add(dcAssemblyName);

            DataColumn dcProjectType = new DataColumn();
            dcProjectType.ColumnName = "ProjectType";
            dcProjectType.Caption = "项目类型";
            dcProjectType.DataType = typeof(string);
            dataTable.Columns.Add(dcProjectType);

            DataColumn dcFileDirectoryName = new DataColumn();
            dcFileDirectoryName.ColumnName = "FileDirectoryName";
            dcFileDirectoryName.Caption = "文件目录名称";
            dcFileDirectoryName.DataType = typeof(string);
            dataTable.Columns.Add(dcFileDirectoryName);

            DataColumn dcFilePath = new DataColumn();
            dcFilePath.ColumnName = "FilePath";
            dcFilePath.Caption = "文件路径";
            dcFilePath.DataType = typeof(string);
            dataTable.Columns.Add(dcFilePath);

            DataColumn dcMakeCount = new DataColumn();
            dcMakeCount.ColumnName = "MakeCount";
            dcMakeCount.Caption = "生成次数";
            dcMakeCount.DataType = typeof(int);
            dataTable.Columns.Add(dcMakeCount);
            LoadingLib.Win10Style.LoadingHelper.ShowBlueLoading("正在加载数据,请稍等...", this, o =>
            {
                int iId = 0;
                foreach (string strFile in listFiles)
                {
                    DataRow dr = dataTable.NewRow();
                    iId++;
                    dr["Id"] = iId;
                    try
                    {
                        List<string> listDirectoryNames = strFile.Split(new string[] { @"\" }, StringSplitOptions.None).ToList();
                        string strFileDirectoryName = string.Empty;
                        for (int iList = 0; iList < listDirectoryNames.Count; iList++)
                        {
                            if (iList + 1 < listDirectoryNames.Count)
                            {
                                if (listDirectoryNames[iList + 1].ToUpper().Contains(".csproj".ToUpper()))
                                {
                                    strFileDirectoryName = listDirectoryNames[iList];
                                }
                            }
                        }
                        //项目目录
                        dr["FileDirectoryName"] = strFileDirectoryName;
                        string strAssemblyName = string.Empty;

                        //程序集名称
                        strAssemblyName = ReadCsprojAssemblyName(strFile);
                        if (string.IsNullOrEmpty(strAssemblyName))
                        {
                            strAssemblyName = strFileDirectoryName;
                        }
                        dr["AssemblyName"] = strAssemblyName;

                        //项目类型 Library|类库 WinExe|Windows应用程序 Exe|控制台应用程序
                        string strProjectType = ReadCsProjectType(strFile);
                        if (string.IsNullOrEmpty(strProjectType))
                        {
                            strProjectType = "Library";//默认项目类型为类库
                        }
                        dr["ProjectType"] = strProjectType;

                        dr["MakeCount"] = 0;
                    }
                    catch
                    {
                        dr["FileDirectoryName"] = DBNull.Value;
                    }
                    dr["FilePath"] = strFile;
                    dataTable.Rows.Add(dr);
                };
            });

            dgvProjcet.DataSource = dataTable;
            dgvProjcet.Tag = dataTable.Copy();

            foreach (DataGridViewColumn dgvc in dgvProjcet.Columns)
            {
                dgvc.ReadOnly = false;
                try
                {
                    dgvc.HeaderText = dataTable.Columns[dgvc.DataPropertyName].Caption;
                    dgvc.ReadOnly = true;
                }
                catch
                {

                }
            }

            dgvProjcet.Columns["Id"].ReadOnly = true;
            dgvProjcet.Columns["Id"].Visible = false;
            dgvProjcet.Columns["FileDirectoryName"].Visible = false;
            dgvProjcet.Columns["MakeCount"].Visible = false;
            dgvProjcet.SelectionMode = DataGridViewSelectionMode.FullRowSelect;
            dgvProjcet.MultiSelect = false;
            dgvProjcet.Columns.Add(btn);
            dgvProjcet.AutoResizeColumns();
        }

        /// <summary>
        /// 加载Ini数据到窗体中
        /// </summary>
        private void LoadIniDataToForm()
        {
            //设置程序集参数
            SetAssembluyParamStatus(false);

            //编译方式
            cbBuildMethod.Text = ConfigIniFileHelper.GetIniData(ConfigIniName, "FormControlValue", "cbBuildMethod");

            //生成执行方式
            cbBuildExecMethod.Text = ConfigIniFileHelper.GetIniData(ConfigIniName, "FormControlValue", "cbBuildExecMethod");

            //清理
            cbClearProjet.Checked = StringToBool(ConfigIniFileHelper.GetIniData(ConfigIniName, "FormControlStatus", "cbClearProjetChecked"));

            //设置程序集参数
            cbEditVersionParam.Checked = StringToBool(ConfigIniFileHelper.GetIniData(ConfigIniName, "FormControlStatus", "cbEditVersionParamChecked"));

            //编译输出根路径
            cbBuildOutputRootPath.Checked = StringToBool(ConfigIniFileHelper.GetIniData(ConfigIniName, "FormControlStatus", "cbBuildOutputRootPathChecked"));

            //提取输出根路径
            cbExtractRootPath.Checked = StringToBool(ConfigIniFileHelper.GetIniData(ConfigIniName, "FormControlStatus", "cbExtractRootPathChecked"));

            //设置项目根路径
            tbProjcetRootPath.Text = ConfigIniFileHelper.GetIniData(ConfigIniName, "FormData", "ProjcetRootPath");

            //设置编译器路径
            tbMsBuildToolPath.Text = ConfigIniFileHelper.GetIniData(ConfigIniName, "FormData", "MsBuildToolPath");

            //设置编译导出根路径
            tbBuildOutputRootPath.Text = ConfigIniFileHelper.GetIniData(ConfigIniName, "FormData", "BuildOutputRootPath");

            //提取接口
            tbExtractRootPath.Text = ConfigIniFileHelper.GetIniData(ConfigIniName, "FormData", "ExtractRootPath");
        }

        /// <summary>
        /// 显示日志
        /// </summary>
        private void ShowLogs()
        {
            tbInfo.Text = sbLogs.ToString();
            tbInfo.SelectionStart = tbInfo.Text.Length;
        }

        /// <summary>
        /// 校验参数
        /// </summary>
        /// <returns></returns>
        private bool ValidateParams()
        {
            if (cbEditVersionParam.Checked)
            {
                if (StringToVersion(tbAssemblyVersion1.Text.Trim()) < 0
                    || StringToVersion(tbAssemblyVersion1.Text.Trim()) < 0
                    || StringToVersion(tbAssemblyVersion2.Text.Trim()) < 0
                    || StringToVersion(tbAssemblyVersion3.Text.Trim()) < 0
                    || StringToVersion(tbAssemblyVersion4.Text.Trim()) < 0
                    || StringToVersion(tbAssemblyFileVersion1.Text.Trim()) < 0
                    || StringToVersion(tbAssemblyFileVersion2.Text.Trim()) < 0
                    || StringToVersion(tbAssemblyFileVersion3.Text.Trim()) < 0
                    || StringToVersion(tbAssemblyFileVersion4.Text.Trim()) < 0)
                {
                    MessageBox.Show("请输入正确的版本号,版本号各个参数必须是介于0和65534之间的整数！", "错误", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return false;
                }
            }

            if (string.IsNullOrEmpty(cbBuildMethod.Text))
            {
                MessageBox.Show("请选择编译方式！", "错误", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return false;
            }

            if (cbBuildOutputRootPath.Checked)
            {
                if (string.IsNullOrEmpty(Path.GetDirectoryName(tbBuildOutputRootPath + @"\")))
                {
                    MessageBox.Show(
                        "请选择【指定编译输出根路径】合法的路径！当前路径：\r\n" + tbBuildOutputRootPath.Text.Trim() + "不合法！",
                        "错误",
                        MessageBoxButtons.OK,
                        MessageBoxIcon.Error);
                    return false;
                }
            }

            if (string.IsNullOrEmpty(cbBuildExecMethod.Text))
            {
                MessageBox.Show(
                    "请选择生成执行方式！",
                    "错误",
                    MessageBoxButtons.OK,
                    MessageBoxIcon.Error);
                return false;
            }

            if (cbExtractRootPath.Checked)
            {
                if (string.IsNullOrEmpty(Path.GetDirectoryName(tbExtractRootPath + @"\")))
                {
                    MessageBox.Show(
                        "请选择【提取编译后的程序集】的合法路径！当前路径：\r\n" + tbExtractRootPath.Text.Trim() + "不合法！",
                        "错误",
                        MessageBoxButtons.OK,
                        MessageBoxIcon.Error);
                    return false;
                }
            }

            return true;
        }

        /// <summary>
        /// 字符串转换为版本号的正整数，转换失败返回-1
        /// </summary>
        /// <param name="str"></param>
        /// <returns></returns>
        private int StringToVersion(string str)
        {
            try
            {
                int i = Convert.ToInt32(str);
                if (i >= 0 && i <= 65534)
                {
                    return i;
                }
                else
                {
                    return -1;
                }
            }
            catch
            {
                return -1;
            }
        }

        /// <summary>
        /// 读取程序及文件信息
        /// </summary>
        /// <param name="strFilePath"></param>
        /// <returns></returns>
        private DataTable ReadAssemblyInfo(string strFilePath)
        {
            DataTable dataTable = new DataTable("AssemblyInfo.cs");
            dataTable.Columns.Add("LineCode", typeof(int));
            dataTable.Columns.Add("Name", typeof(string));
            dataTable.Columns.Add("Key", typeof(string));
            dataTable.Columns.Add("Value", typeof(string));
            dataTable.Columns.Add("Content", typeof(string));
            try
            {
                FileStream fs = new FileStream(strFilePath, FileMode.Open, FileAccess.Read);
                StreamReader m_streamReader = new StreamReader(fs, System.Text.Encoding.Default);
                //使用StreamReader类来读取文件
                m_streamReader.BaseStream.Seek(0, SeekOrigin.Begin);
                //从数据流中读取每一行，直到文件的最后一行，并在textBox中显示出内容，其中textBox为文本框，如果不用可以改为别的
                string strLine = m_streamReader.ReadLine();
                int LineCode = 0;
                string strName = "";
                string strKey = "";
                string strValue = "";
                while (strLine != null)
                {
                    DataRow row = dataTable.NewRow();
                    LineCode++;
                    ///this.txtFileContent.Text += strLine + "\n";
                    if (!strLine.Trim().Contains(@"//"))
                    {
                        if (strLine.Contains("AssemblyTitle"))
                        {
                            strName = "标题";
                            strKey = "AssemblyTitle";
                        }
                        else if (strLine.Contains("AssemblyDescription"))
                        {
                            strName = "说明";
                            strKey = "AssemblyDescription";
                        }
                        else if (strLine.Contains("AssemblyCompany"))
                        {
                            strName = "公司";
                            strKey = "AssemblyCompany";
                        }
                        else if (strLine.Contains("AssemblyProduct"))
                        {
                            strName = "产品";
                            strKey = "AssemblyProduct";
                        }
                        else if (strLine.Contains("AssemblyCopyright"))
                        {
                            strName = "版权";
                            strKey = "AssemblyCopyright";
                        }
                        else if (strLine.Contains("AssemblyTrademark"))
                        {
                            strName = "商标";
                            strKey = "AssemblyTrademark";
                        }
                        //else if (strLine.Contains("Guid"))
                        //{
                        //    strName = "Guid";
                        //    strKey = "Guid";
                        //}
                        else if (strLine.Contains("AssemblyVersion"))
                        {
                            strName = "程序集版本";
                            strKey = "AssemblyVersion";
                        }
                        else if (strLine.Contains("AssemblyFileVersion"))
                        {
                            strName = "文件版本";
                            strKey = "AssemblyFileVersion";
                        }
                        else
                        {
                            strName = "";
                            strKey = "";
                            strValue = "";
                        }
                        if (!string.IsNullOrEmpty(strKey))
                        {
                            try
                            {
                                int keyLength = strKey.Length;
                                int startIndex = strLine.IndexOf(strKey + "(\"") + keyLength + 2;
                                strValue = strLine.Substring(startIndex, strLine.IndexOf("\")") - startIndex);
                            }
                            catch
                            {

                            }
                        }
                    }
                    row.ItemArray = new object[] { LineCode, strName, strKey, strValue, strLine };
                    dataTable.Rows.Add(row);
                    strLine = m_streamReader.ReadLine();
                }
                //关闭此StreamReader对象
                m_streamReader.Close();
                fs.Close();
            }
            catch (Exception ex)
            {
                sbLogs.AppendLine("【ERROR】 读取文件【" + strFilePath + "】程序集信息出错！");
                sbLogs.AppendLine("\t" + ex.Message);
                //this.Close();
            }
            finally
            {

            }

            sbLogs.AppendLine("【SUCCESS】 读取文件【" + strFilePath + "】成功！");
            return dataTable;
        }

        /// <summary>
        /// 读取C#项目文件中的程序集名称
        /// </summary>
        /// <param name="strFilePath"></param>
        /// <returns></returns>
        private string ReadCsprojAssemblyName(string strFilePath)
        {
            try
            {
                return XmlReaderHelper.LoadXmlFileGetCsprojAssemblyName(strFilePath);
            }
            catch
            {

            }
            return "";
        }

        /// <summary>
        /// 读取C#项目类型
        /// </summary>
        /// <param name="strFilePath"></param>
        /// <returns></returns>
        private string ReadCsProjectType(string strFilePath)
        {
            try
            {
                return XmlReaderHelper.LoadXmlFileGetCsProjectType(strFilePath);
            }
            catch
            {

            }
            return "";
        }

        /// <summary>
        /// 写入程序文件信息
        /// </summary>
        /// <param name="dataTable"></param>
        /// <param name="strFilePath"></param>
        private void WriteAssemblyInfo(DataTable dataTable, string strFilePath)
        {
            try
            {
                StringBuilder sbContentAll = new StringBuilder();
                foreach (DataRow row in dataTable.Rows)
                {
                    #region 修改数据
                    string strNewValue = string.Empty;
                    //if (row["Key"].ToString().Equals("AssemblyTitle") && !string.IsNullOrEmpty(tbAssemblyTitle.Text))
                    //{
                    //    strNewValue = tbAssemblyTitle.Text.Trim();
                    //}
                    //else if (row["Key"].ToString().Equals("AssemblyDescription") && !string.IsNullOrEmpty(tbAssemblyDescription.Text))
                    //{
                    //    strNewValue = tbAssemblyDescription.Text.Trim();
                    //}
                    //else if (row["Key"].ToString().Equals("AssemblyCompany") && !string.IsNullOrEmpty(tbAssemblyCompany.Text))
                    //{
                    //    strNewValue = tbAssemblyCompany.Text.Trim();
                    //}
                    //else if (row["Key"].ToString().Equals("AssemblyProduct") && !string.IsNullOrEmpty(tbAssemblyProduct.Text))
                    //{
                    //    strNewValue = tbAssemblyProduct.Text.Trim();
                    //}
                    //else if (row["Key"].ToString().Equals("AssemblyCopyright") && !string.IsNullOrEmpty(tbAssemblyCopyright.Text))
                    //{
                    //    strNewValue = tbAssemblyCopyright.Text.Trim();
                    //}
                    //else if (row["Key"].ToString().Equals("AssemblyTrademark") && !string.IsNullOrEmpty(tbAssemblyTrademark.Text))
                    //{
                    //    strNewValue = tbAssemblyTrademark.Text.Trim();
                    //}
                    //else
                    if (row["Key"].ToString().Equals("AssemblyVersion"))
                    {
                        //版本号
                        strNewValue = tbAssemblyVersion1.Text.Trim()
                                        + "." + tbAssemblyVersion2.Text.Trim()
                                        + "." + tbAssemblyVersion3.Text.Trim()
                                        + "." + tbAssemblyVersion4.Text.Trim();
                    }
                    else if (row["Key"].ToString().Equals("AssemblyFileVersion"))
                    {
                        //文件版本号
                        strNewValue = tbAssemblyFileVersion1.Text.Trim()
                                        + "." + tbAssemblyFileVersion2.Text.Trim()
                                        + "." + tbAssemblyFileVersion3.Text.Trim()
                                        + "." + tbAssemblyFileVersion4.Text.Trim();
                    }
                    else
                    {
                        strNewValue = "";
                    }
                    #endregion

                    if (!string.IsNullOrEmpty(strNewValue))
                    {
                        if (string.IsNullOrEmpty(row["Value"].ToString()))
                        {
                            string strKey = row["Key"].ToString();
                            int keyLength = strKey.Length;
                            int startIndex = row["Content"].ToString().IndexOf(strKey + "(\"") + keyLength + 2;
                            string strNewContent = row["Content"].ToString().Insert(startIndex, strNewValue);
                            row["Content"] = strNewContent;
                        }
                        else
                        {
                            row["Content"] = row["Content"].ToString().Replace(row["Value"].ToString(), strNewValue);
                        }
                    }

                    //writer.WriteLine(row["Content"].ToString());
                    sbContentAll.AppendLine(row["Content"].ToString());
                }
                //writer.WriteLine(DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss") + " " + msg);
                try
                {    
                    //覆盖文件内容
                    File.WriteAllText(strFilePath, sbContentAll.ToString(), System.Text.Encoding.Default);
                    sbLogs.Append("【SUCCESS】 写入文件【" + strFilePath + "】成功！");
                }
                catch (Exception e)

                {
                    sbLogs.Append("【ERROR】 写入文件【" + strFilePath + "】出错！");
                    sbLogs.Append("\t" + e.Message);
                }


            }
            catch (Exception ex)
            {
                sbLogs.Append("【ERROR】 写入文件【" + strFilePath + "】出错！");
                sbLogs.Append("\t" + ex.Message);
                //this.Close();
            }
            finally
            {

            }
        }

        /// <summary>
        /// 字符串转Bool
        /// </summary>
        /// <param name="strValue"></param>
        /// <returns></returns>
        private bool StringToBool(string strValue)
        {
            bool bRes = false;
            try
            {
                bRes = Convert.ToBoolean(strValue);
            }
            catch
            {

            }
            return bRes;
        }
                
        /// <summary>
        /// 清空程序集参数的值
        /// </summary>
        private void ClearAssembluyParamValue()
        {
            //tbGuid.Text = string.Empty;
            tbAssemblyVersion1.Text = string.Empty;
            tbAssemblyVersion2.Text = string.Empty;
            tbAssemblyVersion3.Text = string.Empty;
            tbAssemblyVersion4.Text = string.Empty;
            tbAssemblyFileVersion1.Text = string.Empty;
            tbAssemblyFileVersion2.Text = string.Empty;
            tbAssemblyFileVersion3.Text = string.Empty;
            tbAssemblyFileVersion4.Text = string.Empty;
        }

        /// <summary>
        /// 设置程序集参数的默认值
        /// </summary>
        private void SetAssembluyParamDefaultValue()
        {
            //tbGuid.Text = string.Empty;
            tbAssemblyVersion1.Text = DateTime.Now.Year.ToString();
            tbAssemblyVersion2.Text = DateTime.Now.ToString("MM");
            tbAssemblyVersion3.Text = DateTime.Now.ToString("dd");
            tbAssemblyVersion4.Text = string.Empty;
            tbAssemblyFileVersion1.Text = DateTime.Now.Year.ToString();
            tbAssemblyFileVersion2.Text = DateTime.Now.ToString("MM");
            tbAssemblyFileVersion3.Text = DateTime.Now.ToString("dd");
            tbAssemblyFileVersion4.Text = string.Empty;
        }

        /// <summary>
        /// 设置程序集参数的状态
        /// </summary>
        /// <param name="status"></param>
        private void SetAssembluyParamStatus(bool status)
        {
            tbAssemblyVersion1.Enabled = status;
            tbAssemblyVersion2.Enabled = status;
            tbAssemblyVersion3.Enabled = status;
            tbAssemblyVersion4.Enabled = status;
            tbAssemblyFileVersion1.Enabled = status;
            tbAssemblyFileVersion2.Enabled = status;
            tbAssemblyFileVersion3.Enabled = status;
            tbAssemblyFileVersion4.Enabled = status;
        }

        /// <summary>
        /// 备份文件
        /// </summary>
        /// <param name="strBackRootPath">备份根路径</param>
        /// <param name="strFilePath">备份文件路径</param>
        /// <returns></returns>
        private bool BackFile(string strBackRootPath,string strFilePath)
        {
            try
            {
                //备份文件
                //检查文件备份路径
                if (!Directory.Exists(strBackRootPath))
                {
                    Directory.CreateDirectory(strBackRootPath);
                }

                //获取当前操作文件的上级目录
                List<string> listDirectoryNames = strFilePath.Split(new string[] { @"\" }, StringSplitOptions.None).ToList();
                string strFileDirectoryName = string.Empty;
                for (int iList = 0; iList < listDirectoryNames.Count; iList++)
                {
                    if (strFilePath.Contains(@"\Properties"))
                    {
                        if (iList + 1 < listDirectoryNames.Count)
                        {
                            if (listDirectoryNames[iList + 1].ToUpper().Equals(@"Properties".ToUpper()))
                            {
                                strFileDirectoryName = listDirectoryNames[iList];
                            }
                        }
                    }
                    else
                    {
                        if (iList + 1 < listDirectoryNames.Count)
                        {
                            if (listDirectoryNames[iList + 1].ToUpper().Equals("AssemblyInfo.cs".ToUpper()))
                            {
                                strFileDirectoryName = listDirectoryNames[iList];
                            }
                        }
                    }
                }

                if (string.IsNullOrEmpty(strFileDirectoryName))
                {
                    //上级目录为空，返回false
                    sbLogs.AppendLine("【ERROR】无法获取路径【" + strFilePath + "】的上级目录！");
                    return false;
                }

                if (!Directory.Exists(Path.GetDirectoryName(strBackRootPath + @"\" + strFileDirectoryName + @"\")))
                {
                    Directory.CreateDirectory(Path.GetDirectoryName(strBackRootPath + @"\" + strFileDirectoryName + @"\"));
                }
                string strFileName = Path.GetFileName(strFilePath);
                string strCopyFilePath = Path.GetDirectoryName(strBackRootPath + @"\" + strFileDirectoryName + @"\") + @"\" + strFileName;
                File.Copy(strFilePath, strCopyFilePath, true);
                sbLogs.AppendLine("【SUCCESS】已将文件\r\t\n【" + strFilePath + "】\r\n备份至\r\n\t【"+ strCopyFilePath+"】");
                return true;

            }
            catch(Exception ex)
            {
                //上级目录为空，返回false
                sbLogs.AppendLine("【ERROR】备份文件【" + strFilePath + "】失败！\r\n\t" + ex.Message);
                return false;
            }
        }
        #endregion
    }
}
