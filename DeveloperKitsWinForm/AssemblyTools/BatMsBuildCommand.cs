﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AssemblyTools
{
    /// <summary>
    /// Bat编译方式
    /// </summary>
    public class BatMsBuildCommand : IMSBuildCommand
    {
        /// <summary>
        /// 输出路径
        /// </summary>
        public string OutPutPath
        {
            get;
            set;
        } = string.Empty;

        /// <summary>
        /// 是否清理
        /// </summary>
        public bool IsClear
        {
            get;
            set;
        } = false;

        /// <summary>
        /// 编译方式
        /// </summary>
        public EnumMSBuildBuidMethod BuildMethod
        {
            get;
            set;
        } = EnumMSBuildBuidMethod.Release;      

        /// <summary>
        /// 获取命令
        /// </summary>
        /// <param name="strMSBuildPath">MSBuild目录</param>
        /// <param name="strProjcetRootPath">项目目录</param>
        /// <returns></returns>
        public string GetCommand(string strMSBuildPath,string strProjcetRootPath)
        {
            StringBuilder sbCommand = new StringBuilder();
            List<string> listCommandParam = new List<string>();
            if (IsClear)
            {
                listCommandParam.Add(@"/t:Clean");
            }
            listCommandParam.Add(@"/t:Build");
            listCommandParam.Add(@"/p:Configuration=" + BuildMethod.ToString());
            if (!string.IsNullOrEmpty(OutPutPath))
            {
                listCommandParam.Add("/p:OutDir=\"" + Path.GetDirectoryName(OutPutPath + @"\") + "\"");
            }
            sbCommand.AppendLine("@echo off");
            sbCommand.AppendLine("title MSBuild批处理");
            sbCommand.AppendLine("");
            sbCommand.AppendLine("@echo off");
            sbCommand.AppendLine("echo 正在获取管理员权限...");
            sbCommand.AppendLine("");
            sbCommand.AppendLine("%1 mshta vbscript:CreateObject("+ "\"" + "Shell.Application" + "\"" + ").ShellExecute(" + "\"" + "cmd.exe" + "\"" + "," + "\"" + "/c %~s0 ::" + "\"" + "," + "\"" + "" + "\"" + "," + "\"" + "runas" + "\"" + ",1)(window.close)&&exit");
            sbCommand.AppendLine("");
            sbCommand.AppendLine("@echo off");
            sbCommand.AppendLine("echo 已获取管理员权限...");
            sbCommand.AppendLine("");
            sbCommand.AppendLine("@echo off");
            sbCommand.AppendLine("echo 开始编译...");
            sbCommand.AppendLine("");
            sbCommand.AppendLine(@"cd /d " + strMSBuildPath.ToUpper().Replace("MSBuild.exe", ""));
            sbCommand.AppendLine(@"msbuild " + "\"" + strProjcetRootPath + "\" " + string.Join(" ", listCommandParam.ToArray()));
            sbCommand.AppendLine("");
            sbCommand.AppendLine("@echo off");
            sbCommand.AppendLine("");
            sbCommand.AppendLine("pause");
            return sbCommand.ToString();
        }

        /// <summary>
        /// 获取编译和复制命令
        /// </summary>
        /// <param name="strMSBuildPath">MSBuild目录</param>
        /// <param name="strProjcetRootPath">项目目录</param>
        /// <param name="strExtractPath">复制目标路径</param>
        /// <param name="strAssemblyName">程序集名称</param>
        /// <returns></returns>
        public string GetBuildAndCopyCommand(string strMSBuildPath, string strProjcetRootPath, string strExtractPath, string strAssemblyName)
        {
            StringBuilder sbCommand = new StringBuilder();
            List<string> listCommandParam = new List<string>();
            if (IsClear)
            {
                listCommandParam.Add(@"/t:Clean");
            }
            listCommandParam.Add(@"/t:Build");
            listCommandParam.Add(@"/p:Configuration=" + BuildMethod.ToString());
            if (!string.IsNullOrEmpty(OutPutPath))
            {
                listCommandParam.Add("/p:OutDir=\"" + Path.GetDirectoryName(OutPutPath + @"\") + "\"");
            }
            sbCommand.AppendLine("@echo off");
            sbCommand.AppendLine("title MSBuild批处理");
            sbCommand.AppendLine("");
            sbCommand.AppendLine("@echo off");
            sbCommand.AppendLine("echo 正在获取管理员权限...");
            sbCommand.AppendLine("");
            sbCommand.AppendLine("%1 mshta vbscript:CreateObject(" + "\"" + "Shell.Application" + "\"" + ").ShellExecute(" + "\"" + "cmd.exe" + "\"" + "," + "\"" + "/c %~s0 ::" + "\"" + "," + "\"" + "" + "\"" + "," + "\"" + "runas" + "\"" + ",1)(window.close)&&exit");
            sbCommand.AppendLine("");
            sbCommand.AppendLine("@echo off");
            sbCommand.AppendLine("echo 已获取管理员权限...");
            sbCommand.AppendLine("");
            sbCommand.AppendLine("@echo off");
            sbCommand.AppendLine("echo 开始编译...");
            sbCommand.AppendLine("");
            sbCommand.AppendLine(@"cd /d " + strMSBuildPath.ToUpper().Replace("MSBuild.exe", ""));
            sbCommand.AppendLine(@"msbuild " + "\"" + strProjcetRootPath + "\" " + string.Join(" ", listCommandParam.ToArray()));
            sbCommand.AppendLine("");
            sbCommand.AppendLine("copy " + "\"" + Path.GetDirectoryName(OutPutPath + @"\") + @"\" + strAssemblyName + "\" \"" + Path.GetDirectoryName(strExtractPath + @"\") + "\"");
            sbCommand.AppendLine("");
            sbCommand.AppendLine("@echo off");
            sbCommand.AppendLine("");
            sbCommand.AppendLine("pause");
            return sbCommand.ToString();
        }
    }
}
