﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AssemblyTools
{
    /// <summary>
    /// MSBuild命令接口类
    /// </summary>
    interface IMSBuildCommand
    {
        /// <summary>
        /// 输出路径
        /// </summary>
        string OutPutPath
        {
            get;
            set;
        }

        /// <summary>
        /// 是否清理
        /// </summary>
        bool IsClear
        {
            get;
            set;
        }

        /// <summary>
        /// 编译方式
        /// </summary>
        EnumMSBuildBuidMethod BuildMethod
        {
            get;
            set;
        }
        
        /// <summary>
        /// 获取命令
        /// </summary>
        /// <param name="strMSBuildPath">MSBuild目录</param>
        /// <param name="strProjcetRootPath">项目跟目录</param>
        /// <returns></returns>
        string GetCommand(string strMSBuildPath,string strProjcetRootPath);

        /// <summary>
        /// 获取编译和复制命令
        /// </summary>
        /// <param name="strMSBuildPath">MSBuild目录</param>
        /// <param name="strProjcetRootPath">项目目录</param>
        /// <param name="strExtractPath">复制目标路径</param>
        /// <param name="strAssemblyName">程序集名称</param>
        /// <returns></returns>
        string GetBuildAndCopyCommand(string strMSBuildPath, string strProjcetRootPath, string strExtractPath, string strAssemblyName);

    }
}
