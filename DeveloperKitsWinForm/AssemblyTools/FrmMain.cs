﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace AssemblyTools
{
    public partial class FrmMain : Form
    {
        [DllImport("kernel32.dll")]
        public static extern bool AllocConsole();

        [DllImport("msvcrt.dll", SetLastError = false, CallingConvention = CallingConvention.Cdecl, CharSet = CharSet.Ansi)]
        private extern static void system(string command); // longjmp

        public FrmMain()
        {
            InitializeComponent();
        }

        /// <summary>
        /// 批量修改程序集信息
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnAssmblyInfo_Click(object sender, EventArgs e)
        {
            new FrmAssemblyInfoBatchEdit().ShowDialog();
        }

        /// <summary>
        /// 生成MSBuild编译代码
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnBuild_Click(object sender, EventArgs e)
        {
            new FrmMsBuildManage().ShowDialog();
        }
        
        /// <summary>
        /// Dll,Exe提取图片
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnExtractAssmblyImg_Click(object sender, EventArgs e)
        {
            new FrmExtractAssmblyImg().ShowDialog();
        }

        private void btnTest_Click(object sender, EventArgs e)
        {
            //string s = Path.GetDirectoryName(@"E:\Codes\VsProjectDir\DevelopmentTools\DeveloperKitsWinForm\LanguageTranslate\bb.cs\");

            //Process process = new Process();

            //process.StartInfo.FileName = "cmd";
            //process.StartInfo.UseShellExecute = false;
            //process.StartInfo.RedirectStandardInput = true;
            //process.StartInfo.RedirectStandardOutput = true;
            //process.StartInfo.RedirectStandardError = true;
            //process.StandardInput.AutoFlush = true;

            //process.StartInfo.CreateNoWindow = false;
            //process.Start();

            //process.StandardInput.WriteLine("ver");
            //process.BeginOutputReadLine();
            //process.StandardInput.WriteLine("exit");
            //string str = process.StandardOutput.ReadToEnd();

            //process.WaitForExit();//等待程序执行完退出进程
            //process.Close();
            //Process process = Process.Start(@"cmd", @"cd C:\Program Files (x86)\Microsoft Visual Studio\2017\Community\MSBuild\15.0\Bin \r\n c \r\n msbuild -help");

            //Process process = Process.Start(@"cmd", @"/ver");

            //string str = @"cd /d C:\PROGRAM FILES (X86)\MICROSOFT VISUAL STUDIO\2017\COMMUNITY\MSBUILD\15.0\BIN\MSBuild.exe";
            //str = Path.GetExtension(str);
            //str += "\r\n";
            //str += "msbuild -help";

            ////richTextBox1.Text = ProcessHelper.Execute(str);
            ////Clipboard.SetText(richTextBox1.Text);

            //EnumMSBuildBuidMethod BuildMethod = EnumMSBuildBuidMethod.Debug;
            //str = BuildMethod.ToString();
        }

        private void FrmMain_Load(object sender, EventArgs e)
        {
#if DEBUG
            btnTest.Visible = true;
#else
            btnTest.Visible = false;
#endif
        }
    }
}
