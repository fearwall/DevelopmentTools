﻿namespace AssemblyTools
{
    partial class FrmMain
    {
        /// <summary>
        /// 必需的设计器变量。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// 清理所有正在使用的资源。
        /// </summary>
        /// <param name="disposing">如果应释放托管资源，为 true；否则为 false。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows 窗体设计器生成的代码

        /// <summary>
        /// 设计器支持所需的方法 - 不要修改
        /// 使用代码编辑器修改此方法的内容。
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmMain));
            this.btnAssmblyInfo = new System.Windows.Forms.Button();
            this.btnBuild = new System.Windows.Forms.Button();
            this.btnTest = new System.Windows.Forms.Button();
            this.btnExtractAssmblyImg = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // btnAssmblyInfo
            // 
            this.btnAssmblyInfo.Location = new System.Drawing.Point(12, 12);
            this.btnAssmblyInfo.Name = "btnAssmblyInfo";
            this.btnAssmblyInfo.Size = new System.Drawing.Size(600, 23);
            this.btnAssmblyInfo.TabIndex = 0;
            this.btnAssmblyInfo.Text = "修改程序集信息";
            this.btnAssmblyInfo.UseVisualStyleBackColor = true;
            this.btnAssmblyInfo.Click += new System.EventHandler(this.btnAssmblyInfo_Click);
            // 
            // btnBuild
            // 
            this.btnBuild.Location = new System.Drawing.Point(12, 41);
            this.btnBuild.Name = "btnBuild";
            this.btnBuild.Size = new System.Drawing.Size(600, 23);
            this.btnBuild.TabIndex = 1;
            this.btnBuild.Text = "MSBuild编译项目";
            this.btnBuild.UseVisualStyleBackColor = true;
            this.btnBuild.Click += new System.EventHandler(this.btnBuild_Click);
            // 
            // btnTest
            // 
            this.btnTest.Location = new System.Drawing.Point(12, 406);
            this.btnTest.Name = "btnTest";
            this.btnTest.Size = new System.Drawing.Size(600, 23);
            this.btnTest.TabIndex = 2;
            this.btnTest.Text = "测试";
            this.btnTest.UseVisualStyleBackColor = true;
            this.btnTest.Click += new System.EventHandler(this.btnTest_Click);
            // 
            // btnExtractAssmblyImg
            // 
            this.btnExtractAssmblyImg.Location = new System.Drawing.Point(12, 70);
            this.btnExtractAssmblyImg.Name = "btnExtractAssmblyImg";
            this.btnExtractAssmblyImg.Size = new System.Drawing.Size(600, 23);
            this.btnExtractAssmblyImg.TabIndex = 3;
            this.btnExtractAssmblyImg.Text = "Dll、Exe图片提取";
            this.btnExtractAssmblyImg.UseVisualStyleBackColor = true;
            this.btnExtractAssmblyImg.Click += new System.EventHandler(this.btnExtractAssmblyImg_Click);
            // 
            // FrmMain
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(624, 441);
            this.Controls.Add(this.btnExtractAssmblyImg);
            this.Controls.Add(this.btnTest);
            this.Controls.Add(this.btnBuild);
            this.Controls.Add(this.btnAssmblyInfo);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "FrmMain";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "所有工具";
            this.Load += new System.EventHandler(this.FrmMain_Load);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button btnAssmblyInfo;
        private System.Windows.Forms.Button btnBuild;
        private System.Windows.Forms.Button btnTest;
        private System.Windows.Forms.Button btnExtractAssmblyImg;
    }
}

