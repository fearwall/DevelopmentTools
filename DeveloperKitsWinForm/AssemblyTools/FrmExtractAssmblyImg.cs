﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Resources;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace AssemblyTools
{
    public partial class FrmExtractAssmblyImg : Form
    {
        string ConfigIniName = "ExtractAssmblyImg";

        public FrmExtractAssmblyImg()
        {
            InitializeComponent();
        }

        /// <summary>
        /// 程序集路径
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnAssemblyPath_Click(object sender, EventArgs e)
        {
            string strFilePath = "";
            if (string.IsNullOrEmpty(tbAssemblyPath.Text.Trim()))
            {
                strFilePath = CommonLib.Helper.Common.FilePathHelper.FilePathSelect("Exe|*.exe|程序集|*.dll");
                if (string.IsNullOrEmpty(strFilePath))
                {
                    MessageBox.Show("请选择指定的文件【.exe或.dll】！", "错误", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return;
                }
                tbAssemblyPath.Text = strFilePath;
            }
            else
            {
                strFilePath = tbAssemblyPath.Text.Trim();
            }

            if (!File.Exists(strFilePath) && !strFilePath.Contains("*.dll") && !strFilePath.Contains("*.exe"))
            {
                MessageBox.Show("请选择指定的文件【.exe或.dll】！", "错误", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
        }

        /// <summary>
        /// 保存路径
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnSavePath_Click(object sender, EventArgs e)
        {
            string strFolderPath = CommonLib.Helper.Common.FilePathHelper.FolderPathSelect(tbSavePath.Text.Trim(), true);
            if (string.IsNullOrEmpty(strFolderPath))
            {
                MessageBox.Show("请选择图片保存目录！", "错误", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
            else
            {                
                tbSavePath.Text = strFolderPath;
            }
            //设置选中的路径到缓存中
            ConfigIniFileHelper.SetIniData(ConfigIniName, "FormData", "SavePath", strFolderPath);
        }

        /// <summary>
        /// 提取
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnExtract_Click(object sender, EventArgs e)
        {
            Extract();
        }

        private void FrmExtractAssmblyImg_Load(object sender, EventArgs e)
        {
            tbSavePath.Text = ConfigIniFileHelper.GetIniData(ConfigIniName, "FormData", "SavePath");
        }

        /// <summary>
        /// 提取
        /// </summary>
        private void Extract()
        {
            string appPath = tbAssemblyPath.Text.Trim();
            string strOutPath = tbSavePath.Text.Trim();            

            List<string> listBatchFilePath = new List<string>();
            LoadingLib.Win10Style.LoadingHelper.ShowBlueLoading("正在提取图片,请稍等...", this, o =>
            {
                if (!Directory.Exists(strOutPath))
                {
                    Directory.CreateDirectory(strOutPath);
                }

                //从Exe里面提取图标和图片
                Assembly assm = Assembly.LoadFrom(appPath);
                HashSet<string> l_strHashType = new HashSet<string>();
                try
                {
                    foreach (string resName in assm.GetManifestResourceNames())
                    {
                        Stream stream = assm.GetManifestResourceStream(resName);
                        ResourceReader rr = new ResourceReader(stream);
                        IDictionaryEnumerator enumerator = rr.GetEnumerator();
                        while (enumerator.MoveNext())
                        {
                            DictionaryEntry de = (DictionaryEntry)enumerator.Current;
                            l_strHashType.Add(de.Value.GetType().ToString());
                            try
                            {
                                switch (de.Value.GetType().ToString())
                                {
                                    case "System.Drawing.Icon":
                                        System.Drawing.Icon ic = (Icon)de.Value;
                                        //路径根据自己需要更改
                                        using (System.IO.Stream sm = new FileStream(strOutPath + de.Key.ToString() + ".ico", FileMode.Create))
                                        {
                                            ic.Save(sm);
                                        }

                                        break;
                                    case "System.Drawing.Bitmap":
                                        System.Drawing.Bitmap icbmp = (Bitmap)de.Value;
                                        icbmp.Save(strOutPath + de.Key.ToString() + ".bmp");
                                        break;
                                    case "System.Windows.Forms.ImageListStreamer":
                                        System.Windows.Forms.ImageListStreamer ils = (System.Windows.Forms.ImageListStreamer)de.Value;
                                        //Image img = Image.FromStream()
                                        ImageList imageList = new ImageList();
                                        imageList.ImageStream = ils;
                                        DateTime dateTime = DateTime.Now;
                                        foreach (Image image in imageList.Images)
                                        {
                                            //ImageFormat imageFormat = new ImageFormat(image.RawFormat.Guid);
                                            string strFormat = string.Empty;
                                            GetImageFormat(image, out strFormat);
                                            string name = strOutPath + dateTime.ToString("MMddHHmmss") + DateTime.Now.Millisecond.ToString() + strFormat;
                                            image.Save(name, image.RawFormat);
                                        }
                                        foreach (string strKey in imageList.Images.Keys)
                                        {
                                            string strKeys = strKey;
                                        }

                                        break;
                                    default:
                                        break;
                                }
                            }
                            catch (Exception ex)
                            {

                            }
                            //de.Key是资源名
                            //de.Value是资源内容
                        }
                    }
                }
                catch (Exception ex)
                {

                }
                MessageBox.Show("图标抽取完成！");
            });
        }

        /// <summary>
        /// 校验参数
        /// </summary>
        /// <returns></returns>
        private bool ValidateParam()
        {
            if (string.IsNullOrEmpty(tbAssemblyPath.Text.Trim()))
            {
                MessageBox.Show("请选择被提取的程序集！", "错误", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return false;
            }
            if (string.IsNullOrEmpty(tbSavePath.Text.Trim()))
            {
                MessageBox.Show("请选择保存路径！", "错误", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return false;
            }

            if (!File.Exists(tbAssemblyPath.Text.Trim()))
            {
                MessageBox.Show("选中的文件：\r\n" + tbAssemblyPath.Text + "不存在！", "错误", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return false;
            }

            if (string.IsNullOrEmpty(Path.GetDirectoryName(tbSavePath.Text.Trim()) + @"\"))
            {
                MessageBox.Show("保存的路径：\r\n" + tbSavePath.Text + "不是有效路径！", "错误", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return false;
            }
            return true;
        }


        /// <summary>
        /// 获取Image图片格式
        /// </summary>
        /// <param name="_img"></param>
        /// <param name="format"></param>
        /// <returns></returns>
        private static System.Drawing.Imaging.ImageFormat GetImageFormat(Image _img, out string format)
        {
            if (_img.RawFormat.Guid.Equals(System.Drawing.Imaging.ImageFormat.Bmp.Guid))
            {
                format = ".bmp";
                return System.Drawing.Imaging.ImageFormat.Bmp;
            }
            if (_img.RawFormat.Guid.Equals(System.Drawing.Imaging.ImageFormat.Emf.Guid))
            {
                format = ".emf";
                return System.Drawing.Imaging.ImageFormat.Emf;
            }
            if (_img.RawFormat.Guid.Equals(System.Drawing.Imaging.ImageFormat.Exif.Guid))
            {
                format = ".exif";
                return System.Drawing.Imaging.ImageFormat.Exif;
            }
            if (_img.RawFormat.Guid.Equals(System.Drawing.Imaging.ImageFormat.Gif.Guid))
            {
                format = ".gif";
                return System.Drawing.Imaging.ImageFormat.Gif;
            }
            if (_img.RawFormat.Guid.Equals(System.Drawing.Imaging.ImageFormat.Icon.Guid))
            {
                format = ".icon";
                return System.Drawing.Imaging.ImageFormat.Icon;
            }
            if (_img.RawFormat.Guid.Equals(System.Drawing.Imaging.ImageFormat.Jpeg.Guid))
            {
                format = ".jpg";
                return System.Drawing.Imaging.ImageFormat.Jpeg;
            }
            if (_img.RawFormat.Guid.Equals(System.Drawing.Imaging.ImageFormat.MemoryBmp.Guid))
            {
                format = ".bmp";
                return System.Drawing.Imaging.ImageFormat.MemoryBmp;
            }
            if (_img.RawFormat.Guid.Equals(System.Drawing.Imaging.ImageFormat.Png.Guid))
            {
                format = ".png";
                return System.Drawing.Imaging.ImageFormat.Png;
            }
            format = ".png";
            return null;
        }
    }
}
