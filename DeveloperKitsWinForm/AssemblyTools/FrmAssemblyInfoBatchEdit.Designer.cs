﻿namespace AssemblyTools
{
    partial class FrmAssemblyInfoBatchEdit
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmAssemblyInfoBatchEdit));
            this.label1 = new System.Windows.Forms.Label();
            this.tbBatchPath = new System.Windows.Forms.TextBox();
            this.btnBatchPath = new System.Windows.Forms.Button();
            this.label2 = new System.Windows.Forms.Label();
            this.tbSinglePath = new System.Windows.Forms.TextBox();
            this.btnSingleEdit = new System.Windows.Forms.Button();
            this.label3 = new System.Windows.Forms.Label();
            this.cbModel = new System.Windows.Forms.ComboBox();
            this.tbInfo = new System.Windows.Forms.TextBox();
            this.gbParams = new System.Windows.Forms.GroupBox();
            this.tbBackFilePath = new System.Windows.Forms.TextBox();
            this.btnBackFilePath = new System.Windows.Forms.Button();
            this.cbBackFile = new System.Windows.Forms.CheckBox();
            this.label13 = new System.Windows.Forms.Label();
            this.tbAssemblyFileVersion4 = new System.Windows.Forms.TextBox();
            this.tbAssemblyFileVersion3 = new System.Windows.Forms.TextBox();
            this.tbAssemblyFileVersion2 = new System.Windows.Forms.TextBox();
            this.tbAssemblyVersion4 = new System.Windows.Forms.TextBox();
            this.tbAssemblyVersion3 = new System.Windows.Forms.TextBox();
            this.tbAssemblyVersion2 = new System.Windows.Forms.TextBox();
            this.btnMake = new System.Windows.Forms.Button();
            this.tbAssemblyFileVersion1 = new System.Windows.Forms.TextBox();
            this.label12 = new System.Windows.Forms.Label();
            this.tbAssemblyVersion1 = new System.Windows.Forms.TextBox();
            this.label11 = new System.Windows.Forms.Label();
            this.tbAssemblyTrademark = new System.Windows.Forms.TextBox();
            this.label10 = new System.Windows.Forms.Label();
            this.tbAssemblyCopyright = new System.Windows.Forms.TextBox();
            this.label9 = new System.Windows.Forms.Label();
            this.tbAssemblyProduct = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.tbAssemblyCompany = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.tbAssemblyDescription = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.tbAssemblyTitle = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.tcButtom = new System.Windows.Forms.TabControl();
            this.tpLogInfo = new System.Windows.Forms.TabPage();
            this.tpMultiFile = new System.Windows.Forms.TabPage();
            this.plFileSelectFull = new System.Windows.Forms.Panel();
            this.dgvBatch = new System.Windows.Forms.DataGridView();
            this.plFileSelectTop = new System.Windows.Forms.Panel();
            this.btnInvertSelect = new System.Windows.Forms.Button();
            this.btnSelectAll = new System.Windows.Forms.Button();
            this.plTop = new System.Windows.Forms.Panel();
            this.pcParams = new System.Windows.Forms.Panel();
            this.plFull = new System.Windows.Forms.Panel();
            this.gbParams.SuspendLayout();
            this.tcButtom.SuspendLayout();
            this.tpLogInfo.SuspendLayout();
            this.tpMultiFile.SuspendLayout();
            this.plFileSelectFull.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvBatch)).BeginInit();
            this.plFileSelectTop.SuspendLayout();
            this.plTop.SuspendLayout();
            this.pcParams.SuspendLayout();
            this.plFull.SuspendLayout();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(87, 37);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(89, 12);
            this.label1.TabIndex = 2;
            this.label1.Text = "文件批量修改：";
            // 
            // tbBatchPath
            // 
            this.tbBatchPath.Location = new System.Drawing.Point(182, 33);
            this.tbBatchPath.Name = "tbBatchPath";
            this.tbBatchPath.ReadOnly = true;
            this.tbBatchPath.Size = new System.Drawing.Size(673, 21);
            this.tbBatchPath.TabIndex = 3;
            // 
            // btnBatchPath
            // 
            this.btnBatchPath.Enabled = false;
            this.btnBatchPath.Location = new System.Drawing.Point(853, 32);
            this.btnBatchPath.Name = "btnBatchPath";
            this.btnBatchPath.Size = new System.Drawing.Size(75, 23);
            this.btnBatchPath.TabIndex = 4;
            this.btnBatchPath.Text = "选择";
            this.btnBatchPath.UseVisualStyleBackColor = true;
            this.btnBatchPath.Click += new System.EventHandler(this.btnBatchPath_Click);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(87, 66);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(89, 12);
            this.label2.TabIndex = 5;
            this.label2.Text = "单个文件修改：";
            // 
            // tbSinglePath
            // 
            this.tbSinglePath.Location = new System.Drawing.Point(182, 63);
            this.tbSinglePath.Name = "tbSinglePath";
            this.tbSinglePath.ReadOnly = true;
            this.tbSinglePath.Size = new System.Drawing.Size(673, 21);
            this.tbSinglePath.TabIndex = 6;
            // 
            // btnSingleEdit
            // 
            this.btnSingleEdit.Enabled = false;
            this.btnSingleEdit.Location = new System.Drawing.Point(853, 62);
            this.btnSingleEdit.Name = "btnSingleEdit";
            this.btnSingleEdit.Size = new System.Drawing.Size(75, 23);
            this.btnSingleEdit.TabIndex = 7;
            this.btnSingleEdit.Text = "选择";
            this.btnSingleEdit.UseVisualStyleBackColor = true;
            this.btnSingleEdit.Click += new System.EventHandler(this.btnSingleEdit_Click);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(111, 11);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(65, 12);
            this.label3.TabIndex = 0;
            this.label3.Text = "修改模式：";
            // 
            // cbModel
            // 
            this.cbModel.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbModel.FormattingEnabled = true;
            this.cbModel.Items.AddRange(new object[] {
            "批量模式",
            "单文件模式"});
            this.cbModel.Location = new System.Drawing.Point(182, 8);
            this.cbModel.Name = "cbModel";
            this.cbModel.Size = new System.Drawing.Size(746, 20);
            this.cbModel.TabIndex = 1;
            this.cbModel.SelectedIndexChanged += new System.EventHandler(this.cbModel_SelectedIndexChanged);
            // 
            // tbInfo
            // 
            this.tbInfo.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tbInfo.Location = new System.Drawing.Point(3, 3);
            this.tbInfo.Multiline = true;
            this.tbInfo.Name = "tbInfo";
            this.tbInfo.ReadOnly = true;
            this.tbInfo.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.tbInfo.Size = new System.Drawing.Size(994, 460);
            this.tbInfo.TabIndex = 0;
            // 
            // gbParams
            // 
            this.gbParams.Controls.Add(this.tbBackFilePath);
            this.gbParams.Controls.Add(this.btnBackFilePath);
            this.gbParams.Controls.Add(this.cbBackFile);
            this.gbParams.Controls.Add(this.label13);
            this.gbParams.Controls.Add(this.tbAssemblyFileVersion4);
            this.gbParams.Controls.Add(this.tbAssemblyFileVersion3);
            this.gbParams.Controls.Add(this.tbAssemblyFileVersion2);
            this.gbParams.Controls.Add(this.tbAssemblyVersion4);
            this.gbParams.Controls.Add(this.tbAssemblyVersion3);
            this.gbParams.Controls.Add(this.tbAssemblyVersion2);
            this.gbParams.Controls.Add(this.btnMake);
            this.gbParams.Controls.Add(this.tbAssemblyFileVersion1);
            this.gbParams.Controls.Add(this.label12);
            this.gbParams.Controls.Add(this.tbAssemblyVersion1);
            this.gbParams.Controls.Add(this.label11);
            this.gbParams.Controls.Add(this.tbAssemblyTrademark);
            this.gbParams.Controls.Add(this.label10);
            this.gbParams.Controls.Add(this.tbAssemblyCopyright);
            this.gbParams.Controls.Add(this.label9);
            this.gbParams.Controls.Add(this.tbAssemblyProduct);
            this.gbParams.Controls.Add(this.label8);
            this.gbParams.Controls.Add(this.tbAssemblyCompany);
            this.gbParams.Controls.Add(this.label7);
            this.gbParams.Controls.Add(this.tbAssemblyDescription);
            this.gbParams.Controls.Add(this.label6);
            this.gbParams.Controls.Add(this.tbAssemblyTitle);
            this.gbParams.Controls.Add(this.label5);
            this.gbParams.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gbParams.Enabled = false;
            this.gbParams.Location = new System.Drawing.Point(0, 0);
            this.gbParams.Name = "gbParams";
            this.gbParams.Size = new System.Drawing.Size(1008, 143);
            this.gbParams.TabIndex = 0;
            this.gbParams.TabStop = false;
            this.gbParams.Text = "参数值";
            // 
            // tbBackFilePath
            // 
            this.tbBackFilePath.Enabled = false;
            this.tbBackFilePath.Location = new System.Drawing.Point(182, 87);
            this.tbBackFilePath.Name = "tbBackFilePath";
            this.tbBackFilePath.Size = new System.Drawing.Size(416, 21);
            this.tbBackFilePath.TabIndex = 27;
            // 
            // btnBackFilePath
            // 
            this.btnBackFilePath.Enabled = false;
            this.btnBackFilePath.Location = new System.Drawing.Point(596, 86);
            this.btnBackFilePath.Name = "btnBackFilePath";
            this.btnBackFilePath.Size = new System.Drawing.Size(75, 23);
            this.btnBackFilePath.TabIndex = 28;
            this.btnBackFilePath.Text = "选择";
            this.btnBackFilePath.UseVisualStyleBackColor = true;
            this.btnBackFilePath.Click += new System.EventHandler(this.btnBackFilePath_Click);
            // 
            // cbBackFile
            // 
            this.cbBackFile.AutoSize = true;
            this.cbBackFile.Location = new System.Drawing.Point(44, 89);
            this.cbBackFile.Name = "cbBackFile";
            this.cbBackFile.Size = new System.Drawing.Size(132, 16);
            this.cbBackFile.TabIndex = 26;
            this.cbBackFile.Text = "生成前备份文件至：";
            this.cbBackFile.UseVisualStyleBackColor = true;
            this.cbBackFile.CheckedChanged += new System.EventHandler(this.cbBackFile_CheckedChanged);
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Font = new System.Drawing.Font("宋体", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label13.ForeColor = System.Drawing.Color.Red;
            this.label13.Location = new System.Drawing.Point(179, 115);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(632, 16);
            this.label13.TabIndex = 25;
            this.label13.Text = "*数值为空则不做改变；版本号必须填写！版本号必须是介于0和65534之间的整数；";
            // 
            // tbAssemblyFileVersion4
            // 
            this.tbAssemblyFileVersion4.Location = new System.Drawing.Point(631, 60);
            this.tbAssemblyFileVersion4.Name = "tbAssemblyFileVersion4";
            this.tbAssemblyFileVersion4.Size = new System.Drawing.Size(40, 21);
            this.tbAssemblyFileVersion4.TabIndex = 21;
            // 
            // tbAssemblyFileVersion3
            // 
            this.tbAssemblyFileVersion3.Location = new System.Drawing.Point(585, 60);
            this.tbAssemblyFileVersion3.Name = "tbAssemblyFileVersion3";
            this.tbAssemblyFileVersion3.Size = new System.Drawing.Size(40, 21);
            this.tbAssemblyFileVersion3.TabIndex = 20;
            // 
            // tbAssemblyFileVersion2
            // 
            this.tbAssemblyFileVersion2.Location = new System.Drawing.Point(537, 60);
            this.tbAssemblyFileVersion2.Name = "tbAssemblyFileVersion2";
            this.tbAssemblyFileVersion2.Size = new System.Drawing.Size(40, 21);
            this.tbAssemblyFileVersion2.TabIndex = 19;
            // 
            // tbAssemblyVersion4
            // 
            this.tbAssemblyVersion4.Location = new System.Drawing.Point(322, 60);
            this.tbAssemblyVersion4.Name = "tbAssemblyVersion4";
            this.tbAssemblyVersion4.Size = new System.Drawing.Size(40, 21);
            this.tbAssemblyVersion4.TabIndex = 16;
            // 
            // tbAssemblyVersion3
            // 
            this.tbAssemblyVersion3.Location = new System.Drawing.Point(276, 60);
            this.tbAssemblyVersion3.Name = "tbAssemblyVersion3";
            this.tbAssemblyVersion3.Size = new System.Drawing.Size(40, 21);
            this.tbAssemblyVersion3.TabIndex = 15;
            // 
            // tbAssemblyVersion2
            // 
            this.tbAssemblyVersion2.Location = new System.Drawing.Point(228, 60);
            this.tbAssemblyVersion2.Name = "tbAssemblyVersion2";
            this.tbAssemblyVersion2.Size = new System.Drawing.Size(40, 21);
            this.tbAssemblyVersion2.TabIndex = 14;
            // 
            // btnMake
            // 
            this.btnMake.Location = new System.Drawing.Point(778, 85);
            this.btnMake.Name = "btnMake";
            this.btnMake.Size = new System.Drawing.Size(150, 23);
            this.btnMake.TabIndex = 24;
            this.btnMake.Text = "生成";
            this.btnMake.UseVisualStyleBackColor = true;
            this.btnMake.Click += new System.EventHandler(this.btnMake_Click);
            // 
            // tbAssemblyFileVersion1
            // 
            this.tbAssemblyFileVersion1.Location = new System.Drawing.Point(491, 60);
            this.tbAssemblyFileVersion1.Name = "tbAssemblyFileVersion1";
            this.tbAssemblyFileVersion1.Size = new System.Drawing.Size(40, 21);
            this.tbAssemblyFileVersion1.TabIndex = 18;
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(420, 63);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(65, 12);
            this.label12.TabIndex = 17;
            this.label12.Text = "文件版本：";
            // 
            // tbAssemblyVersion1
            // 
            this.tbAssemblyVersion1.Location = new System.Drawing.Point(182, 60);
            this.tbAssemblyVersion1.Name = "tbAssemblyVersion1";
            this.tbAssemblyVersion1.Size = new System.Drawing.Size(40, 21);
            this.tbAssemblyVersion1.TabIndex = 13;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(99, 63);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(77, 12);
            this.label11.TabIndex = 12;
            this.label11.Text = "程序集版本：";
            // 
            // tbAssemblyTrademark
            // 
            this.tbAssemblyTrademark.Location = new System.Drawing.Point(778, 33);
            this.tbAssemblyTrademark.Name = "tbAssemblyTrademark";
            this.tbAssemblyTrademark.Size = new System.Drawing.Size(150, 21);
            this.tbAssemblyTrademark.TabIndex = 11;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(731, 38);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(41, 12);
            this.label10.TabIndex = 10;
            this.label10.Text = "商标：";
            // 
            // tbAssemblyCopyright
            // 
            this.tbAssemblyCopyright.Location = new System.Drawing.Point(491, 33);
            this.tbAssemblyCopyright.Name = "tbAssemblyCopyright";
            this.tbAssemblyCopyright.Size = new System.Drawing.Size(180, 21);
            this.tbAssemblyCopyright.TabIndex = 9;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(444, 38);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(41, 12);
            this.label9.TabIndex = 8;
            this.label9.Text = "版权：";
            // 
            // tbAssemblyProduct
            // 
            this.tbAssemblyProduct.Location = new System.Drawing.Point(182, 33);
            this.tbAssemblyProduct.Name = "tbAssemblyProduct";
            this.tbAssemblyProduct.Size = new System.Drawing.Size(180, 21);
            this.tbAssemblyProduct.TabIndex = 7;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(135, 38);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(41, 12);
            this.label8.TabIndex = 6;
            this.label8.Text = "产品：";
            // 
            // tbAssemblyCompany
            // 
            this.tbAssemblyCompany.Location = new System.Drawing.Point(778, 6);
            this.tbAssemblyCompany.Name = "tbAssemblyCompany";
            this.tbAssemblyCompany.Size = new System.Drawing.Size(150, 21);
            this.tbAssemblyCompany.TabIndex = 5;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(731, 11);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(41, 12);
            this.label7.TabIndex = 4;
            this.label7.Text = "公司：";
            // 
            // tbAssemblyDescription
            // 
            this.tbAssemblyDescription.Location = new System.Drawing.Point(491, 6);
            this.tbAssemblyDescription.Name = "tbAssemblyDescription";
            this.tbAssemblyDescription.Size = new System.Drawing.Size(180, 21);
            this.tbAssemblyDescription.TabIndex = 3;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(444, 11);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(41, 12);
            this.label6.TabIndex = 2;
            this.label6.Text = "说明：";
            // 
            // tbAssemblyTitle
            // 
            this.tbAssemblyTitle.Location = new System.Drawing.Point(182, 6);
            this.tbAssemblyTitle.Name = "tbAssemblyTitle";
            this.tbAssemblyTitle.Size = new System.Drawing.Size(180, 21);
            this.tbAssemblyTitle.TabIndex = 1;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(135, 11);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(41, 12);
            this.label5.TabIndex = 0;
            this.label5.Text = "标题：";
            // 
            // tcButtom
            // 
            this.tcButtom.Controls.Add(this.tpLogInfo);
            this.tcButtom.Controls.Add(this.tpMultiFile);
            this.tcButtom.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tcButtom.Location = new System.Drawing.Point(0, 0);
            this.tcButtom.Name = "tcButtom";
            this.tcButtom.SelectedIndex = 0;
            this.tcButtom.Size = new System.Drawing.Size(1008, 492);
            this.tcButtom.TabIndex = 0;
            // 
            // tpLogInfo
            // 
            this.tpLogInfo.Controls.Add(this.tbInfo);
            this.tpLogInfo.Location = new System.Drawing.Point(4, 22);
            this.tpLogInfo.Name = "tpLogInfo";
            this.tpLogInfo.Padding = new System.Windows.Forms.Padding(3);
            this.tpLogInfo.Size = new System.Drawing.Size(1000, 466);
            this.tpLogInfo.TabIndex = 0;
            this.tpLogInfo.Text = "日志信息";
            this.tpLogInfo.UseVisualStyleBackColor = true;
            // 
            // tpMultiFile
            // 
            this.tpMultiFile.Controls.Add(this.plFileSelectFull);
            this.tpMultiFile.Controls.Add(this.plFileSelectTop);
            this.tpMultiFile.Location = new System.Drawing.Point(4, 22);
            this.tpMultiFile.Name = "tpMultiFile";
            this.tpMultiFile.Padding = new System.Windows.Forms.Padding(3);
            this.tpMultiFile.Size = new System.Drawing.Size(1000, 466);
            this.tpMultiFile.TabIndex = 1;
            this.tpMultiFile.Text = "文件选择";
            this.tpMultiFile.UseVisualStyleBackColor = true;
            // 
            // plFileSelectFull
            // 
            this.plFileSelectFull.Controls.Add(this.dgvBatch);
            this.plFileSelectFull.Dock = System.Windows.Forms.DockStyle.Fill;
            this.plFileSelectFull.Location = new System.Drawing.Point(3, 26);
            this.plFileSelectFull.Name = "plFileSelectFull";
            this.plFileSelectFull.Size = new System.Drawing.Size(994, 437);
            this.plFileSelectFull.TabIndex = 2;
            // 
            // dgvBatch
            // 
            this.dgvBatch.AllowUserToAddRows = false;
            this.dgvBatch.AllowUserToDeleteRows = false;
            this.dgvBatch.AllowUserToOrderColumns = true;
            this.dgvBatch.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvBatch.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgvBatch.Location = new System.Drawing.Point(0, 0);
            this.dgvBatch.Name = "dgvBatch";
            this.dgvBatch.RowTemplate.Height = 23;
            this.dgvBatch.Size = new System.Drawing.Size(994, 437);
            this.dgvBatch.TabIndex = 0;
            this.dgvBatch.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvBatch_CellContentClick);
            this.dgvBatch.CellValueChanged += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvBatch_CellValueChanged);
            this.dgvBatch.RowPostPaint += new System.Windows.Forms.DataGridViewRowPostPaintEventHandler(this.dgvBatch_RowPostPaint);
            this.dgvBatch.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(this.dgvBatch_MouseDoubleClick);
            // 
            // plFileSelectTop
            // 
            this.plFileSelectTop.Controls.Add(this.btnInvertSelect);
            this.plFileSelectTop.Controls.Add(this.btnSelectAll);
            this.plFileSelectTop.Dock = System.Windows.Forms.DockStyle.Top;
            this.plFileSelectTop.Location = new System.Drawing.Point(3, 3);
            this.plFileSelectTop.Name = "plFileSelectTop";
            this.plFileSelectTop.Size = new System.Drawing.Size(994, 23);
            this.plFileSelectTop.TabIndex = 1;
            // 
            // btnInvertSelect
            // 
            this.btnInvertSelect.Location = new System.Drawing.Point(156, 0);
            this.btnInvertSelect.Name = "btnInvertSelect";
            this.btnInvertSelect.Size = new System.Drawing.Size(150, 23);
            this.btnInvertSelect.TabIndex = 26;
            this.btnInvertSelect.Text = "反选";
            this.btnInvertSelect.UseVisualStyleBackColor = true;
            this.btnInvertSelect.Click += new System.EventHandler(this.btnInvertSelect_Click);
            // 
            // btnSelectAll
            // 
            this.btnSelectAll.Location = new System.Drawing.Point(0, 0);
            this.btnSelectAll.Name = "btnSelectAll";
            this.btnSelectAll.Size = new System.Drawing.Size(150, 23);
            this.btnSelectAll.TabIndex = 25;
            this.btnSelectAll.Text = "全选";
            this.btnSelectAll.UseVisualStyleBackColor = true;
            this.btnSelectAll.Click += new System.EventHandler(this.btnSelectAll_Click);
            // 
            // plTop
            // 
            this.plTop.Controls.Add(this.tbSinglePath);
            this.plTop.Controls.Add(this.tbBatchPath);
            this.plTop.Controls.Add(this.label3);
            this.plTop.Controls.Add(this.label1);
            this.plTop.Controls.Add(this.btnBatchPath);
            this.plTop.Controls.Add(this.cbModel);
            this.plTop.Controls.Add(this.label2);
            this.plTop.Controls.Add(this.btnSingleEdit);
            this.plTop.Dock = System.Windows.Forms.DockStyle.Top;
            this.plTop.Location = new System.Drawing.Point(0, 0);
            this.plTop.Name = "plTop";
            this.plTop.Size = new System.Drawing.Size(1008, 94);
            this.plTop.TabIndex = 0;
            // 
            // pcParams
            // 
            this.pcParams.Controls.Add(this.gbParams);
            this.pcParams.Dock = System.Windows.Forms.DockStyle.Top;
            this.pcParams.Location = new System.Drawing.Point(0, 94);
            this.pcParams.Name = "pcParams";
            this.pcParams.Size = new System.Drawing.Size(1008, 143);
            this.pcParams.TabIndex = 3;
            // 
            // plFull
            // 
            this.plFull.Controls.Add(this.tcButtom);
            this.plFull.Dock = System.Windows.Forms.DockStyle.Fill;
            this.plFull.Location = new System.Drawing.Point(0, 237);
            this.plFull.Name = "plFull";
            this.plFull.Size = new System.Drawing.Size(1008, 492);
            this.plFull.TabIndex = 4;
            // 
            // FrmAssemblyInfoBatchEdit
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1008, 729);
            this.Controls.Add(this.plFull);
            this.Controls.Add(this.pcParams);
            this.Controls.Add(this.plTop);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.Name = "FrmAssemblyInfoBatchEdit";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "程序集信息批量修改";
            this.Load += new System.EventHandler(this.FrmAssemblyInfoBatchEdit_Load);
            this.gbParams.ResumeLayout(false);
            this.gbParams.PerformLayout();
            this.tcButtom.ResumeLayout(false);
            this.tpLogInfo.ResumeLayout(false);
            this.tpLogInfo.PerformLayout();
            this.tpMultiFile.ResumeLayout(false);
            this.plFileSelectFull.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgvBatch)).EndInit();
            this.plFileSelectTop.ResumeLayout(false);
            this.plTop.ResumeLayout(false);
            this.plTop.PerformLayout();
            this.pcParams.ResumeLayout(false);
            this.plFull.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox tbBatchPath;
        private System.Windows.Forms.Button btnBatchPath;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox tbSinglePath;
        private System.Windows.Forms.Button btnSingleEdit;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.ComboBox cbModel;
        private System.Windows.Forms.TextBox tbInfo;
        private System.Windows.Forms.GroupBox gbParams;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox tbAssemblyTitle;
        private System.Windows.Forms.TextBox tbAssemblyDescription;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox tbAssemblyCompany;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox tbAssemblyProduct;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.TextBox tbAssemblyCopyright;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.TextBox tbAssemblyTrademark;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.TextBox tbAssemblyVersion1;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.TextBox tbAssemblyFileVersion1;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Button btnMake;
        private System.Windows.Forms.TextBox tbAssemblyVersion3;
        private System.Windows.Forms.TextBox tbAssemblyVersion2;
        private System.Windows.Forms.TextBox tbAssemblyVersion4;
        private System.Windows.Forms.TextBox tbAssemblyFileVersion4;
        private System.Windows.Forms.TextBox tbAssemblyFileVersion3;
        private System.Windows.Forms.TextBox tbAssemblyFileVersion2;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.TabControl tcButtom;
        private System.Windows.Forms.TabPage tpLogInfo;
        private System.Windows.Forms.TabPage tpMultiFile;
        private System.Windows.Forms.Panel plTop;
        private System.Windows.Forms.Panel pcParams;
        private System.Windows.Forms.Panel plFull;
        private System.Windows.Forms.CheckBox cbBackFile;
        private System.Windows.Forms.TextBox tbBackFilePath;
        private System.Windows.Forms.Button btnBackFilePath;
        private System.Windows.Forms.DataGridView dgvBatch;
        private System.Windows.Forms.Panel plFileSelectTop;
        private System.Windows.Forms.Button btnSelectAll;
        private System.Windows.Forms.Panel plFileSelectFull;
        private System.Windows.Forms.Button btnInvertSelect;
    }
}