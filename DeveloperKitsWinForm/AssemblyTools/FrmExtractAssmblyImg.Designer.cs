﻿namespace AssemblyTools
{
    partial class FrmExtractAssmblyImg
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmExtractAssmblyImg));
            this.tbAssemblyPath = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.btnAssemblyPath = new System.Windows.Forms.Button();
            this.tbSavePath = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.btnSavePath = new System.Windows.Forms.Button();
            this.btnExtract = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // tbAssemblyPath
            // 
            this.tbAssemblyPath.Location = new System.Drawing.Point(125, 100);
            this.tbAssemblyPath.Name = "tbAssemblyPath";
            this.tbAssemblyPath.Size = new System.Drawing.Size(360, 21);
            this.tbAssemblyPath.TabIndex = 6;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(18, 104);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(101, 12);
            this.label1.TabIndex = 5;
            this.label1.Text = "被提取的程序集：";
            // 
            // btnAssemblyPath
            // 
            this.btnAssemblyPath.Location = new System.Drawing.Point(483, 99);
            this.btnAssemblyPath.Name = "btnAssemblyPath";
            this.btnAssemblyPath.Size = new System.Drawing.Size(75, 23);
            this.btnAssemblyPath.TabIndex = 7;
            this.btnAssemblyPath.Text = "选择";
            this.btnAssemblyPath.UseVisualStyleBackColor = true;
            this.btnAssemblyPath.Click += new System.EventHandler(this.btnAssemblyPath_Click);
            // 
            // tbSavePath
            // 
            this.tbSavePath.Location = new System.Drawing.Point(125, 128);
            this.tbSavePath.Name = "tbSavePath";
            this.tbSavePath.Size = new System.Drawing.Size(360, 21);
            this.tbSavePath.TabIndex = 9;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(30, 132);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(89, 12);
            this.label2.TabIndex = 8;
            this.label2.Text = "图片保存目录：";
            // 
            // btnSavePath
            // 
            this.btnSavePath.Location = new System.Drawing.Point(483, 127);
            this.btnSavePath.Name = "btnSavePath";
            this.btnSavePath.Size = new System.Drawing.Size(75, 23);
            this.btnSavePath.TabIndex = 10;
            this.btnSavePath.Text = "选择";
            this.btnSavePath.UseVisualStyleBackColor = true;
            this.btnSavePath.Click += new System.EventHandler(this.btnSavePath_Click);
            // 
            // btnExtract
            // 
            this.btnExtract.Location = new System.Drawing.Point(125, 155);
            this.btnExtract.Name = "btnExtract";
            this.btnExtract.Size = new System.Drawing.Size(433, 23);
            this.btnExtract.TabIndex = 11;
            this.btnExtract.Text = "提取";
            this.btnExtract.UseVisualStyleBackColor = true;
            this.btnExtract.Click += new System.EventHandler(this.btnExtract_Click);
            // 
            // FrmExtractAssmblyImg
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(584, 311);
            this.Controls.Add(this.btnExtract);
            this.Controls.Add(this.tbSavePath);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.btnSavePath);
            this.Controls.Add(this.tbAssemblyPath);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.btnAssemblyPath);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.Name = "FrmExtractAssmblyImg";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "提取程序集图片";
            this.Load += new System.EventHandler(this.FrmExtractAssmblyImg_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox tbAssemblyPath;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button btnAssemblyPath;
        private System.Windows.Forms.TextBox tbSavePath;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button btnSavePath;
        private System.Windows.Forms.Button btnExtract;
    }
}