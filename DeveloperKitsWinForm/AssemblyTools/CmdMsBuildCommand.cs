﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AssemblyTools
{
    public class CmdMsBuildCommand : IMSBuildCommand
    {
        /// <summary>
        /// 输出路径
        /// </summary>
        public string OutPutPath
        {
            get;
            set;
        } = string.Empty;

        /// <summary>
        /// 是否清理
        /// </summary>
        public bool IsClear
        {
            get;
            set;
        } = false;

        /// <summary>
        /// 编译方式
        /// </summary>
        public EnumMSBuildBuidMethod BuildMethod
        {
            get;
            set;
        } = EnumMSBuildBuidMethod.Release;

        /// <summary>
        /// 获取命令
        /// </summary>
        /// <param name="strMSBuildPath">MSBuild目录</param>
        /// <param name="strProjcetRootPath">项目目录</param>
        /// <returns></returns>
        public string GetCommand(string strMSBuildPath,string strProjcetRootPath)
        {
            StringBuilder sbCommand = new StringBuilder();
            List<string> listCommandParam = new List<string>();
            if (IsClear)
            {
                listCommandParam.Add(@"/t:Clean");
            }
            listCommandParam.Add(@"/t:Build");
            listCommandParam.Add(@"/p:Configuration=" + BuildMethod.ToString());
            if (!string.IsNullOrEmpty(OutPutPath))
            {
                listCommandParam.Add("/p:OutDir=\"" + Path.GetDirectoryName(OutPutPath + @"\") + "\"");
            }
            sbCommand.AppendLine(@"cd /d " + strMSBuildPath.ToUpper().Replace("MSBuild.exe", ""));
            sbCommand.AppendLine("");
            sbCommand.AppendLine(@"msbuild " + "\"" + strProjcetRootPath + "\" " + string.Join(" ",listCommandParam.ToArray()));
            return sbCommand.ToString();
        }

        /// <summary>
        /// 获取编译和复制命令
        /// </summary>
        /// <param name="strMSBuildPath">MSBuild目录</param>
        /// <param name="strProjcetRootPath">项目目录</param>
        /// <param name="strExtractPath">复制目标路径</param>
        /// <param name="strAssemblyName">程序集名称</param>
        /// <returns></returns>
        public string GetBuildAndCopyCommand(string strMSBuildPath, string strProjcetRootPath,string strExtractPath,string strAssemblyName)
        {
            StringBuilder sbCommand = new StringBuilder();
            List<string> listCommandParam = new List<string>();
            if (IsClear)
            {
                listCommandParam.Add(@"/t:Clean");
            }
            listCommandParam.Add(@"/t:Build");
            listCommandParam.Add(@"/p:Configuration=" + BuildMethod.ToString());
            if (!string.IsNullOrEmpty(OutPutPath))
            {
                listCommandParam.Add("/p:OutDir=\"" + Path.GetDirectoryName(OutPutPath + @"\") + "\"");
            }
            sbCommand.AppendLine(@"cd /d " + strMSBuildPath.ToUpper().Replace("MSBuild.exe", ""));
            sbCommand.AppendLine("");
            sbCommand.AppendLine(@"msbuild " + "\"" + strProjcetRootPath + "\" " + string.Join(" ", listCommandParam.ToArray()));
            //sbCommand.AppendLine("copy " + "\"" + Path.GetDirectoryName(OutPutPath + @"\") + @"\" + strAssemblyName + "\" \"" + Path.GetDirectoryName(strExtractPath + @"\") + "\"");
            return sbCommand.ToString();
        }
    }
}
