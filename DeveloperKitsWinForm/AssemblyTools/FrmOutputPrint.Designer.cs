﻿namespace AssemblyTools
{
    partial class FrmOutputPrint
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmOutputPrint));
            this.plTop = new System.Windows.Forms.Panel();
            this.btnCopyCommand = new System.Windows.Forms.Button();
            this.btnCopyInfo = new System.Windows.Forms.Button();
            this.plFull = new System.Windows.Forms.Panel();
            this.rtbInfo = new System.Windows.Forms.RichTextBox();
            this.btnOK = new System.Windows.Forms.Button();
            this.plTop.SuspendLayout();
            this.plFull.SuspendLayout();
            this.SuspendLayout();
            // 
            // plTop
            // 
            this.plTop.Controls.Add(this.btnOK);
            this.plTop.Controls.Add(this.btnCopyCommand);
            this.plTop.Controls.Add(this.btnCopyInfo);
            this.plTop.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.plTop.Location = new System.Drawing.Point(0, 531);
            this.plTop.Name = "plTop";
            this.plTop.Size = new System.Drawing.Size(784, 30);
            this.plTop.TabIndex = 1;
            // 
            // btnCopyCommand
            // 
            this.btnCopyCommand.Location = new System.Drawing.Point(226, 3);
            this.btnCopyCommand.Name = "btnCopyCommand";
            this.btnCopyCommand.Size = new System.Drawing.Size(217, 23);
            this.btnCopyCommand.TabIndex = 1;
            this.btnCopyCommand.Text = "复制命令到粘贴板(&C)";
            this.btnCopyCommand.UseVisualStyleBackColor = true;
            this.btnCopyCommand.Click += new System.EventHandler(this.btnCopyCommand_Click);
            // 
            // btnCopyInfo
            // 
            this.btnCopyInfo.Location = new System.Drawing.Point(3, 3);
            this.btnCopyInfo.Name = "btnCopyInfo";
            this.btnCopyInfo.Size = new System.Drawing.Size(217, 23);
            this.btnCopyInfo.TabIndex = 0;
            this.btnCopyInfo.Text = "复制信息到粘贴板(&X)";
            this.btnCopyInfo.UseVisualStyleBackColor = true;
            this.btnCopyInfo.Click += new System.EventHandler(this.btnCopyInfo_Click);
            // 
            // plFull
            // 
            this.plFull.Controls.Add(this.rtbInfo);
            this.plFull.Dock = System.Windows.Forms.DockStyle.Fill;
            this.plFull.Location = new System.Drawing.Point(0, 0);
            this.plFull.Name = "plFull";
            this.plFull.Size = new System.Drawing.Size(784, 531);
            this.plFull.TabIndex = 0;
            // 
            // rtbInfo
            // 
            this.rtbInfo.Dock = System.Windows.Forms.DockStyle.Fill;
            this.rtbInfo.Location = new System.Drawing.Point(0, 0);
            this.rtbInfo.Name = "rtbInfo";
            this.rtbInfo.ReadOnly = true;
            this.rtbInfo.ScrollBars = System.Windows.Forms.RichTextBoxScrollBars.ForcedVertical;
            this.rtbInfo.Size = new System.Drawing.Size(784, 531);
            this.rtbInfo.TabIndex = 0;
            this.rtbInfo.Text = "";
            // 
            // btnOK
            // 
            this.btnOK.Location = new System.Drawing.Point(555, 3);
            this.btnOK.Name = "btnOK";
            this.btnOK.Size = new System.Drawing.Size(217, 23);
            this.btnOK.TabIndex = 2;
            this.btnOK.Text = "确定(&O)";
            this.btnOK.UseVisualStyleBackColor = true;
            this.btnOK.Click += new System.EventHandler(this.btnOK_Click);
            // 
            // FrmOutputPrint
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(784, 561);
            this.Controls.Add(this.plFull);
            this.Controls.Add(this.plTop);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.Name = "FrmOutputPrint";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "输出信息";
            this.Load += new System.EventHandler(this.FrmOutputPrint_Load);
            this.plTop.ResumeLayout(false);
            this.plFull.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel plTop;
        private System.Windows.Forms.Panel plFull;
        private System.Windows.Forms.Button btnCopyInfo;
        private System.Windows.Forms.Button btnCopyCommand;
        private System.Windows.Forms.RichTextBox rtbInfo;
        private System.Windows.Forms.Button btnOK;
    }
}