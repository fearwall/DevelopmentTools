﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Sample.DesignMode.AbstractFactoryMode;
using Sample.DesignMode.AdapterPatternMode;
using Sample.DesignMode.Bridge;
using Sample.DesignMode.BuilderMode;
using Sample.DesignMode.Decorator;
using Sample.DesignMode.FactoryMode;
//using Sample.DesignMode.SimpleFactoryMode;

namespace SampleConsoleAppTest
{
    class Program
    {
        static void Main(string[] args)
        {
            #region 简单工厂模式示例
            //SenderFactory factory = new SenderFactory();
            //ISender sender = factory.ProduceMail();
            //sender.Sender();
            //Console.WriteLine("按下任意键退出对话框...");
            //Console.ReadLine();
            #endregion

            #region 工厂模式示例
            //IProvider provider = new MailSenderFactory();//或 new SmsSenderFactory()
            //ISender sender = provider.Produce();
            ////输出结果 This is MailSender
            //sender.Send();
            //Console.WriteLine("按下任意键退出对话框...");
            //Console.ReadLine();
            #endregion

            #region 抽象工厂模式示例
            /* 输出结果
                创建欧式的房顶
                创建欧式的地板
                创建欧式的窗户
                创建欧式的房门

                创建现代的房顶
                创建现代的地板
                创建现代的窗户
                创建现代的房门
                按下任意键退出对话框...
             */

            //// 哥哥的欧式风格的房子
            //HouseFactory europeanFactory = new EuropeanFactory();
            //europeanFactory.CreateRoof().Create();
            //europeanFactory.CreateFloor().Create();
            //europeanFactory.CreateWindow().Create();
            //europeanFactory.CreateDoor().Create();

            //Console.WriteLine("");

            ////弟弟的现代风格的房子
            //HouseFactory modernizationFactory = new ModernizationFactory();
            //modernizationFactory.CreateRoof().Create();
            //modernizationFactory.CreateFloor().Create();
            //modernizationFactory.CreateWindow().Create();
            //modernizationFactory.CreateDoor().Create();
            //Console.WriteLine("按下任意键退出对话框...");
            //Console.ReadLine();
            #endregion

            #region 建造者模式示例

            ///*
            // * 输出结果：
            //    汽车开始组装...
            //    组件【 AoDiBuilder's Door 】 已完成.
            //    组件【 AoDiBuilder's Wheel 】 已完成.
            //    组件【 AoDiBuilder's Engine 】 已完成.
            //    汽车所有组件已完成！

            //    汽车开始组装...
            //    组件【 Buick's Door 】 已完成.
            //    组件【 Buick's Wheel 】 已完成.
            //    组件【 Buick's Engine 】 已完成.
            //    汽车所有组件已完成！
            //    按下任意键退出对话框...
            // */
            ////指挥者类
            //Director director = new Director();
            ////建造者角色
            //AoDiBuilder aoDiBuilder = new AoDiBuilder();
            //BuickBuilder buickBuilder = new BuickBuilder();

            ////aoDiBuilder 建造产品
            //director.Construct(aoDiBuilder);

            ////buickBuilder 建造产品
            //director.Construct(buickBuilder);

            ////获取建造产品的结果
            //Car aoDiCar = aoDiBuilder.GetCar();
            //Car buickCar = buickBuilder.GetCar();

            ////输出建造结果
            //aoDiCar.Show();
            //Console.WriteLine("");
            //buickCar.Show();

            //Console.WriteLine("按下任意键退出对话框...");
            //Console.ReadLine();
            #endregion

            #region 适配器模式示例 
            ////对象的适配器模式实现
            ///*
            // * 输出结果：
            //    两孔的充电器可以使用了！
            //    三孔插头可以使用了！
            //    按下任意键退出对话框...
            // */
            ////TwoHoleTarget twoHoleTarget = new ThreeToTwoAdapter();
            ////twoHoleTarget.Request();
            ////Console.WriteLine("按下任意键退出对话框...");
            ////Console.ReadLine();

            ////类的适配器模式实现
            ///*
            // * 输出结果
            //    三孔插头可以使用了！
            //    这是类适配器模式输出的！
            //    按下任意键退出对话框...
            // */
            //ITwoHoleTarget change = new ThreeToTwoAdapter2();
            //change.Request();
            //Console.WriteLine("按下任意键退出对话框...");
            //Console.ReadLine();
            #endregion

            #region 桥接模式示例
            /*
             * 输出结果
                SqlServer2000针对Unix的具体实现
                SqlServer2005针对Unix的具体实现
                按下任意键退出对话框...
             */

            //DataBase db = null;

            ////数据库连接 SqlServer2000的创建实现
            //PlatformImplementor SqlServer2000UnixImp = new SqlServer2000UnixImplementor();
            //db = new SqlServer2000(SqlServer2000UnixImp);
            //db.Create();

            ////数据库连接 SqlSever2005的创建实现
            //PlatformImplementor SqlServer2005UnixImp = new SqlServer2005UnixImplementor();
            //db = new SqlServer2005(SqlServer2005UnixImp);
            //db.Create();

            //Console.WriteLine("按下任意键退出对话框...");
            //Console.ReadLine();
            #endregion

            #region 装饰模式示例
            /*
             * 输出结果
                装修PatrickLiu的房子
                增加安全系统

                装修PatrickLiu的房子
                增加安全系统
                增加保温的功能
                按下任意键退出对话框...
             */

            //这就是我们的饺子馅，需要装饰的房子
            House myselfHouse = new PatrickLiuHouse();

            DecorationStrategy securityHouse = new HouseSecurityDecorator(myselfHouse);
            //房子就有了安全系统了
            securityHouse.Renovation();
            Console.WriteLine("");

            //如果我既要安全系统又要保暖呢，继续装饰就行
            DecorationStrategy securityAndWarmHouse = new KeepWarmDecorator(securityHouse);
            securityAndWarmHouse.Renovation();

            Console.WriteLine("按下任意键退出对话框...");
            Console.ReadLine();
            #endregion
        }
    }
}
