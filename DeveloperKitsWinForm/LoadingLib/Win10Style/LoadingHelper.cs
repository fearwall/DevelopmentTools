﻿using System;
using System.Drawing;
using System.Dynamic;
using System.Threading;
using System.Windows.Forms;

namespace LoadingLib.Win10Style
{
    /// <summary>
    /// 调用方法
    /// LoadingHelper.ShowLoading("有朋自远方来，不亦乐乎。", this, o =>
    /// {
    ///    //这里写处理耗时的代码，代码处理完成则自动关闭该窗口
    ///    Thread.Sleep(10000);
    /// });
    /// </summary>
    public class LoadingHelper
    {
        /// <summary>
        /// 开始加载
        /// </summary>
        /// <param name="message">消息</param>
        /// <param name="ownerForm">父窗体</param>
        /// <param name="work">待执行工作</param>
        /// <param name="workArg">工作参数</param>
        public static void ShowBlueLoading(string message, Form ownerForm, ParameterizedThreadStart work, object workArg = null)
        {
            var loadingForm = new FrmWin10StyleLoading(message);
            dynamic expandoObject = new ExpandoObject();
            expandoObject.Form = loadingForm;
            expandoObject.WorkArg = workArg;
            loadingForm.Color = Color.FromArgb(14,144,210);
            //loadingForm.Color = Color.Blue;
            loadingForm.Opacity = 0.85f;
            loadingForm.SetWorkAction(work, expandoObject);
            loadingForm.ShowDialog(ownerForm);
            if (loadingForm.WorkException != null)
            {
                throw loadingForm.WorkException;
            }
        }

        /// <summary>
        /// 开始加载
        /// </summary>
        /// <param name="message">消息</param>
        /// <param name="ownerForm">父窗体</param>
        /// <param name="work">待执行工作</param>
        /// <param name="workArg">工作参数</param>
        public static void ShowLoading(string message, Form ownerForm, ParameterizedThreadStart work, object workArg = null)
        {
            var loadingForm = new FrmWin10StyleLoading(message);
            dynamic expandoObject = new ExpandoObject();
            expandoObject.Form = loadingForm;
            expandoObject.WorkArg = workArg;
            loadingForm.SetWorkAction(work, expandoObject);
            loadingForm.ShowDialog(ownerForm);
            if (loadingForm.WorkException != null)
            {
                throw loadingForm.WorkException;
            }
        }

        /// <summary>
        /// 开始加载
        /// </summary>
        /// <param name="message">消息</param>
        /// <param name="ownerForm">父窗体</param>
        /// <param name="color">颜色</param>
        /// <param name="work">待执行工作</param>
        /// <param name="workArg">工作参数</param>
        public static void ShowLoading(string message, Form ownerForm,Color color,ParameterizedThreadStart work, object workArg = null)
        {
            var loadingForm = new FrmWin10StyleLoading(message);
            dynamic expandoObject = new ExpandoObject();
            expandoObject.Form = loadingForm;
            expandoObject.WorkArg = workArg;
            loadingForm.Color = color; // System.Drawing.Color.Blue;
            loadingForm.SetWorkAction(work, expandoObject);
            loadingForm.ShowDialog(ownerForm);
            if (loadingForm.WorkException != null)
            {
                throw loadingForm.WorkException;
            }
        }
    }
}
